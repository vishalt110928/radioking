<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
use App\Page;
use App\MenuPage;
use App\MenuPath;
use DB;

class MenuController extends Controller
{
	public function __construct()
	{
		view()->share('active', 'menus');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$li_active = "menus";
		$orderBy = $request->input('orderBy', 'updated_at');
		$sort = $request->input('sort', 'desc');

		$paginate = 10;
		$menus = DB::table('menu_path')->select('menu_path.menu_id', 'menus.sort_order', 'menus.publish', 'pages.title as page_title', DB::raw("(GROUP_CONCAT(m.title ORDER BY menu_path.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;')) as `title`"))->join("menus", "menus.id", "=", "menu_path.menu_id")->leftjoin("pages", "pages.id", "=", "menus.page_id")->leftjoin("menus as m", "m.id", "=", "menu_path.path_id")->groupBy('menu_path.menu_id')->orderBy('title')->paginate($paginate);
		return view('admin.menus.list', compact('menus', 'orderBy', 'sort', 'li_active'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$li_active = "menus";
		$menus = Menu::where('parent_id', 0)->orWhere('parent_id', null)->pluck('title', 'id')->toArray();
		$pages = Page::where('add_menu', '1')->pluck('title', 'id')->toArray();
		return view('admin.menus.create', compact('menus', 'li_active', 'pages'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$mn_pg = array();
		$parent_id = $request->input('parent_id');
		$validatedData = $request->validate([
			'title' => 'required|string|min:2|max:30|unique:menus',
		]);
		if ($validatedData) {
			$data = $request->all();
			$menu = new Menu;
			if ($menu->create($data)) {
				$menu_id = DB::getPDO()->lastInsertId();
				$level = 1;
				if ($parent_id) {
					$menuPaths = MenuPath::where('menu_id', $parent_id)->get();
					foreach ($menuPaths as $menuPath) {
						$menu_insert['menu_id'] = $menu_id;
						$menu_insert['path_id'] = $menuPath->menu_id;
						$menu_insert['level'] = $level;
						MenuPath::create($menu_insert);
						$level++;
					}
				}
				$menu_insert['menu_id'] = $menu_id;
				$menu_insert['path_id'] = $menu_id;
				$menu_insert['level'] = $level;
				MenuPath::create($menu_insert);
				$request->session()->flash('success', 'Menu  created successfully.');
				return redirect('/admin/menus');
			} else {
				$request->session()->flash('error', 'Something went wrong.');
				return back()->withInput();
			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$li_active = "menus";
		$menu = Menu::find($id);
		$menus = Menu::where('parent_id', 0)->orWhere('parent_id', null)->pluck('title', 'id')->toArray();
		$pages = Page::where('add_menu', '1')->pluck('title', 'id')->toArray();
		return view('admin.menus.edit', compact('menus', 'id', 'menu', 'li_active', 'pages'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$mn_pg = array();
		$validatedData = $request->validate([
			'title' => 'required|string|min:2|max:30|unique:menus,id,' . $id
		]);
		$parent_id = $request->parent_id;
		if ($validatedData) {
			$data = $request->all();
			$data['publish'] = $request->input('publish');
			$data['new_tab'] = $request->input('new_tab');
			$menu = Menu::find($id);
			if ($menu->update($data)) {
				$query = MenuPath::where('path_id', $id)->orderBy('level')->get();
				if (count($query)) {
					foreach ($query as $menu_path) {
						// Delete the path below the current one
						MenuPath::where('menu_id', $menu_path->menu_id)->where('level', '<', $menu_path->level)->delete();
						$path = array();
						$new_parents = MenuPath::where('menu_id', $parent_id)->orderBy('level')->get();
						foreach ($new_parents as $new_parent) {
							$path[] = $new_parent['path_id'];
						}

						// Get whats left of the nodes current path
						$left_nodes = MenuPath::where('menu_id', $menu_path->menu_id)->orderBy('level')->get();
						foreach ($left_nodes as $left_node) {
							$path[] = $left_node['path_id'];
						}
						$level = 1;

						foreach ($path as $path_id) {

							$path_exisit = 	MenuPath::where('menu_id', $menu_path->menu_id)->where('path_id', $path_id)->first();
							if ($path_exisit) {
								$path_exisit->delete();
								MenuPath::create(['menu_id' => $menu_path->menu_id, 'path_id' => $path_id, 'level' => $level]);
							} else {
								MenuPath::create(['menu_id' => $menu_path->menu_id, 'path_id' => $path_id, 'level' => $level]);
							}
							$level++;
						}
					}
				} else {
					// Delete the path below the current one
					MenuPath::where('menu_id', $id)->delete();

					// Fix for records with no paths
					$level = 1;
					$query = MenuPath::where('menu_id', $request->parent_id)->orderBy('level')->get();
					foreach ($query as $result) {
						MenuPath::create(['menu_id' => $id, 'path_id' => $result->path_id, 'level' => $level]);
						$level++;
					}
					$path_exisit = 	MenuPath::where('menu_id',$id)->where('path_id,$id')->first();
					if ($path_exisit) {
						$path_exisit->delete();
						MenuPath::create(['menu_id' => $id, 'path_id' => $id, 'level' => $level]);
					} else {
						MenuPath::create(['menu_id' => $id, 'path_id' => $id, 'level' => $level]);
					}
				}
				$request->session()->flash('success', 'Menus updated successfully.');
				return redirect('admin/menus');
			} else {
				$request->session()->flash('error', 'Something went wrong while updating station.');
				return back()->withInput();
			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id)
	{
		$menu = Menu::find($id);
		if ($menu) {
			$menu->delete();
			$request->session()->flash('success', 'Menu deleted successfully.');
			return redirect('admin/new');
		} else {
			$request->session()->flash('error', 'Something went wrong while updating station.');
			return back()->withInput();
		}
	}
	public function menus_delete(Request $request)
	{
		$ids = $request->input('selected');
		if ($ids) {
			foreach ($ids as $id) {
				$this->menu_delete($id);
			}
			$request->session()->flash('success', 'Menu deleted successfully.');
		}
		return redirect('admin/menus');
	}

	public function menu_delete($id)
	{
		MenuPath::where('menu_id', $id)->delete();
		Menu::where('id', $id)->delete();
		$subMenus = MenuPath::where('path_id', $id)->get();
		foreach ($subMenus as $subMenu) {
			$this->menu_delete($subMenu->menu_id);
		}
	}
}
