<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTrackLike extends Model
{
    protected $table = "users_tracks_likes";
    protected $fillable = ['user_id','track_id'];

}
