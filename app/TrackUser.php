<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackUser extends Model
{
	protected $table = 'tracks_users';
	protected $fillable = ['user_id', 'track_id'];
}
