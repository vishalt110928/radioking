<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Spatie\Feed\Feedable;
use Spatie\Feed\FeedItem;

class Rssfeed extends Model implements Feedable
{
	protected $table = "rssfeeds";
	protected $fillable = ['title','image','rss_url','rssfeed_select','rssfeeds_cat_id','publish_time','publish','end_time','des'];
	public function getPublishTimeAttribute($value)
	{
		//return $value;
		$publish_time = "";
		if($value) {
			$publish_time = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
		}        
		return $publish_time;
	}
	public function getEndTimeAttribute($value)
	{
		$end_time = "";
		if($value) {
			$end_time = Carbon::parse($value)->isoFormat('D-MM-YYYY  H:mm:ss');
		}	
		return $end_time;
	}

	public function setPublishTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['publish_time'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['publish_time'] = $value;
		}
	}
	public function setEndTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['end_time'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['end_time'] = $value;
		}
	}
	public function cat() {
		return $this->belongsTo('App\RssfeedsCat','rssfeeds_cat_id');
	}

	public function toFeedItem()
	{
		return FeedItem::create([
			'id' => $this->id,
			'title' => $this->title,
			'summary' => str_limit($this->des,35),
			'updated' => $this->updated_at,
			'link' => $this->rss_url,
			'author' => "",
		]);
	}

	public static function getFeedItems()
	{
	   return Rssfeed::all();
	}
}
