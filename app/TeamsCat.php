<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamsCat extends Model
{
	protected $table = 'teams_cats';
	protected $fillable = ['name','publish','parent_cat','des','sort_order'];		
}
