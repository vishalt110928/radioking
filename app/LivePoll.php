<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LivePoll extends Model
{
	protected $table = "livepolls";
	protected $fillable = ['pos_neg','user_id','table'];

}
