<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GallerysCat extends Model
{
 	protected $table = 'gallerys_cats';
 	protected $fillable = ['name','publish','parent_cat','des','sort_order'];
}
