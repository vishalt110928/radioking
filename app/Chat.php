<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
	protected $table = 'chat';
	protected $fillable = ['active_chat','censored_word'];
}
