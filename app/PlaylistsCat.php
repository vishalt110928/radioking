<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaylistsCat extends Model
{
	protected $table = 'playlists_cats';
	protected $fillable = ['name','publish','parent_cat','des','sort_order'];
}
