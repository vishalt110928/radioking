<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
	protected $table = 'playlists';
	protected $fillable = ['title','image','artist','buy_link','publish','audio_sample','playlists_cat_id'];

	public function cat() {
		return $this->belongsTo('App\PlaylistsCat','playlists_cat_id');
	}

}
