<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PagesCat extends Model
{
	protected $table = 'pages_cats';
	protected $fillable = ['name','publish','parent_cat','des','sort_order'];
}
