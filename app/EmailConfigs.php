<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailConfigs extends Model
{
	protected $table = 'email_configs';
	protected $fillable = ['sender','host','port','user','password','con_security'];
}
