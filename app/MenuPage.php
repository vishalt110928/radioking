<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuPage extends Model
{
	public $incrementing = true;
	protected $table = 'menus_pages';
	protected $fillable = ['menu_id','page_id','sort_order'];
}
