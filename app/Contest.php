<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class Contest extends Model
{
	protected $table = 'contests';
	protected $fillable = ['title','image','contest_start_date','contest_end_date','publish_time','publish','end_time','question','contests_cat_id','des'];

	public function getPublishTimeAttribute($value)
	{
		//return $value;
		$publish_time = "";
		if($value) {
			$publish_time = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
		}        
		return $publish_time;
	}
	public function getSlugAttribute($value) {
		return  ($this->id.'-'.str_replace(' ','-',str_replace('/','-',$this->title)));
	} 
	public function getContestStartDateAttribute($value)
	{
		//return $value;
		$from = "";
		if($value) {
			$from = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
		}        
		return $from;
	}
	public function getContestEndDateAttribute($value)
	{
		//return $value;
		$to = "";
		if($value) {
			$to = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
		}        
		return $to;
	}
	public function getEndTimeAttribute($value)
	{
		$end_time = "";
		if($value) {
			$end_time = Carbon::parse($value)->isoFormat('D-MM-YYYY  H:mm:ss');
		}	
		return $end_time;
	}

	public function setPublishTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['publish_time'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['publish_time'] = $value;
		}
	}
	public function setContestStartDateAttribute($value)
	{		
		if($value) {
			$this->attributes['contest_start_date'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['contest_start_date'] = $value;
		}
	}
	public function setContestEndDateAttribute($value)
	{		
		if($value) {
			$this->attributes['contest_end_date'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['contest_end_date'] = $value;
		}
	}
	public function setEndTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['end_time'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['end_time'] = $value;
		}
	}
	public function cat() {
		return $this->belongsTo('App\ContestsCat','contests_cat_id');
	}
}
