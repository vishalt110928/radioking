<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuPath extends Model
{
	protected $table = "menu_path";
	protected $fillable =['menu_id','path_id','level'];
}
