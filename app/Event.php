<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Event extends Model
{
	protected $table = 'events';
	protected $fillable = ['title','image','from','to','publish_time','publish','end_time','des','events_cat_id','address','city','country','postal_code'];

	public function getPublishTimeAttribute($value)
	{
		//return $value;
		$publish_time = "";
		if($value) {
			$publish_time = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
		}        
		return $publish_time;
	}
	public function getFromAttribute($value)
	{
		//return $value;
		$from = "";
		if($value) {
			$from = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
		}        
		return $from;
	}
	public function getToAttribute($value)
	{
		//return $value;
		$to = "";
		if($value) {
			$to = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
		}        
		return $to;
	}
	public function getEndTimeAttribute($value)
	{
		$end_time = "";
		if($value) {
			$end_time = Carbon::parse($value)->isoFormat('D-MM-YYYY  H:mm:ss');
		}	
		return $end_time;
	}

	public function getSlugAttribute($value) {
		return  ($this->id.'-'.str_replace(' ','-',str_replace('/','-',$this->title)));
	} 
	public function setPublishTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['publish_time'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['publish_time'] = $value;
		}
	}
	public function setFromAttribute($value)
	{		
		if($value) {
			$this->attributes['from'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['from'] = $value;
		}
	}
	public function setToAttribute($value)
	{		
		if($value) {
			$this->attributes['to'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['to'] = $value;
		}
	}
	public function setEndTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['end_time'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['end_time'] = $value;
		}
	}


	public function cat() {
		return $this->belongsTo('App\EventsCat','events_cat_id');
	}

}
