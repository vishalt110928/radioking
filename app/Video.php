<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Video extends Model
{
	protected $table = 'videos';
	protected $fillable = ['title','image','publish_time','publish','end_time','description','videos_cat_id','video_url'];
	public function getPublishTimeAttribute($value)
	{
		$publish_time = "";
		if($value) {
			$publish_time = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
		}        
		return $publish_time;
	}
	public function getEndTimeAttribute($value)
	{
		$end_time = "";
		if($value) {
			$end_time = Carbon::parse($value)->isoFormat('D-MM-YYYY  H:mm:ss');
		}	
		return $end_time;
	}
	public function getSlugAttribute($value) {
		return  ($this->id.'-'.str_replace(' ','-',str_replace('/','-',$this->title)));
	} 

	public function setPublishTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['publish_time'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['publish_time'] = $value;
		}
	}
	public function setEndTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['end_time'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['end_time'] = $value;
		}
	}


	public function cat() {
		return $this->belongsTo('App\VideosCat','videos_cat_id');
	}
}
