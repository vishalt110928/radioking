<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class News extends Model
{
	protected $table = 'news';
	protected $fillable = ['title','image','views','publish_time','publish','end_time','description','news_cat_id'];

	public function getPublishTimeAttribute($value)
	{
		//return $value;
		$publish_time = "";
		if($value) {
			$publish_time = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
		}        
		return $publish_time;
	}
	public function getEndTimeAttribute($value)
	{
		$end_time = "";
		if($value) {
			$end_time = Carbon::parse($value)->isoFormat('D-MM-YYYY  H:mm:ss');
		}	
		return $end_time;
	}
	public function getSlugAttribute($value) {
		return  ($this->id.'-'.str_replace(' ','-',str_replace('/','-',$this->title)));
	} 

	public function setPublishTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['publish_time'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['publish_time'] = $value;
		}
	}
	public function setEndTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['end_time'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['end_time'] = $value;
		}
	}


	public function cat() {
		return $this->belongsTo('App\NewsCat','news_cat_id');
	}
}
