<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendResetPasswordLink extends Mailable
{
  use Queueable, SerializesModels;
  protected $token;
  protected $email;


  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($token,$email="")
  {
    $this->token = $token;
    $this->email = $email;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->view('admin.auth.linkreset')->with([
      'token' => $this->token,
      'email' => $this->email,
    ]);;
  }
}
