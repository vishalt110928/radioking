<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPlaylistLike extends Model
{
	protected $table = "users_playlist_likes";
	protected $fillable = ['user_id','playlist_id'];
}

