<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContestPart extends Model
{
	protected $table = 'contest_parts';
	protected $fillable = ['contest_id','user_id'];
}
