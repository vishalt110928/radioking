<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsCat extends Model
{
	protected $table = 'news_cats';
	protected $fillable = ['name','publish','parent_cat','des','sort_order'];
	public function news() {
		return $this->hasMany('App\News');
	}

}
