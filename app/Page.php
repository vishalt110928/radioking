<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Page extends Model
{
	protected $table = 'pages';
	protected $fillable = ['title','pages_cat_id','publish_time','add_menu','content','publish'];
	public function getPublishTimeAttribute($value)
	{
		$publish_time = "";
		if($value) {
			$publish_time = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
		}        
		return $publish_time;
	}

	public function setPublishTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['publish_time'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['publish_time'] = $value;
		}
	}

	public function getSlugAttribute($value) {
			return ($this->id.'-'.str_replace(' ','-',str_replace('/','-',$this->title)));
		}

	public function cat() {
		return $this->belongsTo('App\PagesCat','pages_cat_id');
	}

	public function blocks() {
		return $this->belongsToMany('App\Block','blocks_pages','page_id','block_id')->withPivot('sort_order');
	}

	public function menus() {
		return $this->hasMany('App\Menu','page_id');
	}
}
