<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramsCat extends Model
{
	protected $table = 'news_cats';
	protected $fillable = ['name','publish','parent_cat','des','sort_order'];
}
