<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContestsCat extends Model
{
	protected $table = 'contests_cats';
	protected $fillable = ['name','publish','parent_cat','des','sort_order'];
}
