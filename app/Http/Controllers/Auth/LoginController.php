<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */

  use AuthenticatesUsers;

  /**
   * Where to redirect users after login.
   *
   * @var string
   */
  protected $redirectTo = '/admin/dashboard';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('guest')->except('logout');
  }

  public function showLoginForm(Request $request)
  {
    if($request->is('admin/*')) {
      return view('admin.auth.login');
    }
    return view('auth.login');
  }

  protected function authenticated(Request $request, $user)
  {
    if($request->is('admin/*')) {
      if($user->users_role_id != "1") {
        Auth::logout();
        return redirect('admin/login');
      }
      return redirect('admin/dashboard');
    }
    else{
      return redirect('/');
    }
  }

  protected function loggedOut(Request $request)
  {
    if($request->is('admin/*')) {
      return redirect('admin/login');
    }
  }


}
