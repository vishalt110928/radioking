<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class LanguageController extends Controller
{
    
    public function select_lang(Request $request){
    	$validatedData = "";
    	$validatedData = $request->validate([
    	        'check' => 'required'
    	   ]);
    	if($validatedData){
    		/*config(['app.locale' => $request->check]);
    		echo  config('app.locale');
    		return view('locale');
    		exit;*/
    		App::setLocale($request->check);
    		$locale = App::getLocale();

    		$request->session()->put('locale', $locale);

    		/*echo session('locale');
    		exit;*/
    		return redirect()->back();
    		
    	}
    }
}
