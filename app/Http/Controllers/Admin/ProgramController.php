<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProgramsCat;
use App\Program;
use Carbon\Carbon;

class ProgramController extends Controller
{
	public function __construct() {
	  view()->share('active','programs');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$li_active = "programs_manage";
		$appends = array();
		$query_string = '';
		$programs_cats = ProgramsCat::pluck('name','id');

		$filter_programs = $request->input('filter_programs');
		$appends['filter_programs'] = $filter_programs;
		$query_string = 'filter_programs='.$filter_programs;


		$filter_title = $request->input('filter_title');
		$appends['filter_title'] = $filter_title;
		$query_string = 'filter_title='.$filter_title;

		$filter_cat = $request->input('filter_cat');
		$appends['filter_cat'] = $filter_cat;
		$query_string = '&filter_cat='.$filter_cat;

		$filter_end_date = $request->input('filter_end_date');
		$appends['filter_end_date'] = $filter_end_date;
		$query_string = '&filter_end_date='.$filter_end_date;

		$filter_publish_date = $request->input('filter_publish_date');
		$appends['filter_publish_date'] = $filter_publish_date;
		$query_string = '&filter_publish_date='.$filter_publish_date;

		$orderBy = $request->input('orderBy','updated_at');
		$sort = $request->input('sort','desc');

		$paginate = 10;
		$programs = Program::when($filter_title, function ($query, $filter_title) {
			return $query->where('name', 'like','%'.$filter_title.'%');
		})
		->when($filter_cat, function ($query, $filter_cat) {
			return $query->where('programs_cat_id',$filter_cat);
		})
		->when($filter_end_date, function ($query, $filter_end_date) {
			return $query->where('end_date', '>',(Carbon::parse($filter_end_date)));
		}) ->when($filter_publish_date, function ($query, $filter_publish_date) {
			return $query->where('publish_date', '<',(Carbon::parse($filter_publish_date)));
		})->when($filter_programs, function ($query, $filter_programs) {
      return $query->where('name', 'like','%'.$filter_programs.'%')->orWhere('description', 'like','%'.$filter_programs.'%');
    })->when($orderBy, function ($query, $orderBy)  use($sort) {
      if($orderBy == "category") {
        return $query->join('programs_cats', 'programs_cats.id', '=', 'programs.programs_cat_id')
        ->orderBy('programs_cats.name',$sort);
      }
      return $query->orderBy($orderBy,$sort);
    })->paginate($paginate);
    $query =  http_build_query($request->except(['orderBy','sort']));

		return view('admin.programs.list',compact('programs','filter_cat','filter_title','filter_end_date','filter_publish_date','appends','query_string','programs_cats','li_active','query','filter_programs','orderBy','sort'));

	}

	/**
	 * Show the form for creating a program resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$li_active = "add_program";
		$programs_cats = ProgramsCat::pluck('name','id');
		return view('admin.programs.create',compact('programs_cats','li_active'));
	}

	/**
	 * Store a programly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$imagename = '';
		$validatedData = $request->validate([
			'name' => 'required|min:2|max:30',
			'image'=>'required|image'
		]);
		if($validatedData){
			if($request->hasFile('image')){
				$image = $request->file('image');
				$destinationPath = public_path('/uploads');
				$imagename = time().'.'.$image->getClientOriginalExtension();
				if($image->move($destinationPath, $imagename)){

				}
				else{
					$request->session()->flash('error', 'Something went wrong while uploading file.');
					return back()->withInput();
				}
			}
			$data = $request->all();
			$data['image']= $imagename;
			$data['brod_days'] =$request->input('brod_days');
			$program = new Program;
			if($program->create($data)){
				$request->session()->flash('success', 'Programs  created successfully.');
				return redirect('/admin/programs');
			}
			else{
				$request->session()->flash('error', 'Something went wrong.');
				return back()->withInput();
			}

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
	    //
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$li_active = "programs_manage";
		$program = Program::find($id);
		$programs_cats = ProgramsCat::pluck('name','id');
		return view('admin.programs.edit',compact('program','id','programs_cats','li_active'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$imagename = '';
		$validatedData = $request->validate([
			'name' => 'required|min:2|max:30',
			'image'=>'image',
		]);
		if($validatedData){
			if($request->hasFile('image')){
				$image = $request->file('image');
				$destinationPath = public_path('/uploads');
				$imagename = time().'.'.$image->getClientOriginalExtension();
				if($image->move($destinationPath, $imagename)){

				}
				else{
					$request->session()->flash('error', 'Something went wrong while uploading file.');
					return back()->withInput();
				}
			}

			$data = $request->all();
			$data['publish'] = $request->input('publish');
			if($request->hasFile('image')){
				$data['image']= $imagename;
			}
			$data['brod_days'] =$request->input('brod_days');
			$programs = Program::find($id);
			if($programs->update($data)){
				$request->session()->flash('success', 'Programs updated successfully.');
				return redirect('admin/programs');
			}
			else{
				$request->session()->flash('error', 'Something went wrong while updating station.');
				return back()->withInput();
			}

		} 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request , $id)
	{
		$programs = Programs::find($id);
		if($programs) {
			$programs->delete();
			$request->session()->flash('success', 'Programs deleted successfully.');
			return redirect('admin/program');
		}
		else{
			$request->session()->flash('error', 'Something went wrong while updating station.');
			return back()->withInput();
		}
	}
	public function programs_delete(Request $request) {
		$ids = $request->input('selected');
		if($ids){
			foreach($ids as $id) {
				$programs = Program::find($id);
				if($programs) {
					$programs->delete();
				}
				else{
				}

			}
			$request->session()->flash('success', 'Programs deleted successfully.');
		}
		return redirect('admin/programs');
	}
	

	public function programs_cat_index(Request $request) {
		$li_active = "programs_cat";
		$pagination = 10;
		$appends = array();
		$query_string = '';
		$filter_programs_cat = $request->input('filter_programs_cat');
		$appends['filter_programs_cat'] = $filter_programs_cat;
		$query_string = 'filter_programs_cat='.$filter_programs_cat;

		$programs_cats = ProgramsCat::when($filter_programs_cat, function ($query, $filter_programs_cat) {
			return $query->where('name', 'like','%'.$filter_programs_cat.'%')->orWhere('des', 'like','%'.$filter_programs_cat.'%');
		})->paginate($pagination);
		return view('admin.programs.programs_cat_list',compact('programs_cats','filter_programs_cat','appends','query_string','li_active'));
	}
	
	public function programs_cat_create() {
		$li_active = "programs_cat";
		return view('admin.programs.programs_cat_create',compact('li_active'));
	}

	public function programs_cat_store(Request $request) {
		$imagename = '';
		$validatedData = $request->validate([
			'name' => 'required|min:2|max:30',
			'sort_order'=>'numeric'
		]);
		if($validatedData){
			$data = $request->all();
			$programsCat =new ProgramsCat;
			if($programsCat->create($data)){
				$request->session()->flash('success', 'Programs Category  created successfully.');
				return redirect('/admin/programs_cat');
			}
			else{
				$request->session()->flash('error', 'Something went wrong.');
				return back()->withInput();
			}

		}
	}

	public function programs_cat_edit($id) {
		$li_active = "programs_cat";
		$programs_cat = ProgramsCat::find($id);
		if($programs_cat) {
			return view('admin.programs.programs_cat_edit',compact('id','programs_cat','li_active'));
		}
	}
	public function programs_cat_update(Request $request, $id) {
		$imagename = '';
		$validatedData = $request->validate([
			'name' => 'required|min:2|max:30'
		]);
		if($validatedData){
			$data = $request->all();
			$data['publish'] = $request->input('publish');
			$programs_cat = ProgramsCat::find($id);
			if($programs_cat->update($data)){
				$request->session()->flash('success', 'Programs Category updated successfully.');
				return redirect('admin/programs_cat');
			}
			else{
				$request->session()->flash('error', 'Something went wrong while updating programs category.');
				return back()->withInput();
			}

		}
	}
	public function programs_cat_destroy(Request $request , $id) {
		$programs_cat = ProgramsCat::find($id);
		if($programs_cat) {
			$programs_cat->delete();
			$request->session()->flash('success', 'Programs Category deleted successfully.');
			return redirect('admin/programs_cat');
		}
		else{
			$request->session()->flash('error', 'Something went wrong while deleting programs category.');
			return back()->withInput();
		}
	}
	public function programs_cat_delete(Request $request) {
		$programs_ids = $request->input('selected');
		if($programs_ids){
			foreach($programs_ids as $program_id) {
				$programs_cat = ProgramsCat::find($program_id);
				if($programs_cat) {
					$programs_cat->delete();

				}
				else{
				}

			}
			$request->session()->flash('success', 'Programs Category deleted successfully.');
		}
		return redirect('admin/programs_cat');

	}

}
