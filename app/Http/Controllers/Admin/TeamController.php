<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Team;
use App\TeamsCat;
use Carbon\Carbon;

class TeamController extends Controller
{
  public function __construct() {
    view()->share('active','teams');
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $li_active = "teams_manage";
    $appends = array();
    $query_string = '';
    $teams_cats = TeamsCat::pluck('name','id');

    $filter_name_dis = $request->input('filter_name_dis');
    $appends['filter_name_dis'] = $filter_name_dis;
    $query_string = 'filter_name_dis='.$filter_name_dis;

    $filter_name = $request->input('filter_name');
    $appends['filter_name'] = $filter_name;
    $query_string = 'filter_name='.$filter_name;

    $filter_cat = $request->input('filter_cat');
    $appends['filter_cat'] = $filter_cat;
    $query_string = '&filter_cat='.$filter_cat;

    $filter_publish_time = $request->input('filter_publish_time');
    $appends['filter_publish_time'] = $filter_publish_time;
    $query_string = '&filter_publish_time='.$filter_publish_time;

    $orderBy = $request->input('orderBy','updated_at');
    $sort = $request->input('sort','desc');

    $paginate = 10;
    $teams = Team::when($filter_name, function ($query, $filter_name) {
      return $query->where('name', 'like','%'.$filter_name.'%');
    })
    ->when($filter_cat, function ($query, $filter_cat) {
      return $query->where('teams_cat_id',$filter_cat);
    }) ->when($filter_publish_time, function ($query, $filter_publish_time) {
      return $query->where('publish_time', '<',(Carbon::parse($filter_publish_time)));
    })->when($filter_name_dis, function ($query, $filter_name_dis) {
      return $query->where('name', 'like','%'.$filter_name_dis.'%')->orWhere('description', 'like','%'.$filter_name_dis.'%');
    })->when($orderBy, function ($query, $orderBy)  use($sort) {
      if($orderBy == "category") {
        return $query->join('teams_cats', 'teams_cats.id', '=', 'teams.teams_cat_id')
        ->orderBy('teams_cats.name',$sort);
      }
      return $query->orderBy($orderBy,$sort);
    })->paginate($paginate);
    $query =  http_build_query($request->except(['orderBy','sort']));
    return view('admin.teams.list',compact('teams','filter_cat','filter_name','filter_publish_time','appends','query_string','teams_cats','filter_name_dis','orderBy','sort','li_active','query'));

  }

  /**
  * Show the form for creating a team resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $li_active = "add_team";
    $teams_cats = TeamsCat::pluck('name','id');
    return view('admin.teams.create',compact('teams_cats','li_active'));
  }

  /**
  * Store a teamly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $imagename = '';
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
      'image'=>'required|image'
    ]);
    if($validatedData){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }
      $data = $request->all();
      $data['image']= $imagename;
      $teams = new Team;
      if($teams->create($data)){
        $request->session()->flash('success', 'Team  created successfully.');
        return redirect('/admin/teams');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }

    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
     //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $li_active = "teams_manage";
    $team = Team::find($id);
    $teams_cats = TeamsCat::pluck('name','id');
    return view('admin.teams.edit',compact('team','id','teams_cats','li_active'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $imagename = '';
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
      'image'=>'image',
    ]);
    if($validatedData){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }

      $data = $request->all();
      $data['publish'] = $request->input('publish');
      if($request->hasFile('image')){
        $data['image']= $imagename;
      }
      $teams = Team::find($id);
      if($teams->update($data)){
        $request->session()->flash('success', 'Team updated successfully.');
        return redirect('admin/teams');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating station.');
        return back()->withInput();
      }

    } 
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy(Request $request , $id)
  {
    $teams = Team::find($id);
    if($teams) {
      $teams->delete();
      $request->session()->flash('success', 'Team deleted successfully.');
      return redirect('admin/team');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while updating station.');
      return back()->withInput();
    }
  }

  public function teams_delete(Request $request) {
    $ids = $request->input('selected');
    if($ids){
      foreach($ids as $id) {
        $teams = Team::find($id);
        if($teams) {
          $teams->delete();
        }
        else{
        }

      }
      $request->session()->flash('success', 'Team deleted successfully.');
    }
    return redirect('admin/teams');
  }


  public function teams_cat_index(Request $request) {
    $li_active = "teams_cat";
    $pagination = 10;
    $appends = array();
    $query_string = '';
    $filter_teams_cat = $request->input('filter_teams_cat');
    $appends['filter_teams_cat'] = $filter_teams_cat;
    $query_string = 'filter_teams_cat='.$filter_teams_cat;

    $teams_cats = TeamsCat::when($filter_teams_cat, function ($query, $filter_teams_cat) {
      return $query->where('name', 'like','%'.$filter_teams_cat.'%')->orWhere('des', 'like','%'.$filter_teams_cat.'%');
    })->paginate($pagination);
    return view('admin.teams.teams_cat_list',compact('teams_cats','filter_teams_cat','appends','query_string','li_active'));
  }

  public function teams_cat_create() {
    $li_active = "teams_cat";
    return view('admin.teams.teams_cat_create',compact('li_active'));
  }

  public function teams_cat_store(Request $request) {
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
      'sort_order'=>'numeric'
    ]);
    if($validatedData){
      $data = $request->all();
      $teamsCat = new TeamsCat;
      if($teamsCat->create($data)){
        $request->session()->flash('success', 'Team Category  created successfully.');
        return redirect('/admin/teams_cat');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }

    }
  }

  public function teams_cat_edit($id) {
    $li_active = "teams_cat";
    $teams_cat = TeamsCat::find($id);
    if($teams_cat) {
      return view('admin.teams.teams_cat_edit',compact('id','teams_cat','li_active'));
    }
  }

  public function teams_cat_update(Request $request, $id) {
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
    ]);
    if($validatedData){
      $data = $request->all();
      $data['publish'] = $request->input('publish');
      $teams_cat = TeamsCat::find($id);
      if($teams_cat->update($data)){
        $request->session()->flash('success', 'Team Category updated successfully.');
        return redirect('admin/teams_cat');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating teams category.');
        return back()->withInput();
      }

    }
  }

  public function teams_cat_destroy(Request $request , $id) {
    $teams_cat = TeamsCat::find($id);
    if($teams_cat) {
      $teams_cat->delete();
      $request->session()->flash('success', 'Team Category deleted successfully.');
      return redirect('admin/teams_cat');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while deleting teams category.');
      return back()->withInput();
    }
  }

  public function teams_cat_delete(Request $request) {
    $teams_ids = $request->input('selected');
    if($teams_ids){
      foreach($teams_ids as $team_id) {
        $teams_cat = TeamsCat::find($team_id);
        if($teams_cat) {
          $teams_cat->delete();

        }
        else{
        }
      }
      $request->session()->flash('success', 'Team Category deleted successfully.');
    }
    return redirect('admin/teams_cat');

  }
}
