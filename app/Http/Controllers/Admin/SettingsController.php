<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GenConfigs;
use App\ExtApis;
use App\SocNets;
use App\Chat;
use App\EmailConfigs;
use App\PrivacyConfigs;

class SettingsController extends Controller
{
	public function __construct() {
		view()->share('active','configs');
	}

	public function gen_configs() {
		$li_active = "gen_configs";
		$gen_config = GenConfigs::first();
		return view('admin.settings.gen_configs',compact('gen_config','li_active'));
	}
	public function gen_configs_update(Request $request) {
		$id = $request->input('id');
		$data = $request->all();
		$data['site_maintainance'] = $request->input('site_maintainance');
		$data['player_maintainance'] = $request->input('player_maintainance');
		$genConfig = GenConfigs::updateOrCreate(
			['id'=>$id],$data
		);
		if($genConfig) {
			$request->session()->flash('success', 'General Configuration updated successfully.');
			return redirect('admin/gen_configs');
		}

	}
	public function ext_apis() {
		$li_active = "ext_apis";
		$ext_api = ExtApis::first();
		return view('admin.settings.ext_apis',compact('ext_api','li_active'));	
	}
	public function ext_apis_update(Request $request) {
		$id = $request->input('id');
		$data = $request->all();
		$ext_api = ExtApis::updateOrCreate(
			['id'=>$id],$data
		);
		if($ext_api) {
			$request->session()->flash('success', 'External Api updated successfully.');
			return redirect('admin/ext_apis');
		}
	}
	public function soc_nets() {
		$li_active = "soc_nets";
		$soc_net = SocNets::first();
		return view('admin.settings.soc_nets',compact('soc_net','li_active'));
		
	}
	public function soc_nets_update(Request $request) {
		$id = $request->input('id');
		$data = $request->all();
		$soc_net = SocNets::updateOrCreate(
			['id'=>$id],$data
		);
		if($soc_net) {
			$request->session()->flash('success', 'Social Networks  updated successfully.');
			return redirect('admin/soc_nets');
		}
	}
	public function chats() {
		$li_active = "chats";
		$chat = Chat::first();
		return view('admin.settings.chats',compact('chat','li_active')); 		
	}

	public function chats_update(Request $request) {
		$id = $request->input('id');
		$data = $request->all();
		$data['active_chat']= $request->input('active_chat');
		$chat = Chat::updateOrCreate(
			['id'=>$id],$data
		);
		if($chat) {
			$request->session()->flash('success', 'Chat Configuration updated successfully.');
			return redirect('admin/chats');
		}
	}

	public function email_configs() {
		$li_active = "email_configs";
		$email_config = EmailConfigs::first();
		return view('admin.settings.email_configs',compact('email_config','li_active')); 	
	}
	public function email_configs_update(Request $request) {
		$id = $request->input('id');
		$data = $request->all();
		$email_config = EmailConfigs::updateOrCreate(
			['id'=>$id],$data
		);
		if($email_config) {
			$request->session()->flash('success', 'Chat Configuration updated successfully.');
			return redirect('admin/email_configs');
		}
	}
	public function privacy_configs() {
		$li_active = "privacy_configs";
		$privacy_config = PrivacyConfigs::first();
		return view('admin.settings.privacy_configs',compact('privacy_config','li_active')); 	
	}
	public function privacy_configs_update(Request $request) {
		$id = $request->input('id');
		$data = $request->all();
		$data['policy_link'] = $request->input('policy_link');
		$data['legal_notice_link'] = $request->input('legal_notice_link');
		$privacy_config = PrivacyConfigs::updateOrCreate(
			['id'=>$id],$data
		);
		if($privacy_config) {
			$request->session()->flash('success', 'Chat Configuration updated successfully.');
			return redirect('admin/privacy_configs');
		}
	}


}
