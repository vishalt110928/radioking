<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Dedication;

class DedicationController extends Controller
{
  public function __construct() {
    view()->share('active','dedications');
  }
  /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
  public function index(Request $request)
  {
    $li_active = "dedications_manage";
    $appends = array();
    $query_string = '';

    $filter_dedi = $request->input('filter_dedi');
    $appends['filter_dedi'] = $filter_dedi;
    $query_string = 'filter_dedi='.$filter_dedi;

    $filter_username = $request->input('filter_username');
    $appends['filter_username'] = $filter_username;
    $query_string = 'filter_username='.$filter_username;

    $filter_dedication_message = $request->input('filter_dedication_message');
    $appends['filter_dedication_message'] = $filter_dedication_message;
    $query_string = '&filter_dedication_message='.$filter_dedication_message;

    $filter_publish_time = $request->input('filter_publish_time');
    $appends['filter_publish_time'] = $filter_publish_time;
    $query_string = '&filter_publish_time='.$filter_publish_time;

    $orderBy = $request->input('orderBy','updated_at');
    $sort = $request->input('sort','desc');

    $paginate = 10;
    $dedications = Dedication::when($filter_username, function ($query, $filter_username) {
      return $query->where('username', 'like','%'.$filter_username.'%');
    })
    ->when($filter_dedication_message, function ($query, $filter_dedication_message) {
      return $query->where('dedication_message', 'like','%'.$filter_dedication_message.'%');
    }) ->when($filter_publish_time, function ($query, $filter_publish_time) {
      return $query->where('publish_time', '<',$filter_publish_time);
    })
    ->when($filter_dedi, function ($query, $filter_dedi) {
      return $query->where('username', 'like','%'.$filter_dedi.'%')->orWhere('dedication_message', 'like','%'.$filter_dedi.'%');
    })->paginate($paginate);

    $query =  http_build_query($request->except(['orderBy','sort']));
    return view('admin.dedications.list',compact('dedications','filter_username','filter_dedication_message','filter_publish_time','filter_dedi','orderBy','sort','query','li_active'));
  }

  /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
  public function create()
  {
    $li_active = "add_dedications";
    return view('admin.dedications.create',compact('li_active'));
  }

  /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
  public function store(Request $request)
  {
    $validatedData = $request->validate([
      'username' => 'required|min:2|max:30',
    ]);
    if($validatedData){

      $data = $request->all();
      /*$data['publish_time'] = Carbon::createFromFormat('D MM  YYYY, h:mm:ss A',$data['publish_time']);
      print_r($data['publish_time']);
      exit;
      $data['end_time'] = Carbon::parse($data['end_time']);*/
      $dedication = new Dedication;
      if($dedication->create($data)){
        $request->session()->flash('success', 'Dedication created successfully.');
        return redirect('/admin/dedications');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }
    }
  }

  /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function show($id)
  {
       //
  }

  /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function edit($id)
  {
    $li_active = "dedications_manage";
    $dedication = Dedication::find($id);
    return view('admin.dedications.edit',compact('dedication','id','li_active'));
  }

  /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function update(Request $request, $id)
  {
    $imagename = '';
    $validatedData = $request->validate([
      'username' => 'required|min:2|max:30',
    ]);
    if($validatedData){
      $data = $request->all();
      $data['publish'] = $request->input('publish');
      $dedication = Dedication::find($id);
      if($dedication->update($data)){
        $request->session()->flash('success', 'Dedication updated successfully.');
        return redirect('admin/dedications');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating station.');
        return back()->withInput();
      }

    } 
  }

  /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function destroy(Request $request , $id)
  {
    $dedication = Dedication::find($id);
    if($dedication) {
      $dedication->delete();
      $request->session()->flash('success', 'Dedication deleted successfully.');
      return redirect('admin/new');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while updating station.');
      return back()->withInput();
    }
  }
  public function dedication_delete(Request $request) {
    $ids = $request->input('selected');
    if($ids){
      foreach($ids as $id) {
        $dedication = Dedication::find($id);
        if($dedication) {
          $dedication->delete();
        }
        else{
        }

      }
      $request->session()->flash('success', 'Dedication deleted successfully.');
    }
    return redirect('admin/dedications');
  }

}
