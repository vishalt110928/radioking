<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact_us;

class ContactsController extends Controller
{
	public function __construct() {
	  view()->share('active','contacts');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
	  $appends = array();
	  $query_string = '';
	  $filter_contacts_name = $request->input('filter_contacts_name');
	  $appends['filter_contacts_name'] = $filter_contacts_name;
	  $query_string = 'filter_contacts_name='.$filter_contacts_name;

	  $filter_contacts_email = $request->input('filter_contacts_email');
	  $appends['filter_contacts_email'] = $filter_contacts_email;
	  $query_string = 'filter_contacts_email='.$filter_contacts_email;

	  $filter_contacts_phone = $request->input('filter_contacts_phone');
	  $appends['filter_contacts_phone'] = $filter_contacts_phone;
	  $query_string = '&filter_contacts_phone='.$filter_contacts_phone;

	  $filter_contacts_created = $request->input('filter_contacts_created');
	  $appends['filter_contacts_created'] = $filter_contacts_created;
	  $query_string = '&filter_contacts_created='.$filter_contacts_created;

	  $orderBy = $request->input('orderBy','created_at');
	  $sort = $request->input('sort','desc');

	  $paginate = 10;
	  $contacts = Contact_us::when($filter_contacts_name, function ($query, $filter_contacts_name) {
	    return $query->where('name', 'like','%'.$filter_contacts_name.'%');
	  })->when($filter_contacts_email, function ($query, $filter_contacts_email) {
	    return $query->where('email', 'like','%'.$filter_contacts_email.'%');
	  })->when($filter_contacts_phone, function ($query, $filter_contacts_phone) {
	    return $query->where('phone_no', 'like','%'.$filter_contacts_phone.'%');
	  })->when($filter_contacts_created, function ($query, $filter_contacts_created) {
	    return $query->where('created_at', '>',(Carbon::parse($filter_contacts_created)));
	  })->when($orderBy, function ($query, $orderBy)  use($sort) {
	    return $query->orderBy($orderBy,$sort);
	  })->paginate($paginate);
	  $query =  http_build_query($request->except(['orderBy','sort']));
	  return view('admin.contacts.list',compact('contacts','filter_contacts_name','filter_contacts_email','filter_contacts_created','filter_contacts_phone','appends','query_string','orderBy','sort','query'));

	}
	public function contacts_delete(Request $request) {
	  $ids = $request->input('selected');
	  if($ids){
	    foreach($ids as $id) {
	      $contacts = Contact_us::find($id);
	      if($contacts) {
	        $contacts->delete();
	      }
	      else{
	      }

	    }
	    $request->session()->flash('success', 'contacts deleted successfully.');
	  }
	  return redirect('admin/contacts');
	}
	
}
