<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Carbon\Carbon;
use App\Playlist;
use App\PlaylistsCat;

class PlaylistController extends Controller
{
  public function __construct() {
    view()->share('active','playlists');
  }
  /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
  public function index(Request $request)
  {
    $li_active = "playlists_manage";
    $appends = array();
    $query_string = '';
    $playlists_cats = PlaylistsCat::pluck('name','id');

    $filter_playlists = $request->input('filter_playlists');
    $appends['filter_playlists'] = $filter_playlists;
    $query_string = 'filter_playlists='.$filter_playlists;



    $filter_title = $request->input('filter_title');
    $appends['filter_title'] = $filter_title;
    $query_string = 'filter_title='.$filter_title;

    $filter_cat = $request->input('filter_cat');
    $appends['filter_cat'] = $filter_cat;
    $query_string = '&filter_cat='.$filter_cat;

    $filter_end_time = $request->input('filter_end_time');
    $appends['filter_end_time'] = $filter_end_time;
    $query_string = '&filter_end_time='.$filter_end_time;

    $filter_publish_time = $request->input('filter_publish_time');
    $appends['filter_publish_time'] = $filter_publish_time;
    $query_string = '&filter_publish_time='.$filter_publish_time;
    $orderBy = $request->input('orderBy','updated_at');
    $sort = $request->input('sort','desc');

    $paginate = 10;
    $playlists = Playlist::when($filter_title, function ($query, $filter_title) {
     return $query->where('title', 'like','%'.$filter_title.'%');
   })
    ->when($filter_cat, function ($query, $filter_cat) {
      return $query->where('playlists_cat_id',$filter_cat);
    })
    ->when($filter_end_time, function ($query, $filter_end_time) {
      return $query->where('end_time', '>',(Carbon::parse($filter_end_time)));
    }) ->when($filter_publish_time, function ($query, $filter_publish_time) {
      return $query->where('publish_time', '<',(Carbon::parse($filter_publish_time)));
    })->when($filter_playlists, function ($query, $filter_playlists) {
      return $query->where('title', 'like','%'.$filter_playlists.'%');
    })->when($orderBy, function ($query, $orderBy)  use($sort) {
      if($orderBy == "category") {
        return $query->join('playlists_cats', 'playlists_cats.id', '=', 'playlists.playlists_cat_id')
        ->orderBy('playlists_cats.name',$sort);
      }
      return $query->orderBy($orderBy,$sort);
    }) ->paginate($paginate);
    $query =  http_build_query($request->except(['orderBy','sort']));
    return view('admin.playlists.list',compact('playlists','filter_cat','filter_title','filter_end_time','filter_publish_time','appends','query_string','playlists_cats','li_active','query','orderBy','sort','filter_playlists'));

  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $li_active = "add_playlist";
    $playlists_cats = PlaylistsCat::pluck('name','id');
    return view('admin.playlists.create',compact('playlists_cats','li_active'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $imagename = '';
    $audioname = '';
    $validatedData = $request->validate([
      'title' => 'required|min:2|max:30',
      'image'=>'required|image'
    ]);
    if($validatedData){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }
      if($request->hasFile('audio_sample')){
        $audio_sample = $request->file('audio_sample');
        $destinationPath = public_path('/uploads');
        $audioname = time().'.'.$audio_sample->getClientOriginalExtension();
        if($audio_sample->move($destinationPath, $audioname)){
        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }
      $data = $request->all();
      $data['publish'] = $request->input('publish');
      $data['image']= $imagename;
      $data['audio_sample']= $audioname;
      $playlist = new Playlist;
      if($playlist->create($data)){
        $request->session()->flash('success', 'Playlist  created successfully.');
        return redirect('/admin/playlists');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }

    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
     //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $li_active = "playlists_manage";
    $playlists = Playlist::find($id);
    $playlists_cats = PlaylistsCat::pluck('name','id');
    return view('admin.playlists.edit',compact('playlists','id','playlists_cats','li_active'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $imagename = '';
    $audioname = '';
    $validatedData = $request->validate([
      'title' => 'required|min:2|max:30',
      'image'=>'image',
    ]);
  /*  echo "<pre>";
    print_r($request->all());
    exit;*/
    if($validatedData){

      if($request->hasFile('image')){
        $image = $request->file('image');

        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }


      if($request->hasFile('audio_sample')){
        $audio_sample = $request->file('audio_sample');
        $destinationPath = public_path('/uploads');
        $audioname = time().'.'.$audio_sample->getClientOriginalExtension();
        if($audio_sample->move($destinationPath, $audioname)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }

      $data = $request->all();
      $data['publish'] = $request->input('publish');
      if($request->hasFile('image')){
        $data['image']= $imagename;
      }
      $playlists = Playlist::find($id);
      if($playlists->update($data)){
        $request->session()->flash('success', 'Playlist updated successfully.');
        return redirect('admin/playlists');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating station.');
        return back()->withInput();
      }

    } 
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy(Request $request , $id)
  {
    $playlists = Playlist::find($id);
    if($playlists) {
      $playlists->delete();
      $request->session()->flash('success', 'Playlist deleted successfully.');
      return redirect('admin/new');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while updating station.');
      return back()->withInput();
    }
  }
  public function playlists_delete(Request $request) {
    $ids = $request->input('selected');
    if($ids){
      foreach($ids as $id) {
        $playlists = Playlist::find($id);
        if($playlists) {
          $playlists->delete();
        }
        else{
        }

      }
      $request->session()->flash('success', 'Playlist deleted successfully.');
    }
    return redirect('admin/playlists');
  }


  public function playlists_cat_index(Request $request) {
    $li_active = "playlists_cat";
    $pagination = 10;
    $appends = array();
    $query_string = '';
    $filter_playlists_cat = $request->input('filter_playlists_cat');
    $appends['filter_playlists_cat'] = $filter_playlists_cat;
    $query_string = 'filter_playlists_cat='.$filter_playlists_cat;

    $playlists_cats = PlaylistsCat::when($filter_playlists_cat, function ($query, $filter_playlists_cat) {
     return $query->where('name', 'like','%'.$filter_playlists_cat.'%')->orWhere('des', 'like','%'.$filter_playlists_cat.'%');
   })->paginate($pagination);
    return view('admin.playlists.playlists_cat_list',compact('playlists_cats','filter_playlists_cat','appends','query_string','li_active'));
  }

  public function playlists_cat_create() {
    $li_active = "playlists_cat";
    return view('admin.playlists.playlists_cat_create',compact('li_active'));
  }

  public function playlists_cat_store(Request $request) {
    $imagename = '';
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
      'sort_order'=>'numeric'
    ]);
    if($validatedData){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }
      $data = $request->all();
      $data['image']= $imagename;
      $playlistsCat = new PlaylistsCat;
      if($playlistsCat->create($data)){
        $request->session()->flash('success', 'Playlist Category  created successfully.');
        return redirect('/admin/playlists_cat');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }

    }
  }

  public function playlists_cat_edit($id) {
    $li_active = "playlists_cat";
    $playlists_cat = PlaylistsCat::find($id);
    if($playlists_cat) {
      return view('admin.playlists.playlists_cat_edit',compact('id','playlists_cat','li_active'));
    }
  }
  public function playlists_cat_update(Request $request, $id) {
    $imagename = '';
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
    ]);
    if($validatedData){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }

      $data = $request->all();
      $data['publish'] = $request->input('publish');
      if($request->hasFile('image')){
        $data['image']= $imagename;
      }
      $playlists_cat = PlaylistsCat::find($id);
      if($playlists_cat->update($data)){
        $request->session()->flash('success', 'Playlist Category updated successfully.');
        return redirect('admin/playlists_cat');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating playlists category.');
        return back()->withInput();
      }

    }
  }
  public function playlists_cat_destroy(Request $request , $id) {
    $playlists_cat = PlaylistsCat::find($id);
    if($playlists_cat) {
      $playlists_cat->delete();
      $request->session()->flash('success', 'Playlist Category deleted successfully.');
      return redirect('admin/playlists_cat');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while deleting playlists category.');
      return back()->withInput();
    }
  }
  public function playlists_cat_delete(Request $request) {
    $playlists_ids = $request->input('selected');
    if($playlists_ids){
      foreach($playlists_ids as $new_id) {
        $playlists_cat = PlaylistsCat::find($new_id);
        if($playlists_cat) {
          $playlists_cat->delete();

        }
        else{
        }

      }
      $request->session()->flash('success', 'Playlist Category deleted successfully.');
    }
    return redirect('admin/playlists_cat');

  }
}
