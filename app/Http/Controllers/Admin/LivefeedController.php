<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Livefeed;

class LivefeedController extends Controller
{
	public function __construct() {
		view()->share('active','livefeeds');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$li_active = "livefeeds_manage";
		$appends = array();
		$query_string = '';
		
		$filter_message = $request->input('filter_message');
		$appends['filter_message'] = $filter_message;
		$query_string = 'filter_message='.$filter_message;

		$filter_cat = $request->input('filter_cat');
		$appends['filter_cat'] = $filter_cat;
		$query_string = '&filter_cat='.$filter_cat;

		$filter_end_time = $request->input('filter_end_time');
		$appends['filter_end_time'] = $filter_end_time;
		$query_string = '&filter_end_time='.$filter_end_time;

		$filter_publish_time = $request->input('filter_publish_time');
		$appends['filter_publish_time'] = $filter_publish_time;
		$query_string = '&filter_publish_time='.$filter_publish_time;

		$orderBy = $request->input('orderBy','updated_at');
		$sort = $request->input('sort','desc');

		$paginate = 10;
		$livefeeds = Livefeed::when($filter_message, function ($query, $filter_message) {
			return $query->where('message', 'like','%'.$filter_message.'%');
		})
		->when($filter_cat, function ($query, $filter_cat) {
			return $query->where('livefeeds_cat_id',$filter_cat);
		})
		->when($filter_end_time, function ($query, $filter_end_time) {
			return $query->where('end_time', '>','%'.$filter_end_time.'%');
		}) ->when($filter_publish_time, function ($query, $filter_publish_time) {
			return $query->where('publish_time', '<','%'.$filter_publish_time.'%');
		})->when($orderBy, function ($query, $orderBy)  use($sort) {
      return $query->orderBy($orderBy,$sort);
    })
		->paginate($paginate);
		$query =  http_build_query($request->except(['orderBy','sort']));
		return view('admin.livefeeds.list',compact('livefeeds','filter_cat','filter_message','filter_end_time','filter_publish_time','appends','query_string','orderBy','sort','query','li_active'));

	}

	/**
	 * Show the form for creating a livefeed resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$li_active = "add_livefeed";
		return view('admin.livefeeds.create',compact('li_active'));
	}

	/**
	 * Store a livefeedly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$imagename = '';
		$videoname = "";
		$validatedData = $request->validate([
			'message' => 'required|min:2|max:30',
			'image'=>'required|image'
		]);
		if($validatedData){
			if($request->hasFile('image')){
				$image = $request->file('image');
				$destinationPath = public_path('/uploads');
				$imagename = time().'.'.$image->getClientOriginalExtension();
				if($image->move($destinationPath, $imagename)){

				}
				else{
					$request->session()->flash('error', 'Something went wrong while uploading file.');
					return back()->withInput();
				}
			}
			if($request->hasFile('video')){
				$video = $request->file('video');
				$destinationPath = public_path('/uploads/video');
				$videoname = time().'.'.$video->getClientOriginalExtension();
				if($video->move($destinationPath, $videoname)){

				}
				else{
					$request->session()->flash('error', 'Something went wrong while uploading file.');
					return back()->withInput();
				}
			}
			$data = $request->all();
			$data['image']= $imagename;
			$livefeeds = new Livefeed;
			if($livefeeds->create($data)){
				$request->session()->flash('success', 'Livefeeds  created successfully.');
				return redirect('/admin/livefeeds');
			}
			else{
				$request->session()->flash('error', 'Something went wrong.');
				return back()->withInput();
			}

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
	    //
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$li_active = "livefeeds_manage";
		$livefeeds = Livefeed::find($id);
		return view('admin.livefeeds.edit',compact('livefeeds','id','li_active'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$imagename = '';
		$validatedData = $request->validate([
			'message' => 'required|min:2|max:30',
			'image'=>'image',
		]);
		if($validatedData){
			if($request->hasFile('image')){
				$image = $request->file('image');
				$destinationPath = public_path('/uploads');
				$imagename = time().'.'.$image->getClientOriginalExtension();
				if($image->move($destinationPath, $imagename)){

				}
				else{
					$request->session()->flash('error', 'Something went wrong while uploading file.');
					return back()->withInput();
				}
			}

			if($request->hasFile('video')){
				$video = $request->file('video');
				$destinationPath = public_path('/uploads/video');
				$videoname = time().'.'.$video->getClientOriginalExtension();
				if($video->move($destinationPath, $videoname)){

				}
				else{
					$request->session()->flash('error', 'Something went wrong while uploading file.');
					return back()->withInput();
				}
			}

			$data = $request->all();
			$data['publish'] = $request->input('publish');
			$data['survey'] = $request->input('survey');
			if($request->hasFile('image')){
				$data['image']= $imagename;
			}

			if($request->hasFile('video')){
				$data['video']= $videoname;
			}
			$livefeeds = Livefeed::find($id);
			if($livefeeds->update($data)){
				$request->session()->flash('success', 'Livefeeds updated successfully.');
				return redirect('admin/livefeeds');
			}
			else{
				$request->session()->flash('error', 'Something went wrong while updating station.');
				return back()->withInput();
			}

		} 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request , $id)
	{
		$livefeeds = Livefeeds::find($id);
		if($livefeeds) {
			$livefeeds->delete();
			$request->session()->flash('success', 'Livefeeds deleted successfully.');
			return redirect('admin/livefeed');
		}
		else{
			$request->session()->flash('error', 'Something went wrong while updating station.');
			return back()->withInput();
		}
	}

	public function livefeeds_delete(Request $request) {
		$ids = $request->input('selected');
		if($ids){
			foreach($ids as $id) {
				$livefeeds = Livefeed::find($id);
				if($livefeeds) {
					$livefeeds->delete();
				}
				else{
				}

			}
			$request->session()->flash('success', 'Livefeeds deleted successfully.');
		}
		return redirect('admin/livefeeds');
	}
	
}
