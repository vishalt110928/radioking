<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Video;
use App\VideosCat;

class VideoController extends Controller
{
	public function __construct() {
	  view()->share('active','videos');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$li_active = "videos_manage";
	  $appends = array();
	  $query_string = '';
	  $videos_cats = VideosCat::pluck('name','id');

	  $filter_videos = $request->input('filter_videos');
	  $appends['filter_videos'] = $filter_videos;
	  $query_string = 'filter_videos='.$filter_videos;

	  $filter_title = $request->input('filter_title');
	  $appends['filter_title'] = $filter_title;
	  $query_string = 'filter_title='.$filter_title;

	  $filter_cat = $request->input('filter_cat');
	  $appends['filter_cat'] = $filter_cat;
	  $query_string = '&filter_cat='.$filter_cat;

	  $filter_end_time = $request->input('filter_end_time');
	  $appends['filter_end_time'] = $filter_end_time;
	  $query_string = '&filter_end_time='.$filter_end_time;

	  $filter_publish_time = $request->input('filter_publish_time');
	  $appends['filter_publish_time'] = $filter_publish_time;
	  $query_string = '&filter_publish_time='.$filter_publish_time;

	  $orderBy = $request->input('orderBy','updated_at');
	  $sort = $request->input('sort','desc');

	  $paginate = 10;
	  $videos = Video::when($filter_title, function ($query, $filter_title) {
	    return $query->where('title', 'like','%'.$filter_title.'%');
	  })
	  ->when($filter_cat, function ($query, $filter_cat) {
	    return $query->where('videos_cat_id',$filter_cat);
	         })
	  ->when($filter_end_time, function ($query, $filter_end_time) {
	    return $query->where('end_time', '>',(Carbon::parse($filter_end_time)));
	  }) ->when($filter_publish_time, function ($query, $filter_publish_time) {
	    return $query->where('publish_time', '<',(Carbon::parse($filter_publish_time)));
	  })->when($filter_videos, function ($query, $filter_videos) {
      return $query->where('title', 'like','%'.$filter_videos.'%')->orWhere('description', 'like','%'.$filter_videos.'%');
    })->when($orderBy, function ($query, $orderBy)  use($sort) {
      if($orderBy == "category") {
        return $query->join('videos_cats', 'videos_cats.id', '=', 'videos.videos_cat_id')
        ->orderBy('videos_cats.name',$sort);
      }
      return $query->orderBy($orderBy,$sort);
    })
	  ->paginate($paginate);
	  $query =  http_build_query($request->except(['orderBy','sort']));
	  return view('admin.videos.list',compact('videos','filter_cat','filter_title','filter_end_time','filter_publish_time','appends','query_string','videos_cats','query','filter_videos','orderBy','sort','li_active'));

	}

	/**
	 * Show the form for creating a video resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$li_active = "add_video";
	  $videos_cats = VideosCat::pluck('name','id');
	  return view('admin.videos.create',compact('videos_cats','li_active'));
	}

	/**
	 * Store a videoly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	  $imagename = '';
	  $validatedData = $request->validate([
	    'title' => 'required|min:1|max:100',
	    'image'=>'required|image'
	  ]);
	  if($validatedData){
	    if($request->hasFile('image')){
	      $image = $request->file('image');
	      $destinationPath = public_path('/uploads');
	      $imagename = time().'.'.$image->getClientOriginalExtension();
	      if($image->move($destinationPath, $imagename)){

	      }
	      else{
	        $request->session()->flash('error', 'Something went wrong while uploading file.');
	        return back()->withInput();
	      }
	    }
	    $data = $request->all();
	    $data['image']= $imagename;
	    $videos = new Video;
	    if($videos->create($data)){
	      $request->session()->flash('success', 'Videos  created successfully.');
	      return redirect('/admin/videos');
	    }
	    else{
	      $request->session()->flash('error', 'Something went wrong.');
	      return back()->withInput();
	    }

	  }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
	    //
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$li_active = "videos_manage";
	  $videos = Video::find($id);
	  $videos_cats = VideosCat::pluck('name','id');
	  return view('admin.videos.edit',compact('videos','id','videos_cats','li_active'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
	  $imagename = '';
	  $validatedData = $request->validate([
	    'title' => 'required|min:1|max:100',
	    'image'=>'image',
	  ]);
	  if($validatedData){
	   if($request->hasFile('image')){
	     $image = $request->file('image');
	     $destinationPath = public_path('/uploads');
	     $imagename = time().'.'.$image->getClientOriginalExtension();
	     if($image->move($destinationPath, $imagename)){

	     }
	     else{
	       $request->session()->flash('error', 'Something went wrong while uploading file.');
	       return back()->withInput();
	     }
	   }

	   $data = $request->all();
	   $data['publish'] = $request->input('publish');
	   if($request->hasFile('image')){
	     $data['image']= $imagename;
	        }
	          $videos = Video::find($id);
	          if($videos->update($data)){
	            $request->session()->flash('success', 'Videos updated successfully.');
	            return redirect('admin/videos');
	          }
	          else{
	            $request->session()->flash('error', 'Something went wrong while updating station.');
	            return back()->withInput();
	          }

	        } 
	      }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request , $id)
	{
	  $videos = Videos::find($id);
	  if($videos) {
	    $videos->delete();
	    $request->session()->flash('success', 'Videos deleted successfully.');
	    return redirect('admin/video');
	  }
	  else{
	    $request->session()->flash('error', 'Something went wrong while updating station.');
	    return back()->withInput();
	  }
	}
	public function videos_delete(Request $request) {
	  $ids = $request->input('selected');
	  if($ids){
	    foreach($ids as $id) {
	      $videos = Video::find($id);
	      if($videos) {
	        $videos->delete();
	      }
	      else{
	      }

	    }
	    $request->session()->flash('success', 'Videos deleted successfully.');
	  }
	  return redirect('admin/videos');
	}
	

	public function videos_cat_index(Request $request) {
		$li_active = "videos_cat";
		$pagination = 10;
	  $appends = array();
	  $query_string = '';
	  $filter_videos_cat = $request->input('filter_videos_cat');
	  $appends['filter_videos_cat'] = $filter_videos_cat;
	  $query_string = 'filter_videos_cat='.$filter_videos_cat;

	  $videos_cats = VideosCat::when($filter_videos_cat, function ($query, $filter_videos_cat) {
	    return $query->where('name', 'like','%'.$filter_videos_cat.'%')->orWhere('des', 'like','%'.$filter_videos_cat.'%');
	  })->paginate($pagination);
	  return view('admin.videos.videos_cat_list',compact('videos_cats','filter_videos_cat','appends','query_string','li_active'));
	}
	
	public function videos_cat_create() {
		$li_active = "videos_cat";
		return view('admin.videos.videos_cat_create',compact('li_active'));
	}

	public function videos_cat_store(Request $request) {
		$validatedData = $request->validate([
	    'name' => 'required|min:2|max:30',
	    'sort_order'=>'numeric'
	  ]);
		if($validatedData){
	    $data = $request->all();
	    $videosCat =new VideosCat;
	    if($videosCat->create($data)){
	      $request->session()->flash('success', 'Videos Category  created successfully.');
	      return redirect('/admin/videos_cat');
	    }
	    else{
	      $request->session()->flash('error', 'Something went wrong.');
	      return back()->withInput();
	    }

	  }
	}

	public function videos_cat_edit($id) {
		$li_active = "videos_cat";
	  $videos_cat = VideosCat::find($id);
	  if($videos_cat) {
	    return view('admin.videos.videos_cat_edit',compact('id','videos_cat','li_active'));
	  }
	}
	public function videos_cat_update(Request $request, $id) {
	  $imagename = '';
	  $validatedData = $request->validate([
	    'name' => 'required|min:2|max:30'
	  ]);
	  if($validatedData){
	    $data = $request->all();
	    $data['publish'] = $request->input('publish');
	    $videos_cat = VideosCat::find($id);
	    if($videos_cat->update($data)){
	      $request->session()->flash('success', 'Videos Category updated successfully.');
	      return redirect('admin/videos_cat');
	    }
	    else{
	      $request->session()->flash('error', 'Something went wrong while updating videos category.');
	      return back()->withInput();
	    }

	  }
	}
	public function videos_cat_destroy(Request $request , $id) {
	  $videos_cat = VideosCat::find($id);
	  if($videos_cat) {
	    $videos_cat->delete();
	    $request->session()->flash('success', 'Videos Category deleted successfully.');
	    return redirect('admin/videos_cat');
	  }
	  else{
	    $request->session()->flash('error', 'Something went wrong while deleting videos category.');
	    return back()->withInput();
	  }
	}
	public function videos_cat_delete(Request $request) {
	  $videos_ids = $request->input('selected');
	  if($videos_ids){
	    foreach($videos_ids as $video_id) {
	      $videos_cat = VideosCat::find($video_id);
	      if($videos_cat) {
	        $videos_cat->delete();
	      }
	      else{
	      }
	    }
	    $request->session()->flash('success', 'Videos Category deleted successfully.');
	  }
	  return redirect('admin/videos_cat');
	}
}
