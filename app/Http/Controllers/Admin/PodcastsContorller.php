<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Podcast;
use App\PodcastsCat;


class PodcastsContorller extends Controller
{
  public function __construct() {
    view()->share('active','podcasts');
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $li_active="podcasts_manage";
    $appends = array();
    $query_string = '';
    $podcasts_cats = PodcastsCat::pluck('name','id');

    $filter_podcasts = $request->input('filter_podcasts');
    $appends['filter_podcasts'] = $filter_podcasts;
    $query_string = 'filter_podcasts='.$filter_podcasts;

    $filter_title = $request->input('filter_title');
    $appends['filter_title'] = $filter_title;
    $query_string = 'filter_title='.$filter_title;

    $filter_cat = $request->input('filter_cat');
    $appends['filter_cat'] = $filter_cat;
    $query_string = '&filter_cat='.$filter_cat;

    $filter_end_time = $request->input('filter_end_time');
    $appends['filter_end_time'] = $filter_end_time;
    $query_string = '&filter_end_time='.$filter_end_time;

    $filter_publish_time = $request->input('filter_publish_time');
    $appends['filter_publish_time'] = $filter_publish_time;
    $query_string = '&filter_publish_time='.$filter_publish_time;
    $orderBy = $request->input('orderBy','updated_at');
    $sort = $request->input('sort','desc');

    $paginate = 10;
    $podcasts = Podcast::when($filter_title, function ($query, $filter_title) {
      return $query->where('title', 'like','%'.$filter_title.'%');
    })
    ->when($filter_cat, function ($query, $filter_cat) {
      return $query->where('podcasts_cat_id',$filter_cat);
      
    })
    ->when($filter_end_time, function ($query, $filter_end_time) {
      return $query->where('end_time', '>',(Carbon::parse($filter_end_time)));
    }) ->when($filter_publish_time, function ($query, $filter_publish_time) {
      return $query->where('publish_time', '<',(Carbon::parse($filter_publish_time)));
    })->when($filter_podcasts, function ($query, $filter_podcasts) {
      return $query->where('title', 'like','%'.$filter_podcasts.'%')->orWhere('des', 'like','%'.$filter_podcasts.'%');
    })->when($orderBy, function ($query, $orderBy)  use($sort) {
      if($orderBy == "category") {
        return $query->join('podcasts_cats', 'podcasts_cats.id', '=', 'podcasts.podcasts_cat_id')
        ->orderBy('podcasts_cats.name',$sort);
      }
      return $query->orderBy($orderBy,$sort);
    })->paginate($paginate);
    $query =  http_build_query($request->except(['orderBy','sort']));
    return view('admin.podcasts.list',compact('podcasts','filter_cat','filter_title','filter_end_time','filter_publish_time','appends','query_string','podcasts_cats','li_active','filter_podcasts','query','orderBy','sort'));

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $li_active="add_podcast";
    $podcasts_cats = PodcastsCat::pluck('name','id');
    return view('admin.podcasts.create',compact('podcasts_cats','li_active'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $imagename = '';
    $audioname = "";
    $validatedData = $request->validate([
      'title' => 'required|min:1|max:100',
      'image'=>'required|image'
    ]);
    if($validatedData){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){
        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }
      if($request->hasFile('audio_file')){
        $audio_file = $request->file('audio_file');
        $destinationPath = public_path('/uploads/audio');
        $audioname = time().'.'.$audio_file->getClientOriginalExtension();
        if($audio_file->move($destinationPath, $audioname)){
        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading  audio file.');
          return back()->withInput();
        }
      }
      $data = $request->all();
      $data['image']= $imagename;
      $data['audio_file']= $audioname;
      $podcast = new Podcast;
      if($podcast->create($data)){
        $request->session()->flash('success', 'Podcast  created successfully.');
        return redirect('/admin/podcasts');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }

    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $li_active="podcasts_manage";
    $podcast = Podcast::find($id);
    $podcasts_cats = PodcastsCat::pluck('name','id');
    return view('admin.podcasts.edit',compact('podcast','id','podcasts_cats','li_active'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $imagename = '';
    $validatedData = $request->validate([
      'title' => 'required|min:1|max:100',
      'image'=>'image',
    ]);
    if($validatedData){
     if($request->hasFile('image')){
       $image = $request->file('image');
       $destinationPath = public_path('/uploads');
       $imagename = time().'.'.$image->getClientOriginalExtension();
       if($image->move($destinationPath, $imagename)){

       }
       else{
         $request->session()->flash('error', 'Something went wrong while uploading file.');
         return back()->withInput();
       }
     }

     $data = $request->all();
     $data['publish'] = $request->input('publish');
     if($request->hasFile('image')){
       $data['image']= $imagename;
          }/*
          else{
              $data['image']= $data['previousimage'];
              unset($data['previousimage']);
            }*/
            $podcast = Podcast::find($id);
            if($podcast->update($data)){
              $request->session()->flash('success', 'Podcast updated successfully.');
              return redirect('admin/podcasts');
            }
            else{
              $request->session()->flash('error', 'Something went wrong while updating station.');
              return back()->withInput();
            }

          } 
        }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request , $id)
  {
    $podcast = Podcast::find($id);
    if($podcast) {
      $podcast->delete();
      $request->session()->flash('success', 'Podcast deleted successfully.');
      return redirect('admin/new');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while updating station.');
      return back()->withInput();
    }
  }
  public function podcasts_delete(Request $request) {
    $ids = $request->input('selected');
    if($ids){
      foreach($ids as $id) {
        $podcast = Podcast::find($id);
        if($podcast) {
          $podcast->delete();
        }
        else{
        }

      }
      $request->session()->flash('success', 'Podcast deleted successfully.');
    }
    return redirect('admin/podcasts');
  }
  

  public function podcasts_cat_index(Request $request) {
    $li_active="podcasts_cat";
    $pagination = 10;
    $appends = array();
    $query_string = '';
    $filter_podcasts_cat = $request->input('filter_podcasts_cat');
    $appends['filter_podcasts_cat'] = $filter_podcasts_cat;
    $query_string = 'filter_podcasts_cat='.$filter_podcasts_cat;

    $podcasts_cats = PodcastsCat::when($filter_podcasts_cat, function ($query, $filter_podcasts_cat) {
      return $query->where('name', 'like','%'.$filter_podcasts_cat.'%')->orWhere('des', 'like','%'.$filter_podcasts_cat.'%');
    })->paginate($pagination);
    return view('admin.podcasts.podcasts_cat_list',compact('podcasts_cats','filter_podcasts_cat','appends','query_string','li_active'));
  }
  
  public function podcasts_cat_create() {
    $li_active="podcasts_cat";
    return view('admin.podcasts.podcasts_cat_create',compact('li_active'));
  }

  public function podcasts_cat_store(Request $request) {
    $imagename = '';
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
      'sort_order'=>'numeric'
    ]);
    if($validatedData){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }
      $data = $request->all();
      $data['image']= $imagename;
      $podcastsCat = new PodcastsCat;
      if($podcastsCat->create($data)){
        $request->session()->flash('success', 'Podcast Category  created successfully.');
        return redirect('/admin/podcasts_cat');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }

    }
  }

  public function podcasts_cat_edit($id) {
    $li_active="podcasts_cat";
    $podcasts_cat = PodcastsCat::find($id);
    if($podcasts_cat) {
      return view('admin.podcasts.podcasts_cat_edit',compact('id','podcasts_cat','li_active'));
    }
  }
  public function podcasts_cat_update(Request $request, $id) {
    $imagename = '';
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
    ]);
    if($validatedData){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }

      $data = $request->all();
      $data['publish'] = $request->input('publish');
      if($request->hasFile('image')){
        $data['image']= $imagename;
      }
      $podcasts_cat = PodcastsCat::find($id);
      if($podcasts_cat->update($data)){
        $request->session()->flash('success', 'Podcast Category updated successfully.');
        return redirect('admin/podcasts_cat');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating podcasts category.');
        return back()->withInput();
      }

    }
  }
  public function podcasts_cat_destroy(Request $request , $id) {
    $podcasts_cat = PodcastsCat::find($id);
    if($podcasts_cat) {
      $podcasts_cat->delete();
      $request->session()->flash('success', 'Podcast Category deleted successfully.');
      return redirect('admin/podcasts_cat');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while deleting podcasts category.');
      return back()->withInput();
    }
  }
  public function podcasts_cat_delete(Request $request) {
    $podcasts_ids = $request->input('selected');
    if($podcasts_ids){
      foreach($podcasts_ids as $new_id) {
        $podcasts_cat = PodcastsCat::find($new_id);
        if($podcasts_cat) {
          $podcasts_cat->delete();

        }
        else{
        }

      }
      $request->session()->flash('success', 'Podcast Category deleted successfully.');
    }
    return redirect('admin/podcasts_cat');

  }
}
