<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UsersRole;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserController extends Controller
{
	public function __construct() {
		view()->share('active','users');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$li_active = "users_manage";
		$appends = array();
		$query_string = '';
		$users_roles = UsersRole::pluck('role','id');

		$filter_users = $request->input('filter_users');
		$appends['filter_users'] = $filter_users;
		$query_string = 'filter_users='.$filter_users;

		$filter_name = $request->input('filter_name');
		$appends['filter_name'] = $filter_name;
		$query_string = 'filter_name='.$filter_name;

		$filter_role = $request->input('filter_role');
		$appends['filter_role'] = $filter_role;
		$query_string = '&filter_role='.$filter_role;

		$filter_birthday = $request->input('filter_birthday');
		$appends['filter_birthday'] = $filter_birthday;
		$query_string = '&filter_birthday='.$filter_birthday;

		$orderBy = $request->input('orderBy','updated_at');
		$sort = $request->input('sort','desc');

		$paginate = 10;
		$users = User::when($filter_name, function ($query, $filter_name) {
			return $query->where('name', 'like','%'.$filter_name.'%');
		})->when($filter_role, function ($query, $filter_role) {
			return $query->where('users_role_id',$filter_role);
		}) ->when($filter_birthday, function ($query, $filter_birthday) {
			return $query->where('birthday', '<',(Carbon::parse($filter_birthday)));
		})->when($filter_users, function ($query, $filter_users) {
			return $query->where('name', 'like','%'.$filter_users.'%')->orWhere('personal_persentation', 'like','%'.$filter_users.'%')->orWhere('hobbies', 'like','%'.$filter_users.'%');
		})->when($orderBy, function ($query, $orderBy)  use($sort) {
			if($orderBy == "role") {
				return $query->join('users_cats', 'users_cats.id', '=', 'users.users_cat_id')
				->orderBy('users_cats.name',$sort);
			}
			return $query->orderBy($orderBy,$sort);
		})
		->paginate($paginate);
		$query =  http_build_query($request->except(['orderBy','sort']));
		return view('admin.users.list',compact('users','filter_role','filter_name','filter_end_time','filter_birthday','appends','query_string','users_roles','orderBy','sort','query','filter_users','li_active'));

	}

	/**
	 * Show the form for creating a user resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$li_active = "add_users";
		$users_roles = UsersRole::pluck('role','id');
		return view('admin.users.create',compact('users_roles','li_active'));
	}

	/**
	 * Store a userly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$imagename = '';
		$validatedData = $request->validate([
			'name' => 'required|string|min:2|max:30|unique:users',
			'image'=>'required|image',
			'password' => 'confirmed'

		]);
		if($validatedData){
			if($request->hasFile('image')){
				$image = $request->file('image');
				$destinationPath = public_path('/uploads');
				$imagename = time().'.'.$image->getClientOriginalExtension();
				if($image->move($destinationPath, $imagename)){

				}
				else{
					$request->session()->flash('error', 'Something went wrong while uploading file.');
					return back()->withInput();
				}
			}
			$data = $request->all();
			if($request->input('password'))
			{
				$data['password'] =	Hash::make($request->input('password'));
			}
			else  {
			$data['password'] = "";
			}
			$data['image']= $imagename;
			$users =new User;
			if($users->create($data)){
				$request->session()->flash('success', 'User  created successfully.');
				return redirect('/admin/users');
			}
			else{
				$request->session()->flash('error', 'Something went wrong.');
				return back()->withInput();
			}

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
	    //
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$li_active = "users_manage";
		$user = User::find($id);
		$users_roles = UsersRole::pluck('role','id');
		return view('admin.users.edit',compact('user','id','users_roles','li_active'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$imagename = '';
		if(!isset($request->publish)) {
			$request->publish = 0;
		}
		$validatedData = $request->validate([
			'name' => 'required|string|min:2|max:30|unique:users,id,'.$id,
			'image'=>'image',
			'password' => 'confirmed'

		]);
		if($validatedData){
			if($request->hasFile('image')){
				$image = $request->file('image');
				$destinationPath = public_path('/uploads');
				$imagename = time().'.'.$image->getClientOriginalExtension();
				if($image->move($destinationPath, $imagename)){

				}
				else{
					$request->session()->flash('error', 'Something went wrong while uploading file.');
					return back()->withInput();
				}
			}

			$data = $request->all();
			if($request->input('password'))
			{
				$data['password'] =	Hash::make($request->input('password'));
			}
			else  {
				unset($data['password']);
			}
			$data['publish'] = $request->input('publish');
			if($request->hasFile('image')){
				$data['image']= $imagename;
			}
			$users = User::find($id);
			if($users->update($data)){
				$request->session()->flash('success', 'User updated successfully.');
				return redirect('admin/users');
			}
			else{
				$request->session()->flash('error', 'Something went wrong while updating station.');
				return back()->withInput();
			}

		} 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request , $id)
	{
		$users = User::find($id);
		if($users) {
			$users->delete();
			$request->session()->flash('success', 'User deleted successfully.');
			return redirect('admin/user');
		}
		else{
			$request->session()->flash('error', 'Something went wrong while updating station.');
			return back()->withInput();
		}
	}
	public function users_delete(Request $request) {
		$ids = $request->input('selected');
		if($ids){
			foreach($ids as $id) {
				$users = User::find($id);
				if($users) {
					$users->delete();
				}
				else{
				}

			}
			$request->session()->flash('success', 'User deleted successfully.');
		}
		return redirect('admin/users');
	}

	public function users_role_index(Request $request) {
		$li_active = "users_role";
		$pagination = 10;
		$appends = array();
		$query_string = '';
		$filter_users_role = $request->input('filter_users_role');
		$appends['filter_users_role'] = $filter_users_role;
		$query_string = 'filter_users_role='.$filter_users_role;

		$users_roles = UsersRole::when($filter_users_role, function ($query, $filter_users_role) {
			return $query->where('name', 'like','%'.$filter_users_role.'%')->orWhere('des', 'like','%'.$filter_users_role.'%');
		})->paginate($pagination);
		return view('admin.users.users_role_list',compact('users_roles','filter_users_role','appends','query_string','li_active'));
	}
	
	public function users_role_create() {
		$li_active = "users_role";
		return view('admin.users.users_role_create',compact('li_active'));
	}

	public function users_role_store(Request $request) {
		$validatedData = $request->validate([
			'role' => 'required|min:2|max:30',
			'sort_order'=>'numeric'
		]);
		if($validatedData) {
			$data = $request->all();
			$usersRole = new UsersRole;
			if($usersRole->create($data)){
				$request->session()->flash('success', 'News Category  created successfully.');
				return redirect('/admin/users_role');
			}
			else{
				$request->session()->flash('error', 'Something went wrong.');
				return back()->withInput();
			}

		}
	}

	public function users_role_edit($id) {
		$li_active = "users_role";
		$users_role = UsersRole::find($id);
		if($users_role) {
			return view('admin.users.users_role_edit',compact('id','users_role','li_active'));
		}
	}
	public function users_role_update(Request $request, $id) {
		$validatedData = $request->validate([
			'role' => 'required|min:2|max:30',
			'sort_order'=>'numeric'
		]);
		if($validatedData){
			$data = $request->all();
			$data['publish'] = $request->input('publish');
			$users_role = UsersRole::find($id);
			if($users_role->update($data)){
				$request->session()->flash('success', 'News Category updated successfully.');
				return redirect('admin/users_role');
			}
			else{
				$request->session()->flash('error', 'Something went wrong while updating users role.');
				return back()->withInput();
			}
		}
	}
	public function users_role_destroy(Request $request , $id) {
		$users_role = UsersRole::find($id);
		if($users_role) {
			$users_role->delete();
			$request->session()->flash('success', 'News Category deleted successfully.');
			return redirect('admin/users_role');
		}
		else{
			$request->session()->flash('error', 'Something went wrong while deleting users role.');
			return back()->withInput();
		}
	}
	public function users_role_delete(Request $request) {
		$users_ids = $request->input('selected');
		if($users_ids){
			foreach($users_ids as $user_id) {
				$users_role = UsersRole::find($user_id);
				if($users_role) {
					$users_role->delete();
				}
				else{
				}
			}
			$request->session()->flash('success', 'News Category deleted successfully.');
		}
		return redirect('admin/users_role');
	}
	
}
