<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EventsCat;
use App\Event;
use Carbon\Carbon;

class EventController extends Controller
{
  public function __construct() {
    view()->share('active','events');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $li_active = "events_manage";
      $appends = array();
      $query_string = '';
      $events_cats = EventsCat::pluck('name','id');

      $filter_events = $request->input('filter_events');
      $appends['filter_events'] = $filter_events;
      $query_string = 'filter_events='.$filter_events;

      $filter_title = $request->input('filter_title');
      $appends['filter_title'] = $filter_title;
      $query_string = 'filter_title='.$filter_title;

      $filter_cat = $request->input('filter_cat');
      $appends['filter_cat'] = $filter_cat;
      $query_string = '&filter_cat='.$filter_cat;

      $filter_end_time = $request->input('filter_end_time');
      $appends['filter_end_time'] = $filter_end_time;
      $query_string = '&filter_end_time='.$filter_end_time;

      $filter_publish_time = $request->input('filter_publish_time');
      $appends['filter_publish_time'] = $filter_publish_time;
      $query_string = '&filter_publish_time='.$filter_publish_time;

      $orderBy = $request->input('orderBy','updated_at');
      $sort = $request->input('sort','desc');

      $paginate = 10;
      $events = Event::when($filter_title, function ($query, $filter_title) {
        return $query->where('title', 'like','%'.$filter_title.'%');
      })
      ->when($filter_cat, function ($query, $filter_cat) {
        return $query->where('news_cat_id',$filter_cat);
      })
      ->when($filter_end_time, function ($query, $filter_end_time) {
        return $query->where('end_time', '>', (Carbon::parse($filter_end_time)));
      }) ->when($filter_publish_time, function ($query, $filter_publish_time) {
        return $query->where('publish_time', '<',(Carbon::parse($filter_publish_time)));
      })->when($filter_events, function ($query, $filter_events) {
        return $query->where('title', 'like','%'.$filter_events.'%')->orWhere('description', 'like','%'.$filter_events.'%');
      })->when($orderBy, function ($query, $orderBy)  use($sort) {
        if($orderBy == "category") {
          return $query->join('events_cats', 'events_cats.id', '=', 'events.events_cat_id')
          ->orderBy('events_cats.name',$sort);
        }
        return $query->orderBy($orderBy,$sort);
      })->paginate($paginate);

      $query =  http_build_query($request->except(['orderBy','sort']));

      return view('admin.events.list',compact('events','filter_cat','filter_title','filter_end_time','filter_publish_time','appends','query_string','events_cats','orderBy','sort','filter_events','query','li_active'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $li_active = "add_event";
      $events_cats = EventsCat::pluck('name','id');
      return view('admin.events.create',compact('events_cats','li_active'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     $imagename = '';
     $validatedData = $request->validate([
       'title' => 'required|min:2|max:30',
       'image'=>'required|image'
     ]);
     if($validatedData){
       if($request->hasFile('image')){
         $image = $request->file('image');
         $destinationPath = public_path('/uploads');
         $imagename = time().'.'.$image->getClientOriginalExtension();
         if($image->move($destinationPath, $imagename)){

         }
         else{
           $request->session()->flash('error', 'Something went wrong while uploading file.');
           return back()->withInput();
         }
       }
       $data = $request->all();
       $data['image']= $imagename;
       $event = new Event;
       if($event->create($data)){
         $request->session()->flash('success', 'Event  created successfully.');
         return redirect('/admin/events');
       }
       else{
         $request->session()->flash('error', 'Something went wrong.');
         return back()->withInput();
       }

     }
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $li_active = "events_manage";
      $event = Event::find($id);
      $events_cats = EventsCat::pluck('name','id');
      return view('admin.events.edit',compact('event','id','events_cats','li_active'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $imagename = '';
      $validatedData = $request->validate([
        'title' => 'required|min:2|max:30',
        'image'=>'image',
      ]);
      if($validatedData){
       if($request->hasFile('image')){
         $image = $request->file('image');
         $destinationPath = public_path('/uploads');
         $imagename = time().'.'.$image->getClientOriginalExtension();
         if($image->move($destinationPath, $imagename)){

         }
         else{
           $request->session()->flash('error', 'Something went wrong while uploading file.');
           return back()->withInput();
         }
       }

       $data = $request->all();
       if($request->hasFile('image')){
         $data['image']= $imagename;
       }
       $data['publish'] = $request->input('publish');
       $event = Event::find($id);
       if($event->update($data)){
        $request->session()->flash('success', 'Event updated successfully.');
        return redirect('admin/events');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating station.');
        return back()->withInput();
      }

    } 
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $event = Event::find($id);
      if($event) {
        $event->delete();
        $request->session()->flash('success', 'Event deleted successfully.');
        return redirect('admin/event');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating station.');
        return back()->withInput();
      }
    }

    public function events_delete(Request $request) {
      $ids = $request->input('selected');
      if($ids){
        foreach($ids as $id) {
          $event = Event::find($id);
          if($event) {
            $event->delete();
          }
          else{
          }

        }
        $request->session()->flash('success', 'Event deleted successfully.');
      }
      return redirect('admin/events');
    }

    public function events_cat_index(Request $request) {
      $li_active = "events_cat";
      $pagination = 10;
      $appends = array();
      $query_string = '';
      $filter_events_cat = $request->input('filter_events_cat');
      $appends['filter_events_cat'] = $filter_events_cat;
      $query_string = 'filter_events_cat='.$filter_events_cat;

      $events_cats = EventsCat::when($filter_events_cat, function ($query, $filter_events_cat) {
        return $query->where('name', 'like','%'.$filter_events_cat.'%')->orWhere('des', 'like','%'.$filter_events_cat.'%');
      })->paginate($pagination);
      return view('admin.events.events_cat_list',compact('events_cats','filter_events_cat','appends','query_string','li_active'));
    }

    public function events_cat_create() {
     $li_active = "events_cat";
      return view('admin.events.events_cat_create',compact('li_active'));
    }

    public function events_cat_store(Request $request) {
      $validatedData = $request->validate([
        'name' => 'required|min:2|unique:events_cats',
        'sort_order'=>'numeric'
      ]);
      if($validatedData){
        $data = $request->all();
        $eventsCat = new EventsCat;
        if($eventsCat->create($data)){
          $request->session()->flash('success', 'events Category  created successfully.');
          return redirect('/admin/events_cat');
        }
        else{
          $request->session()->flash('error', 'Something went wrong.');
          return back()->withInput();
        }

      }
    }

    public function events_cat_edit($id) {
      $li_active = "events_cat";
      $events_cat = EventsCat::find($id);
      if($events_cat) {
        return view('admin.events.events_cat_edit',compact('id','events_cat','li_active'));
      }
    }
    public function events_cat_update(Request $request, $id) {
      $imagename = '';
      $validatedData = $request->validate([
        'name' => 'required|min:2|unique:events_cats,id,'.$id,
        'sort_order'=>'numeric'
      ]);
      if($validatedData){
        $data = $request->all();
        $data['publish'] = $request->input('publish');
        $events_cat = EventsCat::find($id);
        if($events_cat->update($data)){
          $request->session()->flash('success', 'events Category updated successfully.');
          return redirect('admin/events_cat');
        }
        else{
          $request->session()->flash('error', 'Something went wrong while updating events category.');
          return back()->withInput();
        }

      }
    }
    public function events_cat_destroy(Request $request , $id) {
      $events_cat = EventsCat::find($id);
      if($events_cat) {
        $events_cat->delete();
        $request->session()->flash('success', 'events Category deleted successfully.');
        return redirect('admin/events_cat');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while deleting events category.');
        return back()->withInput();
      }
    }
    public function events_cat_delete(Request $request) {
      $events_ids = $request->input('selected');
      if($events_ids){
        foreach($events_ids as $new_id) {
          $events_cat = EventsCat::find($new_id);
          if($events_cat) {
            $events_cat->delete();

          }
          else{
          }

        }
        $request->session()->flash('success', 'events Category deleted successfully.');
      }
      return redirect('admin/events_cat');

    }
  }
