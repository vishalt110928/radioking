<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ads;

class AdsController extends Controller
{
	public function __construct() {
	  view()->share('active','ads');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$li_active = "ads_manage";
		$appends = array();
		$query_string = '';

		$filter_title = $request->input('filter_title');
		$appends['filter_title'] = $filter_title;
		$query_string = 'filter_title='.$filter_title;

		$filter_camp_start = $request->input('filter_camp_start');
		$appends['filter_camp_start'] = $filter_camp_start;
		$query_string = '&filter_camp_start='.$filter_camp_start;

		$filter_camp_end = $request->input('filter_camp_end');
		$appends['filter_camp_end'] = $filter_camp_end;
		$query_string = '&filter_camp_end='.$filter_camp_end;

		$orderBy = $request->input('orderBy','updated_at');
		$sort = $request->input('sort','desc');

		$paginate = 10;
		$ads = Ads::when($filter_title, function ($query, $filter_title) {
			return $query->where('title', 'like','%'.$filter_title.'%');
		})->when($filter_camp_end, function ($query, $filter_camp_end) {
			return $query->where('campaign_end_date', '>',(Carbon::parse($filter_camp_end)));
		}) ->when($filter_camp_start, function ($query, $filter_camp_start) {
			return $query->where('campaign_start_date', '<',(Carbon::parse($filter_camp_start)));
		})->when($orderBy, function ($query, $orderBy)  use($sort) {
      return $query->orderBy($orderBy,$sort);
    })
		->paginate($paginate);
		$query =  http_build_query($request->except(['orderBy','sort']));
		return view('admin.ads.list',compact('ads','filter_cat','filter_title','filter_camp_end','filter_camp_start','appends','query_string','query','orderBy','sort','li_active'));

	}

	/**
	 * Show the form for creating a ad resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$li_active = "add_ads";
		return view('admin.ads.create',compact('li_active'));
	}

	/**
	 * Store a adly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$imagename = '';
		$validatedData = $request->validate([
			'title' => 'required|min:2|max:30',
			'image'=>'required|image'
		]);
		if($validatedData){
			$data = $request->all();
			if($request->hasFile('image')){
				$image = $request->file('image');
				$destinationPath = public_path('/uploads');
				$imagename = time().'.'.$image->getClientOriginalExtension();
				if($image->move($destinationPath, $imagename)){
					if(isset($data['js_code'])) {
						unset($data['js_code']);
					}
								$data['image']= $imagename;
				}
				else{
					$request->session()->flash('error', 'Something went wrong while uploading file.');
					return back()->withInput();
				}
			}
			$ads = new Ads;
			if($ads->create($data)){
				$request->session()->flash('success', 'Ads  created successfully.');
				return redirect('/admin/ads');
			}
			else{
				$request->session()->flash('error', 'Something went wrong.');
				return back()->withInput();
			}

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
	    //
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$li_active = "ads_manage";
		$ad = Ads::find($id);
		return view('admin.ads.edit',compact('ad','id','li_active'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$imagename = '';
		$validatedData = $request->validate([
			'title' => 'required|min:2|max:30',
			'image'=>'image',
		]);
		if($validatedData){
			$data = $request->all();
			if($request->hasFile('image')){
				$image = $request->file('image');
				$destinationPath = public_path('/uploads');
				$imagename = time().'.'.$image->getClientOriginalExtension();
				if($image->move($destinationPath, $imagename)){
					if(isset($data['js_code'])) {
						unset($data['js_code']);
					}
				}
				else{
					$request->session()->flash('error', 'Something went wrong while uploading file.');
					return back()->withInput();
				}
			}
			$data['publish'] = $request->input('publish');
			$data['display_block'] = $request->input('display_block');
			$data['display_top'] = $request->input('display_top');
			if($request->hasFile('image')){
				$data['image']= $imagename;
			}else{
				if(isset($data['js_code'])) {
					$data['image'] = "";
				}
			}
			$ads = Ads::find($id);
			if($ads->update($data)){
				$request->session()->flash('success', 'Ads updated successfully.');
				return redirect('admin/ads');
			}
			else{
				$request->session()->flash('error', 'Something went wrong while updating station.');
				return back()->withInput();
			}

		} 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request , $id)
	{
		$ads = Ads::find($id);
		if($ads) {
			$ads->delete();
			$request->session()->flash('success', 'Ads deleted successfully.');
			return redirect('admin/ad');
		}
		else{
			$request->session()->flash('error', 'Something went wrong while updating station.');
			return back()->withInput();
		}
	}
	public function ads_delete(Request $request) {
		$ids = $request->input('selected');
		if($ids){
			foreach($ids as $id) {
				$ads = Ads::find($id);
				if($ads) {
					$ads->delete();
				}
				else{
				}

			}
			$request->session()->flash('success', 'Ads deleted successfully.');
		}
		return redirect('admin/ads');
	}
}
