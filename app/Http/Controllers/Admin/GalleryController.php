<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;
use App\GallerysCat;
use App\Photo;
use App\GalleryPhoto;
use DB;

class GalleryController extends Controller
{
	public function __construct()
	{
		view()->share('active', 'gallerys');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$li_active = "gallerys_manage";
		$appends = array();
		$query_string = '';
		$gallerys_cats = GallerysCat::pluck('name', 'id');

		$filter_title = $request->input('filter_title');
		$appends['filter_title'] = $filter_title;
		$query_string = 'filter_title=' . $filter_title;

		$filter_cat = $request->input('filter_cat');
		$appends['filter_cat'] = $filter_cat;
		$query_string = '&filter_cat=' . $filter_cat;

		$filter_end_time = $request->input('filter_end_time');
		$appends['filter_end_time'] = $filter_end_time;
		$query_string = '&filter_end_time=' . $filter_end_time;

		$filter_publish_time = $request->input('filter_publish_time');
		$appends['filter_publish_time'] = $filter_publish_time;
		$query_string = '&filter_publish_time=' . $filter_publish_time;

		$orderBy = $request->input('orderBy', 'updated_at');
		$sort = $request->input('sort', 'desc');

		$paginate = 10;
		$gallerys = Gallery::when($filter_title, function ($query, $filter_title) {
			return $query->where('title', 'like', '%' . $filter_title . '%');
		})
			->when($filter_cat, function ($query, $filter_cat) {
				return $query->where('gallerys_cat_id', $filter_cat);
			})
			->when($filter_end_time, function ($query, $filter_end_time) {
				return $query->where('end_time', '>', '%' . $filter_end_time . '%');
			})->when($filter_publish_time, function ($query, $filter_publish_time) {
				return $query->where('publish_time', '<', '%' . $filter_publish_time . '%');
			})->when($orderBy, function ($query, $orderBy)  use ($sort) {
				if ($orderBy == "category") {
					return $query->join('gallerys_cats', 'gallerys_cats.id', '=', 'gallerys.gallerys_cat_id')
						->orderBy('gallerys_cats.name', $sort);
				}
				return $query->orderBy($orderBy, $sort);
			})->paginate($paginate);
		$query =  http_build_query($request->except(['orderBy', 'sort']));
		return view('admin.gallerys.list', compact('gallerys', 'filter_cat', 'filter_title', 'filter_end_time', 'filter_publish_time', 'appends', 'query_string', 'gallerys_cats', 'query', 'orderBy', 'sort', 'li_active'));
	}

	/**
	 * Show the form for creating a gallery resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$li_active = "gallerys_manage";
		$gallerys_cats = GallerysCat::pluck('name', 'id');
		return view('admin.gallerys.create', compact('gallerys_cats', 'li_active'));
	}

	/**
	 * Store a galleryly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validatedData = $request->validate([
			'title' => 'required|min:2|max:30'
		]);
		if ($validatedData) {
			$data = $request->all();
			$gallerys = new Gallery;
			if ($gallerys->create($data)) {
				$request->session()->flash('success', 'Gallery  created successfully.');
				return redirect('/admin/gallerys');
			} else {
				$request->session()->flash('error', 'Something went wrong.');
				return back()->withInput();
			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$li_active = "gallerys_manage";
		$gallerys = Gallery::with('photos')->find($id);
		$gallerys_cats = GallerysCat::pluck('name', 'id');
		return view('admin.gallerys.edit', compact('gallerys', 'id', 'gallerys_cats', 'li_active'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$imagename = '';
		$validatedData = $request->validate([
			'title' => 'required|min:2|max:30'
		]);
		if ($validatedData) {
			$data = $request->all();
			$data['publish'] = $request->input('publish');
			$gallerys = Gallery::find($id);
			if ($gallerys->update($data)) {
				$request->session()->flash('success', 'Gallery updated successfully.');
				return redirect('admin/gallerys');
			} else {
				$request->session()->flash('error', 'Something went wrong while updating station.');
				return back()->withInput();
			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id)
	{
		$gallerys = Gallery::find($id);
		if ($gallerys) {
			$gallerys->delete();
			$request->session()->flash('success', 'Gallery deleted successfully.');
			return redirect('admin/gallery');
		} else {
			$request->session()->flash('error', 'Something went wrong while updating station.');
			return back()->withInput();
		}
	}
	public function gallerys_delete(Request $request)
	{
		$ids = $request->input('selected');
		if ($ids) {
			foreach ($ids as $id) {
				$gallerys = Gallery::find($id);
				if ($gallerys) {
					$gallerys->delete();
				} else { }
			}
			$request->session()->flash('success', 'Gallerys deleted successfully.');
		}
		return redirect('admin/gallerys');
	}


	public function gallerys_cat_index(Request $request)
	{
		$li_active = "gallerys_cat";
		$pagination = 10;
		$appends = array();
		$query_string = '';
		$filter_gallerys_cat = $request->input('filter_gallerys_cat');
		$appends['filter_gallerys_cat'] = $filter_gallerys_cat;
		$query_string = 'filter_gallerys_cat=' . $filter_gallerys_cat;

		$gallerys_cats = GallerysCat::when($filter_gallerys_cat, function ($query, $filter_gallerys_cat) {
			return $query->where('name', 'like', '%' . $filter_gallerys_cat . '%')->orWhere('des', 'like', '%' . $filter_gallerys_cat . '%');
		})->paginate($pagination);
		return view('admin.gallerys.gallerys_cat_list', compact('gallerys_cats', 'filter_gallerys_cat', 'appends', 'query_string', 'li_active'));
	}

	public function gallerys_cat_create()
	{
		$li_active = "gallerys_cat";
		return view('admin.gallerys.gallerys_cat_create', compact('li_active'));
	}

	public function gallerys_cat_store(Request $request)
	{
		$imagename = '';
		$validatedData = $request->validate([
			'name' => 'required|min:2|max:30',
			'sort_order' => 'numeric'
		]);
		if ($validatedData) {
			$data = $request->all();
			$gallerysCat = new GallerysCat;
			if ($gallerysCat->create($data)) {
				$request->session()->flash('success', 'Gallerys Category  created successfully.');
				return redirect('/admin/gallerys_cat');
			} else {
				$request->session()->flash('error', 'Something went wrong.');
				return back()->withInput();
			}
		}
	}

	public function gallerys_cat_edit($id)
	{
		$li_active = "gallerys_cat";
		$gallerys_cat = GallerysCat::find($id);
		if ($gallerys_cat) {
			return view('admin.gallerys.gallerys_cat_edit', compact('id', 'gallerys_cat', 'li_active'));
		}
	}
	public function gallerys_cat_update(Request $request, $id)
	{
		$imagename = '';
		$validatedData = $request->validate([
			'name' => 'required|min:2|max:30',
		]);
		if ($validatedData) {
			$data = $request->all();
			$data['publish'] = $request->input('publish');
			$gallerys_cat = GallerysCat::find($id);
			if ($gallerys_cat->update($data)) {
				$request->session()->flash('success', 'Gallerys Category updated successfully.');
				return redirect('admin/gallerys_cat');
			} else {
				$request->session()->flash('error', 'Something went wrong while updating gallerys category.');
				return back()->withInput();
			}
		}
	}
	public function gallerys_cat_destroy(Request $request, $id)
	{
		$gallerys_cat = GallerysCat::find($id);
		if ($gallerys_cat) {
			$gallerys_cat->delete();
			$request->session()->flash('success', 'Gallerys Category deleted successfully.');
			return redirect('admin/gallerys_cat');
		} else {
			$request->session()->flash('error', 'Something went wrong while deleting gallerys category.');
			return back()->withInput();
		}
	}
	public function gallerys_cat_delete(Request $request)
	{
		$gallerys_ids = $request->input('selected');
		if ($gallerys_ids) {
			foreach ($gallerys_ids as $gallery_id) {
				$gallerys_cat = GallerysCat::find($gallery_id);
				if ($gallerys_cat) {
					$gallerys_cat->delete();
				} else { }
			}
			$request->session()->flash('success', 'Gallerys Category deleted successfully.');
		}
		return redirect('admin/gallerys_cat');
	}

	public function gallerys_photos(Request $request)
	{
		$gallerys = Gallery::where('publish', 1)->pluck('title', 'id');
		return view('admin.gallerys.photos', compact('gallerys'));
	}

	public function gallerys_photos_store(Request $request)
	{
		$validatedData = $request->validate([
			'gallery_id' => 'required',
			'photos.*' => 'mimes:jpeg,jpg,png',
		]);
		$photos = $request->file('photos');
		foreach($photos as $photo) {
			$destinationPath = public_path('/uploads');
			$type = $photo->getClientOriginalExtension();
			$imagename = time().'.'.$photo->getClientOriginalExtension();
			if($photo->move($destinationPath, $imagename)){
				$photo = new Photo;
				$data['type'] = $type;
				$data['path'] = $imagename;
				$photo->create($data);
				$gallery_photo = new GalleryPhoto;
				$gallery['gallery_id'] = $request->input('gallery_id');
				$gallery['photo_id'] =  DB::getPDO()->lastInsertId();
				$gallery_photo->create($gallery);
			}
		}
		return redirect('admin/photos');
	}

	public function photo_delete(Request $request , $gallery_id = "", $photo_id = ""){
		$response = array();
		$photo  = Photo::find($photo_id);
		if($photo) {
			$photo->delete();
			DB::table("gallery_photos")->where('gallery_id',$gallery_id)->where('photo_id',$photo_id)->delete();
		}
		if($request->ajax()) {
			$response['status'] = "success";
			$response['message'] = "Successfully deleted selected photo.";
			return response()->json($response);
		}
		else{
			return redirect('admin/gallerys/'.$gallery_id.'/edit');
		}
	}
}
