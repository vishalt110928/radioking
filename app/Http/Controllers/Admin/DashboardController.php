<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notification;
use Carbon\Carbon;
use DB;
use App\Slider;
use App\News;
use App\Track;
use	App\Podcast;
use App\Playlist;
use App\Video;
use	App\Contest;
use App\Event;
use App\Artist;
use App\Team;
use App\Dedication;
use App\Page;
use App\Program;
use Illuminate\Support\Facades\View;
use App\Contact_us;
use App\User;
use App\Photo;
use App\Player;
use Auth;

class DashboardController extends Controller
{
	public function index() {
		$now = Carbon::now();
		$sliders = Slider::where('publish', 1)->where('start_date', '<=', $now)->where('end_date', '>=', $now)->count();
		$news = News::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->count();
		$tracks = Track::where('publish', 1)->where('publish_time', '<=', $now)->count();
		$podcasts = Podcast::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->count();
		$playlists = Playlist::where('publish', 1)->count();
		$programs = Program::where('publish', 1)->where('publish_date', '<=', $now)->where('end_date', '>=', $now)->count();
		$videos = Video::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->count();
		$contests = Contest::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->count();
		$events = Event::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->count();
		$artists = Artist::where('publish', 1)->where('publish_time', '<', $now)->count();
		$players  = Player::where('publish', 1)->where('publish_time', '<', $now)->count();
		return view('admin.dashboard.dashboard',compact('sliders', 'news', 'tracks', 'podcasts', 'playlists', 'programs', 'videos', 'contests', 'events', 'artists','players'));
	}

	public function viewed_all() {
		DB::table('notifications')->update(['viewed'=>'1']);
		return redirect('admin/dashboard');
	}
}
