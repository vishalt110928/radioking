<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\NewsCat;
use App\News;
use Carbon\Carbon;

class NewsCOntroller extends Controller
{

  public function __construct() {
    view()->share('active','news');
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $li_active = "news_manage";
    $appends = array();
    $query_string = '';
    $news_cats = NewsCat::pluck('name','id');

    $filter_news = $request->input('filter_news');
    $appends['filter_news'] = $filter_news;
    $query_string = 'filter_news='.$filter_news;

    $filter_title = $request->input('filter_title');
    $appends['filter_title'] = $filter_title;
    $query_string = 'filter_title='.$filter_title;

    $filter_cat = $request->input('filter_cat');
    $appends['filter_cat'] = $filter_cat;
    $query_string = '&filter_cat='.$filter_cat;

    $filter_end_time = $request->input('filter_end_time');
    $appends['filter_end_time'] = $filter_end_time;
    $query_string = '&filter_end_time='.$filter_end_time;

    $filter_publish_time = $request->input('filter_publish_time');
    $appends['filter_publish_time'] = $filter_publish_time;
    $query_string = '&filter_publish_time='.$filter_publish_time;

    $orderBy = $request->input('orderBy','updated_at');
    $sort = $request->input('sort','desc');

    $paginate = 10;
    $news = News::when($filter_title, function ($query, $filter_title) {
      return $query->where('title', 'like','%'.$filter_title.'%');
    })
    ->when($filter_cat, function ($query, $filter_cat) {
      return $query->where('news_cat_id',$filter_cat);
    })
    ->when($filter_end_time, function ($query, $filter_end_time) {
      return $query->where('end_time', '>',(Carbon::parse($filter_end_time)));
    }) ->when($filter_publish_time, function ($query, $filter_publish_time) {
      return $query->where('publish_time', '<',(Carbon::parse($filter_publish_time)));
    })->when($filter_news, function ($query, $filter_news) {
      return $query->where('title', 'like','%'.$filter_news.'%')->orWhere('description', 'like','%'.$filter_news.'%');
    })->when($orderBy, function ($query, $orderBy)  use($sort) {
      if($orderBy == "category") {
        return $query->join('news_cats', 'news_cats.id', '=', 'news.news_cat_id')
        ->orderBy('news_cats.name',$sort);
      }
      return $query->orderBy($orderBy,$sort);
    })->paginate($paginate);
    $query =  http_build_query($request->except(['orderBy','sort']));
    return view('admin.news.list',compact('news','filter_cat','filter_title','filter_end_time','filter_publish_time','appends','query_string','news_cats','orderBy','sort','query','filter_news','li_active'));

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $li_active = "add_news";
    $news_cats = NewsCat::pluck('name','id');
    return view('admin.news.create',compact('news_cats','li_active'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $imagename = '';
    $validatedData = $request->validate([
      'title' => 'required|string|min:1|max:100|unique:news',
      'image'=>'required|image',

    ]);
    if($validatedData){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }
      $data = $request->all();
      $data['image']= $imagename;
      $news = new News;
      if($news->create($data)){
        $request->session()->flash('success', 'News  created successfully.');
        return redirect('/admin/news');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }

    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $li_active = "news_manage";
    $news = News::find($id);
    $news_cats = NewsCat::pluck('name','id');
    return view('admin.news.edit',compact('news','id','news_cats','li_active'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $imagename = '';
    if(!isset($request->publish)) {
      $request->publish = 0;
    }
    $validatedData = $request->validate([
      'title' => 'required|string|min:1|max:100|unique:news,id,'.$id,
      'image'=>'image',

    ]);
    if($validatedData){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }

      $data = $request->all();
      $data['publish'] = $request->input('publish');
      if($request->hasFile('image')){
        $data['image']= $imagename;
      }
      $news = News::find($id);
      if($news->update($data)){
        $request->session()->flash('success', 'News updated successfully.');
        return redirect('admin/news');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating station.');
        return back()->withInput();
      }

    } 
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request , $id)
  {
    $news = News::find($id);
    if($news) {
      $news->delete();
      $request->session()->flash('success', 'News deleted successfully.');
      return redirect('admin/new');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while updating station.');
      return back()->withInput();
    }
  }
  public function news_delete(Request $request) {
    $ids = $request->input('selected');
    if($ids){
      foreach($ids as $id) {
        $news = News::find($id);
        if($news) {
          $news->delete();
        }
        else{
        }

      }
      $request->session()->flash('success', 'News deleted successfully.');
    }
    return redirect('admin/news');
  }
  

  public function news_cat_index(Request $request) {
    $li_active = "news_cat";
  	$pagination = 10;
    $appends = array();
    $query_string = '';
    $filter_news_cat = $request->input('filter_news_cat');
    $appends['filter_news_cat'] = $filter_news_cat;
    $query_string = 'filter_news_cat='.$filter_news_cat;

    $news_cats = NewsCat::when($filter_news_cat, function ($query, $filter_news_cat) {
      return $query->where('name', 'like','%'.$filter_news_cat.'%')->orWhere('des', 'like','%'.$filter_news_cat.'%');
    })->paginate($pagination);
    return view('admin.news.news_cat_list',compact('news_cats','filter_news_cat','appends','query_string','li_active'));
  }
  
  public function news_cat_create() {
    $li_active = "news_cat";
  	return view('admin.news.news_cat_create',compact('li_active'));
  }

  public function news_cat_store(Request $request) {
  	$validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
      'sort_order'=>'numeric'
    ]);
  	if($validatedData) {
      $data = $request->all();
      $newsCat = new NewsCat;
      if($newsCat->create($data)){
        $request->session()->flash('success', 'News Category  created successfully.');
        return redirect('/admin/news_cat');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }

    }
  }

  public function news_cat_edit($id) {
    $li_active = "news_cat";
    $news_cat = NewsCat::find($id);
    if($news_cat) {
      return view('admin.news.news_cat_edit',compact('id','news_cat','li_active'));
    }
  }
  public function news_cat_update(Request $request, $id) {
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
      'sort_order'=>'numeric'
    ]);
    if($validatedData){
      $data = $request->all();
      $data['publish'] = $request->input('publish');
      $news_cat = NewsCat::find($id);
      if($news_cat->update($data)){
        $request->session()->flash('success', 'News Category updated successfully.');
        return redirect('admin/news_cat');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating news category.');
        return back()->withInput();
      }

    }
  }
  public function news_cat_destroy(Request $request , $id) {
    $news_cat = NewsCat::find($id);
    if($news_cat) {
      $news_cat->delete();
      $request->session()->flash('success', 'News Category deleted successfully.');
      return redirect('admin/news_cat');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while deleting news category.');
      return back()->withInput();
    }
  }
  public function news_cat_delete(Request $request) {
    $news_ids = $request->input('selected');
    if($news_ids){
      foreach($news_ids as $new_id) {
        $news_cat = NewsCat::find($new_id);
        if($news_cat) {
          $news_cat->delete();

        }
        else{
        }

      }
      $request->session()->flash('success', 'News Category deleted successfully.');
    }
    return redirect('admin/news_cat');

  }

}
