<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Artist;
use App\ArtistsCat;
use \Carbon\Carbon;

class ArtistController extends Controller
{
  public function __construct() {
    view()->share('active','artists');
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $li_active = "artists_manage";
    $appends = array();
    $query_string = '';
    $artists_cats = ArtistsCat::pluck('name','id');

    $filter_artists = $request->input('filter_artists');
    $appends['filter_artists'] = $filter_artists;
    $query_string = 'filter_artists='.$filter_artists;

    $filter_name = $request->input('filter_name');
    $appends['filter_name'] = $filter_name;
    $query_string = 'filter_name='.$filter_name;

    $filter_cat = $request->input('filter_cat');
    $appends['filter_cat'] = $filter_cat;
    $query_string = '&filter_cat='.$filter_cat;

    $filter_end_time = $request->input('filter_end_time');
    $appends['filter_end_time'] = $filter_end_time;
    $query_string = '&filter_end_time='.$filter_end_time;

    $filter_publish_time = $request->input('filter_publish_time');
    $appends['filter_publish_time'] = $filter_publish_time;
    $query_string = '&filter_publish_time='.$filter_publish_time;

    $orderBy = $request->input('orderBy','updated_at');
    $sort = $request->input('sort','desc');

    $paginate = 10;
    $artists = Artist::when($filter_name, function ($query, $filter_name) {
      return $query->where('name', 'like','%'.$filter_name.'%');
    })
    ->when($filter_cat, function ($query, $filter_cat) {
      return $query->where('artists_cat_id',$filter_cat);
    })
    ->when($filter_end_time, function ($query, $filter_end_time) {
      return $query->where('end_time', '>',(Carbon::parse($filter_end_time)));
    }) ->when($filter_publish_time, function ($query, $filter_publish_time) {
      return $query->where('publish_time', '<',(Carbon::parse($filter_publish_time)));
    })->when($filter_artists, function ($query, $filter_artists) {
      return $query->where('name', 'like','%'.$filter_artists.'%')->orWhere('biography', 'like','%'.$filter_artists.'%');
    })->when($orderBy, function ($query, $orderBy)  use($sort) {
      if($orderBy == "category") {
        return $query->join('artists_cats', 'artists_cats.id', '=', 'artists.artists_cat_id')
        ->orderBy('artists_cats.name',$sort);
      }
      return $query->orderBy($orderBy,$sort);
    })->paginate($paginate);
    $query =  http_build_query($request->except(['orderBy','sort']));
    return view('admin.artists.list',compact('artists','filter_cat','filter_name','filter_end_time','filter_publish_time','appends','query_string','artists_cats','filter_artists','query','orderBy','sort','li_active'));

  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $li_active = "add_artist";
    $artists_cats = ArtistsCat::pluck('name','id');
    return view('admin.artists.create',compact('artists_cats','li_active'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $imagename = '';
    $validatedData = $request->validate([
     'name' => 'required|min:2|max:30',
     'image'=>'required|image'
   ]);
    if($validatedData){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }
      $data = $request->all();
      $data['image']= $imagename;
      $artists = new Artist;
      if($artists->create($data)){
        $request->session()->flash('success', 'Artist  created successfully.');
        return redirect('/admin/artists');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }

    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
         //
  }

  /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function edit($id)
  {
    $li_active = "artists_manage";
    $artist = Artist::find($id);
    $artists_cats = ArtistsCat::pluck('name','id');
    return view('admin.artists.edit',compact('artist','id','artists_cats','li_active'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $imagename = '';
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
      'image'=>'image',
    ]);
    if($validatedData){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }

      $data = $request->all();
      $data['publish'] = $request->input('publish');
      if($request->hasFile('image')){
        $data['image']= $imagename;
      }
      $artists = Artist::find($id);
      if($artists->update($data)){
        $request->session()->flash('success', 'Artist updated successfully.');
        return redirect('admin/artists');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating station.');
        return back()->withInput();
      }

    } 
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy(Request $request , $id)
  {
    $artists = Artist::find($id);
    if($artists) {
      $artists->delete();
      $request->session()->flash('success', 'Artist deleted successfully.');
      return redirect('admin/new');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while updating station.');
      return back()->withInput();
    }
  }

  public function artists_delete(Request $request) {
    $ids = $request->input('selected');
    if($ids){
      foreach($ids as $id) {
        $artists = Artist::find($id);
        if($artists) {
          $artists->delete();
        }
        else{
        }

      }
      $request->session()->flash('success', 'Artist deleted successfully.');
    }
    return redirect('admin/artists');
  }


  public function artists_cat_index(Request $request) {
    $li_active = "artists_cat";
    $pagination = 10;
    $appends = array();
    $query_string = '';
    $filter_artists_cat = $request->input('filter_artists_cat');
    $appends['filter_artists_cat'] = $filter_artists_cat;
    $query_string = 'filter_artists_cat='.$filter_artists_cat;

    $artists_cats = ArtistsCat::when($filter_artists_cat, function ($query, $filter_artists_cat) {
      return $query->where('name', 'like','%'.$filter_artists_cat.'%')->orWhere('des', 'like','%'.$filter_artists_cat.'%');
    })->paginate($pagination);
    return view('admin.artists.artists_cat_list',compact('artists_cats','filter_artists_cat','appends','query_string','li_active
      '));
  }

  public function artists_cat_create() {
    $li_active = "artists_cat";
    return view('admin.artists.artists_cat_create',compact('li_active'));
  }

  public function artists_cat_store(Request $request) {
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
      'sort_order'=>'numeric'
    ]);
    if($validatedData){
      $data = $request->all();
      $artistsCat = new ArtistsCat;
      if($artistsCat->create($data)){
        $request->session()->flash('success', 'Artist Category  created successfully.');
        return redirect('/admin/artists_cat');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }

    }
  }

  public function artists_cat_edit($id) {
    $li_active = "artists_cat";
    $artists_cat = ArtistsCat::find($id);
    if($artists_cat) {
      return view('admin.artists.artists_cat_edit',compact('id','artists_cat','li_active'));
    }
  }
  public function artists_cat_update(Request $request, $id) {
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
    ]);
    if($validatedData){
      $data = $request->all();
      $data['publish'] = $request->input('publish');
      $artists_cat = ArtistsCat::find($id);
      if($artists_cat->update($data)){
        $request->session()->flash('success', 'Artist Category updated successfully.');
        return redirect('admin/artists_cat');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating artists category.');
        return back()->withInput();
      }

    }
  }
  public function artists_cat_destroy(Request $request , $id) {
    $artists_cat = ArtistsCat::find($id);
    if($artists_cat) {
      $artists_cat->delete();
      $request->session()->flash('success', 'Artist Category deleted successfully.');
      return redirect('admin/artists_cat');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while deleting artists category.');
      return back()->withInput();
    }
  }
  public function artists_cat_delete(Request $request) {
    $artists_ids = $request->input('selected');
    if($artists_ids){
      foreach($artists_ids as $new_id) {
        $artists_cat = ArtistsCat::find($new_id);
        if($artists_cat) {
          $artists_cat->delete();

        }
        else{
        }

      }
      $request->session()->flash('success', 'Artist Category deleted successfully.');
    }
    return redirect('admin/artists_cat');

  }
}
