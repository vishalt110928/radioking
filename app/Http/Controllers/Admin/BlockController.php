<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Block;
use App\Page;
use App\BlockPage;
use DB;


class BlockController extends Controller
{
	public function __construct() {
		view()->share('active','blocks');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$li_active = "blocks";
		$orderBy = $request->input('orderBy','updated_at');
		$sort = $request->input('sort','desc');
		$paginate = 10;
		$blocks = Block::when($orderBy, function ($query, $orderBy)  use($sort) {
			return $query->orderBy($orderBy,$sort);
		})->paginate($paginate);
		return view('admin.blocks.list',compact('blocks','orderBy','sort','li_active'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$li_active = "blocks";
		$pages = Page::pluck('title','id')->toArray();
		return view('admin.blocks.create',compact('pages','li_active'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$bl_pg = array();
		$validatedData = $request->validate([
			'title'=>'string'
		]);
		if($validatedData){
			$data = $request->all();
			$block = new Block;
			if($block->create($data)){
				$block_pages = $request->input('blocks');
				if(count($block_pages)) {
					$i = 0;
					foreach($block_pages as $block_page) {
						$bl_pg[$i]['sort_order'] = $block_page['sort_order'];
						$bl_pg[$i]['page_id'] = $block_page['page'];
						$bl_pg[$i]['block_id'] = DB::getPdo()->lastInsertId();
						$i++;
					}
				}
				BlockPage::insert($bl_pg);
				$request->session()->flash('success', 'Block created successfully.');
				return redirect('/admin/blocks');
			} 
			else{
				$request->session()->flash('error', 'Something went wrong.');
				return back()->withInput();
			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
	    //
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$li_active = "blocks";
		$block = Block::find($id);
		$pages = Page::pluck('title','id')->toArray();
		return view('admin.blocks.edit',compact('pages','id','block','li_active'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$validatedData = $request->validate([
			'title' => 'string|min:2|max:30|unique:blocks,id,'.$id
		]);
		if($validatedData){
			$data = $request->all();
			$data['publish'] = $request->input('publish');
			$block = Block::find($id);
			if($block->update($data)){
				$block_pages = $request->input('blocks');
				if(count($block_pages)) {
					BlockPage::where('block_id',$id)->delete();
					$i = 0;
					foreach($block_pages as $block_page) {
						$bl_pg[$i]['sort_order'] = $block_page['sort_order'];
						$bl_pg[$i]['page_id'] = $block_page['page'];
						$bl_pg[$i]['block_id'] =$id;
						$i++;
					}
				}
				BlockPage::insert($bl_pg);
				$request->session()->flash('success', 'Blocks updated successfully.');
				return redirect('admin/blocks');
			}
			else{
				$request->session()->flash('error', 'Something went wrong while updating station.');
				return back()->withInput();
			}

		} 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request , $id)
	{
		$block = Block::find($id);
		if($block) {
			$block->delete();
			$request->session()->flash('success', 'Block deleted successfully.');
			return redirect('admin/new');
		}
		else{
			$request->session()->flash('error', 'Something went wrong while updating station.');
			return back()->withInput();
		}
	}
	public function blocks_delete(Request $request) {
		$ids = $request->input('selected');
		if($ids){
			foreach($ids as $id) {
				$block = Block::find($id);
				if($block) {
					$block->delete();
					BlockPage::where('block_id',$id)->delete();
				}
				else{
				}

			}
			$request->session()->flash('success', 'Block deleted successfully.');
		}
		return redirect('admin/blocks');
	}
}
