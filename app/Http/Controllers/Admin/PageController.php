<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PagesCat;
use App\Page;
use Carbon\Carbon;


class PageController extends Controller
{
  public function __construct() {
    view()->share('active','pages');
  }
  /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
  public function index(Request $request)
  {
    $li_active = "pages_manage";
    $appends = array();
    $query_string = '';
    $pages_cats = PagesCat::pluck('name','id');

    $filter_pages = $request->input('filter_pages');
    $appends['filter_pages'] = $filter_pages;
    $query_string = 'filter_pages='.$filter_pages;

    $filter_title = $request->input('filter_title');
    $appends['filter_title'] = $filter_title;
    $query_string = 'filter_title='.$filter_title;

    $filter_cat = $request->input('filter_cat');
    $appends['filter_cat'] = $filter_cat;
    $query_string = '&filter_cat='.$filter_cat;

    $filter_end_time = $request->input('filter_end_time');
    $appends['filter_end_time'] = $filter_end_time;
    $query_string = '&filter_end_time='.$filter_end_time;

    $filter_publish_time = $request->input('filter_publish_time');
    $appends['filter_publish_time'] = $filter_publish_time;
    $query_string = '&filter_publish_time='.$filter_publish_time;

    $orderBy = $request->input('orderBy','updated_at');
    $sort = $request->input('sort','desc');

    $paginate = 10;
    $pages = Page::when($filter_title, function ($query, $filter_title) {
      return $query->where('title', 'like','%'.$filter_title.'%');
    })
    ->when($filter_cat, function ($query, $filter_cat) {
      return $query->where('pages_cat_id',$filter_cat);
    })
    ->when($filter_end_time, function ($query, $filter_end_time) {
      return $query->where('end_time', '>',(Carbon::parse($filter_end_time)));
    }) ->when($filter_publish_time, function ($query, $filter_publish_time) {
      return $query->where('publish_time', '<',(Carbon::parse($filter_publish_time)));
    })->when($filter_pages, function ($query, $filter_pages) {
      return $query->where('title', 'like','%'.$filter_pages.'%')->orWhere('content', 'like','%'.$filter_pages.'%');
    })->when($orderBy, function ($query, $orderBy)  use($sort) {
      if($orderBy == "category") {
        return $query->join('pages_cats', 'pages_cats.id', '=', 'pages.pages_cat_id')
        ->orderBy('pages_cats.name',$sort);
      }
      return $query->orderBy($orderBy,$sort);
    })->paginate($paginate);
     $query =  http_build_query($request->except(['orderBy','sort']));
    return view('admin.pages.list',compact('pages','filter_cat','filter_title','filter_end_time','filter_publish_time','appends','query_string','pages_cats','li_active','orderBy','sort','query','filter_pages'));

  }

  /**
  * Show the form for creating a page resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $li_active = "add_page";
    $pages_cats = PagesCat::pluck('name','id');
    return view('admin.pages.create',compact('pages_cats','li_active'));
  }

  /**
    * Store a pagely created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
  public function store(Request $request)
  {
    $imagename = '';
    $validatedData = $request->validate([
      'title' => 'required|min:2|max:30',
    ]);
    if($validatedData){
      $data = $request->all();
      $data['image']= $imagename;
      $page =new Page;
      if($page->create($data)){
        $request->session()->flash('success', 'Pages  created successfully.');
        return redirect('/admin/pages');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }

    }
  }

  /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function show($id)
  {
       //
  }

  /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function edit($id)
  {
    $li_active = "pages_manage";
    $page = Page::find($id);
    $pages_cats = PagesCat::pluck('name','id');
    return view('admin.pages.edit',compact('page','id','pages_cats','li_active'));
  }

  /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function update(Request $request, $id)
  {
    $imagename = '';
    $validatedData = $request->validate([
      'title' => 'required|min:2|max:30',
    ]);
    if($validatedData){
      $data = $request->all();
      $data['publish'] = $request->input('publish');
      $pages = Page::find($id);
      if($pages->update($data)){
        $request->session()->flash('success', 'Pages updated successfully.');
        return redirect('admin/pages');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating station.');
        return back()->withInput();
      }

    } 
  }

  /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function destroy(Request $request , $id)
  {
    $pages = Pages::find($id);
    if($pages) {
      $pages->delete();
      $request->session()->flash('success', 'Pages deleted successfully.');
      return redirect('admin/page');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while updating station.');
      return back()->withInput();
    }
  }

  public function pages_delete(Request $request) {
    $ids = $request->input('selected');
    if($ids){
      foreach($ids as $id) {
        $pages = Page::find($id);
        if($pages) {
          $pages->delete();
        }
        else{
        }

      }
      $request->session()->flash('success', 'Pages deleted successfully.');
    }
    return redirect('admin/pages');
  }


  public function pages_cat_index(Request $request) {
    $li_active = "pages_cat";
    $pagination = 10;
    $appends = array();
    $query_string = '';
    $filter_pages_cat = $request->input('filter_pages_cat');
    $appends['filter_pages_cat'] = $filter_pages_cat;
    $query_string = 'filter_pages_cat='.$filter_pages_cat;

    $pages_cats = PagesCat::when($filter_pages_cat, function ($query, $filter_pages_cat) {
      return $query->where('name', 'like','%'.$filter_pages_cat.'%')->orWhere('des', 'like','%'.$filter_pages_cat.'%');
    })->paginate($pagination);
    return view('admin.pages.pages_cat_list',compact('pages_cats','filter_pages_cat','appends','query_string','li_active'));
  }

  public function pages_cat_create() {
    $li_active = "pages_cat";
    return view('admin.pages.pages_cat_create',compact('li_active'));
  }

  public function pages_cat_store(Request $request) {
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
      'sort_order'=>'numeric'
    ]);
    if($validatedData){
      $data = $request->all();
      $pagesCat = new PagesCat;
      if($pagesCat->create($data)){
        $request->session()->flash('success', 'Pages Category  created successfully.');
        return redirect('/admin/pages_cat');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }

    }
  }

  public function pages_cat_edit($id) {
    $li_active = "pages_cat";
    $pages_cat = PagesCat::find($id);
    if($pages_cat) {
      return view('admin.pages.pages_cat_edit',compact('id','pages_cat','li_active'));
    }
  }
  public function pages_cat_update(Request $request, $id) {
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
    ]);
    if($validatedData){
      $data = $request->all();
      $data['publish'] = $request->input('publish');
      $pages_cat = PagesCat::find($id);
      if($pages_cat->update($data)){
        $request->session()->flash('success', 'Pages Category updated successfully.');
        return redirect('admin/pages_cat');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating pages category.');
        return back()->withInput();
      }

    }
  }
  public function pages_cat_destroy(Request $request , $id) {
    $pages_cat = PagesCat::find($id);
    if($pages_cat) {
      $pages_cat->delete();
      $request->session()->flash('success', 'Pages Category deleted successfully.');
      return redirect('admin/pages_cat');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while deleting pages category.');
      return back()->withInput();
    }
  }

  public function pages_cat_delete(Request $request) {
    $pages_ids = $request->input('selected');
    if($pages_ids){
      foreach($pages_ids as $page_id) {
        $pages_cat = PagesCat::find($page_id);
        if($pages_cat) {
          $pages_cat->delete();

        }
        else{
        }

      }
      $request->session()->flash('success', 'Pages Category deleted successfully.');
    }
    return redirect('admin/pages_cat');

  }
}
