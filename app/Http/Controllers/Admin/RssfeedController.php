<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rssfeed;
use App\RssfeedsCat;

class RssfeedController extends Controller
{
	public function __construct() {
		view()->share('active','rssfeeds');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$li_active = "rssfeeds_manage";
		$appends = array();
		$query_string = '';
		$rssfeeds_cats = RssfeedsCat::pluck('name','id');

		$filter_feeds = $request->input('filter_feeds');
		$appends['filter_feeds'] = $filter_feeds;
		$query_string = 'filter_feeds='.$filter_feeds;


		$filter_title = $request->input('filter_title');
		$appends['filter_title'] = $filter_title;
		$query_string = 'filter_title='.$filter_title;

		$filter_cat = $request->input('filter_cat');
		$appends['filter_cat'] = $filter_cat;
		$query_string = '&filter_cat='.$filter_cat;

		$filter_end_time = $request->input('filter_end_time');
		$appends['filter_end_time'] = $filter_end_time;
		$query_string = '&filter_end_time='.$filter_end_time;

		$filter_publish_time = $request->input('filter_publish_time');
		$appends['filter_publish_time'] = $filter_publish_time;
		$query_string = '&filter_publish_time='.$filter_publish_time;

		$orderBy = $request->input('orderBy','updated_at');
		$sort = $request->input('sort','desc');
		$paginate = 10;
		$rssfeeds = Rssfeed::when($filter_title, function ($query, $filter_title) {
			return $query->where('title', 'like','%'.$filter_title.'%');
		})
		->when($filter_cat, function ($query, $filter_cat) {
			return $query->where('rssfeeds_cat_id',$filter_cat);
		})
		->when($filter_end_time, function ($query, $filter_end_time) {
			return $query->where('end_time', '>','%'.$filter_end_time.'%');
		}) ->when($filter_publish_time, function ($query, $filter_publish_time) {
			return $query->where('publish_time', '<','%'.$filter_publish_time.'%');
		})->when($filter_feeds, function ($query, $filter_feeds) {
      return $query->where('title', 'like','%'.$filter_feeds.'%')->orWhere('des', 'like','%'.$filter_feeds.'%');
    })->when($orderBy, function ($query, $orderBy)  use($sort) {
      if($orderBy == "category") {
        return $query->join('rssfeeds_cats', 'rssfeeds_cats.id', '=', 'rssfeeds.rssfeeds_cat_id')
        ->orderBy('rssfeeds_cats.name',$sort);
      }
      return $query->orderBy($orderBy,$sort);
    })->paginate($paginate);
		$query =  http_build_query($request->except(['orderBy','sort']));
		return view('admin.rssfeeds.list',compact('rssfeeds','filter_cat','filter_title','filter_end_time','filter_publish_time','appends','query_string','rssfeeds_cats','query','filter_feeds','orderBy','sort','li_active'));

	}

	/**
	 * Show the form for creating a rssfeed resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$li_active = "add_rssfeed";
		$rssfeeds_cats = RssfeedsCat::pluck('name','id');
		return view('admin.rssfeeds.create',compact('rssfeeds_cats','li_active'));
	}

	/**
	 * Store a rssfeedly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$imagename = '';
		$validatedData = $request->validate([
			'title' => 'required|min:2|max:30',
			'image'=>'required|image'
		]);
		if($validatedData){
			if($request->hasFile('image')){
				$image = $request->file('image');
				$destinationPath = public_path('/uploads');
				$imagename = time().'.'.$image->getClientOriginalExtension();
				if($image->move($destinationPath, $imagename)){

				}
				else{
					$request->session()->flash('error', 'Something went wrong while uploading file.');
					return back()->withInput();
				}
			}
			$data = $request->all();
			$data['image']= $imagename;
			$rssfeeds = new Rssfeed;
			if($rssfeeds->create($data)){
				$request->session()->flash('success', 'Rssfeeds  created successfully.');
				return redirect('/admin/rssfeeds');
			}
			else{
				$request->session()->flash('error', 'Something went wrong.');
				return back()->withInput();
			}

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
	    //
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$li_active = "rssfeeds_manage";
		$rssfeeds = Rssfeed::find($id);
		$rssfeeds_cats = RssfeedsCat::pluck('name','id');
		return view('admin.rssfeeds.edit',compact('rssfeeds','id','rssfeeds_cats','li_active'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$imagename = '';
		$validatedData = $request->validate([
			'title' => 'required|min:2|max:30',
			'image'=>'image',
		]);
		if($validatedData){
			if($request->hasFile('image')){
				$image = $request->file('image');
				$destinationPath = public_path('/uploads');
				$imagename = time().'.'.$image->getClientOriginalExtension();
				if($image->move($destinationPath, $imagename)){

				}
				else{
					$request->session()->flash('error', 'Something went wrong while uploading file.');
					return back()->withInput();
				}
			}

			$data = $request->all();
			$data['publish'] = $request->input('publish');
			if($request->hasFile('image')){
				$data['image']= $imagename;
	        }/*
	        else{
	            $data['image']= $data['previousimage'];
	            unset($data['previousimage']);
	          }*/
	          $rssfeeds = Rssfeed::find($id);
	          if($rssfeeds->update($data)){
	          	$request->session()->flash('success', 'Rssfeeds updated successfully.');
	          	return redirect('admin/rssfeeds');
	          }
	          else{
	          	$request->session()->flash('error', 'Something went wrong while updating station.');
	          	return back()->withInput();
	          }

	        } 
	      }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request , $id)
	{
		$rssfeeds = Rssfeeds::find($id);
		if($rssfeeds) {
			$rssfeeds->delete();
			$request->session()->flash('success', 'Rssfeeds deleted successfully.');
			return redirect('admin/rssfeed');
		}
		else{
			$request->session()->flash('error', 'Something went wrong while updating station.');
			return back()->withInput();
		}
	}
	public function rssfeeds_delete(Request $request) {
		$ids = $request->input('selected');
		if($ids){
			foreach($ids as $id) {
				$rssfeeds = Rssfeeds::find($id);
				if($rssfeeds) {
					$rssfeeds->delete();
				}
				else{
				}

			}
			$request->session()->flash('success', 'Rssfeeds deleted successfully.');
		}
		return redirect('admin/rssfeeds');
	}
	

	public function rssfeeds_cat_index(Request $request) {
		$li_active = "rssfeeds_cat";
		$pagination = 10;
		$appends = array();
		$query_string = '';
		$filter_rssfeeds_cat = $request->input('filter_rssfeeds_cat');
		$appends['filter_rssfeeds_cat'] = $filter_rssfeeds_cat;
		$query_string = 'filter_rssfeeds_cat='.$filter_rssfeeds_cat;

		$rssfeeds_cats = RssfeedsCat::when($filter_rssfeeds_cat, function ($query, $filter_rssfeeds_cat) {
			return $query->where('name', 'like','%'.$filter_rssfeeds_cat.'%')->orWhere('des', 'like','%'.$filter_rssfeeds_cat.'%');
		})->paginate($pagination);
		return view('admin.rssfeeds.rssfeeds_cat_list',compact('rssfeeds_cats','filter_rssfeeds_cat','appends','query_string','li_active'));
	}
	
	public function rssfeeds_cat_create() {
			$li_active = "rssfeeds_cat";
		return view('admin.rssfeeds.rssfeeds_cat_create',compact('li_active'));
	}

	public function rssfeeds_cat_store(Request $request) {
		$validatedData = $request->validate([
			'name' => 'required|min:2|max:30',
			'sort_order'=>'numeric'
		]);
		if($validatedData){
			$data = $request->all();
			$rssfeedsCat = new RssfeedsCat;
			if($rssfeedsCat->create($data)){
				$request->session()->flash('success', 'Rssfeeds Category  created successfully.');
				return redirect('/admin/rssfeeds_cat');
			}
			else{
				$request->session()->flash('error', 'Something went wrong.');
				return back()->withInput();
			}

		}
	}

	public function rssfeeds_cat_edit($id) {
		$li_active = "rssfeeds_cat";
		$rssfeeds_cat = RssfeedsCat::find($id);
		if($rssfeeds_cat) {
			return view('admin.rssfeeds.rssfeeds_cat_edit',compact('id','rssfeeds_cat','li_active'));
		}
	}
	public function rssfeeds_cat_update(Request $request, $id) {
		$imagename = '';
		$validatedData = $request->validate([
			'name' => 'required|min:2|max:30',
		]);
		if($validatedData){

			$data = $request->all();
			$data['publish'] = $request->input('publish');
			$rssfeeds_cat = RssfeedsCat::find($id);
			if($rssfeeds_cat->update($data)){
				$request->session()->flash('success', 'Rssfeeds Category updated successfully.');
				return redirect('admin/rssfeeds_cat');
			}
			else{
				$request->session()->flash('error', 'Something went wrong while updating rssfeeds category.');
				return back()->withInput();
			}

		}
	}
	public function rssfeeds_cat_destroy(Request $request , $id) {
		$rssfeeds_cat = RssfeedsCat::find($id);
		if($rssfeeds_cat) {
			$rssfeeds_cat->delete();
			$request->session()->flash('success', 'Rssfeeds Category deleted successfully.');
			return redirect('admin/rssfeeds_cat');
		}
		else{
			$request->session()->flash('error', 'Something went wrong while deleting rssfeeds category.');
			return back()->withInput();
		}
	}
	public function rssfeeds_cat_delete(Request $request) {
		$rssfeeds_ids = $request->input('selected');
		if($rssfeeds_ids){
			foreach($rssfeeds_ids as $rssfeed_id) {
				$rssfeeds_cat = RssfeedsCat::find($rssfeed_id);
				if($rssfeeds_cat) {
					$rssfeeds_cat->delete();

				}
				else{
				}

			}
			$request->session()->flash('success', 'Rssfeeds Category deleted successfully.');
		}
		return redirect('admin/rssfeeds_cat');

	}

}
