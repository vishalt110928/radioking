<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Player;
use Carbon\Carbon;

class PlayerController extends Controller
{
	public function __construct() {
	  view()->share('active','players');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
	  $li_active = "players_manage";
	  $appends = array();
	  $query_string = '';

	  $filter_name = $request->input('filter_name');
	  $appends['filter_name'] = $filter_name;
	  $query_string = 'filter_name='.$filter_name;

	  $filter_end_time = $request->input('filter_end_time');
	  $appends['filter_end_time'] = $filter_end_time;
	  $query_string = '&filter_end_time='.$filter_end_time;

	  $filter_publish_time = $request->input('filter_publish_time');
	  $appends['filter_publish_time'] = $filter_publish_time;
	  $query_string = '&filter_publish_time='.$filter_publish_time;

	  $orderBy = $request->input('orderBy','updated_at');
	  $sort = $request->input('sort','desc');

	  $paginate = 10;
	  $players = Player::when($filter_name, function ($query, $filter_name) {
	    return $query->where('name', 'like','%'.$filter_name.'%');
	  })->when($filter_end_time, function ($query, $filter_end_time) {
	    return $query->where('end_time', '>',(Carbon::parse($filter_end_time)));
	  }) ->when($filter_publish_time, function ($query, $filter_publish_time) {
	    return $query->where('publish_time', '<',(Carbon::parse($filter_publish_time)));
	  })->when($orderBy, function ($query, $orderBy)  use($sort) {
	    return $query->orderBy($orderBy,$sort);
	  })
	  ->paginate($paginate);
	  $query =  http_build_query($request->except(['orderBy','sort']));
	  return view('admin.players.list',compact('players','filter_cat','filter_name','filter_end_time','filter_publish_time','appends','query_string','orderBy','sort','query','li_active'));

	}

	/**
	 * Show the form for creating a player resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	  $li_active = "add_player";
	  return view('admin.players.create',compact('li_active'));
	}

	/**
	 * Store a playerly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	  $imagename = '';
	  $validatedData = $request->validate([
	    'name' => 'required|string|min:2|max:30|unique:players',
	    'image'=>'required|image',

	  ]);
	  if($validatedData){
	    if($request->hasFile('image')){
	      $image = $request->file('image');
	      $destinationPath = public_path('/uploads');
	      $imagename = time().'.'.$image->getClientOriginalExtension();
	      if($image->move($destinationPath, $imagename)){

	      }
	      else{
	        $request->session()->flash('error', 'Something went wrong while uploading file.');
	        return back()->withInput();
	      }
	    }
	    $data = $request->all();
	    $data['image']= $imagename;
	    $players = new Player;
	    if($players->create($data)){
	      $request->session()->flash('success', 'Player  created successfully.');
	      return redirect('/admin/players');
	    }
	    else{
	      $request->session()->flash('error', 'Something went wrong.');
	      return back()->withInput();
	    }

	  }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
	    //
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
	  $li_active = "players_manage";
	  $player = Player::find($id);
	  return view('admin.players.edit',compact('player','id','li_active'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
	  $imagename = '';
	  if(!isset($request->publish)) {
	    $request->publish = 0;
	  }
	  $validatedData = $request->validate([
	    'name' => 'required|string|min:2|max:30|unique:players,id,'.$id,
	    'image'=>'image',

	  ]);
	  if($validatedData){
	    if($request->hasFile('image')){
	      $image = $request->file('image');
	      $destinationPath = public_path('/uploads');
	      $imagename = time().'.'.$image->getClientOriginalExtension();
	      if($image->move($destinationPath, $imagename)){

	      }
	      else{
	        $request->session()->flash('error', 'Something went wrong while uploading file.');
	        return back()->withInput();
	      }
	    }

	    $data = $request->all();
	    $data['publish'] = $request->input('publish');
	     $data['active_his'] = $request->input('active_his');
	      $data['auto_play'] = $request->input('auto_play');
	    if($request->hasFile('image')){
	      $data['image']= $imagename;
	    }
	    $players = Player::find($id);
	    if($players->update($data)){
	      $request->session()->flash('success', 'Player updated successfully.');
	      return redirect('admin/players');
	    }
	    else{
	      $request->session()->flash('error', 'Something went wrong while updating station.');
	      return back()->withInput();
	    }

	  } 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request , $id)
	{
	  $players = Player::find($id);
	  if($players) {
	    $players->delete();
	    $request->session()->flash('success', 'Player deleted successfully.');
	    return redirect('admin/player');
	  }
	  else{
	    $request->session()->flash('error', 'Something went wrong while updating station.');
	    return back()->withInput();
	  }
	}
	public function players_delete(Request $request) {
	  $ids = $request->input('selected');
	  if($ids){
	    foreach($ids as $id) {
	      $players = Player::find($id);
	      if($players) {
	        $players->delete();
	      }
	      else{
	      }

	    }
	    $request->session()->flash('success', 'Player deleted successfully.');
	  }
	  return redirect('admin/players');
	}
	
}
