<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slider;

class SliderController extends Controller
{ 
  public function __construct() {
    view()->share('active','sliders');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $li_active = "sliders_manage";
    $appends = array();
    $query_string = '';

    $filter_sliders = $request->input('filter_sliders');
    $appends['filter_sliders'] = $filter_sliders;
    $query_string = 'filter_sliders='.$filter_sliders;

    $filter_title = $request->input('filter_title');
    $appends['filter_title'] = $filter_title;
    $query_string = 'filter_title='.$filter_title;

    $filter_end_date = $request->input('filter_end_date');
    $appends['filter_end_date'] = $filter_end_date;
    $query_string = '&filter_end_date='.$filter_end_date;

    $filter_start_date = $request->input('filter_start_date');
    $appends['filter_start_date'] = $filter_start_date;
    $query_string = '&filter_start_date='.$filter_start_date;

    $filter_publish_time = $request->input('filter_publish_time');
    $appends['filter_publish_time'] = $filter_publish_time;
    $query_string = '&filter_publish_time='.$filter_publish_time;

    $orderBy = $request->input('orderBy','updated_at');
    $sort = $request->input('sort','desc');

    $paginate = 10;
    $sliders = Slider::when($filter_title, function ($query, $filter_title) {
      return $query->where('title', 'like','%'.$filter_title.'%');
    }) ->when($filter_start_date, function ($query, $filter_start_date) {
      return $query->where('start_date', '>',$filter_start_date);
    })
    ->when($filter_end_date, function ($query, $filter_end_date) {
      return $query->where('end_date', '>',$filter_end_date);
    })->when($filter_publish_time, function ($query, $filter_publish_time) {
      return $query->where('publish_time', '<','%'.$filter_publish_time.'%');
    })->paginate($paginate);
    $query =  http_build_query($request->except(['orderBy','sort']));
    return view('admin.sliders.list',compact('sliders','filter_cat','filter_title','filter_start_date','filter_end_date','filter_publish_time','appends','query_string','slider_cats','li_active','filter_sliders','orderBy','sort','query'));

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  { 
    $li_active = "add_slider";
    return view('admin.sliders.create',compact('li_active'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $imagename = '';
    $validatedData = $request->validate([
      'title' => 'required|min:2|max:30',
      'image'=>'required|image'
    ]);
    if($validatedData){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }
      $data = $request->all();
     /* echo "<pre>";
      print_r($data);
      exit;
  */
      $data['image']= $imagename;
      $slider = new Slider;
      if($slider->create($data)){
        $request->session()->flash('success', 'Slider created successfully.');
        return redirect('/admin/sliders');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }

    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $li_active = "sliders_manage";
    $slider = Slider::find($id);
    return view('admin.sliders.edit',compact('slider','id','li_active'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $imagename = '';
    $validatedData = $request->validate([
      'title' => 'required|min:2|max:30',
      'image'=>'image',
    ]);
    if($validatedData){
     if($request->hasFile('image')){
       $image = $request->file('image');
       $destinationPath = public_path('/uploads');
       $imagename = time().'.'.$image->getClientOriginalExtension();
       if($image->move($destinationPath, $imagename)){

       }
       else{
         $request->session()->flash('error', 'Something went wrong while uploading file.');
         return back()->withInput();
       }
     }

     $data = $request->all();
     $data['publish'] = $request->input('publish');
     if($request->hasFile('image')){
       $data['image']= $imagename;
          }/*
          else{
              $data['image']= $data['previousimage'];
              unset($data['previousimage']);
            }*/
            $slider = Slider::find($id);
            if($slider->update($data)){
              $request->session()->flash('success', 'Sliders updated successfully.');
              return redirect('admin/sliders');
            }
            else{
              $request->session()->flash('error', 'Something went wrong while updating station.');
              return back()->withInput();
            }

          } 
        }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request , $id)
  {
    $slider = Slider::find($id);
    if($slider) {
      $slider->delete();
      $request->session()->flash('success', 'Sliders deleted successfully.');
      return redirect('admin/sliders');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while updating station.');
      return back()->withInput();
    }
  }
  public function slider_delete(Request $request) {
    $ids = $request->input('selected');
    if($ids){
      foreach($ids as $id) {
        $slider = Slider::find($id);
        if($slider) {
          $slider->delete();
        }
        else{
        }
      }
      $request->session()->flash('success', 'Sliders deleted successfully.');
    }
    return redirect('admin/sliders');
  }
  
}
