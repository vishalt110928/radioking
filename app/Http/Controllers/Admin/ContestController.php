<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contest;
Use App\ContestsCat;

class ContestController extends Controller
{
  public function __construct() {
    view()->share('active','contests');
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $li_active="contests_manage";
    $appends = array();
    $query_string = '';
    $contests_cats = ContestsCat::pluck('name','id');


    $filter_contests = $request->input('filter_contests');
    $appends['filter_contests'] = $filter_contests;
    $query_string = 'filter_contests='.$filter_contests;

    $filter_title = $request->input('filter_title');
    $appends['filter_title'] = $filter_title;
    $query_string = 'filter_title='.$filter_title;

    $filter_cat = $request->input('filter_cat');
    $appends['filter_cat'] = $filter_cat;
    $query_string = '&filter_cat='.$filter_cat;

    $filter_end_time = $request->input('filter_end_time');
    $appends['filter_end_time'] = $filter_end_time;
    $query_string = '&filter_end_time='.$filter_end_time;

    $filter_publish_time = $request->input('filter_publish_time');
    $appends['filter_publish_time'] = $filter_publish_time;
    $query_string = '&filter_publish_time='.$filter_publish_time;

    $orderBy = $request->input('orderBy','updated_at');
    $sort = $request->input('sort','desc');

    $paginate = 10;
    $contests = Contest::when($filter_title, function ($query, $filter_title) {
      return $query->where('title', 'like','%'.$filter_title.'%');
    })
    ->when($filter_cat, function ($query, $filter_cat) {
      return $query->where('contests_cat_id',$filter_cat);
    })
    ->when($filter_end_time, function ($query, $filter_end_time) {
      return $query->where('end_time', '>',$filter_end_time);
    }) ->when($filter_publish_time, function ($query, $filter_publish_time) {
      return $query->where('publish_time', '<',$filter_publish_time);
    })->when($filter_contests, function ($query, $filter_contests) {
      return $query->where('title', 'like','%'.$filter_contests.'%')->orWhere('des', 'like','%'.$filter_contests.'%');
    })->when($orderBy, function ($query, $orderBy)  use($sort) {
      if($orderBy == "category") {
        return $query->join('contests_cats', 'contests_cats.id', '=', 'contests.contests_cat_id')
        ->orderBy('contests_cats.name',$sort);
      }
      return $query->orderBy($orderBy,$sort);
    })->paginate($paginate);

    $query =  http_build_query($request->except(['orderBy','sort']));
    return view('admin.contests.list',compact('contests','filter_cat','filter_title','filter_end_time','filter_publish_time','appends','query_string','contests_cats','li_active','filter_contests','query','orderBy','sort'));

  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $li_active="add_contest";
    $contests_cats = ContestsCat::pluck('name','id');
    return view('admin.contests.create',compact('contests_cats','li_active'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $imagename = '';
    $validatedData = $request->validate([
      'title' => 'required|min:1|max:100',
      'image'=>'required|image'
    ]);
    if($validatedData){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }
      $data = $request->all();
      $data['image']= $imagename;
      $contests = new Contest;
      if($contests->create($data)){
        $request->session()->flash('success', 'Contest  created successfully.');
        return redirect('/admin/contests');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }

    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
     //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $li_active="contests_manage";
    $contests = Contest::find($id);
    $contests_cats = ContestsCat::pluck('name','id');
    return view('admin.contests.edit',compact('contests','id','contests_cats','li_active'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $imagename = '';
    $validatedData = $request->validate([
      'title' => 'required|min:1|max:100',
      'image'=>'image',
    ]);
    if($validatedData){
      if($request->hasFile('image')){
        $image = $request->file('image');
        $destinationPath = public_path('/uploads');
        $imagename = time().'.'.$image->getClientOriginalExtension();
        if($image->move($destinationPath, $imagename)){

        }
        else{
          $request->session()->flash('error', 'Something went wrong while uploading file.');
          return back()->withInput();
        }
      }

      $data = $request->all();
      $data['publish'] = $request->input('publish');
      if($request->hasFile('image')){
        $data['image']= $imagename;
      }
      $contests = Contest::find($id);
      if($contests->update($data)){
        $request->session()->flash('success', 'Contest updated successfully.');
        return redirect('admin/contests');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating station.');
        return back()->withInput();
      }

    } 
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy(Request $request , $id)
  {
    $contests = Contest::find($id);
    if($contests) {
      $contests->delete();
      $request->session()->flash('success', 'Contest deleted successfully.');
      return redirect('admin/new');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while updating station.');
      return back()->withInput();
    }
  }
  public function contests_delete(Request $request) {
    $ids = $request->input('selected');
    if($ids){
      foreach($ids as $id) {
        $contests = Contest::find($id);
        if($contests) {
          $contests->delete();
        }
        else{
        }

      }
      $request->session()->flash('success', 'Contest deleted successfully.');
    }
    return redirect('admin/contests');
  }


  public function contests_cat_index(Request $request) {
    $li_active="contests_cat";
    $pagination = 10;
    $appends = array();
    $query_string = '';
    $filter_contests_cat = $request->input('filter_contests_cat');
    $appends['filter_contests_cat'] = $filter_contests_cat;
    $query_string = 'filter_contests_cat='.$filter_contests_cat;

    $contests_cats = ContestsCat::when($filter_contests_cat, function ($query, $filter_contests_cat) {
     return $query->where('name', 'like','%'.$filter_contests_cat.'%')->orWhere('des', 'like','%'.$filter_contests_cat.'%');
   })->orderBy('updated_at','desc')->paginate($pagination);
    return view('admin.contests.contests_cat_list',compact('contests_cats','filter_contests_cat','appends','query_string','li_active'));
  }

  public function contests_cat_create() {
    $li_active="contests_cat";
    return view('admin.contests.contests_cat_create',compact('li_active'));
  }

  public function contests_cat_store(Request $request) {
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
      'sort_order'=>'numeric'
    ]);
    if($validatedData){
      $data = $request->all();
      $contestsCat = new ContestsCat;
      if($contestsCat->create($data)){
        $request->session()->flash('success', 'Contest Category  created successfully.');
        return redirect('/admin/contests_cat');
      }
      else{
        $request->session()->flash('error', 'Something went wrong.');
        return back()->withInput();
      }

    }
  }

  public function contests_cat_edit($id) {
    $li_active="contests_cat";
    $contests_cat = ContestsCat::find($id);
    if($contests_cat) {
      return view('admin.contests.contests_cat_edit',compact('id','contests_cat','li_active'));
    }
  }
  public function contests_cat_update(Request $request, $id) {
    $validatedData = $request->validate([
      'name' => 'required|min:2|max:30',
    ]);
    if($validatedData){
      $data = $request->all();
      $data['publish'] = $request->input('publish');
      $contests_cat = ContestsCat::find($id);
      if($contests_cat->update($data)){
        $request->session()->flash('success', 'Contest Category updated successfully.');
        return redirect('admin/contests_cat');
      }
      else{
        $request->session()->flash('error', 'Something went wrong while updating contests category.');
        return back()->withInput();
      }

    }
  }
  public function contests_cat_destroy(Request $request , $id) {
    $contests_cat = ContestsCat::find($id);
    if($contests_cat) {
      $contests_cat->delete();
      $request->session()->flash('success', 'Contest Category deleted successfully.');
      return redirect('admin/contests_cat');
    }
    else{
      $request->session()->flash('error', 'Something went wrong while deleting contests category.');
      return back()->withInput();
    }
  }
  public function contests_cat_delete(Request $request) {
    $contests_ids = $request->input('selected');
    if($contests_ids){
      foreach($contests_ids as $new_id) {
        $contests_cat = ContestsCat::find($new_id);
        if($contests_cat) {
          $contests_cat->delete();

        }
        else{
        }

      }
      $request->session()->flash('success', 'Contest Category deleted successfully.');
    }
    return redirect('admin/contests_cat');
  }

}
