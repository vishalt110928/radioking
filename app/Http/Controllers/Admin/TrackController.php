<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Track;
use Carbon\Carbon;

class TrackController extends Controller
{
	public function __construct() {
		view()->share('active','tracks');
	}
	/**
	  * Display a listing of the resource.
	  *
	  * @return \Illuminate\Http\Response
	  */
	public function index(Request $request)
	{
		$li_active = "tracks_manage";
		$appends = array();
		$query_string = '';

		$filter_tracks = $request->input('filter_tracks');
		$appends['filter_tracks'] = $filter_tracks;
		$query_string = 'filter_tracks='.$filter_tracks;


		$filter_title = $request->input('filter_title');
		$appends['filter_title'] = $filter_title;
		$query_string = 'filter_title='.$filter_title;

		$filter_end_time = $request->input('filter_end_time');
		$appends['filter_end_time'] = $filter_end_time;
		$query_string = '&filter_end_time='.$filter_end_time;

		$filter_publish_time = $request->input('filter_publish_time');
		$appends['filter_publish_time'] = $filter_publish_time;
		$query_string = '&filter_publish_time='.$filter_publish_time;

		$orderBy = $request->input('orderBy','updated_at');
		$sort = $request->input('sort','desc');


		$paginate = 10;
		$tracks = Track::when($filter_title, function ($query, $filter_title) {
			return $query->where('title', 'like','%'.$filter_title.'%');
		})
		->when($filter_end_time, function ($query, $filter_end_time) {
			return $query->where('end_time', '>',(Carbon::parse($filter_end_time)));
		}) ->when($filter_publish_time, function ($query, $filter_publish_time) {
			return $query->where('publish_time', '<',(Carbon::parse($filter_publish_time)));
		})
		->when($filter_tracks, function ($query, $filter_tracks) {
			return $query->where('title', 'like','%'.$filter_tracks.'%')->orWhere('description', 'like','%'.$filter_tracks.'%');
		})->when($orderBy, function ($query, $orderBy)  use($sort) {
			return $query->orderBy($orderBy,$sort);
		})->paginate($paginate);
		$query =  http_build_query($request->except(['orderBy','sort']));
		return view('admin.tracks.list',compact('tracks','filter_cat','filter_title','filter_end_time','filter_publish_time','appends','query_string','query','orderBy','sort','filter_tracks','li_active'));
	}

	/**
	* Show the form for creating a new resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function create()
	{
		$li_active = "tracks_manage";
		return view('admin.tracks.create',compact('li_active'));
	}

	/**
	* Store a newly created resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/
	public function store(Request $request)
	{
		$imagename = '';
		$audioname = '';
		$validatedData = $request->validate([
			'title' => 'required|min:2|max:30',
			'image'=>'required|image'
		]);
		if($validatedData){
			if($request->hasFile('image')){
				$image = $request->file('image');
				$destinationPath = public_path('/uploads');
				$imagename = time().'.'.$image->getClientOriginalExtension();
				if($image->move($destinationPath, $imagename)){

				}
				else{
					$request->session()->flash('error', 'Something went wrong while uploading file.');
					return back()->withInput();
				}
			}
			if($request->hasFile('audio_sample')){
				$audio_sample = $request->file('audio_sample');
				$destinationPath = public_path('/uploads');
				$audioname = time().'.'.$audio_sample->getClientOriginalExtension();
				if($audio_sample->move($destinationPath, $audioname)){
				}
				else{
					$request->session()->flash('error', 'Something went wrong while uploading file.');
					return back()->withInput();
				}
			}
			$data = $request->all();
			$data['image']= $imagename;
			$data['audio_sample']= $audioname;
			$track = new Track;
			if($track->create($data)){
				$request->session()->flash('success', 'Track  created successfully.');
				return redirect('/admin/tracks');
			}
			else{
				$request->session()->flash('error', 'Something went wrong.');
				return back()->withInput();
			}

		}
	}

	/**
	* Display the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function show($id)
	{
	   //
	}

	/**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function edit($id)
	{
		$li_active = "tracks_manage";
		$tracks = Track::find($id);
		return view('admin.tracks.edit',compact('tracks','id','li_active'));
	}

	/**
	* Update the specified resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function update(Request $request, $id)
	{
		$imagename = '';
		$audioname = '';
		$validatedData = $request->validate([
			'title' => 'required|min:2|max:30',
			'image'=>'image',
		]);
	  if($validatedData){
	  	if($request->hasFile('image')) {
	  		$image = $request->file('image');
	  		$destinationPath = public_path('/uploads');
	  		$imagename = time().'.'.$image->getClientOriginalExtension();
	  		if($image->move($destinationPath, $imagename)){
	  		}
	  		else{
	  			$request->session()->flash('error', 'Something went wrong while uploading file.');
	  			return back()->withInput();
	  		}
	  	}
	  	if($request->hasFile('audio_sample')){
	  		$audio_sample = $request->file('audio_sample');
	  		$destinationPath = public_path('/uploads');
	  		$audioname = time().'.'.$audio_sample->getClientOriginalExtension();
	  		if($audio_sample->move($destinationPath, $audioname)){
	  		}
	  		else{
	  			$request->session()->flash('error', 'Something went wrong while uploading file.');
	  			return back()->withInput();
	  		}
	  	}

	  	$data = $request->all();
	  	$data['audio_sample'] = $audioname;	  	$data['publish'] = $request->input('publish');
	  	if($request->hasFile('image')){
	  		$data['image']= $imagename;
	  	}
	  	$tracks = Track::find($id);
	  	if($tracks->update($data)){
	  		$request->session()->flash('success', 'Track updated successfully.');
	  		return redirect('admin/tracks');
	  	}
	  	else{
	  		$request->session()->flash('error', 'Something went wrong while updating station.');
	  		return back()->withInput();
	  	}

	  } 
	}

	/**
	* Remove the specified resource from storage.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function destroy(Request $request , $id)
	{
		$tracks = Track::find($id);
		if($tracks) {
			$tracks->delete();
			$request->session()->flash('success', 'Track deleted successfully.');
			return redirect('admin/new');
		}
		else{
			$request->session()->flash('error', 'Something went wrong while updating station.');
			return back()->withInput();
		}
	}
	public function tracks_delete(Request $request) {
		$ids = $request->input('selected');
		if($ids){
			foreach($ids as $id) {
				$tracks = Track::find($id);
				if($tracks) {
					$tracks->delete();
				}
				else{
				}

			}
			$request->session()->flash('success', 'Track deleted successfully.');
		}
		return redirect('admin/tracks');
	}

}
