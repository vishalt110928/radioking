<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\News;
use App\Track;
use	App\Podcast;
use App\Playlist;
use App\Video;
use	App\Contest;
use App\Event;
use App\Artist;
use App\Team;
use App\Dedication;
use App\Page;
use App\Program;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;
use App\Contact_us;
use App\User;
use App\TrackUser;
use App\UserTrackLike;
use App\UserPlaylistLike;
use App\PlayerLike;
use App\Photo;
use App\Notification;
use App\ContestPart;
use App\Rssfeed;
use App\LivePoll;
use Auth;

class RadioKingController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index(Request $request)
	{
		$now = Carbon::now();
		$sliders = Slider::where('publish', 1)->where('start_date', '<=', $now)->where('end_date', '>=', $now)->paginate(5);
		$news = News::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->paginate(4);
		$tracks = Track::where('publish', 1)->where('publish_time', '<=', $now)->paginate(4);
		$podcasts = Podcast::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->paginate(4);
		$playlists = Playlist::where('publish', 1)->paginate(4);
		$programs = Program::where('publish', 1)->where('publish_date', '<=', $now)->where('end_date', '>=', $now)->paginate(4);
		$videos = Video::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->paginate(9);
		$contests = Contest::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->paginate(4);
		$events = Event::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->paginate(4);
		$artists = Artist::where('publish', 1)->where('publish_time', '<', $now)->paginate(12);
		$notpushstate = $request->input('notpushstate');

		if ($request->ajax() && $notpushstate == 1) {
			return response()->view('ajax_blade.home', compact('sliders', 'news', 'tracks', 'podcasts', 'playlists', 'programs', 'videos', 'contests', 'events', 'artists'));
		} elseif ($request->ajax() && $notpushstate == 0) {
			return response()->view('home', compact('sliders', 'news', 'tracks', 'podcasts', 'playlists', 'programs', 'videos', 'contests', 'events', 'artists'));
		} else {
			return view('home', compact('sliders', 'news', 'tracks', 'podcasts', 'playlists', 'programs', 'videos', 'contests', 'events', 'artists'));
		}
	}

	public function page(Request $request, $slug)
	{
		$blocks = array();
		$sidebars = array();
		$page = "";
		$now = Carbon::now();
		$response = $this->block_sidebar($slug);
		$blocks = $response['blocks'];
		$sidebars = $response['sidebars'];
		$page = $response['page'];
		if (View::exists($slug)) {
			switch ($slug) {
				case 'news':
				$pages[$slug]  = News::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->paginate(4);
				break;
				case 'programs':
				$brod_days = $request->query('brod_days');
				$pages['brod_days'] = $brod_days;
				$pages[$slug]  = Program::when($brod_days, function ($query, $filter_end_time) use($brod_days) {
					switch ($brod_days) {
						case 'all':
						return $query->where('brod_days->all', '1');
						break;
						case 'mon':
						return $query->where('brod_days->mon', '1');
						case 'tue':
						return $query->where('brod_days->tue', '1');
						case 'wed':
						return $query->where('brod_days->wed', '1');
						case 'thr':
						return $query->where('brod_days->thr', '1');
						case 'fri':
						return $query->where('brod_days->fri', '1');
						case 'sat':
						return $query->where('brod_days->sat', '1');
						case 'sun':
						return $query->where('brod_days->sun', '1');
						default:
						return $query;
						break;
					}
				})->where('publish', 1)->where('publish_date', '<=', $now)->where('end_date', '>=', $now)->paginate(4);
				break;
				case 'contests':
				$pages[$slug]  = Contest::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->paginate(4);
				break;
				case 'podcasts':
				$pages[$slug]  = Podcast::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->paginate(4);
				break;
				case 'events':
				$pages[$slug]  = Event::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->paginate(4);
				break;
				case 'top_10':
				$pages['musics']  = Track::where('publish', 1)->where('publish_time', '<=', $now)->paginate(4);
				break;
				case 'artists':
				$name = $request->query('name');
				$pages['name'] = $name;
				$pages[$slug]  = Artist::when($name, function ($query, $filter_end_time) use($name) {
					if($name == 'all') {
						return $query;
					}else{
						return $query->where('name','like' ,'%'.$name.'%');
					}
				})->where('publish', 1)->where('publish_time', '<=', $now)->paginate(4);
				break;
				case 'playlists':
				$pages[$slug]  =  Playlist::where('publish', 1)->paginate(4);
				break;
				case 'podcasts':
				$pages[$slug]  = Podcast::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->paginate(4);
				break;
				case 'videos':
				$pages[$slug]  = Video::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->paginate(9);
				break;
				case 'dedications':
				$pages[$slug]  = Dedication::where('publish', 1)->where('publish_time', '<=', $now)->paginate(4);
				break;
				case 'contests':
				$pages[$slug]  = Contest::where('publish', 1)->where('publish_time', '<=', $now)->where('end_time', '>=', $now)->paginate(4);
				break;
				case 'teams':
				$pages[$slug]  = Team::where('publish', 1)->where('publish_time', '<=', $now)->paginate(4);;
				break;
				default:
			}
			$notpushstate = $request->input('notpushstate');
			if ($request->ajax() && $notpushstate == 1) {
				return response()->view('ajax_blade.' . $slug, compact('pages', 'blocks', 'sidebars'));
			} elseif ($request->ajax() && $notpushstate == 0) {
				return response()->view($slug, compact('pages', 'blocks', 'sidebars'));
			} else {
				return view($slug, compact('pages', 'blocks', 'sidebars'));
			}
		} else {
			//$id = explode('-', $slug)[0];
			if ($request->ajax() && $page && $notpushstate == 1) {
				return response()->view('ajax_blade.pages', compact('page', 'blocks', 'sidebars'));
			} elseif ($request->ajax() && $page && $notpushstate == 0) {
				return response()->view('pages', compact('page', 'blocks', 'sidebars'));
			} elseif (!$page) {
				$page['content'] = "Sorry No Page Exist.";
				return view('pages', compact('page', 'blocks', 'sidebars'));
			} else {
				return view('pages', compact('page', 'blocks', 'sidebars'));
			}
		}
	}

	public function contact_us(Request $request)
	{
		$response = array();
		$blocks = array();
		$sidebars = array();
		$response = $this->block_sidebar('contact_us');
		$blocks = $response['blocks'];
		$sidebars = $response['sidebars'];
		$notpushstate = $request->input('notpushstate');
		if ($request->ajax() && $notpushstate == 1) {
			return response()->view('ajax_blade.contact_us', compact('blocks', 'sidebars'));
		} elseif ($request->ajax() && $notpushstate == 0) {
			return response()->view('contact_us', compact('blocks', 'sidebars'));
		} else {
			return view('contact_us', compact('blocks', 'sidebars'));
		}
	}
	public function contact_us_store(Request $request)
	{
		$response = array();
		$validatedData = $request->validate([
			'name' => 'required|string|max:30|',
			'email' => 'required|email|string|unique:contact_us',
			'subject' => 'required|max:100',
			'message' => 'required',
			'g-recaptcha-response'=>'required|recaptcha'
		]);
		if ($validatedData) {
			$data = $request->all();
			$contact = new Contact_us;
			if ($contact->create($data)) {
				$notification['title'] = "New Contact Arrived";
				$notification['href'] = "admin/contacts";
				$notification['viewed'] = "0";
				Notification::updateOrCreate(['title'=>$notification['title']],['href'=>$notification['href'],'viewed'=>$notification['viewed']]);
				if ($request->ajax()) {
					$response['status'] = 200;
					$response['message'] = "You have contact us successfully.";
					return response()->json($response);
				} else {
					$request->session()->flash('success', 'Contact Us created successfully.');
					return redirect('/contact_us');
				}
			} else {
				if ($request->ajax()) {
					$response['status'] = 400;
					$response['message'] = "Something hava gone wrong.";
					return response()->json($response, 400);
				} else {
					$request->session()->flash('success', 'Contact Us created successfully.');
					return back()->withInput();
				}
			}
		}
	}

	public function news_detail(Request $request, $slug)
	{
		$path = request()->segment(1);
		$blocks = array();
		$sidebars = array();
		$id = explode('-',$slug)[0];
		$response = $this->block_sidebar($path);
		$blocks = $response['blocks'];
		$sidebars = $response['sidebars'];
		$notpushstate = $request->input('notpushstate');
		$new = News::find($id);
		if ($new) {
			$news_same_cats = News::where('news_cat_id', $new->news_cat_id)->paginate(2);
			if ($request->ajax() && $notpushstate == 1) {
				return response()->view('ajax_blade.news_detail', compact('blocks', 'sidebars', 'new', 'news_same_cats'));
			} elseif ($request->ajax() && $notpushstate == 0) {
				return response()->view('news_detail', compact('blocks', 'sidebars', 'new', 'news_same_cats'));
			} else {
				return view('news_detail', compact('blocks', 'sidebars', 'new', 'news_same_cats'));
			}
		}
	}

	public function team_detail(Request $request, $slug)
	{
		$path = request()->segment(1);
		$blocks = array();
		$sidebars = array();
		$id = explode('-',$slug)[0];
		$response = $this->block_sidebar($path);
		$blocks = $response['blocks'];
		$sidebars = $response['sidebars'];
		$notpushstate = $request->input('notpushstate');
		$team = Team::find($id);
		if ($team) {
			if ($request->ajax() && $notpushstate == 1) {
				return response()->view('ajax_blade.team_detail', compact('blocks', 'sidebars', 'team'));
			} elseif ($request->ajax() && $notpushstate == 0) {
				return response()->view('team_detail', compact('blocks', 'sidebars', 'team'));
			} else {
				return view('team_detail', compact('blocks', 'sidebars', 'team'));
			}
		}
	}


	public function program_detail(Request $request, $slug)
	{
		$blocks = array();
		$sidebars = array();
		$id = explode('-',$slug)[0];
		$path = request()->segment(1);
		$response = $this->block_sidebar($path);
		$blocks = $response['blocks'];
		$sidebars = $response['sidebars'];
		$program = Program::find($id);
		$notpushstate = $request->input('notpushstate');
		if ($program) {
			$program_same_cats = Program::where('programs_cat_id', $program->programs_cat_id)->paginate(2);
			if ($request->ajax() && $notpushstate == 1) {
				return response()->view('ajax_blade.program_detail', compact('blocks', 'sidebars', 'program', 'program_same_cats'));
			} elseif ($request->ajax() && $notpushstate == 0) {
				return response()->view('program_detail', compact('blocks', 'sidebars', 'program', 'program_same_cats'));
			} else {
				return view('program_detail', compact('blocks', 'sidebars', 'program', 'program_same_cats'));
			}
		}
	}
	public function podcast_detail(Request $request, $slug)
	{
		$blocks = array();
		$sidebars = array();
		$id = explode('-',$slug)[0];
		$path = request()->segment(1);
		$response = $this->block_sidebar($path);
		$blocks = $response['blocks'];
		$sidebars = $response['sidebars'];
		$podcast = Podcast::find($id);
		$notpushstate = $request->input('notpushstate');
		if ($podcast) {
			if ($request->ajax() && $notpushstate == 1) {
				return response()->view('ajax_blade.podcast_detail', compact('blocks', 'sidebars', 'podcast'));
			} elseif ($request->ajax() && $notpushstate == 0) {
				return response()->view('podcast_detail', compact('blocks', 'sidebars', 'podcast'));
			} else {
				return view('podcast_detail', compact('blocks', 'sidebars', 'podcast'));
			}
		}
	}

	public function video_detail(Request $request, $slug)
	{
		$blocks = array();
		$sidebars = array();
		$id = explode('-',$slug)[0];
		$path = request()->segment(1);
		$response = $this->block_sidebar($path);
		$blocks = $response['blocks'];
		$sidebars = $response['sidebars'];
		$notpushstate = $request->input('notpushstate');
		$video = Video::find($id);
		if ($video) {
			if ($request->ajax() && $notpushstate == 1) {
				return response()->view('ajax_blade.video_detail', compact('blocks', 'sidebars', 'video'));
			} elseif ($request->ajax() && $notpushstate == 0) {
				return response()->view('video_detail', compact('blocks', 'sidebars', 'video'));
			} else {
				return view('video_detail', compact('blocks', 'sidebars', 'video'));
			}
		}
	}
	public function contest_detail(Request $request, $slug)
	{
		$blocks = array();
		$sidebars = array();
		$id = explode('-',$slug)[0];
		$path = request()->segment(1);
		$response = $this->block_sidebar($path);
		$blocks = $response['blocks'];
		$sidebars = $response['sidebars'];
		$notpushstate = $request->input('notpushstate');
		$contest = Contest::find($id);
		if ($contest) {
			if ($request->ajax() && $notpushstate == 1) {
				return response()->view('ajax_blade.contest_detail', compact('blocks', 'sidebars', 'contest'));
			} elseif ($request->ajax() && $notpushstate == 0) {
				return response()->view('contest_detail', compact('blocks', 'sidebars', 'contest'));
			} else {
				return view('contest_detail', compact('blocks', 'sidebars', 'contest'));
			}
		}
	}
	public function event_detail(Request $request, $slug)
	{
		$blocks = array();
		$sidebars = array();
		$id = explode('-',$slug)[0];
		$path = request()->segment(1);
		$response = $this->block_sidebar($path);
		$blocks = $response['blocks'];
		$sidebars = $response['sidebars'];
		$notpushstate = $request->input('notpushstate');
		$event = Event::find($id);
		if ($event) {
			if ($request->ajax() && $notpushstate == 1) {
				return response()->view('ajax_blade.event_detail', compact('blocks', 'sidebars', 'event'));
			} elseif ($request->ajax() && $notpushstate == 0) {
				return response()->view('event_detail', compact('blocks', 'sidebars', 'event'));
			} else {
				return view('event_detail', compact('blocks', 'sidebars', 'event'));
			}
		}
	}
	public function artist_detail(Request $request, $slug)
	{
		$blocks = array();
		$sidebars = array();
		$id = explode('-',$slug)[0];
		$path = request()->segment(1);
		$response = $this->block_sidebar($path);
		$blocks = $response['blocks'];
		$sidebars = $response['sidebars'];
		$notpushstate = $request->input('notpushstate');
		$artist = Artist::find($id);
		if ($artist) {
			if ($request->ajax() && $notpushstate == 1) {
				return response()->view('ajax_blade.artist_detail', compact('blocks', 'sidebars', 'artist'));
			} elseif ($request->ajax() && $notpushstate == 0) {
				return response()->view('artist_detail', compact('blocks', 'sidebars', 'artist'));
			} else {
				return view('artist_detail', compact('blocks', 'sidebars', 'artist'));
			}
		}
	}
	public function photos(Request $request)
	{
		$blocks = array();
		$sidebars = array();
		$path = request()->segment(1);
		$response = $this->block_sidebar($path);
		$blocks = $response['blocks'];
		$sidebars = $response['sidebars'];
		$photos = Photo::paginate(15);
		$notpushstate = $request->input('notpushstate');
		if ($request->ajax() && $notpushstate == 1) {
			return response()->view('ajax_blade.photos', compact('blocks', 'sidebars', 'photos'));
		} elseif ($request->ajax() && $notpushstate == 0) {
			return response()->view('photos', compact('blocks', 'sidebars', 'photos'));
		} else {
			return view('photos', compact('blocks', 'sidebars', 'photos'));
		}
	}
	public function block_sidebar($slug)
	{
		$response = array();
		$blocks = array();
		$sidebars = array();
		$page = Page::with(['blocks' => function ($query) {
			$query->orderBy('sort_order', 'desc');
		}])->where('title', $slug)->first();
		$i = 0;
		if (isset($page->blocks)) {
			foreach ($page->blocks as $bl) {
				$side = $bl->title;
				switch ($side) {
					case 'programs':
					$blocks[$bl->title]  = Program::get()->toArray();
					$blocks[$bl->title] = array_chunk($blocks[$bl->title], 4);
					// echo "<pre>";
					// print_r($blocks[$bl->title]);
					// exit;
					break;
					case 'news':
					$blocks[$bl->title]  = News::paginate(4);
					break;
					case 'contests':
					$blocks[$bl->title]  = Contest::paginate(4);
					break;
					case 'podcasts':
					$blocks[$bl->title]  = Podcast::paginate(4);
					break;
					case 'events':
					$blocks[$bl->title]  = Event::paginate(4);
					break;
					default:
				}
				$sidebars[$i] = $side;
				$i++;
			}
		} else {
			$blocks = array();
		}
		$response['blocks'] = $blocks;
		$response['sidebars'] = $sidebars;
		$response['page'] = $page;
		return $response;
	}

	public function user_profile(Request $request, $id)
	{
		$auth_user = Auth::user();
		if ($auth_user) {
			return view('user_profile', compact('auth_user', 'id'));
		} else {
			return back()->withInput();
		}
	}

	public function user_profile_store(Request $request, $id)
	{
		$imagename = '';
		if (!isset($request->publish)) {
			$request->publish = 0;
		}
		$validatedData = $request->validate([
			'name' => 'required|string|min:2|max:30|unique:users,id,' . $id,
			'image' => 'image',
			'password' => 'confirmed'
		]);
		if ($validatedData) {
			if ($request->hasFile('image')) {
				$image = $request->file('image');
				$destinationPath = public_path('/uploads');
				$imagename = time() . '.' . $image->getClientOriginalExtension();
				if ($image->move($destinationPath, $imagename)) { } else {
					$request->session()->flash('error', 'Something went wrong while uploading file.');
					return back()->withInput();
				}
			}

			$data = $request->all();
			if ($request->input('password')) {
				$data['password'] =	Hash::make($request->input('password'));
			} else {
				unset($data['password']);
			}
			$data['publish'] = $request->input('publish');
			if ($request->hasFile('image')) {
				$data['image'] = $imagename;
			}
			$users = User::find($id);
			if ($users->update($data)) {
				$request->session()->flash('success', 'User updated successfully.');
				return redirect('user_profile/' . $id);
			} else {
				$request->session()->flash('error', 'Something went wrong while updating station.');
				return back()->withInput();
			}
		}
	}
	public function played_tracks(Request $request)
	{
		$response = array();
		$track_id = $request->input('id');
		if (Auth::check()) {
			$data['user_id'] = Auth::user()->id;
			$data['track_id'] = $track_id;
			$track_user = TrackUser::updateOrCreate($data);
			if ($track_user) {
				$response['status'] = 200;
				$response['message'] = "Track successfully added to the played list.";
				return response()->json($response, 200);
			} else {
				$response['status'] = 400;
				$response['message'] = "Something have gone wrong.";
				return response()->json($response, 400);
			}
		}
	}

	public function auth_played_tracks(Request $request)
	{
		$blocks = array();
		$sidebars = array();
		$path = request()->segment(1);
		$response = $this->block_sidebar($path);
		$blocks = $response['blocks'];
		$sidebars = $response['sidebars'];
		$notpushstate = $request->input('notpushstate');
		if (Auth::check()) {
			$tracks = Auth::user()->tracks()->paginate(5);

			if ($request->ajax() && $notpushstate == 1) {
				return response()->view('ajax_blade.auth_played_tracks', compact('blocks', 'sidebars', 'tracks'));
			} elseif ($request->ajax() && $notpushstate == 0) {
				return response()->view('auth_played_tracks', compact('blocks', 'sidebars', 'tracks'));
			} else {
				return view('auth_played_tracks', compact('blocks', 'sidebars', 'tracks'));
			}
		}
	}

	public function send_dedication(Request $request)
	{
		$response = array();
		$data = array();
		$validatedData = $request->validate([
			'username' => 'required|string|',
			'dedication_message' => 'required'
		]);
		if ($validatedData) {
			$data = $request->all();
			$data['publish_time'] = \Carbon\Carbon::now();
			$dedication = new Dedication;
			if ($dedication->create($data)) {
				if ($request->ajax()) {
					$response['status'] = 200;
					$response['message'] = "You have send dedication successfully.";
					return response()->json($response);
				} else {
					$request->session()->flash('status', 'You have send dedication successfully.');
					return back()->withInput();
				}
			}
		}
	}

	public function track_playlist_like(Request $request)
	{
		$response = array();
		$data = $request->all();
		if (auth()->check()) {
			switch ($data['type']) {
				case "player":
				$data['user_id'] = Auth::user()->id;
				$data['player_id'] = $request->input('id');
				$user_player_like = PlayerLike::where('player_id', $data['player_id'])->where('user_id', $data['user_id'])->first();
				if ($user_player_like) { 
					$response['status'] = 200;
					$response['message'] = "Player is already in the like list.";
					return response()->json($response);
				} 
				$player_like = new PlayerLike;
				if ($player_like->create($data)) {
					$response['status'] = 200;
					$response['message'] = "You have liked track successfully.";
					return response()->json($response);
				} else {
					$response['status'] = 400;
					$response['message'] = "Something have gone wrong.";
					return response()->json($response, 400);
				}
				break;
				case "track":
				$data['user_id'] = Auth::user()->id;
				$data['track_id'] = $request->input('id');
				$user_track_like = UserTrackLike::where('track_id', $data['track_id'])->where('user_id', $data['user_id'])->first();
				if ($user_track_like) { 
					$response['status'] = 200;
					$response['message'] = "Track is already in the like list.";
					return response()->json($response);
				} else {
					$track_like = new UserTrackLike;
					if ($track_like->create($data)) {
						$response['status'] = 200;
						$response['message'] = "You have liked track successfully.";
						return response()->json($response);
					} else {
						$response['status'] = 400;
						$response['message'] = "Something have gone wrong.";
						return response()->json($response, 400);
					}
				}
				break;
				case "playlist":
				$data['user_id'] = Auth::user()->id;
				$data['playlist_id'] = $request->input('id');

				$user_playlist_like = UserPlaylistLike::where('playlist_id', $data['playlist_id'])->where('user_id', $data['user_id'])->first();
				if ($user_playlist_like) { 
					$response['status'] = 200;
					$response['message'] = "Playlist is already in the like list.";
					return response()->json($response);
				}
				$playlist_like = new UserPlaylistLike;
				if ($playlist_like->create($data)) {
					$response['status'] = 200;
					$response['message'] = "You have liked playlist successfully.";
					return response()->json($response);
				} else {
					$response['status'] = 400;
					$response['message'] = "Something have gone wrong.";
					return response()->json($response, 400);
				}
				break;
				default:
				$response['status'] = 200;
				$response['message'] = "No match found.";
				return response()->json($response);
			}
		}
	}

	public function contest_part(Request $request) {
		$response = array();
		$contest_id = $request->input('contest_id');
		if(Auth::check()) {
			$user_id = AUth::user()->id;
			$data['contest_id'] = $contest_id;
			$data['user_id'] = $user_id;
			$contest_part = new ContestPart;
			if($contest_part->create($data)) {
				$response['status'] = 200;
				$response['message'] = "You have Taken Participate In Contest Successfully." ;
				return response()->json($response,200);
			}
			else{
				$response['status'] = 400;
				$response['message'] = "Something went wrong while taking participate in contest." ;
				return response()->json($response,400);
			}
		}
		else {
			$response['status'] = 400;
			$response['message'] = "You should have login first to take participate." ;
			return response()->json($response,400);
		}
	}

	public function feed(Request $request){
		$feeds = Rssfeed::all();
		$rssfeed =  "<?xml version='1.0' encoding='UTF-8'?><rss version='2.0'><channel><title>RadioKing</title><link>ekamhub.com/radioking</link><description>RssFeed</description><language>en-us</language>";
		foreach ($feeds as $feed) {
			$rssfeed .=  "<item><title>$feed->title</title><link>$feed->rss_url</link><description>$feed->des</description></item>";
		}

		$rssfeed.= "</channel></rss>";
		echo($rssfeed);
		exit;

	}

	public function poll_ana(Request $request) {
		$data = array();
		$response = array();
		if(!isset($request->pos_neg)){
			$response['pos_count'] = LivePoll::where('pos_neg','1')->count();
			$response['neg_count'] = LivePoll::where('pos_neg','0')->count();
			$response['total'] = LivePoll::count();
			return response()->json($response,200);
		}
		$validatedData = $request->validate([
			'pos_neg' => 'required'
		]);
		$data = $request->all();
		$data['user_id'] = Auth::user()->id;
		$livepoll = new LivePoll;
		if($livepoll->create($data)) {
			$response['status'] = '200';
			$response['message'] = 'You have voted successfully';
			$response['pos_count'] = LivePoll::where('pos_neg','1')->count();
			$response['neg_count'] = LivePoll::where('pos_neg','0')->count();
			$response['total'] = LivePoll::count();
			return response()->json($response,200);
		}
		else{
			$response['status'] = 400;
			$response['message'] = "Something have gone wrong";
			return response()->json($response);
		}


	}

	// public function programs(Request $request)
	// {
	// 	$blocks = array();
	// 	$sidebars = array();
	// 	$page = Page::with(['blocks' => function ($query) {
	// 		$query->orderBy('sort_order', 'desc');
	// 	}])->where('title', 'news')->first();
	// 	$i = 0;
	// 	foreach ($page->blocks as $bl) {
	// 		$side = $bl->title;
	// 		$model = ucfirst($bl->title);
	// 		switch ($model) {
	// 			case 'Program':
	// 				$blocks[$bl->title]  = Program::paginate(4);
	// 				break;
	// 			case 'News':
	// 				$blocks[$bl->title]  = News::paginate(4);
	// 				break;
	// 			case 'Contest':
	// 				$blocks[$bl->title]  = Contest::paginate(4);
	// 				break;
	// 			case 'Podcast':
	// 				$blocks[$bl->title]  = Podcast::paginate(4);
	// 				break;
	// 			case 'Event':
	// 				$blocks[$bl->title]  = Event::paginate(4);
	// 				break;
	// 			default:
	// 		}
	// 		$sidebars[$i] = $side;
	// 		$i++;
	// 	}
	// 	$programs = Program::paginate(5);
	// 	if ($request->ajax()) {
	// 		return response()->view('shows', compact('programs', 'blocks', 'sidebars'));
	// 	} else {
	// 		return view('shows', compact('programs', 'blocks', 'sidebars'));
	// 	}
	// }

	// public function teams(Request $request)
	// {
	// 	$blocks = array();
	// 	$sidebars = array();
	// 	$page = Page::with(['blocks' => function ($query) {
	// 		$query->orderBy('sort_order', 'desc');
	// 	}])->where('title', 'news')->first();
	// 	$i = 0;
	// 	foreach ($page->blocks as $bl) {
	// 		$side = $bl->title;
	// 		$model = ucfirst($bl->title);
	// 		switch ($model) {
	// 			case 'Program':
	// 				$blocks[$bl->title]  = Program::paginate(4);
	// 				break;
	// 			case 'News':
	// 				$blocks[$bl->title]  = News::paginate(4);
	// 				break;
	// 			case 'Contest':
	// 				$blocks[$bl->title]  = Contest::paginate(4);
	// 				break;
	// 			case 'Podcast':
	// 				$blocks[$bl->title]  = Podcast::paginate(4);
	// 				break;
	// 			case 'Event':
	// 				$blocks[$bl->title]  = Event::paginate(4);
	// 				break;
	// 			default:
	// 		}
	// 		$sidebars[$i] = $side;
	// 		$i++;
	// 	}
	// 	$teams = Team::paginate(5);
	// 	if ($request->ajax()) {
	// 		return response()->view('teams', compact('teams', 'blocks', 'sidebars'));
	// 	} else {
	// 		return view('teams', compact('teams', 'blocks', 'sidebars'));
	// 	}
	// }

	// public function events(Request $request)
	// {
	// 	$news = News::paginate(4);
	// 	$programs = Program::paginate(4);
	// 	$events = Event::paginate(4);
	// 	$podcasts = Podcast::paginate(4);
	// 	$contests = Contest::paginate(4);
	// 	$teams = Team::paginate(8);
	// 	if ($request->ajax()) {
	// 		return response()->view('events', compact('news', 'programs', 'podcasts', 'events', 'contests', 'teams'));
	// 	} else {
	// 		return view('events', compact('news', 'programs', 'podcasts', 'events', 'contests', 'teams'));
	// 	}
	// }

	// public function music(Request $request)
	// {
	// 	$events = Event::paginate(4);
	// 	$podcasts = Podcast::paginate(4);
	// 	$contests = Contest::paginate(4);
	// 	$musics = Track::paginate(5);
	// 	if ($request->ajax()) {
	// 		return response()->view('musics', compact('podcasts', 'events', 'contests', 'teams', 'musics'));
	// 	} else {
	// 		return view('musics', compact('podcasts', 'events', 'contests', 'teams', 'musics'));
	// 	}
	// }
	// public function artists(Request $request)
	// {
	// 	$events = Event::paginate(4);
	// 	$podcasts = Podcast::paginate(4);
	// 	$contests = Contest::paginate(4);
	// 	$artists = Artist::paginate(10);
	// 	if ($request->ajax()) {
	// 		return response()->view('artists', compact('podcasts', 'events', 'contests', 'artists'));
	// 	} else {
	// 		return view('artists', compact('podcasts', 'events', 'contests', 'artists'));
	// 	}
	// }

	// public function playlists(Request $request)
	// {
	// 	$events = Event::paginate(4);
	// 	$podcasts = Podcast::paginate(4);
	// 	$contests = Contest::paginate(4);
	// 	$playlists = Playlist::paginate(10);
	// 	if ($request->ajax()) {
	// 		return response()->view('playlists', compact('podcasts', 'events', 'contests', 'playlists'));
	// 	} else {
	// 		return view('playlists', compact('podcasts', 'events', 'contests', 'playlists'));
	// 	}
	// }

	// public function podcasts(Request $request)
	// {
	// 	$events = Event::paginate(4);
	// 	$podcasts = Podcast::paginate(4);
	// 	$contests = Contest::paginate(4);
	// 	$news = News::paginate(4);
	// 	$programs = Program::paginate(8);
	// 	if ($request->ajax()) {
	// 		return response()->view('podcasts', compact('podcasts', 'events', 'contests', 'programs', 'news'));
	// 	} else {
	// 		return view('podcasts', compact('podcasts', 'events', 'contests', 'programs', 'news'));
	// 	}
	// }

	// public function videos(Request $request)
	// {
	// 	$videos = Video::paginate(8);
	// 	$events = Event::paginate(4);
	// 	$podcasts = Podcast::paginate(4);
	// 	$contests = Contest::paginate(4);
	// 	$news = News::paginate(4);
	// 	$programs = Program::paginate(8);
	// 	if ($request->ajax()) {
	// 		return response()->view('videos', compact('podcasts', 'events', 'contests', 'programs', 'news', 'videos'));
	// 	} else {
	// 		return view('videos', compact('podcasts', 'events', 'contests', 'programs', 'news', 'videos'));
	// 	}
	// }

	// public function dedications(Request $request)
	// {
	// 	$dedications = Dedication::paginate(4);
	// 	$events = Event::paginate(4);
	// 	$podcasts = Podcast::paginate(4);
	// 	$contests = Contest::paginate(4);
	// 	$news = News::paginate(4);
	// 	$programs = Program::paginate(8);
	// 	if ($request->ajax()) {
	// 		return response()->view('dedications', compact('dedications', 'podcasts', 'events', 'contests', 'programs', 'news', 'videos'));
	// 	} else {
	// 		return view('dedications', compact('dedications', 'podcasts', 'events', 'contests', 'programs', 'news', 'videos'));
	// 	}
	// }

	// public function contests(Request $request)
	// {
	// 	$videos = Video::paginate(8);
	// 	$events = Event::paginate(4);
	// 	$podcasts = Podcast::paginate(4);
	// 	$contests = Contest::paginate(4);
	// 	$news = News::paginate(4);
	// 	$programs = Program::paginate(8);
	// 	if ($request->ajax()) {
	// 		return response()->view('contests', compact('podcasts', 'events', 'contests', 'programs', 'news', 'videos'));
	// 	} else {
	// 		return view('contests', compact('podcasts', 'events', 'contests', 'programs', 'news', 'videos'));
	// 	}
	// }

}
