<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\News;
use App\Menu;
use Auth;
use App\Player;
use App\Photo;
use App\SocNets;
use App\ExtApis;
use App\Ads;
use App\PrivacyConfigs;

class SiteInfoComposer
{

  /**
   * Create a new profile composer.
   *
   * @param  UserRepository  $users
   * @return void
   */
  public function __construct()
  { }

  /**
   * Bind data to the view.
   *
   * @param  View  $view
   * @return void
   */
  public function compose(View $view)
  {
    $menus = Menu::withCount(['menu'])->where('publish', 1)->where('parent_id', 0)->orWhere('parent_id', null)->get();
    $latest_news = News::paginate(4);
    $players = Player::orderBy('created_at','desc')->paginate(2);
    $photos = Photo::paginate(4);
    $soc_nets = SocNets::first();
    $ext_apis = ExtApis::first();
    $privacy_configs = PrivacyConfigs::first();
    $ads= Ads::first();
    $view->with('menus', $menus);
    $view->with('latest_news', $latest_news);
    $view->with('players', $players);
    $view->with('photos', $photos);
    $view->with('soc_nets', $soc_nets);
    $view->with('ext_apis', $ext_apis);
    $view->with('privacy_configs', $privacy_configs);
    $view->with('ads',$ads);
    if (Auth::check()) {
      $last_user_track = Auth::user()->tracks()->orderBy('updated_at', 'desc')->first();
      $last_user_tracks = Auth::user()->tracks()->orderBy('updated_at', 'desc')->get();
      $view->with('last_user_track', $last_user_track);
      $view->with('last_user_tracks', $last_user_tracks);
    }
  }
}
