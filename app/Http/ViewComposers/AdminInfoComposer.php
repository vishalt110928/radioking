<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\SiteInfo;
use App\ContactUs;
use App\SocialIcon;
use App\AppIcon;
use App\Station;
use App\Notification;
use App\NotificationAll;

class AdminInfoComposer
{

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
      $viewed = Notification::where('viewed','1')->count();
      $not_viewed = Notification::whereNull('viewed')->orWhere('viewed','0')->count();
      $notifications = Notification::whereNull('viewed')->orWhere('viewed','0')->get();
      $view->with('viewed',$viewed);
      $view->with('not_viewed',$not_viewed);
      $view->with('notifications',$notifications);
    }
  }