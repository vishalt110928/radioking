<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class Artist extends Model
{
	protected $table = 'artists';
	protected $fillable = ['name','image','des','publish_time','publish','nick_name','complete_name','birth_date','genres','activity','member','previous_member','nationality','instruments','websites','facebook_page','twitter_page','artists_cat_id','years_active','biography'];
	public function getPublishTimeAttribute($value)
	{
		//return $value;
		$publish_time = "";
		if($value) {
			$publish_time = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
		}        
		return $publish_time;
	}
	public function getSlugAttribute($value) {
		return  ($this->id.'-'.str_replace(' ','-',str_replace('/','-',$this->name)));
	} 

	public function setPublishTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['publish_time'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['publish_time'] = $value;
		}
	}


	public function cat() {
		return $this->belongsTo('App\ArtistsCat','artists_cat_id');
	}
}
