<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersRole extends Model
{
	protected $table = "roles";
	protected $fillable = ['role','sort_order'];
}
