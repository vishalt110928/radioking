<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class Dedication extends Model
{
   protected $table = 'dedications';
   protected $fillable = ['username','dedication_message','publish_time','publish'];

   public function getPublishTimeAttribute($value)
   {
   	//return $value;
   	$publish_time = "";
   	if($value) {
   		$publish_time = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
   	}        
   	return $publish_time;
   }

   public function setPublishTimeAttribute($value)
   {		
   	if($value) {
   		$this->attributes['publish_time'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
   	}
   	else {
   		$this->attributes['publish_time'] = $value;
   	}
   }
   
}
