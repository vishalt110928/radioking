<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Team extends Model
{
	protected $table = 'teams';
	protected $fillable = ['name','publish','des','teams_cat_id','image','role','publish_time'];
	public function getPublishTimeAttribute($value)
	{
		//return $value;
		$publish_time = "";
		if($value) {
			$publish_time = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
		}        
		return $publish_time;
	}
	public function getSlugAttribute($value) {
		return  ($this->id.'-'.str_replace(' ','-',str_replace('/','-',$this->title)));
	} 

	public function setPublishTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['publish_time'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['publish_time'] = $value;
		}
	}

	public function cat() {
		return $this->belongsTo('App\TeamsCat','teams_cat_id');
	}	
}
