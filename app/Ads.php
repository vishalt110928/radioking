<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Ads extends Model
{
	protected $table = "ads";
	protected $fillable = ['title','image','js_code','link','display_top','display_block','format','max_count','publish','campaign_start_date','campaign_end_date'];

	public function getCampaignStartDateAttribute($value)
	{
		//return $value;
		$publish_time = "";
		if($value) {
			$publish_time = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
		}        
		return $publish_time;
	}
	public function getCampaignEndDateAttribute($value)
	{
		$end_time = "";
		if($value) {
			$end_time = Carbon::parse($value)->isoFormat('D-MM-YYYY  H:mm:ss');
		}	
		return $end_time;
	}

	public function setCampaignStartDateAttribute($value)
	{		
		if($value) {
			$this->attributes['campaign_start_date'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['campaign_start_date'] = $value;
		}
	}
	public function setCampaignEndDateAttribute($value)
	{		
		if($value) {
			$this->attributes['campaign_end_date'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['campaign_end_date'] = $value;
		}
	}
}
