<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocNets extends Model
{
	protected $table = 'soc_nets';
	protected $fillable = ['facebook_url','twitter_url','youtube_url','instagram_url','snapchat_url','dailymotion_url','iphone_app_url','android_app_url','facebook_id','facebook_app_id','twitter_widget_code','twitter_consumer_key','twitter_consumer_secret','twitter_access_token','twitter_access_token_secret'];
}
