<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistsCat extends Model
{
	protected $table = 'artists_cats';
	protected $fillable = ['name','publish','parent_cat','des','sort_order'];
}
