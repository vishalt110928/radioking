<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
	protected $table = 'blocks';
	protected $fillable = ['title','publish'];

	public function pages() {
		return $this->belongsToMany('App\Page','blocks_pages','block_id','page_id')->withPivot([
			'sort_order'
		]);;
	}
}

