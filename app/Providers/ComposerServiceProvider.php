<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ComposerServiceProvider extends ServiceProvider
{
  /**
   * Register services.
   *
   * @return void
   */
  public function register()
  {
  }

  /**
   * Bootstrap services.
   *
   * @return void
   */
  public function boot(Request $request)
  {
    if(!$request->is('admin/*')){
      View::composer(
        '*', 'App\Http\ViewComposers\SiteInfoComposer'
      );
    }
    if($request->is('admin/*')){
      View::composer(
        '*', 'App\Http\ViewComposers\AdminInfoComposer'
      );
    }
  }
}
