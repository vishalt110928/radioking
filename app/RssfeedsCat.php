<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RssfeedsCat extends Model
{
	protected $table = 'rssfeeds_cats';
	protected $fillable = ['name','publish','parent_cat','des','sort_order'];
}
