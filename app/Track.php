<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Track extends Model
{
	protected $table = 'tracks';
	protected $fillable = ['title','image','artist','buy_link','publish','audio_sample','publish_time'];
	public function getPublishTimeAttribute($value)
	{
		$publish_time = "";
		if($value) {
			$publish_time = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
		}        
		return $publish_time;
	}

	public function setPublishTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['publish_time'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['publish_time'] = $value;
		}
	}
	public function user() {
		return  $this->belongsToMany('App\user','tracks_users','track_id','user_id');
	 }
}
