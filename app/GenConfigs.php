<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenConfigs extends Model
{
	protected $table = 'gen_configs';
	protected $fillable = ['site_title','page_title','keywords','meta_dis','js_script','head_code','lang','time_zone','site_maintainance','player_maintainance','text_maintainance'];
}
