<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrivacyConfigs extends Model
{
 	protected $table = 'privacy_configs';
 	protected $fillable = ['privacy_policy','policy_link','legal_notice','legal_notice_link'];
}
