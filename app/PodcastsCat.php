<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PodcastsCat extends Model
{
	protected $table = 'podcasts_cats';
	protected $fillable = ['name','publish','parent_cat','des','sort_order'];
}
