<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideosCat extends Model
{
	protected $table = 'videos_cats';
	protected $fillable = ['name','publish','parent_cat','des','sort_order'];
}
