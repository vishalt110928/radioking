<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class BlockPage extends Model
{
	public $incrementing = true;
	protected $table = 'blocks_pages';
	protected $fillable = ['block_id','page_id','sort_order'];
}
