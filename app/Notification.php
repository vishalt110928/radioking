<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Notification extends Model
{
	protected $table = "notifications";
	protected $fillable = ['title','href','viewed'];

	public function getAgoAttribute($value)
	    {
	    	$value = '';
	    	$updated_at = $this->updated_at;
	    	if($updated_at){
	    		$now = Carbon::now();
	    		$value = $now->diffInHours($updated_at);
	    		return $value;
	    	}
	        return ucfirst($value);
	    }

}
