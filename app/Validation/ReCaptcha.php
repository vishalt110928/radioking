<?php namespace App\Validation;

use GuzzleHttp\Client;
use App\ExtApis;

class ReCaptcha
{
  public function validate($attribute, $value, $parameters, $validator)
  {
    $ext_apis = ExtApis::first();
    $client = new Client();
    $response = $client->post(
      'https://www.google.com/recaptcha/api/siteverify',
      ['form_params' =>
      [
        'secret' => $ext_apis->google_api_secret,
        'response' => $value
      ]
    ]
  );

    $body = json_decode((string)$response->getBody());
    return $body->success;
  }
}