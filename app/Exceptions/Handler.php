<?php

namespace App\Exceptions;

use Exception;
use App\GenConfigs;
use App\Player;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
      'password',
      'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
      parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
      if ($exception instanceof \Illuminate\Foundation\Http\Exceptions\MaintenanceModeException) {
        $gen_configs = GenConfigs::first();
         $players = Player::orderBy('created_at','desc')->paginate(2);
        return response()->view('errors.503', ['gen_configs' => $gen_configs,'players'=>$players], 200)->header('Content-Type', 'text/html; charset=utf-8');
     }

     return parent::render($request, $exception);
   }
 }
