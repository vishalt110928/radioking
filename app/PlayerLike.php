<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerLike extends Model
{
	protected $table = 'player_likes';
	protected $fillable =['user_id','player_id'];
}
