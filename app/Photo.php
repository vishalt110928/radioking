<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
	protected $table = 'photos';
	protected $fillable = ['path','type'];

	public function gallery() {
		return $this->belongsTo('App\Gallery');
	}
}
