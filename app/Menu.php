<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
	protected $table = "menus";
	protected $fillable = ['title','url','page_id','parent_id','publish','new_tab','sort_order','level'];
	public function page() {
		return $this->belongsTo('App\Page','page_id');
	}
	public function menu() {
		return $this->hasMany('App\Menu','parent_id');
	}
}
