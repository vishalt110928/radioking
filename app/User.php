<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;
use App\Mail\SendResetPasswordLink;
use Illuminate\Support\Facades\Mail;


class User extends Authenticatable
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name','email','password','firstname','civility','birthday','mobile_no','address','city','postal_code','country','personal_presentation','hobbies','other_info','users_role_id','image'];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'email_verified_at' => 'datetime',
  ];
  public function getBirthdayAttribute($value)
  {
    //return $value;
    $birthday = "";
    if($value) {
      $birthday = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
    }        
    return $birthday;
  }

  public function setBirthdayAttribute($value)
  {   
    if($value) {
      $this->attributes['birthday'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
    }
    else {
      $this->attributes['birthday'] = $value;
    }
  }

  public function role(){
    return  $this->belongsTo('App\UsersRole','users_role_id');
  }
  public function tracks() {
   return  $this->belongsToMany('App\Track','tracks_users','user_id','track_id');
  }
 /* public function sendPasswordResetNotification($token)
  { 

    $email = $this->email;
    $mail = Mail::to($email)->send(new SendResetPasswordLink($token,$email));
    echo "mail".$mail;
    echo "user.php";
    exit; 
  }*/
}
