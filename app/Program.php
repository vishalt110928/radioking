<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Program extends Model
{
	protected $table = 'programs';
	protected $fillable = ['name','image','djs','publish_date','publish','end_date','description','programs_cat_id','start_time','end_time','link_podcast_cat','brod_days'];

	protected $casts = ['brod_days' => 'array',];

	public function getPublishDateAttribute($value)
	{
		$publish_time = "";
		if($value) {
			$publish_time = Carbon::parse($value)->isoFormat('D-MM-YYYY H:mm:ss');
		}        
		return $publish_time;
	}
	public function getEndDateAttribute($value)
	{
		$end_time = "";
		if($value) {
			$end_time = Carbon::parse($value)->isoFormat('D-MM-YYYY  H:mm:ss');
		}	
		return $end_time;
	}

	public function getStartTimeAttribute($value)
	{
		$start_time = "";
		if($value) {
			return $value;
		}        
		return $start_time;
	}

	public function getEndTimeAttribute($value)
	{
		$end_time = "";
		if($value) {
			return $value;
		}        
		return $end_time;
	}

	public function getBrodDaysAttribute($value)
	{
		$value =json_decode($value,true);
		if(is_array($value) && count($value)) {
			$value =  array_keys($value,'1',false);
			return $value;
		}
		else{
			return "";
		}
	}
	public function getSlugAttribute($value) {
		return  ($this->id.'-'.str_replace(' ','-',str_replace('/','-',$this->name)));
	} 


	public function setPublishDateAttribute($value)
	{		
		if($value) {
			$this->attributes['publish_date'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['publish_date'] = $value;
		}
	}
	public function setEndDateAttribute($value)
	{		
		if($value) {
			$this->attributes['end_date'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['end_date'] = $value;
		}
	}
	public function setStartTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['publish_date'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['publish_date'] = $value;
		}
	}
	public function setEndTimeAttribute($value)
	{		
		if($value) {
			$this->attributes['publish_date'] = Carbon::parse($value)->isoFormat('YYYY-MM-D H:mm:ss');
		}
		else {
			$this->attributes['publish_date'] = $value;
		}
	}


	public function cat() {
		return $this->belongsTo('App\NewsCat','news_cat_id');
	}
}
