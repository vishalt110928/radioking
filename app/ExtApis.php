<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtApis extends Model
{
  protected $table = 'ext_apis';
  protected $fillable = ['last_fm_key','itune_affiliate_code','ga_code','google_api','google_api_secret','mailchimp_api','unique_last_id'];
}
