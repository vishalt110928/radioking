<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('ads', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('title')->nullable();
      $table->string('image')->nullable();
      $table->string('js_code')->nullable();
      $table->string('link')->nullable();
      $table->string('display_top')->nullable();
      $table->string('display_block')->nullable();
      $table->string('max_count')->nullable();
      $table->string('format')->nullable();
      $table->dateTime('campaign_start_date')->nullable();
      $table->tinyInteger('publish')->default(1);
      $table->dateTime('campaign_end_date')->nullable();
      $table->tinyInteger('ads_type')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('ads');
  }
}
