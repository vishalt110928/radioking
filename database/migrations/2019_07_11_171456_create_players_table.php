<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('players', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('name')->nullable();
      $table->string('image')->nullable();
      $table->string('identifier')->nullable();
      $table->integer('stream_type')->nullable();
      $table->integer('active_his')->nullable();
      $table->integer('auto_play')->nullable();
      $table->integer('sort_order')->nullable();
      $table->dateTime('publish_time')->nullable();
      $table->dateTime('end_time')->nullable();
      $table->tinyInteger('publish')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('players');
  }
}
