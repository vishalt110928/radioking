<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('events', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('title')->nullable();
      $table->string('image')->nullable();
      $table->string('address')->nullable();
      $table->string('city')->nullable();
      $table->string('country')->nullable();
      $table->string('postal_code')->nullable();
      $table->dateTime('publlish_time')->nullable();
      $table->dateTime('from')->nullable();
      $table->dateTime('to')->nullable();
      $table->text('des')->nullable();
      $table->tinyInteger('publish')->default(1);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('events');
  }
}
