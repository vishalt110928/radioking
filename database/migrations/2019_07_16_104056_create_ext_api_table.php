<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtApiTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('ext_apis', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('last_fm_key')->nullable();
      $table->string('itune_affiliate_code')->nullable();
      $table->text('ga_code')->nullable();
      $table->text('google_api')->nullable();
      $table->text('google_api_secret')->nullable();
      $table->text('mailchimp_api')->nullable();
      $table->text('unique_last_id')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('ext_apis');
  }
}
