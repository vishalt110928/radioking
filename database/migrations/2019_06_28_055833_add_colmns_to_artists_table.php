<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColmnsToArtistsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('artists', function (Blueprint $table) {
      $table->string('artists_cat_id')->nullable();
      $table->tinyInteger('publish')->nullable();
      $table->dateTime('publish_time')->nullable();
      $table->string('nick_name')->nullable();
      $table->string('complete_name')->nullable();
      $table->dateTime('birth_date')->nullable();
      $table->string('genres')->nullable();
      $table->string('activity')->nullable();
      $table->string('member')->nullable();
      $table->string('previous_member')->nullable();
      $table->string('nationality')->nullable();
      $table->string('instruments')->nullable();
      $table->string('years_active')->nullable();
      $table->string('instruments')->nullable();
      $table->string('websites')->nullable();
      $table->string('facebook_page')->nullable();
      $table->string('twitter_page')->nullable();
      $table->text('biography')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('artists', function (Blueprint $table) {
      Schema::dropIfExists('artists');
    });
  }
}
