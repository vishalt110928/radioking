<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
  /**
    * Run the migrations.
    *
    * @return void
  */
  public function up()
  {
    Schema::create('banners', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('title')->nullable();
      $table->string('image')->nullable();
      $table->string('slide_url')->nullable();
      $table->dateTime('start_time')->nullable();
      $table->dateTime('end_time')->nullable();
      $table->dateTime('publlish_time')->nullable();
      $table->tinyInteger('publish')->default(1);
      $table->smallInteger('recurrence')->deafult(0);
      $table->smallInteger('sort_order')->default(0);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('banners');
  }
}
