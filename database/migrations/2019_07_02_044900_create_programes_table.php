<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('programes', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('name')->nullable();
      $table->string('image')->nullable();
      $table->string('djs')->nullable();
      $table->integer('programes_cat_id')->nullable();
      $table->dateTime('publish_date')->nullable();
      $table->dateTime('end_date')->nullable();
      $table->string('start_time')->nullable();
      $table->string('end_time')->nullable();
      $table->tinyInteger('publish')->default(1);
      $table->string('brod_days')->nullable();
      $table->string('link_podcast_cat')->nullable();
      $table->text('description')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('programes');
  }
}
