<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocNetsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('soc_nets', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('facebook_url')->nullable();
      $table->string('twitter_url')->nullable();
      $table->string('youtube_url')->nullable();
      $table->string('instagram_url')->nullable();
      $table->string('snapchat_url')->nullable();
      $table->string('dailymotion_url')->nullable();
      $table->string('iphone_app_url')->nullable();
      $table->string('android_app_url')->nullable();
      $table->string('facebook_id')->nullable();
      $table->string('facebook_app_id')->nullable();
      $table->string('twitter_widget_code')->nullable();
      $table->string('twitter_consumer_key')->nullable();
      $table->string('twitter_consumer_secret')->nullable();
      $table->string('twitter_access_token')->nullable();
      $table->string('twitter_access_token_secret')->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('soc_nets');
  }
}
