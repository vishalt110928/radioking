<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRssFeedTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('rssfeeds', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('title')->nullable();
      $table->string('image')->nullable();
      $table->string('rss_url')->nullable();
      $table->string('rssfeed_select')->nullable();
      $table->integer('rssfeeds_cat_id')->nullable();
      $table->dateTime('publish_time')->nullable();
      $table->tinyInteger('publish')->default(1);
      $table->dateTime('end_time')->nullable();
      $table->text('des')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('rssfeeds');
  }
}
