<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContestsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('contests', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('title')->nullable();
      $table->string('image')->nullable();
      $table->dateTime('contest_start_date')->nullable();
      $table->dateTime('contest_end_date')->nullable();
      $table->dateTime('publish_time')->nullable();
      $table->tinyInteger('publish')->nullable();
      $table->dateTime('end_time')->nullable();
      $table->string('question')->nullable();
      $table->text('des')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('contests');
  }
}
