<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrivacyConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('privacy_configs', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->text('privacy_policy')->nullable();
        $table->tinyInteger('policy_link')->nullable();
        $table->text('legal_notice')->nullable();
        $table->tinyInteger('legal_notice_link')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('privacy_configs');
    }
  }
