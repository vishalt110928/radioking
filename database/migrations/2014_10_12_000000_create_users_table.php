<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('users', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('name')->nullable();
        $table->string('email')->unique();
        $table->timestamp('email_verified_at')->nullable();
        $table->string('password');
        $table->string('firstname')->nullable();
        $table->string('civility')->nullable();
        $table->dateTime('birthday')->nullable();
        $table->string('mobile_no')->nullable();
        $table->string('address')->nullable();
        $table->string('city')->nullable();
        $table->string('postal_code')->nullable();
        $table->tinyInteger('country')->default(0);
        $table->text('personal_presentation')->nullable();
        $table->text('hobbies')->nullable();
        $table->text('other_info')->nullable();
        $table->rememberToken();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('users');
    }
  }
