<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePlaylistsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('playlists', function (Blueprint $table) {
      $table->string('artist')->nullable();
      $table->string('buy_link')->nullable();
      $table->string('audio_sample')->nullable();
      $table->tinyInteger('publish')->default(0);
      $table->integer('playlists_cat')->nullable();

    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('playlists', function (Blueprint $table) {
          //
    });
  }
}
