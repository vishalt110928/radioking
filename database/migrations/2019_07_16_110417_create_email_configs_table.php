<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailConfigsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('email_configs', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('sender')->nullable();
      $table->string('host')->nullable();
      $table->string('port')->nullable();
      $table->string('user')->nullable();
      $table->string('password')->nullable();
      $table->string('con_security')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('email_configs');
  }
}
