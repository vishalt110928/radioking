<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenreralConfigs extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('gen_configs', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('site_title')->nullable();
      $table->string('page_title')->nullable();
      $table->string('keywords')->nullable();
      $table->string('meta_dis')->nullable();
      $table->string('js_script')->nullable();
      $table->string('head_code')->nullable();
      $table->string('lang')->nullable();
      $table->string('time_zone')->nullable();
      $table->tinyInteger('site_maintainance')->nullable();
      $table->tinyInteger('palyer_maintainance')->nullable();
      $table->string('text_maintainance')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('gen_configs');
  }
}
