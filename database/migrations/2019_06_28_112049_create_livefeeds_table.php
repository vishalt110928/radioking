<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivefeedsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('livefeeds', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('message')->nullable();
      $table->string('image')->nullable();
      $table->string('video')->nullable();
      $table->tinyInteger('publish')->nullable();
      $table->tinyInteger('survey')->nullable();
      $table->dateTime('publish_time')->nullable();
      $table->dateTime('end_time')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('livefeeds');
  }
}
