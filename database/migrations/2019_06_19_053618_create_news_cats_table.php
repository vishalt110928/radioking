<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsCatsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('news_cats', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('name')->nullable();
      $table->tinyInteger('publish')->nullable();
      $table->string('parent_cat')->nullable();
      $table->text('des')->nullable();
      $table->integer('sort_order')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('news_cats');

   // ALTER TABLE `news` CHANGE `category` `news_cat_id` INT(11) NULL DEFAULT NULL;
  }
}
