<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('programs', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('name')->nullable();
      $table->string('image')->nullable();
      $table->string('djs')->nullable();
      $table->string('city')->nullable();
      $table->string('country')->nullable();
      $table->string('postal_code')->nullable();
      $table->dateTime('publlish_time')->nullable();
      $table->dateTime('end_date')->nullable();
      $table->dateTime('start_time')->nullable();
      $table->dateTime('end_time')->nullable();
      $table->string('broadcast_days')->nullable();
      $table->string('podcast_cat')->nullable();
      $table->text('des')->nullable();
      $table->tinyInteger('publish')->default(1);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('programs');
  }
}
