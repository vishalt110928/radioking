<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDedicationsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('dedications', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('username')->nullable();
      $table->dateTime('dedication_date')->nullable();
      $table->string('dedication_message')->nullable();
      $table->tinyInteger('publish')->default(0);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('dedications');
  }
}
