<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('menus', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('title')->nullable();
      $table->string('url')->nullable();
      $table->integer('page_id')->nullable();
      $table->integer('parent_id')->nullable();
      $table->tinyInteger('publish')->nullable();
      $table->tinyInteger('new_tab')->nullable();
      $table->integer('sort_order')->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('menu', function (Blueprint $table) {
          //
    });
  }
}
