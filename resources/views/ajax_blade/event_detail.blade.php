
<script type="text/javascript"></script>

<div class="container m-b-15 centered">
</div>

<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="{{url('/')}}" class="toptip detail" title="Home"><i class="fa fa-home"></i></a></li>
			<li><a href="{{url('events')}}" class="detail">Events</a></li>
			<li><a href="#">{{$event->title}}</a></li>
			<li class="active">{{$event->title}}</li>
		</ol>
	</div>
</div>



<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						{{$event->title}}
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>


	<div class="row">

		<div class="col-xs-12  col-md-8  ">
			<div class="m-t-30">
				<div class="article afficher-content">
					<div class="wrap">
						<img src="{{url('public/uploads/'.$event->image)}}" style="width: 100%;" class="img-responsive m-b-5 m-t-20 rect_750_450" />
						<span class="infos-article">
							<i class="fa fa-clock"></i>{{ \Carbon\Carbon::parse($event->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')}}
						</span>
						<div class="m-t-15 m-b-15"></div>
						<p>{{$event->des}}</p>
					</div>
				</div>

				<div id="comments" class="row comment-row m-t-20">
					<div class="col-xs-12">
						<!-- COMMENTAIRES -->
						<div class="form-group">
							<span class="comment-img avatar-default"><i class="fa fa-user"></i></span>
							<div class="fb-comments" data-href="{{request()->fullUrl()}}" data-width="" data-numposts="5"></div>
						</div>

						<div class="form-group" style="float:right;">
							<div class="col-xs-3">
								<a href="javascript:void(0)" id="sendComment" class="btn btn-primary">Comment</a>
							</div>
						</div>
						<div class="clear"></div>
						<div id="list-comments">

						</div>

						<div class="row">
							<div class="col-xs-12">
							</div>
						</div>

						<!-- Fin commentaires -->
					</div>
				</div>
				<div id="fb-root"></div>
				<script crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
			</div>
		</div>
	</div>
</div>
