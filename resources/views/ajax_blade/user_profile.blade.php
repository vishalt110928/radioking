<div class="container m-b-15 centered">
</div>

<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="{{url('/')}}" class="toptip" title="Home"><i class="fa fa-home"></i></a></li>
			<li class="active">Member area</li>
		</ol>
	</div>
</div>



<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						Member area </h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>
	<div class="row">
		<div class="col-xs-12  col-md-8  list_display">
			<!-- Affichage contenu -->
			<div class="m-t-30">
				<div class="row listed" id="user-profile">
					<!-- _________________________ Start true content _________________________ -->
					@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					@if(session()->has('success'))
					<div class="alert alert-success  alert-dismissible">
						<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong>Success!</strong> {{session('success')}}
					</div>
					@elseif(session()->has('error'))
					<div class="alert alert-danger  alert-dismissible">
						<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<strong>Danger!</strong> {{session('error')}}
					</div>
					@endif
					<div class="col-xs-12">
						<h2>Edit profile</h2>
						<legend>Profile</legend>
					</div>
					{{Form::open(['url' => 'user_profile_store/'.$id, 'method' => 'put' , 'class' =>'card' , 'files' => true , 'id'=>'profile_update'])}}
					@csrf
					<div class="col-xs-12 col-sm-3" style="float : left; text-align: center;">
						<span class="image_shadow_container">
							<img class="img-thumbnail" id="photo" name="photo" src="{{url('public/uploads/'.auth()->user()->image)}}" alt="" /></a>
						</span>
						<span class="btn btn-primary btn-file">
							<span class="fileupload-new">Select file</span>
							<input id="fileupload" type="file" name="image" accept="image/*" />
						</span>
						<input type="checkbox" name="imageRemoved" class="imageRemoved hidden" value="0">
					</div>
					<div class="col-xs-12 col-sm-9" style="float : left;">
						<div class="form-group">
							<label class="form-label">Enter Name</label>
							{{Form::text('name',$auth_user->name,['placeholder'=>'Enter Name' , 'class'=>'form-control'])}}
						</div>
						<div class="form-group">
							<label class="form-label">Enter Civility</label>
							{{Form::select('civility',array('1'=>'Mr','2'=>'Mrs'),$auth_user->civility, ['placeholder' => 'Select Civility','class'=>'form-control'])}}
						</div>
						<div class="form-group">
							<label class="form-label">Enter FirstName</label>
							{{Form::text('firstname',$auth_user->firstname,['placeholder'=>'Enter FirstName' , 'class'=>'form-control'])}}
						</div>
						<div class="form-group">
							<label class="form-label">Enter BirthDay</label>
							{{Form::text('birthday',$auth_user->birthday,['placeholder'=>'Enter Birthday' , 'class'=>'form-control'])}}
						</div>
						<div class="form-group">
							<label class="form-label">Enter Mobile No</label>
							{{Form::text('mobile_no',$auth_user->mobile_no,['placeholder'=>'Enter Mobile No' , 'class'=>'form-control'])}}
						</div>
						<div class="form-group">
							<label class="form-label">Enter Address</label>
							{{Form::text('address',$auth_user->address,['placeholder'=>'Enter Address' , 'class'=>'form-control'])}}
						</div>
						<div class="form-group">
							<label class="form-label">Enter City</label>
							{{Form::text('city',$auth_user->city,['placeholder'=>'Enter City' , 'class'=>'form-control'])}}
						</div>
						<div class="form-group">
							<label class="form-label">Enter Country</label>
							{{Form::text('country',$auth_user->country,['placeholder'=>'Enter Country' , 'class'=>'form-control'])}}
						</div>
						<div class="form-group">
							<label class="form-label">Enter Postal Code</label>
							{{Form::text('postal_code',$auth_user->postal_code,['placeholder'=>'Enter Postal Code' , 'class'=>'form-control'])}}
						</div>
						<div class="form-group">
							<label class="form-label">Personal Persentation</label>
							{{ Form::textarea('personal_presentation',$auth_user->personal_presentation, ['class' => 'form-control','size' =>'50x3']) }}
						</div>
						<div class="form-group">
							<label class="form-label">Hobbies</label>
							{{ Form::textarea('hobbies',$auth_user->hobbies, ['class' => 'form-control','size' =>'50x3']) }}
						</div>
						<div class="form-group">
							<label class="form-label">Other Info</label>
							{{ Form::textarea('other_info',$auth_user->other_info, ['class' => 'form-control','size' =>'50x3']) }}
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 m-t-20">
				<legend>Login information</legend>
				<div class="form-group">
					<label class="form-label">Enter Email</label>
					{{Form::text('email',$auth_user->email,['placeholder'=>'Enter Email' ,'class'=>'form-control'])}}
				</div>
				<div class="form-group">
					<label class="form-label">Enter Password</label>
					{{Form::password('password',['placeholder'=>'Enter Password' ,'class'=>'form-control'])}}
				</div>
				<div class="form-group">
					<label class="form-label">Enter Password Confirmation</label>
					{{Form::password('password_confirmation',['placeholder'=>'Enter Password' ,'class'=>'form-control'])}}
				</div>
				<div class="m-t-10">
					<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
					{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
				</div>
			</div>
			{{Form::close()}}
			<a href="{{url('delete_account')}}" class="delete_link">Deleting your account</a>
			<!-- _________________________ end true Content _________________________ -->
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#fileupload').change(function() {
			$('#photo').css('display', 'block');
			var input = this;
			var url = $(this).val();
			var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
			if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('#photo').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			} else {
				$('#photo').attr('src', '/assets/no_preview.png');
			}
		});
	})
</script>