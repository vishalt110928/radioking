<div class="container m-b-15 centered">
</div>
<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="{{url('/home')}}" class="toptip detail" title="Home"><i class="fa fa-home"></i></a></li>
			<li class="active">Events</li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						Events
						<a class="rss-link" href="#" target="_blank">
							<img class="rss-logo" style="width: 13px; margin-top: 1px;" src="{{url('public/img/rss.png')}}" />
							<span class="m-l-0 m-r-0 rss-text">RSS</span>
						</a>
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>

	<div class="row">

		<div class="col-xs-12  col-md-8  list_display">
			<!-- Affichage contenu -->
			<div class="m-t-30">
				@foreach($pages['events'] as $event)
				<div class="current_date m-b-20 m-t-20">@if($loop->first) {{'currently'}} @else @if($event->publish_time)
					{{ \Carbon\Carbon::parse($event->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')}}
					@endif @endif</div>

				<div class="row simple_elements list_element light_shadow boxed m-l-0 m-r-0 m-b-20 list_equipe">
					<a class="whole-div-link img-link detail" href="{{url('event_detail/'.$event->slug)}}" class="list-action-zone">
						<div class="col-xs-4 p-l-0 p-r-0">
							<div class="cover-preview list-img-zone">
								<img class="list-img-thumb news_sidebar_other" src="{{url('public/uploads/'.$event->image)}}">
							</div>
						</div>
					</a>
					<div class="col-xs-8">
						<a class="whole-div-link title-link detail" href="{{url('event_detail/'.$event->slug)}}" class="list-action-zone">
							<h3 class="ellipsis ellipsis-2l m-t-15 m-b-10">
								{{$event->title}} </h3>
						</a>
						<span class="ellipsis ellipsis-2l hidden-xs list_text">
							{{$event->des}} </span>
					</div>
				</div>
				@endforeach
				<div class="clear"></div>
			</div>
			<div class="pagination">
				{{$pages['events']->links()}}
			</div>
		</div>
		<div class="hidden-xs hidden-sm col-md-4" id="sidebar">
		@foreach($sidebars as $sidebar)
		@include('aside.'.$sidebar)
		@endforeach 
		</div>
	</div>
</div>
</div>