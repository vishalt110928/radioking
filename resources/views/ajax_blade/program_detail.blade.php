<script type="text/javascript"></script>

<div class="container m-b-15 centered">
</div>

<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="/" class="toptip" title="Home"><i class="fa fa-home"></i></a></li>
			<li><a href="{{url('programs')}}">Programs</a></li>
			<li class="active">{{$program->name}}</li>
		</ol>
	</div>
</div>



<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						{{$program->name}} </h1>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container body_wrap boxed">
		<div class="layout-headtitle-border"></div>


		<div class="row">

			<div class="col-xs-12  col-md-8  ">
				<!-- Affichage contenu -->
				<div class="m-t-30">
					<div class="article afficher-content">
						<div class="wrap bloc-par2">
							<img src="{{url('public/uploads/'.$program->image)}}" style="width: 100%;" class="img-responsive m-b-15 m-t-20 rect_750_450" />
							<div class="m-t-0 m-b-25 program-date">
								@if(is_array($program->brod_days))
								@if(in_array('all',$program->brod_days))
								{{'all'}}
								@else
								{{implode(',',$program->brod_days)}}
								@endif
								@else
								{{$program->brod_days}}
								@endif	
								,
								From {{$program->start_time}} to {{$program->end_time}}
							</div>
							<p>{{$program->description}}</p>
							<h3 class="m-t-30 m-b-20">Program DJ(s)</h3>
							<div class="row m-l-0 m-r-0 m-b-25 anim-row">
								<div class="col-xs-6 m-b-10">
									<div class="row">
										<div class="col-xs-3 m-l-0 m-r-0 p-l-0 p-r-0">
										</div>
										<div class="col-xs-9">
											<div>
												<a href="/team/mirko-1">
													<h4 class="m-t-0 m-b-5 ellipsis ellipsis-2l">{{$program->djs}}</h4>
												</a>
											</div>
											<span class="anim-fonction">DJ</span>
										</div>
									</div>
								</div>
							</div>



							<h3 class="m-t-30 m-b-20">Program Podcasts</h3>
							<div class="row anim-row row-m-10-xs">
								@foreach($program_same_cats as $program_same_cats)
								<div class="col-xs-6 p-l-10-xs p-r-10-xs">
									<a href="{{url('news_detail/'.$program_same_cats->id)}}">
										<div class="cover-preview">
											<img src="{{url('public/uploads/'.$program_same_cats->image)}}" alt="{{$program_same_cats->title}}" class="news_sidebar_other"/>
										</div>
									</a>
									<a href="{{url('news_detail/'.$program_same_cats->id)}}">
										<h4>{{$program_same_cats->title}}</h4>
									</a>
								</div>
								@endforeach
							</div>
						</div>
					</div>
					<div id="comments" class="row comment-row m-t-20">
						<div class="col-xs-12">
							<!-- COMMENTAIRES -->
							<div class="form-group">
								<span class="comment-img avatar-default"><i class="fa fa-user"></i></span>
								<div class="fb-comments" data-href="{{request()->fullUrl()}}" data-width="" data-numposts="5"></div>
							</div>

							<div class="form-group" style="float:right;">
								<div class="col-xs-3">
									<a href="javascript:void(0)" id="sendComment" class="btn btn-primary">Comment</a>
								</div>
							</div>
							<div class="clear"></div>
							<div id="list-comments">

							</div>

							<div class="row">
								<div class="col-xs-12">
								</div>
							</div>

							<!-- Fin commentaires -->
						</div>
					</div>
					<div id="fb-root"></div>
					<script crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
					<script type="text/javascript">
						FB.XFBML.parse();
					</script>
				</div>
				<div class="hidden-xs hidden-sm col-md-4" id="sidebar">
					@foreach($sidebars as $sidebar)
					@include('aside.'.$sidebar)
					@endforeach
				</div>
			</div>
		</div>