
<script type="text/javascript"></script>

<div class="container m-b-15 centered">
</div>

<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="index.html" class="toptip" title="Home"><i class="fa fa-home"></i></a></li>
			<li class="active">Played tracks</li>
		</ol>
	</div>
</div>



<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						Played tracks <a class="rss-link" href="rss-feed-30-player-1" target="_blank"><img class="rss-logo" style="width: 13px; margin-top: 1px;" src="images/rss.png" /><span class="m-l-0 m-r-0 rss-text">RSS</span></a> </h1>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container body_wrap boxed">
		<div class="layout-headtitle-border"></div>


		<div class="row">

			<div class="col-xs-12  col-md-8  list_display">
				<!-- Affichage contenu -->
				<div class="m-t-30">
					<div class="ckoi_form ckoi_bureau" id="liste_titres">
					</div>
					<div class="article">

						<div class="row ckoi_row track-row musiquetopckoi-row m-l-0 m-r-0 m-t-20 m-b-10 boxed" id="topvote">
							@foreach($tracks as $track)
							<div class="col-xs-3 col-sm-2 song-img-zone p-l-10-xs p-r-10-xs">
								<a>
									<div class="cover-preview">
										<span class="cover-infos">
											<strong></strong>
										</span>
										<img src="{{url('public/uploads/'.$track->image)}}" alt="{{$track->title}}" class="song-img" />
									</div>
								</a>
							</div>

							<div class="col-xs-5 col-sm-6 title-col-liste bigger-480">
								<h2 class="artiste ellipsis ellipsis-1l m-t-5 m-b-0">
									{{$track->title}} <div class="box-play like-music like-inline">
										<span class="hidden datastring"></span>
										<a href="javascript:;">
											<i class="fa fa-fw fa-heart-o"></i>
										</a>
									</div>
								</h2>
								<p class="title ellipsis ellipsis-1l">Adele</p>
							</div>

							<div class="col-xs-4 hidden-480 btn-zone p-r-10-xs">
								<br /><span class="hidden" id="avote_1"></span>
							</div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="pagination">
					{{$tracks->links()}}
				</div>
			</div>
			<div class="hidden-xs hidden-sm col-md-4" id="sidebar">
					@foreach($sidebars as $sidebar)
					@include('aside.'.$sidebar)
					@endforeach
				</div>
		</div>
	</div>