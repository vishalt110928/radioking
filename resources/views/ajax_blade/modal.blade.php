<div id="modal-dedicaces" class="modal fade" role="dialog" data-backdrop="static">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i>
				</button>
				<h4>Send a dedication</h4>
			</div>
			<div class="modal-body">
				<div id="connexion-ajax-errors" style="display : none;"></div>
				<form method="post" id="ajoutform" action="{{url('send_dedication')}}">
					<div class="form-group">
						<input type="text" class="form-control" id="pseudo" name="pseudo" placeholder="Username" value="" maxlength="12" required aria-describedby="inputErrorUsernameDedi" />
					</div>
					<div class="form-group">
						<textarea name="message" id="message" class="form-control" maxlength="300" rows="2" placeholder="Message" required aria-describedby="inputErrorMessageDedi"></textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button id="envoyer-dedicace" class="modal-connexion btn btn-primary">Send my dedication</button>
			</div>
		</div>
	</div>
</div>


<div id="modal_mdp_oublie" class="modal fade" role="dialog" data-backdrop="static">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i>
				</button>
				<h4>Forgot your password? No problem.</h4>
			</div>
			<div class="modal-body">
				<div id="mdp_oublie_errors" style="display : none;"></div>
				<form method="post" id="mdpoublieformnouveau" action="/utilisateurs/index/oublie">
					<div class="form-group">
						<label for="mail_client" class="headline headlinemodal">Type your e-mail address here :</label>
						<input type="text" class="form-control" id="mail_client" name="mail_client" placeholder="Mail" required />
						<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button class="mdpoublie_client modal-mdp-oublie btn btn-success">Send</button>
			</div>
		</div>
	</div>
</div>