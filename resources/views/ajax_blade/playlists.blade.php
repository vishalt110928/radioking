<div class="container m-b-15 centered">
</div>
<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="{{url('/home')}}" class="toptip detail" title="Home"><i class="fa fa-home"></i></a></li>
			<li class="active">Playlists</li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						Playlists
						<a class="rss-link" href="#" target="_blank">
							<img class="rss-logo" style="width: 13px; margin-top: 1px;" src="{{url('public/img/rss.png')}}" />
							<span class="m-l-0 m-r-0 rss-text">RSS</span>
						</a>
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>
	<div class="row">
		<div class="col-xs-12  col-md-8  list_display">
			<!-- Affichage contenu -->
			<div class="m-t-30">
				<div class="article row track-row row-m-10-xs bloc-par2">
					@foreach($pages['playlists'] as $playlist)
					<div class="layout-bloc col-sm-6 col-xs-6 m-b-40 p-l-10-xs p-r-10-xs">
						<div class="light_shadow boxed">
							<div class="col-sm-6 col-xs-12 p-l-0 p-r-0">
								<span class="box-play like-music no-float like-oncover">
									<span class="hidden datastring"></span>
									<a href="javascript:;">
										<i class="fa fa-fw fa-heart-o"></i>
									</a>
								</span>
								<a class="whole-div-link" class="">
									<div class="cover-preview list-img-zone">
										<img class="list-img-thumb square_180" style='width: 100%;' src="{{url('public/uploads/'.$playlist->image)}}">
										<div class="cover-play"
										onclick='playFile("{{url('public/uploads/'.$playlist->audio_sample)}}","{{$playlist->title}}", "{{url('public/uploads/'.$playlist->image)}}","{{$playlist->id}}","playlist","1","1")'>
										<i class="fa fa-play-circle"></i>
									</div>
								</div>
							</a>
						</div>
						<div class="col-sm-6 col-xs-12 p-l-0-xs p-r-0-xs">
							<h4 class="bloc-track-title ellipsis ellipsis-2l">
								{{$playlist->title}}
							</h4>
							<span class="bloc-track-artist ellipsis ellipsis-2l">{{$playlist->artist}}</span>
							<a target="itunes_store" class="btn btn-default-bloc detail" href="{{$playlist->buy_link}}">
								<i class="fa fa-shopping-cart"></i> Buy this track
							</a>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
		<div class="pagination">
			{{$pages['playlists']->links()}}
		</div>
	</div>
	<div class="hidden-xs hidden-sm col-md-4" id="sidebar">
		@foreach($sidebars as $sidebar)
		@include('aside.'.$sidebar)
		@endforeach
	</div>
</div>
</div>
</div>