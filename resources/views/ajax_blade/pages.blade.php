@if(! isset($notpushstate) || ($notpushstate != '1'))
@extends('layouts.web')
@section('slider')
@endif
<div class="container m-b-15 centered">
</div>
<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="index.html" class="toptip" title="Home"><i class="fa fa-home"></i></a></li>
			<li class="active">@isset($page->title){{$page->title}} @endisset</li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						@isset($page->title){{$page->title}} @endisset
						<a class="rss-link" href="#" target="_blank">
							<img class="rss-logo" style="width: 13px; margin-top: 1px;" src="{{url('public/img/rss.png')}}" />
							<span class="m-l-0 m-r-0 rss-text">RSS</span>
						</a>
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>
@if(! isset($notpushstate) || ($notpushstate != '1'))
@endsection
@section('content')
@endif
<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>
	<div class="row">
		<div class="col-xs-12  col-md-8  list_display">
			<!-- Affichage contenu -->
			<div class="m-t-30">
				@if(isset($page->content))
				{{$page->content}}
				@elseif(isset($page['content']))
				{{$page['content']}}
				@endif
			</div>
		</div>

		<div class="hidden-xs hidden-sm col-md-4" id="sidebar">
		@foreach($sidebars as $sidebar)
		@include('aside.'.$sidebar)
		@endforeach
		</div>
	</div>
</div>
</div>
@if(! isset($notpushstate) || ($notpushstate != '1'))
@endsection
@endif