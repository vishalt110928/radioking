<div class="container m-b-15 centered">
</div>
<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="index.html" class="toptip" title="Home"><i class="fa fa-home"></i></a></li>
			<li class="active">Contact Us</li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						Contact Us
						<a class="rss-link" href="#" target="_blank">
							<img class="rss-logo" style="width: 13px; margin-top: 1px;" src="{{url('public/img/rss.png')}}" />
							<span class="m-l-0 m-r-0 rss-text">RSS</span>
						</a>
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>
	<div class="row">

		<div class="col-xs-12  col-md-8  list_display">
			<!-- Affichage contenu -->
			<div class="m-t-30">
				<div class="alert alert-danger" style="display: none;">
					Your message could not be sent.<br />There are errors in the form.</div>
				<div class="alert alert-success form_success" style="display: none;">Thanks ! <br /> Your message have been sent.</div>
				<form action="/contact_us_store" class="no-ajaxy row" method="post" id="contactform">
					@csrf
					<div class="form-group col-sm-6">
						<label for="name" class="control-label">Name <span class="yellow">*</span></label>
						<div>
							<input type="text" class="form-control" id="name" name="name" placeholder="Name" value="" maxlength="30" />
							<div class="contact_name"></div>
						</div>
					</div>
					<div class="form-group col-sm-6">
						<label for="email" class="control-label">Email <span class="yellow">*</span></label>
						<div>
							<input class="form-control" type="text" name="email" id="email" value="" />
							<div class="contact_email"></div>
						</div>
					</div>

					<div class="form-group col-sm-6">
						<label for="url" class="control-label">Phone number</label>
						<input class="form-control" type="text" name="phone_no" id="phone-number" value="" size="22""/>
						<div class= " contact_phone_no"></div>
			</div>
			<div class=" form-group col-sm-6">
				<label for="subject" class="control-label">Subject <span class="yellow">*</span></label>
				<div>
					<input class="form-control" type="text" name="subject" id="subject" value="" maxlength="100" />
					<div class="contact_subject"></div>
				</div>
			</div>

			<div class="form-group col-sm-12">
				<label for="message" class="control-label">Message <span class="yellow">*</span></label>
				<div>
					<textarea class="form-control" name="message" id="message" rows="5"></textarea>
					<div class="contact_message"></div>
				</div>
			</div>
			<div class="form-group" style="float:right;">
				<div class="col-xs-3 m-t-15">
					<button class="btn btn-primary" type="submit" class="contact_us_submit"><i class="fa fa-send"></i> Send message</button>
				</div>
			</div>
			</form>
			<div class="alert alert-success" style="display: none;" id="contact-send-success">
				Thanks ! <br /> Your message have been sent.</div>
		</div>
	</div>
	<div class="hidden-xs hidden-sm col-md-4" id="sidebar">
		@foreach($sidebars as $sidebar)
		@include('aside.'.$sidebar)
		@endforeach
	</div>
</div>
</div>
</div>
<script>
	$(document).ready(function() {
		$("#contactform").validate({
			rules: {
				name: {
					required: true
				},
				email: {
					required: true,
					email: true
				},
				phone_no: {
					digits: true
				},
				subject: {
					required: true,
					maxlength: 100
				},
				message: {
					required: true,
				}
			},
			messages: {},
			submitHandler: function(form) {
				var formData = $(form).serialize();
				$.ajax({
					method: "POST",
					url: "{{url('/contact_us_store')}}",
					dataType: "json",
					data: formData,
					success: function(response) {
						alert(response.message);
						$('#contactform')[0].reset();
					},
					error: function(response) {
						console.log(response.status)
						if (response.status == '419') {
							window.location.reload();
						}
						if (response.status == "422") {
							var errors = response.responseJSON.errors;
							for (key in errors) {
								$('#contactform').find('.contact_' + key).html('').html(errors[key]);
							}
						} else {
							alert('something have gone wrong');
						}
					}
				});
				return false;
			}
		});
	});
</script>