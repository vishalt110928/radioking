<script type="text/javascript"></script>

<div class="container m-b-15 centered">
</div>

<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="/" class="toptip" title="Home"><i class="fa fa-home"></i></a></li>
			<li><a href="/news-1">News</a></li>
			<li class="active">{{$new->title}}</li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						{{$new->title}}
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>


	<div class="row">

		<div class="col-xs-12  col-md-8  ">
			<!-- Affichage contenu -->
			<div class="m-t-30">
				<div class="article afficher-content">
					<div class="wrap">
						<img src="{{url('public/uploads/'.$new->image)}}" style="width: 100%;" class="img-responsive m-b-5 m-t-20" />
						<br />
						<span class="infos-article">
							<i class="fa fa-clock"></i> {{ \Carbon\Carbon::parse($new->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')}} - <i class="fa fa-eye"></i> {{$new->views}} views</span>
						<p>{{$new->description}}</p>
					</div>

					<div class="same-subject">
						<h3 class="m-t-30 m-b-20">See also</h3>
						<div class="row anim-row row-m-10-xs">
							@foreach($news_same_cats as $news_same_cat)
							<div class="col-xs-6 p-l-10-xs p-r-10-xs">
								<a href="{{url('news_detail/'.$news_same_cat->id)}}">
									<div class="cover-preview">
										<img src="{{url('public/uploads/'.$news_same_cat->image)}}" alt="{{$news_same_cat->title}}" class="rect_360_240" />
									</div>
								</a>
								<a href="{{url('news_detail/'.$news_same_cat->id)}}">
									<h4>{{$news_same_cat->title}}</h4>
								</a>
							</div>
							@endforeach
						</div>
					</div>
				</div>

				<div id="comments" class="row comment-row m-t-20">
					<div class="col-xs-12">
						<div class="form-group">
							<span class="comment-img avatar-default"><i class="fa fa-user"></i></span>
							<div class="fb-comments" data-href="{{request()->fullUrl()}}" data-width="" data-numposts="5"></div>
						</div>

						<div class="form-group" style="float:right;">
							<div class="col-xs-3">
								<a href="javascript:void(0)" id="sendComment" class="btn btn-primary">Comment</a>
							</div>
						</div>
						<div class="clear"></div>
						<div id="list-comments">

						</div>

						<div class="row">
							<div class="col-xs-12">
							</div>
						</div>

						<!-- Fin commentaires -->
					</div>
				</div>
				<div id="fb-root"></div>
				<script crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
				<script type="text/javascript">
					FB.XFBML.parse();
				</script>
			</div>
		</div>
		<div class="hidden-xs hidden-sm col-md-4" id="sidebar">
			@foreach($sidebars as $sidebar)
			@include('aside.'.$sidebar)
			@endforeach
		</div>
	</div>
</div>