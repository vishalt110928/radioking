<div class="container m-b-15 centered">
</div>
<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="{{url('/home')}}" class="toptip detail" title="Home"><i class="fa fa-home"></i></a></li>
			<li class="active">Programs</li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						Programs
						<a class="rss-link" href="#" target="_blank">
							<img class="rss-logo" style="width: 13px; margin-top: 1px;" src="{{url('public/img/rss.png')}}" />
							<span class="m-l-0 m-r-0 rss-text">RSS</span>
						</a>
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>
	<div class="row">
		<div class="col-xs-12  col-md-8  list_display">
			<!-- Affichage contenu -->
			<div class="m-t-30">
				<ul id="catpicker" class="pagination pagination-sm pagination-custom pagination-emissions">
					<li>
						<a href="{{url('programs?brod_days=all')}}" id="allcat" class="@if(isset($pages['brod_days']) && $pages['brod_days'] == 'all') active @endif button detail">
							<span class="hidden-xs">All</span>
							<span class="hidden-sm hidden-md hidden-lg">All</span>
						</a>
					</li>
					<li>
						<a href="{{url('programs?brod_days=mon')}}" id="lundi" class="filter button @if(isset($pages['brod_days']) && $pages['brod_days'] == 'mon')active @endif detail">
							<span class="hidden-xs">Monday</span>
							<span class="hidden-sm hidden-md hidden-lg">Mo</span>
						</a>
					</li>
					<li>
						<a href="{{url('programs?brod_days=tue')}}" id="mardi" class="filter button detail @if(isset($pages['brod_days']) && $pages['brod_days'] == 'tue') active @endif">
							<span class="hidden-xs">Tuesday</span>
							<span class="hidden-sm hidden-md hidden-lg">Tu</span>
						</a>
					</li>
					<li>
						<a href="{{url('programs?broad_days=wed')}}" id="mercredi" class="filter button detail @if(isset($pages['brod_days']) && $pages['brod_days'] == 'wed') active @endif">
							<span class="hidden-xs">Wed.</span>
							<span class="hidden-sm hidden-md hidden-lg">We</span>
						</a>
					</li>
					<li>
						<a href="{{url('programs?brod_days=thr')}}" id="jeudi" class="filter button detail @if(isset($pages['brod_days']) && $pages['brod_days'] == 'thu') active @endif">
							<span class="hidden-xs">Thurs.</span>
							<span class="hidden-sm hidden-md hidden-lg">Th</span>
						</a>
					</li>
					<li>
						<a href="{{url('programs?brod_days=fri')}}" id="vendredi" class="filter button detail @if(isset($pages['brod_days']) && $pages['brod_days'] == 'fri') active @endif">
							<span class="hidden-xs">Friday</span>
							<span class="hidden-sm hidden-md hidden-lg">Fr</span>
						</a>
					</li>
					<li>
						<a href="{{url('programs?brod_days=sat')}}" id="samedi" class="filter button detail @if(isset($pages['brod_days']) && $pages['brod_days'] == 'sat') active @endif">
							<span class="hidden-xs">Sat.</span>
							<span class="hidden-sm hidden-md hidden-lg">Sa</span>
						</a>
					</li>
					<li>
						<a href="{{url('programs?brod_days=sun')}}" id="dimanche" class="filter button detail @if(isset($pages['brod_days']) && $pages['brod_days'] == 'sun') active @endif">
							<span class="hidden-xs">Sunday</span>
							<span class="hidden-sm hidden-md hidden-lg">Su</span>
						</a>
					</li>
				</ul>
				@foreach($pages['programs'] as $program)
				<div class="row other_elements list_element light_shadow boxed m-l-0 m-r-0 m-b-20 discounted-item lundi mardi mercredi jeudi vendredi ">
					<div class="col-xs-4 p-l-0 p-r-0">
						<a class="whole-div-link img-link detail" href="{{url('program_detail/'.$program->slug)}}" >
							<div class="cover-preview list-img-zone">
								<img class="list-img-thumb news_sidebar_other" src="{{url('public/uploads/'.$program->image)}}">
							</div>
						</a>
					</div>
					<div class="col-xs-8 p-r-10-xs">

						<a class="whole-div-link title-link detail" href="{{url('program_detail/'.$program->slug)}}" >
							<h4 class="ellipsis ellipsis-2l m-t-10 m-b-10 m-t-5-xs m-b-0-xs">
								{{$program->name}} </h4>
						</a>
						<div class="m-t-0 m-b-15 m-b-0-xs program-date">
							@if(is_array($program->brod_days))
							@if(in_array('all',$program->brod_days))
							{{'all'}}
							@else
							{{implode(',',$program->brod_days)}}
							@endif
							@else
							{{$program->brod_days}}
							@endif
							From {{$program->start_time}} to {{$program->end_time}}
						</div>
						<span class="ellipsis ellipsis-2l hidden-xs list_text">
							{{$program->description}}
						</span>
					</div>
				</div>
				@endforeach
			</div>
				<div class="pagination">
					{{$pages['programs']->links()}}
				</div>
		</div>

		<div class="hidden-xs hidden-sm col-md-4" id="sidebar">
			@foreach($sidebars as $sidebar)
			@include('aside.'.$sidebar)
			@endforeach
		</div>
	</div>
</div>
</div>
