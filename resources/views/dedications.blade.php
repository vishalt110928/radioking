@if(! isset($notpushstate) || ($notpushstate != '1'))
@extends('layouts.web')
@section('slider')
@endif
<div class="container m-b-15 centered">
</div>
<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="index.html" class="toptip" title="Home"><i class="fa fa-home"></i></a></li>
			<li class="active">Dedications</li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						Dedications
						<a class="rss-link" href="#" target="_blank">
							<img class="rss-logo" style="width: 13px; margin-top: 1px;" src="{{url('public/img/rss.png')}}" />
							<span class="m-l-0 m-r-0 rss-text">RSS</span>
						</a>
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>
@if(! isset($notpushstate) || ($notpushstate != '1'))
@endsection
@section('content')
@endif
<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>
	<div class="row">

		<div class="col-xs-12  col-md-8  list_display">
			<!-- Affichage contenu -->
			<div class="m-t-30">
				<div class="article">
					<div class="row m-b-20">
						<div class="col-xs-12">
							<a class="btn btn-primary btn-block btn-lg dedication_modal">
								Send dedication </a>
							<div id="notif" class="alert alert-success"></div>
						</div>
					</div>
					@foreach($pages['dedications'] as $dedication)
					<div class="row m-b-15">
						<div class="col-xs-12">
							<div class="testimonial row">
								<div class="testimonial-author col-xs-4">
									<span class="ellipsis ellipsis-2l">
										{{$dedication->username}}
									</span>
									<div class="testimonial-time">
										@if($dedication->publish_time)
										{{ \Carbon\Carbon::parse($dedication->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')}}
										@endif
									</div>
								</div>
								<div class="testimonial-msg col-xs-8"><i class="fa fa-quote-left player-colored"></i>
									{{$dedication->dedication_message}}
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
			<div class="pagination">
				{{$pages['dedications']->links()}}
			</div>
		</div>


		<div class="hidden-xs hidden-sm col-md-4" id="sidebar">
			@foreach($sidebars as $sidebar)
			@include('aside.'.$sidebar)
			@endforeach
		</div>
	</div>
</div>
@if(! isset($notpushstate) || ($notpushstate != '1'))
@endsection
@endif
@section('script')
<script>
	$(document).ready(function() {
		$(document).on('click', '.dedication_modal', function() {
			$('#dedication_modal').modal('show');
		});

		$("#dedication_form").validate({
			rules: {
				username: {
					required: true
				},
				dedication_message: {
					required: true,
				}
			},
			messages: {},
			submitHandler: function(form) {
				var formData = $(form).serialize();
				$.ajax({
					method: "POST",
					url: "{{url('/send_dedication')}}",
					dataType: "json",
					data: formData,
					success: function(response) {
						alert(response.message);
						$('#dedication_modal').modal('hide');
						$('#dedication_form')[0].reset();
					},
					error: function(response) {
						console.log(response.status)
						if (response.status == '419') {
							window.location.reload();
						}
						if (response.status == "422") {
							var errors = response.responseJSON.errors;
							for (key in errors) {
								$('#dedication_form').find('.dedication_' + key).html('').html(errors[key]);
							}
						} else {
							alert('something have gone wrong');
						}
					}
				});
				return false;
			}
		});
	});
</script>
@endsection