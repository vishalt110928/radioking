@if(! isset($notpushstate) || ($notpushstate != '1'))
@extends('layouts.web')
@section('content')
@endif
<script type="text/javascript"></script>
<div class="container m-b-15 centered">
</div>
<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="{{url('/')}}" class="toptip detail" title="Home"><i class="fa fa-home"></i></a></li>
			<li><a href="#">Artists</a></li>
			<li class="active">{{$artist->name}}</li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						{{$artist->name}} </h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container body_wrap boxed">
		<div class="layout-headtitle-border"></div>
		<div class="row">
			<div class="col-xs-12  col-md-8  ">
				<!-- Affichage contenu -->
				<div class="m-t-30">
					<div class="article afficher-content">
						<div class="row">
							<div class="col-xs-12">
								<div class="col-sm-4 p-l-0 p-r-0 m-r-20">
									<img src="{{url('public/uploads/'.$artist->image)}}" class="artistes-img img-responsive m-b-5 m-t-20 rect_250_375" />
									<span class="infos-article">
										<i class="fa fa-eye"></i> views</span>
										<div class="table-responsive">
											<table class="table table-striped table-bordered artistes-table">
												<tr>
													<th>Genre</th>
													<td>{{$artist->genres}}</td>
												</tr>
												<tr>
													<th>Nick Name</th>
													<td>{{$artist->nick_name}}</td>
												</tr>
												<tr>
													<th>Complete Name</th>
													<td>{{$artist->complete_name}}</td>
												</tr>
												<tr>
													<th>BrithDay</th>
													<td>{{$artist->birth_date}}</td>
												</tr>
												<tr>
													<th>Acitivity</th>
													<td>{{$artist->acitivity}}</td>
												</tr>
												<tr>
													<th>Nationality</th>
													<td>{{$artist->nationality}}</td>
												</tr>
												<tr>
													<th>Instruments</th>
													<td>{{$artist->instruments}}</td>
												</tr>
												<tr>
													<th>Websites</th>
													<td>{{$artist->websites}}</td>
												</tr>
												<tr>
													<th>Facebook Page</th>
													<td>{{$artist->facebook_page}}</td>
												</tr>
												<tr>
													<th>Twitter Page</th>
													<td>{{$artist->twitter_page}}</td>
												</tr>
												<tr>
													<th>Year Active</th>
													<td>{{$artist->years_active}}</td>
												</tr>
											</table>
										</div>
									</div>
									<p class="m-t-20">{{$artist->biography}}</p>
									<div class="clear"></div>
									<div id="artistFull">
									</div>
								</div>
							</div>
						</div>
						<div id="comments" class="row comment-row m-t-20">
							<div class="col-xs-12">
								<!-- COMMENTAIRES -->
								<div class="form-group">
									<span class="comment-img avatar-default"><i class="fa fa-user"></i></span>
									<div class="fb-comments" data-href="{{request()->fullUrl()}}" data-width="" data-numposts="5"></div>
								</div>

								<div class="form-group" style="float:right;">
									<div class="col-xs-3">
										<a href="javascript:void(0)" id="sendComment" class="btn btn-primary">Comment</a>
									</div>
								</div>
								<div class="clear"></div>
								<div id="list-comments">
								</div>
								<div class="row">
									<div class="col-xs-12">
									</div>
								</div>
								<!-- Fin commentaires -->
							</div>
						</div>
						<div id="fb-root"></div>
						<script crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
					</div>
				</div>
			</div>
		</div>
		@if(! isset($notpushstate) || ($notpushstate != '1'))
		@endsection
		@endif