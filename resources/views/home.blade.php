@if(! isset($notpushstate) || ($notpushstate != '1'))
@extends('layouts.web')
@section('slider')
@endif
<div class="container">
	<div id="banner-slick">
		@foreach($sliders as $slider)
		<div>
			<a href="{{$slider->slide_url}}" target="_blank">
				<img src="{{url('public/uploads/'.$slider->image)}}" alt="" class="slider_banner" />
			</a>
		</div>
		@endforeach
	</div>
</div>
@if(! isset($notpushstate) || ($notpushstate != '1'))
@endsection
@section('content')
@endif
<div class="container body_wrap boxed m-t-15">
	<div class="row">
		<div class="col-md-12">
			@if($ads->display_top)
			@if($ads->image)
			<img src="{{url('public/uploads/'.$ads->image)}}">
			@else
			{!! $ads->js_code !!}
			@endif
			@endif
		</div>
	</div>
	<!-- A n'utiliser qu'une fois sinon conflit avec les autres sliders -->
	<div class="row">
		<div class="col-xs-12 col-md-8">
			<div class="bloc-content">
				<div class="wrap">
					<div class="row">
						<div class="col-md-12">
							<div class="bloc-content-title">
								<div class="bloc-title-readmore">
									<a href="{{url('news')}}" class="read-more-title detail">
										<i class="fa fa-arrow-circle-o-right"></i> See more
									</a>
								</div>
								<h2 class="ellipsis ellipsis-1l">News</h2>
							</div>
						</div>
					</div>
					@foreach($news as $new)
					@if($loop->first)
					<div class="row one_feature m-b-25 itemBloc">
						<div class="col-md-6">
							<div class="cover-preview">
								<a href="{{url('news_detail/'.$new->slug)}}" class="detail">
									<img src="{{url('public/uploads/'.$new->image)}}" class="cover-img news_sidebar_first" />
								</a>
							</div>
						</div>
						<div class="col-md-6">
							<a href="{{url('news_detail/'.$new->slug)}}" class="detail">
								<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-1">
									{{$new->title}}
								</h3>
							</a>
							<span class="trans date">
								<i class="fa fa-clock-o"></i>
								@if($new->publish_time)
								{{ \Carbon\Carbon::parse($new->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')}}
								@endif
							</span>
							<p class="ellipsis ellipsis-4l m-t-15">
								{{$new->description}}
							</p>
						</div>
					</div>
					@else
					<div class="col-xs-6 col-sm-4 itemBloc p-l-10-xs p-r-10-xs m-b-10-xs">
						<a href="{{url('news_detail/'.$new->slug)}}" class="detail">
							<div class="cover-preview ">
								<img src="{{url('public/uploads/'.$new->image)}}" class="cover-img news_sidebar_other" />
							</div>
							<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-2 ">
								{{$new->title}}
							</h3>
						</a>
					</div>
					<div class="noclear-xs"></div>
					@endif
					@endforeach
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-4">
			<div class="bloc-content bc-one-column">
				<div class="bloc-content-title">
					<div class="bloc-title-readmore">
						<a href="{{url('top_10')}}" class="read-more-title detail"><i class="fa fa-arrow-circle-o-right"></i> More</a>
					</div>
					<h2 class="ellipsis ellipsis-1l">Top Music</h2>
				</div>
				<div class="wrap listed">
					@foreach($tracks as $track)
					<div class="row track-row">
						<div class="col-xs-3 col-md-4">
							<a>
								<div class="cover-preview">
									<img src="{{url('public/uploads/'.$track->image)}}" class="cover-img square_img_100" />
									<span class="cover-infos">
										<strong><span class="top-position">{{ ++$loop->index}}</span></strong>
									</span>
								</div>
							</a>
						</div>
						<div class="col-xs-9 col-md-8">
							<div class="box-play player-play like-inline" onclick='playFile("{{url('public/uploads/'.$track->audio_sample)}}","{{$track->title}}", "{{url('public/uploads/'.$track->image)}}","{{$track->id}}","track" ,"1","1")'>
								<a href="javascript:;">
									<i class="fa fa-fw fa-play-circle"></i>
								</a>
							</div>
							<h4 class="bloc-track-title ellipsis ellipsis-1l">
								{{$track->title}}
							</h4>
							<span class="bloc-track-artist ellipsis ellipsis-1l">{{$track->artist}}</span>
							<a target="_blank" class="btn btn-default-bloc" href="{{$track->buy_link}}">
								<i class="fa fa-shopping-cart"></i> Buy this track </a>
							</div>
						</div>
						<div class="row border"></div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
		<!-- Podcaste section -->
		<div class="cl"></div>
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="bloc-content bc-one-column">
					<div class="bloc-content-title">
						<div class="bloc-title-readmore">
							<a href="{{url('podcasts')}}" class="read-more-title detail">
								<i class="fa fa-arrow-circle-o-right"></i> More
							</a>
						</div>
						<h2 class="ellipsis ellipsis-1l">Podcasts</h2>
					</div>
					<div class="wrap listed">
						@foreach($podcasts as $podcast)
						<div class="row">
							<a href="{{url('podcast_detail/'.$podcast->slug)}}" class="detail">
								<div class="col-md-4">
									<div class="cover-preview">
										<img src="{{url('public/uploads/'.$podcast->image)}}" class="cover-img square_img_100" />
									</div>
								</div>
							</a>
							<div class="col-xs-12 col-md-8">
								<div class="box-play player-play like-inline" onclick='playFile("@if($podcast->audio_file){{url('public/uploads/'.$podcast->audio_file)}} @else {{url($podcast->podcast_url)}} @endif ","{{$podcast->title}}", "{{url('public/uploads/'.$podcast->image)}}","{{$podcast->id}}","podcast" ,"0","1")'>
									<a href="javascript:;">
										<i class="fa fa-fw fa-play-circle"></i>
									</a>
								</div>
								<a href="{{url('podcast_detail/'.$podcast->slug)}}" class="detail">
									<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3 m-t-5">
										{{$podcast->title}}
									</h3>
								</a>
							</div>
						</div>
						<div class="row border"></div>
						@endforeach
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-4">
				<div class="bloc-content bc-one-column">
					<div class="bloc-content-title">
						<div class="bloc-title-readmore">
							<a href="{{url('playlists')}}" class="read-more-title detail"><i class="fa fa-arrow-circle-o-right"></i> More
							</a>
						</div>
						<h2 class="ellipsis ellipsis-1l">Playlist</h2>
					</div>
					<div class="wrap listed">
						@foreach($playlists as $playlist)
						<div class="row track-row">
							<div class="col-xs-3 col-md-4">
								<a>
									<div class="cover-preview">
										<img src="{{url('public/uploads/'.$playlist->image)}}" class="cover-img square_img_100" />
										<span class="">
											<strong></strong>
										</span>
										<div class="cover-play player-play" onclick='playFile("{{url('public/uploads/'.$playlist->audio_sample)}}","{{$playlist->title}}", "{{url('public/uploads/'.$playlist->image)}}","{{$playlist->id}}","{{'playlist'}}","1","1")'>
											<i class="fa fa-fw fa-play-circle"></i>
										</div>
									</div>
								</a>
							</div>
							<div class="col-xs-9 col-md-8">
								<div class="box-play like-music like-inline">
									<span class="hidden datastring"></span>
									<a class ="player-vote" data-type ="playlist", data-id = "{{$playlist->id}}"  href="{{url('track_playlist_like')}}">
										<i class="fa fa-fw fa-heart-o"></i>
									</a>
								</div>
								<h4 class="bloc-track-title ellipsis ellipsis-1l">
									{{$playlist->title}}
								</h4>
								<span class="bloc-track-artist ellipsis ellipsis-1l">@isset($playlist->cat->name) {{$playlist->cat->name}} @endisset</span>
								<a target="itunes_store" class="btn btn-default-bloc" href="{{$playlist->buy_link}}">
									<i class="fa fa-shopping-cart"></i> Buy this track
								</a>
							</div>
						</div>
						<div class="row border"></div>
						@endforeach
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-4">
				<div class="bloc-content bc-one-column">
					<div class="bloc-content-title">
						<div class="bloc-title-readmore">
							<a href="{{url('programs')}}" class="read-more-title detail"><i class="fa fa-arrow-circle-o-right"></i> More
							</a>
						</div>
						<h2 class="ellipsis ellipsis-1l">Shows</h2>
					</div>
					<div class="wrap ">
						<div class="row">
							@foreach($programs as $program)
							<a href="{{url('program_detail/'.$program->slug)}}" class="detail">
								<div class="col-xs-6">
									<div class="cover-preview">
										<img src="{{url('public/uploads/'.$program->image)}}" class="cover-img rect_165_110" />
									</div>
									<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3 m-t-5">
										{{$program->name}}
									</h3>
								</div>
							</a>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Podcast section -->
		<!--  Video section -->
		<div class="cl"></div>
		<div class='row'>
			<div class="col-xs-12 col-md-12">
				<div class="bloc-content">
					<div class="wrap">
						<div class="row">
							<div class="col-md-12">
								<div class="bloc-content-title">
									<div class="bloc-title-readmore">
										<a href="{{url('videos')}}" class="read-more-title detail"><i class="fa fa-arrow-circle-o-right"></i> See more
										</a>
									</div>
									<h2 class="ellipsis ellipsis-1l">Latest videos</h2>
								</div>
							</div>
						</div>
						<div class="row row-m-10-xs">
							@foreach($videos as $video)
							@if($loop->index < 3)
							<div class="col-md-4 col-xs-6 m-b-20 p-r-10-xs p-l-10-xs">
								<a href="{{url('video_detail/'.$video->slug)}}" class="detail">
									<div class="cover-preview">
										<img src="{{url('public/uploads/'.$video->image)}}" class="cover-img rect_360_203" />
										<div class="hover_type ">
											<span class="hovervideo"></span>
										</div>
									</div>
									<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-2">
										{{$video->title}}
									</h3>
								</a>
							</div>
							@else
							<div class="col-md-2 col-xs-6 m-b-20 p-r-10-xs p-l-10-xs">
								<a href="{{url('video_detail/'.$video->slug)}}" class="detail">
									<div class="cover-preview">
										<img src="{{url('public/uploads/'.$video->image)}}" class="rect_165_110">
										<span class="hovervideo"></span>
									</div>
									<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3">
										{{$video->title}}
									</h3>
								</a>
							</div>
							@if($loop->index % 2)
							<div class=" noclear-sm"></div>
							@elseif($loop->index == 3)
							<div class="clear noclear-sm"></div>
							@else
							<div class=" clear-sm"></div>
							@endif
							@endif
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Video Section -->
		<!--Contest Section  -->
		<div class="cl">
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-8">
				<div class="bloc-content">
					<div class="wrap">
						<div class="row">
							<div class="col-md-12">
								<div class="bloc-content-title">
									<div class="bloc-title-readmore">
										<a href="{{url('contests')}}" class="read-more-title detail"><i class="fa fa-arrow-circle-o-right"></i> See more
										</a>
									</div>
									<h2 class="ellipsis ellipsis-1l">Enter the contest</h2>
								</div>
							</div>
						</div>
						@foreach($contests as $contest)
						@if($loop->index == 0)
						<div class="row one_feature m-b-25 itemBloc">
							<a href="{{url('contest_detail/'.$contest->slug)}}" class="detail">
								<div class="col-md-6">
									<div class="cover-preview">
										<img src="{{url('public/uploads/'.$contest->image)}}" class="cover-img  news_sidebar_first" />
									</div>
								</div>
								<div class="col-md-6">
									<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-1">
										{{$contest->title}}
									</h3>
									<span class="trans date">
										<i class="fa fa-clock-o"></i>
										@if($contest->publish_time)
										{{ \Carbon\Carbon::parse($contest->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')}}
									@endif </span>
									<p class="ellipsis ellipsis-4l m-t-15">
										{{$contest->des}}
									</p>
								</div>
							</a>
						</div>
						@else
						<div class="col-xs-6 col-sm-4 itemBloc p-l-10-xs p-r-10-xs m-b-10-xs">
							<a href="{{url('contest_detail/'.$contest->slug)}}" class="detail">
								<div class="cover-preview ">
									<img src="{{url('public/uploads/'.$contest->image)}}" class="cover-img news_sidebar_other" />
								</div>
								<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-2 ">
									{{$contest->title}}
								</h3>
							</a>
						</div>
						<div class="noclear-xs"></div>
						@endif
						@endforeach
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-4">
				<div class="bloc-content bc-one-column">
					<div class="bloc-content-title">
						<div class="bloc-title-readmore">
							<a href="{{url('events')}}" class="read-more-title"><i class="fa fa-arrow-circle-o-right"></i> More
							</a>
						</div>
						<h2 class="ellipsis ellipsis-1l">Events</h2>
					</div>
					<div class="wrap listed">
						@foreach($events as $event)
						<div class="row">
							<a href="{{url('event_detail/'.$event->id)}}" class="detail">
								<div class="col-md-4">
									<div class="cover-preview">
										<img src="{{url('public/uploads/'.$event->image)}}" class="cover-img rect_100_67" />
									</div>
								</div>
								<div class="col-xs-12 col-md-8                        ">
									<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3 m-t-5">
										{{$event->title}}
									</h3>
									<span class="trans date">
										<i class="fa fa-calendar"></i>
										@if($event->publish_time)
										{{ \Carbon\Carbon::parse($event->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')}}
									@endif </span>
								</div>
							</a>
						</div>

						<div class="row border"></div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
		<!-- End Contest Section -->
		<!-- Artist Section -->
		<div class="cl"></div>
		<div class='row'>
			<div class="col-xs-12 col-md-12">
				<div id="bloc-slide" class="bloc-content carousel carousel-bloc slide slide-full">
					<div class="wrap">
						<div class="row">
							<div class="col-md-12">
								<div class="bloc-content-title slider-bloc-content-title">
									<h2 class="h2-slider ellipsis ellipsis-1l">Artists</h2>
								</div>
							</div>
						</div>
						<div class="row artist_slick" role="listbox">
							@foreach($artists as $artist)
							<div>
								<a href="{{url('artist_detail/'.$artist->slug)}}" class="detail">
									<div class="cover-preview ">
										<img src="{{url('public/uploads/'.$artist->image)}}" class="cover-img rect_165_193" />
									</div>
								</a>
								<a href="{{url('artist_detail/'.$artist->slug)}}" class="detail">
									<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3">
										{{$artist->name}}
									</h3>
								</a>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--End Artist Section -->
		<!-- Modal Section -->
		<div class="cl">
		</div>
	</div>
	<!-- End Modal Section -->
	@if(! isset($notpushstate) || ($notpushstate != '1'))
	@endsection
	@endif