@if(!isset($notpushstate) || ($notpushstate != 1))
@extends('layouts.web')
@section('slider')
@endif
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						News <a class="rss-link" href="#" target="_blank"><img class="rss-logo" style="width: 13px; margin-top: 1px;" src="{{url('public/img/rss.png')}}" /><span class="m-l-0 m-r-0 rss-text">RSS</span></a>
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>
@if(! isset($notpushstate) || ($notpushstate != 1))
@endsection
@section('content')
@endif
<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>
	<div class="row">
		<div class="col-xs-12  col-md-8  list_display">
			<!-- Affichage contenu -->
			<div class="m-t-30">
				@foreach($pages['news'] as $new)
				@if($loop->index == 0)
				<div class="row first_element list_element light_shadow boxed m-l-0 m-r-0 m-b-30 ">
					<div class="col-sm-6 col-img p-l-0 p-r-0">
						<a class="whole-div-link detail" href="{{url('news_detail/'.$new->slug)}}">
							<div class="cover-preview list-img-zone">
								<img class="list-img-thumb news_sidebar_first" src="{{url('public/uploads/'.$new->image)}}">
							</div>
						</a>
					</div>
					<div class="col-sm-6 p-l-10-xs p-r-10-xs">
						<a class="whole-div-link detail" href="{{url('news_detail/'.$new->slug)}}"/>
							<h2 class="ellipsis ellipsis-3l m-t-0 m-t-10-xs m-b-10">
								{{$new->title}}
							</h2>
						</a>
						<span class="ellipsis ellipsis-4l hidden-xs list_text">
							{{$new->description}}
						</span>
						<span class="trans list_trans hidden-xs">
							{{\Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::parse($new->created_at))}} Days Ago
						</span>
					</div>
				</div>
				@else
				<div class="row other_elements list_element light_shadow boxed m-l-0 m-r-0 m-b-20 ">
					<div class="col-xs-4 p-l-0 p-r-0">
						<a class="whole-div-link img-link detail" href="{{url('news_detail/'.$new->slug)}}" >
							<div class="cover-preview list-img-zone">
								<img class="list-img-thumb news_sidebar_other" src="{{url('public/uploads/'.$new->image)}}">
							</div>
						</a>
					</div>
					<div class="col-xs-8 p-r-10-xs">
						<span class="trans list_trans hidden-xxs">
							{{\Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::parse($new->created_at))}} Days Ago
						</span>

						<a class="whole-div-link title-link detail" href="{{url('news_detail/'.$new->slug)}}">
							<h4 class="ellipsis ellipsis-2l m-t-10 m-b-10 m-t-5-xs m-b-0-xs">
								{{$new->title}}
							</h4>
						</a>
						<span class="ellipsis ellipsis-2l hidden-xs list_text">
							{{$new->description}}
						</span>
					</div>
				</div>
				@endif
				@endforeach
			</div>
			<div class="pagination">
				{{$pages['news']->links()}}
			</div>
		</div>

		<div class="hidden-xs hidden-sm col-md-4" id="sidebar">
			@foreach($sidebars as $sidebar)
			@include('aside.'.$sidebar)
			@endforeach
		</div>
	</div>
</div>
@if(! isset($notpushstate) || ($notpushstate != 1))
@endsection
@endif