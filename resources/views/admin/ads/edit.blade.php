@extends('layouts.admin')
@section('title','Edit Ads')
@section('content')	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Edit Ads</h4>
		</div>

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/ads/'.$id, 'method' => 'put' , 'class' =>'card' , 'files' => true , 'id'=>'edit_form'])}}
				@csrf
				<div class="card-header">
					<h3 class="card-title">Form elements</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label class="form-label">Enter Title</label>
								{{Form::text('title',$ad->title,['placeholder'=>'Enter Title' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label" for="publish">Publish</label>
								{{Form::checkbox('publish', '1',$ad->publish,['class' => 'form-control','id'=>'publish'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Add A Link</label>
								{{Form::text('link',$ad->link,['placeholder'=>'Add a link' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Campaign Start Date</label>
								{{Form::text('campaign_start_date',$ad->campaign_start_date,['placeholder'=>'Campaign Start Date' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Campaign End Date</label>
								{{Form::text('campaign_end_date',$ad->campaign_end_date,['placeholder'=>'Campaign End Date' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label" for="display_top" >Display at top </label>
								{{Form::checkbox('display_top', '1',$ad->display_top,['class' => 'form-control','id'=>'display_top'])}}
							</div>
							<div class="form-group">
								<label class="form-label" for="display_block">Display Block</label>
								{{Form::checkbox('display_block', '1',$ad->display_block,['class' => 'form-control','id'=>'display_block'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Format</label>
								{{Form::select('format',array(),$ad->format,['class' => 'form-control'])}}
							</div>
							<div class="form-group text-center">
								<label>Select Ads Type  *</label>
								@if($ad->ads_type)
								<div class="row">
									<div class="col-lg-6">
										{{ Form::radio('ads_type', 0, false)}}
										Upload Image
									</div>
									<div class="col-lg-6">
										{{ Form::radio('ads_type',1, true)}}
										Javascript Code
									</div>
								</div>
								@else
								<div class="row">
									<div class="col-lg-6">
										{{ Form::radio('ads_type', 0, true)}}
										Upload Image
									</div>
									<div class="col-lg-6">
										{{ Form::radio('ads_type',1, false)}}
										Javascript Code
									</div>
								</div>
								@endif
							</div>
							<div class="form-group">
								<label class="form-label">Javascript Code</label>
								{{ Form::textarea('js_code','', ['class' => 'form-control', 'required' => '', 'size' =>'50x3']) }}
							</div>
							<div class="form-group">
								<label class="form-label">Upload Image </label>
								{{Form::file('image',['class'=>'upload','id'=>'upload'])}}
							</div>
							<img id="img" src="#" alt="your image" style="display: none" />
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
	$(function(){
		$('#upload').change(function(){
			$('#img').css('display','block');
			var input = this;
			var url = $(this).val();
			console.log(url);
			var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
			if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
			{
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#img').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
			else
			{
				$('#img').attr('src', '/assets/no_preview.png');
			}
		});
		$("#edit_form").validate({
			rules: {
				name: {
					required : true,
					minlength: 2,
					maxlength : 30
				},
				
				image : {
					accept : "image/*"
				},
			},
			messages: {
				name: {
					required : "Please specify your name.",
					minlength : "Minimum length should be 2 character.",
					maxlength : "Maximum length should be 30 character.",
				},

				image: {
					accept: "Only image file allowed"
				},
			}
		});
		
		var dateRangePicker = ['input[name= "campaign_start_date"]','input[name= "campaign_end_date"]'];
		dateRangePicker.forEach(PickerFunction);
		ads_type();
		$(document).on('click','input[name = "ads_type"]', function() {
			ads_type();
		})
	});
	function PickerFunction(picker) {
		$(picker).daterangepicker({
			timePicker: true,
			singleDatePicker: true,
			autoUpdateInput: false,
			locale: {
				format: 'DD-MM-YYYY H:mm:ss'
			}
		}, function(chosen_date) {
			$(picker).val(chosen_date.format('DD-MM-YYYY H:mm:ss'));
		});
	}
	function ads_type() {
		$("#edit_form").find('textarea[name = "js_code"]').prop( "disabled", true );
		$("#edit_form").find('input[name = "image"]').prop( "disabled", true );
		var ads_type = $("#edit_form").find('input[name = "ads_type"]:checked').val();
		if(ads_type == 1 ){
			$("#edit_form	").find('textarea[name = "js_code"]').prop( "disabled", false );
			$("#edit_form	").find('input[name = "image"]').prop( "disabled", true );
		}
		else{
			$("#edit_form	").find('textarea[name = "js_code"]').prop( "disabled", true );
			$("#edit_form	").find('input[name = "image"]').prop( "disabled", false );
		}

	}
</script>
@endsection
