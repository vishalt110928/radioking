@extends('layouts.admin')
@section('content')
@if(session()->has('success'))
<div class="alert alert-success  alert-dismissible">
  <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> {{session('success')}}
</div>
@elseif(session()->has('error'))
<div class="alert alert-danger  alert-dismissible">
  <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Danger!</strong> {{session('error')}}
</div>
@endif
<div class="row">
  <div class="col-md-12">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Artists</li>
      </ol>
    </nav>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="widget has-shadow">
      <div class="widget-header bordered no-actions d-flex align-items-center">
       <div class="col-md-8"><h4>ArtistsModel</h4></div>
       <div class="col-md-4">
       {{Form::open(['url' => 'admin/artists', 'method' => 'get' , 'class' =>'filter_form form-inline', 'id'=>'filter_form'])}}
       <input class="form-control" type="text" name ="filter_artists" placeholder="Search" aria-label="Search" value="{{$filter_artists}}">
       {{Form::button('<i class="fa fa-filter"></i>' ,['class'=>'btn btn-primary ml-auto ','id'=>"button-filter",'type'=>'submit'])}}
       {{Form::close()}}
      </div>
    </div>
    <div class="m-2 float-right">
      <a href="{{url('admin/artists/create')}}" class="btn btn-info">Add</a>
      <a href="#" onclick="delete_artists()" class="btn btn-info">Delete</a>
    </div>
    <div class="well m-5">
      {{Form::open(['url' => 'admin/artists', 'method' => 'get' , 'class' =>'card' , 'id'=>'create_form'])}}
      <div class="row padd_filter">
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="input_title">Artists Name </label>
            <input type="text" name="filter_name" value="{{$filter_name}}" placeholder="Artist Name" id="input_title" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
          </div>
          <div class="form-group">
            <label class="control-label" for="input_end_time">End Time</label>
            <input type="text" name="filter_end_time" value="{{$filter_end_time}}" placeholder="Enter end time" id="input_end_time" class="form-control">
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="input_cat">Category</label>
            {{Form::select('filter_cat',$artists_cats ,$filter_cat, ['placeholder' => 'Select Category ','class'=>'form-control'])}}
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="input_publish_time">Publish Time</label>
            <input type="text" name="filter_publish_time" value="{{$filter_publish_time}}" placeholder="Enter Publis time" id="input_publish_time" class="form-control">
          </div>
          {{Form::button('<i class="fa fa-filter"></i> Filter' ,['class'=>'btn btn-primary ml-auto','id'=>"button-filter",'type'=>'submit'])}}
          {{Form::close()}}
        </div>
      </div>
    </div>
    <div class="widget-body">
      <div class="table-responsive">
        <form class="artists_delete" method="post" action = "{{url('admin/artists_delete/')}}">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
          <table class="table table-bordered mb-0">
            <thead>
              <tr>
                <th>
                  <input type="checkbox" name="groupCheck" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" ></th>
                  <th>
                    <a href ="#" class="sort_order">Image</a>
                  </th>
                  <th>
                    @if($orderBy == 'name' && $sort == "desc")
                    <a href ="{{url('admin/artists/?orderBy=name&sort=asc&'.$query)}}">Name <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                    @elseif($orderBy == 'name' && $sort == "asc")
                    <a href ="{{url('admin/artists/?orderBy=name&sort=desc&'.$query)}}">Name <i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                    @else
                    <a href ="{{url('admin/artists/?orderBy=name&sort=desc&'.$query)}}">Name <i class="fa fa-sort" aria-hidden="true"></i></a>
                    @endif
                  <th>
                    @if($orderBy == 'category' && $sort == "desc")
                    <a href ="{{url('admin/artists/?orderBy=category&sort=asc&'.$query)}}">Category <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                    @elseif($orderBy == 'category' && $sort == "asc") 
                    <a href ="{{url('admin/newsartists/?orderBy=category&sort=desc&'.$query)}}">Category <i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                    @else
                    <a href ="{{url('admin/artists/?orderBy=category&sort=desc&'.$query)}}">Category <i class="fa fa-sort" aria-hidden="true"></i></a>
                    @endif
                </th>
                  <th> @if($orderBy == 'publish_time' && $sort == "desc")
                    <a href ="{{url('admin/artists/?orderBy=publish_time&sort=asc&'.$query)}}">Publication <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                    @elseif($orderBy == 'publish_time' && $sort == "asc") 
                    <a href ="{{url('admin/artists/?orderBy=publish_time&sort=desc&'.$query)}}">Publication <i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                    @else
                    <a href ="{{url('admin/artists/?orderBy=publish_time&sort=desc&'.$query)}}">Publication <i class="fa fa-sort" aria-hidden="true"></i></a>
                    @endif
                  </th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($artists as  $artist)
                <tr>
                  <td><input type="checkbox" name="selected[]" value="{{$artist->id}}}" ></td>
                  <td><img width="80" class="img-polaroid" style="width : 80px;" src="{{url('public/uploads/'.$artist->image)}}"></td>
                  <td>{{$artist->name}}</td>
                  <td>@isset($artist->cat->name)
                    {{$artist->cat->name}}
                    @endisset
                  </td>
                  <td>
                    @if($artist->publish_time)
                    {{ \Carbon\Carbon::parse($artist->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')}}
                    @endif
                  </td>
                  <td>
                    @if($artist->publish)
                    <span style="width:100px;"><span class="badge-text badge-text-small info">Publish</span></span>
                    @else
                    <span style="width:100px;"><span class="badge-text badge-text-small danger">Not Publish</span></span>
                    @endif
                  </td>
                  <td class="td-actions">
                    <a href="{{url('admin/artists/'.$artist->id.'/edit/')}}"><i class="la la-edit edit"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="{{url('public/js/bootbox.min.js')}}"></script>
<script type="text/javascript">
  function delete_artists(){
    $('form.artists_delete').submit();
  }
  $(document).ready(function(){
    $(document).on('click','.delete_artists',function(){
      var $this = $(this);
      console.log(this)
      bootbox.confirm("Are you sure?", function(result){
        if(result){
          $this.parent().find('form').submit();
        }
        else{
        }
      });
    });

    $('input[name= "filter_publish_time"],input[name= "filter_end_time"]').daterangepicker({
      timePicker: true,
      singleDatePicker: true,
      autoUpdateInput: false,
      locale: {
        format: 'YYYY/MM/DD H:mm:ss'
      }
    }, function(chosen_date) {
      $('input[name= "filter_publish_time"]').val(chosen_date.format('YYYY/MM/DD H:mm:ss'));
    });

    $('input[name= "filter_end_time"]').daterangepicker({
      timePicker: true,
      singleDatePicker: true,
      autoUpdateInput: false,
      locale: {
        format: 'YYYY/MM/DD H:mm:ss'
      }
    }, function(chosen_date) {
      $('input[name= "filter_end_time"]').val(chosen_date.format('YYYY/MM/DD H:mm:ss'));
    });

  })
</script>
@endsection



