@extends('layouts.admin')
@section('title','Edit News')
@section('content')	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Edit News</h4>
		</div>

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/artists/'.$id, 'method' => 'put' , 'class' =>'card' , 'files' => true , 'id'=>'edit_form'])}}
				@csrf
				<div class="card-header">
					<h3 class="card-title">Form elements</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label class="form-label">Enter Title</label>
								{{Form::text('name',$artist->name,['placeholder'=>'Enter Name' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Publish</label>
								{{Form::checkbox('publish', '1',$artist->publish,['class' => 'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Category</label>
								{{Form::select('artists_cat_id',$artists_cats ,$artist->artists_cat_id, ['placeholder' => 'Select Category','class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Publish Time</label>
								{{Form::text('publish_time',$artist->publish_time,['placeholder'=>'Enter Publish Time' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter End Time</label>
								{{Form::text('end_time',$artist->end_time,['placeholder'=>'Enter End Time' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Nick Name</label>
								{{Form::text('nick_name',$artist->nick_name,['placeholder'=>'Enter Nick Name' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Complete Name</label>
								{{Form::text('complete_name',$artist->complete_name,['placeholder'=>'Enter Complerte Name' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Birth Date</label>
								{{Form::text('birth_date',$artist->birth_date,['placeholder'=>'Enter Birth Date' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Years Active</label>
								{{Form::text('years_active',$artist->years_active,['placeholder'=>'Enter Years Active' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Nationality</label>
								{{Form::text('nationality',$artist->nationality,['placeholder'=>'Enter Nationality' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Genres</label>
								{{Form::text('genres',$artist->genres,['placeholder'=>'Enter Genres' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Member</label>
								{{Form::text('member',$artist->member,['placeholder'=>'Enter Member' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Previous Member</label>
								{{Form::text('previous_member',$artist->previous_member,['placeholder'=>'Enter Previous Member' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Instruments</label>
								{{Form::text('instruments',$artist->instruments,['placeholder'=>'Enter Instruments' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Websites</label>
								{{Form::text('websites',$artist->websites,['placeholder'=>'Enter Websites' ,'class'=>'form-control'])}}
							</div>
								<div class="form-group">
								<label class="form-label">Facebook Page</label>
								{{Form::text('facebook_page',$artist->facebook_page,['placeholder'=>'Enter Facebook Page' ,'class'=>'form-control'])}}
							</div>
								<div class="form-group">
								<label class="form-label">Twitter Page</label>
								{{Form::text('twitter_page',$artist->twitter_page,['placeholder'=>'Enter Twitter Page' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Biography</label>
								{{ Form::textarea('biography',$artist->biography, ['class' => 'form-control', 'required' => '', 'size' =>'50x3']) }}
							</div>
							<div class="form-group">
								<label class="form-label">Upload Image </label>
								{{Form::file('image',['class'=>'upload','id'=>'upload'])}}
							</div>
							<img id="img" src="#" alt="your image" style="display: none" />
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
	$(function(){
		$('#upload').change(function(){
			$('#img').css('display','block');
			var input = this;
			var url = $(this).val();
			console.log(url);
			var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
			if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
			{
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#img').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
			else
			{
				$('#img').attr('src', '/assets/no_preview.png');
			}
		});
		$("#edit_form").validate({
			rules: {
				name: {
					required : true,
					minlength: 2,
					maxlength : 30
				},
				
				image : {
					accept : "image/*"
				},
			},
			messages: {
				name: {
					required : "Please specify your name.",
					minlength : "Minimum length should be 2 character.",
					maxlength : "Maximum length should be 30 character.",
				},

				image: {
					accept: "Only image file allowed"
				},
			}
		});
		$('input[name= "publish_time"],input[name= "end_time"]').daterangepicker({
			timePicker: true,singleDatePicker: true,locale: {
				format: 'YYYY/MM/DD H:mm:ss'
			}
		});
	});
</script>
@endsection
