@extends('layouts.admin')
@section('title','Create News')
@section('content')	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Create Pages</h4>
		</div>

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/pages', 'method' => 'post' , 'class' =>'card' , 'files' => true , 'id'=>'create_form'])}}
				@csrf
				<div class="card-header">
					<h3 class="card-title"></h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label class="form-label">Enter Title</label>
								{{Form::text('title','',['placeholder'=>'Enter Title' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Publish</label>
								{{Form::checkbox('publish', '1',false,['class' => 'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Category</label>
								{{Form::select('pages_cat_id',$pages_cats , null, ['placeholder' => 'Select Category ','class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Publish Time</label>
								{{Form::text('publish_time','',['placeholder'=>'Enter Publish Time' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Menu</label>
								{{Form::text('add_menu','',['placeholder'=>'Enter menu' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Content</label>
								{{ Form::textarea('content','', ['class' => 'form-control', 'required' => '', 'size' =>'50x3']) }}
							</div>
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
	$(function(){
		$("#create_form").validate({
			rules: {
				title: {
					required : true,
					minlength: 2,
					maxlength : 30
				}
			},
			messages: {
				title: {
					required : "Please specify your name.",
					minlength : "Minimum length should be 2 character.",
					maxlength : "Maximum length should be 30 character.",
				}
			}
		});

		var dateRangePicker = ['input[name= "publish_time"]'];
		dateRangePicker.forEach(PickerFunction);
	})
	function PickerFunction(picker) {
		$(picker).daterangepicker({
			timePicker: true,
			singleDatePicker: true,
			autoUpdateInput: false,
			locale: {
				format: 'DD-MM-YYYY H:mm:ss'
			}
		}, function(chosen_date) {
			$(picker).val(chosen_date.format('DD-MM-YYYY H:mm:ss'));
		});
	}
</script>
@endsection
