@extends('layouts.admin')
@section('title','Edit Podcasts')
@section('content')	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Edit Podcasts</h4>
		</div>

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/podcasts/'.$id, 'method' => 'put' , 'class' =>'card' , 'files' => true , 'id'=>'edit_form'])}}
				@csrf
				<div class="card-header">
					<h3 class="card-title">Form elements</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label class="form-label">Enter Title</label>
								{{Form::text('title',$podcast->title,['placeholder'=>'Enter Title' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Publish</label>
								@if($podcast->publish)
								{{Form::checkbox('publish', '1','true',['class' => 'form-control'])}}
								@else 
								{{Form::checkbox('publish', '1','false',['class' => 'form-control'])}}
								@endif
							</div>
							<div class="form-group">
								<label class="form-label">Views</label>
								{{Form::number('views',$podcast->views,['placeholder'=>'Views' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Category</label>
								{{Form::select('podcasts_cat_id',$podcasts_cats ,$podcast->podcasts_cat_id, ['placeholder' => 'Select Category','class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Publish Time</label>
								{{Form::text('publish_time',$podcast->publish_time,['placeholder'=>'Enter Publish Time' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter End Time</label>
								{{Form::text('end_time',$podcast->end_time,['placeholder'=>'Enter End Time' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Description</label>
								{{ Form::textarea('des',$podcast->des, ['class' => 'form-control', 'size' =>'50x3']) }}
							</div>
							<div class="form-group">
								<label class="form-label">Podcast Type</label>
								<div class="row">
									@if($podcast->podcast_type)
									<div class="col-lg-6">
										{{ Form::radio('podcast_type', 0, true)}}
										Upload Track
									</div>
									<div class="col-lg-6">
										{{ Form::radio('podcast_type',1, false)}}
										Remote Url
									</div>
									@else 
									<div class="col-lg-6">
										{{ Form::radio('podcast_type', 0, false)}}
										Upload Track
									</div>
									<div class="col-lg-6">
										{{ Form::radio('podcast_type',1, true)}}
										Remote Url
									</div>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="form-label">Podcast Url</label>
								{{Form::text('podcast_url',$podcast->podcast_url,['placeholder'=>'Enter Title' , 'class'=>'form-control podcast_url'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Audio File</label>
								{{Form::file('audio_file',['class'=>'audio_file','id'=>'audio_file'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Upload Image </label>
								{{Form::file('image',['class'=>'upload','id'=>'upload'])}}
							</div>
							<img id="img" src="#" alt="your image" style="display: none" />
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
	$(function(){
		$("#edit_form").find('input[name = "podcast_url"]').prop( "disabled", true );
		$("#edit_form").find('input[name = "audio_file"]').prop( "disabled", true );

		var podcast_type  = $("#edit_form").find('input[name = "podcast_type"]:checked').val();
		if(podcast_type == 1 ){
			$("#edit_form").find('input[name = "podcast_url"]').prop( "disabled", false );
		}
		else{
			$("#edit_form").find('input[name = "audio_file"]').prop( "disabled", false );
		}
		$(document).on('change','input[name = "podcast_type"]',function() {
			var podcast_type  = $("#edit_form").find('input[name = "podcast_type"]:checked').val();
			if(podcast_type == 1 ){
				$("#edit_form").find('input[name = "podcast_url"]').prop( "disabled", false );
				$("#edit_form").find('input[name = "audio_file"]').prop( "disabled", true );
			}
			else{
				$("#edit_form").find('input[name = "audio_file"]').prop( "disabled", false );
				$("#edit_form").find('input[name = "podcast_url"]').prop( "disabled", true );
			}
		});
		$('#upload').change(function(){
			$('#img').css('display','block');
			var input = this;
			var url = $(this).val();
			var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
			if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
			{
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#img').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
			else
			{
				$('#img').attr('src', '/assets/no_preview.png');
			}
		});
		$("#edit_form").validate({
			rules: {
				name: {
					required : true,
					minlength: 2,
					maxlength : 30
				},
				
				image : {
					accept : "image/*"
				},
			},
			messages: {
				name: {
					required : "Please specify your name.",
					minlength : "Minimum length should be 2 character.",
					maxlength : "Maximum length should be 30 character.",
				},

				image: {
					accept: "Only image file allowed"
				},
			}
		});
		  var dateRangePicker = ['input[name= "publish_time"]','input[name= "end_time"]'];
		  dateRangePicker.forEach(PickerFunction);
		})
		function PickerFunction(picker) {
		  $(picker).daterangepicker({
		    timePicker: true,
		    singleDatePicker: true,
		    autoUpdateInput: false,
		    locale: {
		      format: 'DD-MM-YYYY H:mm:ss'
		    }
		  }, function(chosen_date) {
		    $(picker).val(chosen_date.format('DD-MM-YYYY H:mm:ss'));
		  });
		}
</script>
@endsection
