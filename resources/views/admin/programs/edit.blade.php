@extends('layouts.admin')
@section('title','Edit Programs')
@section('content')	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Edit Programs</h4>
		</div>

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/programs/'.$id, 'method' => 'put' , 'class' =>'card' , 'files' => true , 'id'=>'edit_form'])}}
				@csrf
				<div class="card-header">
					<h3 class="card-title">Form elements</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label class="form-label">Enter Name</label>
								{{Form::text('name',$program->name,['placeholder'=>'Enter Name' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Publish</label>
								{{Form::checkbox('publish', '1',$program->publish,['class' => 'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Brodcast Days</label>
								{{Form::checkbox('brod_days[all]', '1',( in_array('all',$program->brod_days) ? true: false ),['class' => 'form-control'])}}All
								{{Form::checkbox('brod_days[mon]', '1',( in_array('mon',$program->brod_days) ? true: false ),['class' => 'form-control'])}}Mon
								{{Form::checkbox('brod_days[tue]', '1',( in_array('tue',$program->brod_days) ? true: false ),['class' => 'form-control'])}}Tue
								{{Form::checkbox('brod_days[wed]', '1',( in_array('wed',$program->brod_days) ? true: false ),['class' => 'form-control'])}}Wed
								{{Form::checkbox('brod_days[thr]', '1',( in_array('thr',$program->brod_days) ? true: false ),['class' => 'form-control'])}}Thr
								{{Form::checkbox('brod_days[fri]', '1',( in_array('fri',$program->brod_days) ? true: false ),['class' => 'form-control'])}}Fri
								{{Form::checkbox('brod_days[sat]', '1',( in_array('sat',$program->brod_days) ? true: false ),['class' => 'form-control'])}}Sat
								{{Form::checkbox('brod_days[sun]', '1',( in_array('sun',$program->brod_days) ? true: false ),['class' => 'form-control'])}}Sun

							</div>
							<div class="form-group">
								<label class="form-label">Enter Djs</label>
								{{Form::text('djs',$program->djs,['placeholder'=>'Enter Djs' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Programe Category</label>
								{{Form::select('programs_cat_id',$programs_cats , $program->programs_cat_id, ['placeholder' => 'Select Category ','class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Link Podcast Category</label>
								{{Form::select('link_podcast_cat',$programs_cats , $program->link_podcast_cat, ['placeholder' => 'Select Category ','class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Publish Date</label>
								{{Form::text('publish_date',$program->publish_date,['placeholder'=>'Enter Publish Date' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter End Date</label>
								{{Form::text('end_date',$program->end_date,['placeholder'=>'Enter End Date' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Start Time</label>
								{{Form::text('start_time',$program->start_time,['placeholder'=>'Enter Start Time' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter End Time</label>
								{{Form::text('end_time',$program->end_time,['placeholder'=>'Enter End Time' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Description</label>
								{{ Form::textarea('description',$program->description, ['class' => 'form-control','size' =>'50x3']) }}
							</div>
							<div class="form-group">
								<label class="form-label">Upload Image </label>
								{{Form::file('image',['class'=>'upload','id'=>'upload'])}}
							</div>
							<img id="img" src="#" alt="your image" style="display: none" />
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
	$(function(){
		$('#upload').change(function(){
			$('#img').css('display','block');
			var input = this;
			var url = $(this).val();
			console.log(url);
			var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
			if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
			{
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#img').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
			else
			{
				$('#img').attr('src', '/assets/no_preview.png');
			}
		});
		$("#edit_form").validate({
			rules: {
				name: {
					required : true,
					minlength: 2,
					maxlength : 30
				},
				
				image : {
					accept : "image/*"
				},
			},
			messages: {
				name: {
					required : "Please specify your name.",
					minlength : "Minimum length should be 2 character.",
					maxlength : "Maximum length should be 30 character.",
				},

				image: {
					accept: "Only image file allowed"
				},
			}
		});
			var dateRangePicker = ['input[name= "publish_date"]','input[name= "end_date"]'];
				dateRangePicker.forEach(PickerFunction);

				$('input[name = "start_time"],input[name = "end_time"]').daterangepicker({
					singleDatePicker: true,
					timePicker: true,
					timePicker24Hour: true,
					timePickerIncrement: 1,
					timePickerSeconds: true,
					locale: {
						format: 'HH:mm:ss'
					}
				}).on('show.daterangepicker', function (ev, picker) {
					picker.container.find(".calendar-table").hide();
				});
			});
			function PickerFunction(picker) {
		    $(picker).daterangepicker({
		      timePicker: true,
		      singleDatePicker: true,
		      autoUpdateInput: false,
		      locale: {
		        format: 'DD-MM-YYYY H:mm:ss'
		      }
		    }, function(chosen_date) {
		      $(picker).val(chosen_date.format('DD-MM-YYYY H:mm:ss'));
		    });
		  }
</script>
@endsection
