@extends('layouts.admin')
@section('content')
@if(session()->has('success'))
<div class="alert alert-success  alert-dismissible">
  <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> {{session('success')}}
</div>
@elseif(session()->has('error'))
<div class="alert alert-danger  alert-dismissible">
  <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Danger!</strong> {{session('error')}}
</div>
@endif
<div class="row">
  <div class="col-md-12">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">User</li>
      </ol>
    </nav>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="widget has-shadow">
      <div class="widget-header bordered no-actions d-flex align-items-center">
       <div class="col-md-8"><h4>UserModel</h4></div>
       <div class="col-md-4">
         {{Form::open(['url' => 'admin/users', 'method' => 'get' , 'class' =>'filter_form form-inline', 'id'=>'filter_form'])}}
         <input class="form-control" type="text" name ="filter_users" placeholder="Search" aria-label="Search" value="{{$filter_users}}">
         {{Form::button('<i class="fa fa-filter"></i>' ,['class'=>'btn btn-primary ml-auto ','id'=>"button-filter",'type'=>'submit'])}}
         {{Form::close()}}
       </div>
     </div>
     <div class="m-2 float-right">
      <a href="{{url('admin/users/create')}}" class="btn btn-info">Add</a>
      <a href="#" onclick="delete_users()" class="btn btn-info">Delete</a>
    </div>
    <div class="well m-5">
      {{Form::open(['url' => 'admin/users', 'method' => 'get' , 'class' =>'card' , 'id'=>'create_form'])}}
      <div class="row padd_filter">
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="input_title">User Name </label>
            <input type="text" name="filter_name" value="{{$filter_name}}" placeholder="Product Name" id="input_title" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="input_cat">Roles</label>
            {{Form::select('filter_role',$users_roles ,$filter_role, ['placeholder' => 'Select Category ','class'=>'form-control'])}}
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="input_birthday">Birthday</label>
            <input type="text" name="filter_birthday" value="{{$filter_birthday}}" placeholder="Enter Birthday" id="input_birthday" class="form-control">
          </div>
          {{Form::button('<i class="fa fa-filter"></i> Filter' ,['class'=>'btn btn-primary ml-auto','id'=>"button-filter",'type'=>'submit'])}}

          {{Form::button('<i class="fa fa-reset"></i> Reset' ,['class'=>'btn btn-primary ml-auto reset_filter','id'=>"button-filter"])}}

          {{Form::close()}}
        </div>
      </div>
    </div>
    <div class="widget-body">
      <div class="table-responsive">
        <form class="users_delete" method="post" action = "{{url('admin/users_delete/')}}">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
          <table class="table table-bordered mb-0">
            <thead>
              <tr>
                <th>
                  <input type="checkbox" name="groupCheck" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" >
                </th>
                <th>
                  <a href ="#" class="sort_order">Image</a>
                </th>
                <th>
                  @if($orderBy == 'name' && $sort == "desc")
                  <a href ="{{url('admin/users/?orderBy=name&sort=asc&'.$query)}}">Name <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                  @elseif($orderBy == 'name' && $sort == "asc")
                  <a href ="{{url('admin/users/?orderBy=name&sort=desc&'.$query)}}">Name <i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                  @else
                  <a href ="{{url('admin/users/?orderBy=name&sort=desc&'.$query)}}">Name <i class="fa fa-sort" aria-hidden="true"></i></a>
                  @endif
                </th>
                <th>
                  @if($orderBy == 'email' && $sort == "desc")
                  <a href ="{{url('admin/users/?orderBy=email&sort=asc&'.$query)}}">Email <i class="fa fa-sort-desc" aria-hidden="true"></a>
                    @elseif($orderBy == 'email' && $sort == "asc")
                    <a href ="{{url('admin/users/?orderBy=email&sort=desc&'.$query)}}">Email <i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                    @else
                    <a href ="{{url('admin/users/?orderBy=email&sort=desc&'.$query)}}">Email <i class="fa fa-sort" aria-hidden="true"></i></a>
                    @endif
                  </th>
                  <th>
                    @if($orderBy == 'role' && $sort == "desc")
                    <a href ="{{url('admin/users/?orderBy=role&sort=asc&'.$query)}}">Role <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                    @elseif($orderBy == 'role' && $sort == "asc") 
                    <a href ="{{url('admin/users/?orderBy=role&sort=desc&'.$query)}}">Role <i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                    @else
                    <a href ="{{url('admin/users/?orderBy=role&sort=desc&'.$query)}}">Role <i class="fa fa-sort" aria-hidden="true"></i></a>
                    @endif
                  </th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($users as  $user)
                <tr>
                  <td><input type="checkbox" name="selected[]" value="{{$user->id}}}" ></td>
                  <td><img width="80" class="img-polaroid" style="width : 80px;" src="{{url('public/uploads/'.$user->image)}}"></td>
                  <td>{{$user->name}}</td>
                  <td>{{$user->email}}</td>
                  <td>
                    @isset($user->role->role)
                    {{$user->role->role}}
                    @endisset
                  </td>
                  <td class="td-actions">
                    <a href="{{url('admin/users/'.$user->id.'/edit/')}}"><i class="la la-edit edit"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </form>
        </div>
      </div>
      <div class="pagination_div text-center">
        {{$users->appends($appends)->links()}}
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="{{url('public/js/bootbox.min.js')}}"></script>
<script type="text/javascript">
  function delete_users(){
    $('form.users_delete').submit();
  }
  $(document).ready(function(){
    $(document).on('click','.delete_users',function(){
      var $this = $(this);
      console.log(this)
      bootbox.confirm("Are you sure?", function(result){
        if(result){
          $this.parent().find('form').submit();
        }
        else{
        }
      });
    });

    $(document).on('click','.reset_filter',function() {
     $('#create_form').find('input,select').val('');
     $('#create_form').submit();
   })
    var dateRangePicker = ['input[name= "filter_birthday"]'];
    dateRangePicker.forEach(PickerFunction);
  })
  function PickerFunction(picker) {
    $(picker).daterangepicker({
      timePicker: true,
      singleDatePicker: true,
      autoUpdateInput: false,
      locale: {
        format: 'DD-MM-YYYY H:mm:ss'
      }
    }, function(chosen_date) {
      $(picker).val(chosen_date.format('DD-MM-YYYY H:mm:ss'));
    });
  }
</script>
@endsection



