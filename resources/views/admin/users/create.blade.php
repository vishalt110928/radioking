@extends('layouts.admin')
@section('title','Create User')
@section('content')	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Create User</h4>
		</div>
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/users', 'method' => 'post' , 'class' =>'card' , 'files' => true , 'id'=>'create_form'])}}
				@csrf
				<div class="card-header">
					<h3 class="card-title"></h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label class="form-label">Enter Name</label>
								{{Form::text('name','',['placeholder'=>'Enter Name' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Email</label>
								{{Form::text('email','',['placeholder'=>'Enter Email' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Password</label>
								{{Form::password('password',['placeholder'=>'Enter Password' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Password Confirmation</label>
								{{Form::password('password_confirmation',['placeholder'=>'Enter Password' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Civility</label>
								{{Form::select('civility',array('1'=>'Mr','2'=>'Mrs'), null, ['placeholder' => 'Select Civility','class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter FirstName</label>
								{{Form::text('firstname','',['placeholder'=>'Enter FirstName' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter BirthDay</label>
								{{Form::text('birthday','',['placeholder'=>'Enter Birthday' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Mobile No</label>
								{{Form::text('mobile_no','',['placeholder'=>'Enter Mobile No' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Address</label>
								{{Form::text('address','',['placeholder'=>'Enter Address' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter City</label>
								{{Form::text('city','',['placeholder'=>'Enter City' , 'class'=>'form-control'])}}
							</div>
								<div class="form-group">
								<label class="form-label">Enter Country</label>
								{{Form::text('country','',['placeholder'=>'Enter Country' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Postal Code</label>
								{{Form::text('postal_code','',['placeholder'=>'Enter Postal Code' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Personal Persentation</label>
								{{ Form::textarea('personal_presentation','', ['class' => 'form-control','size' =>'50x3']) }}
							</div>
								<div class="form-group">
								<label class="form-label">Hobbies</label>
								{{ Form::textarea('hobbies','', ['class' => 'form-control','size' =>'50x3']) }}
							</div>
								<div class="form-group">
								<label class="form-label">Other Info</label>
								{{ Form::textarea('other_info','', ['class' => 'form-control','size' =>'50x3']) }}
							</div>
								<div class="form-group">
								<label class="form-label">Role</label>
								{{Form::select('users_role_id',$users_roles, null, ['placeholder' => 'Select Category ','class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Upload Image </label>
								{{Form::file('image',['class'=>'upload','id'=>'upload'])}}
							</div>
							<img id="img" src="#" alt="your image" style="display: none" />
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
	$(function(){
		$('#upload').change(function(){
			$('#img').css('display','block');
			var input = this;
			var url = $(this).val();
			var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
			if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
			{
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#img').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
			else
			{
				$('#img').attr('src', '/assets/no_preview.png');
			}
		});
		$("#create_form").validate({
			rules: {
				name: {
					required : true,
					minlength: 2,
					maxlength : 30
				},
				
				image : {
					required: true,
					accept : "image/*"
				},
			},
			messages: {
				name: {
					required : "Please specify your name.",
					minlength : "Minimum length should be 2 character.",
					maxlength : "Maximum length should be 30 character.",
				},

				image: {
					required: "Image field is required",
					accept: "Only image file allowed"
				},
			}
		});

	  var dateRangePicker = ['input[name= "birthday"]'];
	  dateRangePicker.forEach(PickerFunction);
	})

	function PickerFunction(picker) {
	  $(picker).daterangepicker({
	    timePicker: true,
	    singleDatePicker: true,
	    autoUpdateInput: false,
	    locale: {
	      format: 'DD-MM-YYYY H:mm:ss'
	    }
	  }, function(chosen_date) {
	    $(picker).val(chosen_date.format('DD-MM-YYYY H:mm:ss'));
	  });
	}
</script>
@endsection
