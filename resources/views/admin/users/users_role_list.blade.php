@extends('layouts.admin')
@section('content')
@if(session()->has('success'))
<div class="alert alert-success  alert-dismissible">
  <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> {{session('success')}}
</div>
@elseif(session()->has('error'))
<div class="alert alert-danger  alert-dismissible">
  <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Danger!</strong> {{session('error')}}
</div>
@endif
<div class="row">
  <div class="col-md-12">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item ">User</li>
        <li class="breadcrumb-item active" aria-current="page">Role</li>
      </ol>
    </nav>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="widget has-shadow">
      <div class="widget-header bordered no-actions d-flex align-items-center">
       <div class="col-md-8"><h4>UserModel</h4></div>
       <div class="col-md-4">
         {{Form::open(['url' => 'admin/users_role', 'method' => 'get' , 'class' =>'filter_form form-inline', 'id'=>'filter_form'])}}
         <input class="form-control" type="text" name ="filter_users_role" placeholder="Search" aria-label="Search" value="{{$filter_users_role}}">
         {{Form::button('<i class="fa fa-filter"></i>' ,['class'=>'btn btn-primary ml-auto ','id'=>"button-filter",'type'=>'submit'])}}
         {{Form::close()}}
       </div>
     </div>
     <div class="m-2 float-right">
      <a href="{{url('admin/users_role/create')}}" class="btn btn-info">Add</a>
      <a href="#" onclick="delete_users_role()" class="btn btn-info">Delete</a>
    </div>
    <div class="widget-body">
      <div class="table-responsive">
        <form class="users_role_delete" method="post" action = "{{url('admin/users_role_delete/')}}">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
        <table class="table table-bordered mb-0">
          <thead>
            <tr>
              <th>
                <input type="checkbox" name="groupCheck" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" ></th>
                <th><a href ="#" class="sort_order " >Role</a></th>
                <th>Positions</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              @foreach($users_roles as  $users_role)
              <tr>
                <td><input type="checkbox" name="selected[]" value="{{$users_role->id}}}" ></td>
                <td>{{$users_role->role}}</td>
                <td>{{$users_role->sort_order}}</td>
                <td class="td-actions">
                  <a href="{{url('admin/users_role/edit/'.$users_role->id)}}"><i class="la la-edit edit"></i></a>
                </td>
              </tr>

              @endforeach
            </tbody>
          </table>
        </form>
          @if(count($users_roles))
          <div class="">{{$users_roles->links()}}</div>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="{{url('public/js/bootbox.min.js')}}"></script>
<script type="text/javascript">
  function delete_users_role(){
    $('form.users_role_delete').submit();
  }
  $(document).ready(function(){
    $(document).on('click','.delete_users_role',function(){
      var $this = $(this);
      console.log(this)
      bootbox.confirm("Are you sure?", function(result){
        if(result){
          $this.parent().find('form').submit();
        }
        else{
        }
      });
    });
  })
</script>
@endsection



