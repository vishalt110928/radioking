@extends('layouts.admin')
@section('content')
<h1>Dashboard</h1>
<div class="row flex-row">
	<div class="col-xl-4 col-md-6 col-sm-6">
		<div class="widget widget-12 has-shadow">
			<div class="widget-body">
				<div class="media">
					<div class="align-self-center ml-5 mr-5">
						<i class="fa fa-play" aria-hidden="true"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="title text-facebook"> <a href = "{{url('admin/players')}}">Players</a></div>
						<div class="number">{{$players}}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Begin Facebook -->
	<div class="col-xl-4 col-md-6 col-sm-6">
		<div class="widget widget-12 has-shadow">
			<div class="widget-body">
				<div class="media">
					<div class="align-self-center ml-5 mr-5">
						<i class="fa fa-music" aria-hidden="true"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="title text-facebook"> <a href = "{{url('admin/tracks')}}">Musics</a></div>
						<div class="number">{{$tracks}}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Facebook -->
	<!-- Begin Twitter -->
	<div class="col-xl-4 col-md-6 col-sm-6">
		<div class="widget widget-12 has-shadow">
			<div class="widget-body">
				<div class="media">
					<div class="align-self-center ml-5 mr-5">
						<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="title text-twitter"><a href = "{{url('admin/playlists')}}">PlayLists</a></div>
						<div class="number">{{$playlists}}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Twitter -->
	<!-- Begin Linkedin -->
	<div class="col-xl-4 col-md-6 col-sm-6">
		<div class="widget widget-12 has-shadow">
			<div class="widget-body">
				<div class="media">
					<div class="align-self-center ml-5 mr-5">
						<i class="fa fa-arrows-alt" aria-hidden="true"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="title text-linkedin"><a href = "{{url('admin/podcasts')}}">Podcasts</a></div>
						<div class="number">{{$podcasts}}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Linkedin -->
	<div class="col-xl-4 col-md-6 col-sm-6">
		<div class="widget widget-12 has-shadow">
			<div class="widget-body">
				<div class="media">
					<div class="align-self-center ml-5 mr-5">
						<i class="fa fa-youtube-play" aria-hidden="true"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="title text-facebook"><a href = "{{url('admin/videos')}}">Videos</a></div>
						<div class="number">{{$videos}}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Facebook -->
	<!-- Begin Twitter -->
	<div class="col-xl-4 col-md-6 col-sm-6">
		<div class="widget widget-12 has-shadow">
			<div class="widget-body">
				<div class="media">
					<div class="align-self-center ml-5 mr-5">
						<i class="fa fa-plus-circle" aria-hidden="true"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="title text-twitter"><a href = "{{url('admin/contests')}}">Contests</a></div>
						<div class="number">{{$contests}}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Twitter -->
	<!-- Begin Linkedin -->
	<div class="col-xl-4 col-md-6 col-sm-6">
		<div class="widget widget-12 has-shadow">
			<div class="widget-body">
				<div class="media">
					<div class="align-self-center ml-5 mr-5">
						<i class="fa fa-key" aria-hidden="true"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="title text-linkedin"><a href = "{{url('admin/events')}}">Events</a></div>
						<div class="number">{{$events}}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-4 col-md-6 col-sm-6">
		<div class="widget widget-12 has-shadow">
			<div class="widget-body">
				<div class="media">
					<div class="align-self-center ml-5 mr-5">
						<i class="fa fa-address-card" aria-hidden="true"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="title text-facebook"><a href = "{{url('admin/artists')}}">Artists</a></div>
						<div class="number">{{$artists}}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-4 col-md-6 col-sm-6">
		<div class="widget widget-12 has-shadow">
			<div class="widget-body">
				<div class="media">
					<div class="align-self-center ml-5 mr-5">
						<i class="fa fa-newspaper-o" aria-hidden="true"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="title text-linkedin"><a href = "{{url('admin/news')}}">News</a></div>
						<div class="number">{{$news}}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-4 col-md-6 col-sm-6">
		<div class="widget widget-12 has-shadow">
			<div class="widget-body">
				<div class="media">
					<div class="align-self-center ml-5 mr-5">
						<i class="fa fa-product-hunt" aria-hidden="true"></i>
					</div>
					<div class="media-body align-self-center">
						<div class="title text-facebook"><a href = "{{url('admin/programs')}}">Shows</a></div>
						<div class="number">{{$programs}}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection