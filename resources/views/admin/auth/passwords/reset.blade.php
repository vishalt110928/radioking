<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Google Fonts -->
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
  <script>
    WebFont.load({
      google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
      active: function() {
        sessionStorage.fonts = true;
      }
    });
  </script>

  <link rel="apple-touch-icon" sizes="180x180" href="{{url('public/img/apple-touch-icon.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{url('public/img/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{url('public/img/favicon-16x16.png')}}">
  <!-- Stylesheet -->
  <link rel="stylesheet" href="{{url('public/assets/vendors/css/base/bootstrap.min.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{url('public/assets/vendors/css/base/elisyam-1.5.min.css')}}">
  <link rel="stylesheet" href="{{url('public/css/style.css')}}">
</head>
<body class="bg-white">
  <!-- Begin Preloader -->
  <div id="preloader">
    <div class="canvas">
      <img src="{{url('public/img/front_logo.png')}}" alt="logo" class="loader-logo">
      <div class="spinner"></div>   
    </div>
  </div>
  <!-- End Preloader -->
  <!-- Begin Container -->
  <div class="container-fluid no-padding h-100">
    <div class="row flex-row h-100 bg-white">
      <!-- Begin Left Content -->
      <div class="col-xl-3 col-lg-5 col-md-5 col-sm-12 col-12 no-padding">
        <div class="elisyam-bg background-03">
          <div class="elisyam-overlay overlay-08"></div>
          <div class="authentication-col-content-2 mx-auto text-center">
            <div class="logo-centered">
              <a href="db-default.html">
                <img src="{{url('public/img/front_logo.png')}}" alt="logo">
              </a>
            </div>
            <ul class="login-nav nav nav-tabs mt-5 justify-content-center" role="tablist" id="animate-tab">
              <li><a class="active" href="{{url('admin/login')}}">Sign In</a></li>
            </ul>
          </div>
        </div>
      </div>
      <!-- End Left Content -->
      <!-- Begin Right Content -->
      <div class="col-xl-9 col-lg-7 col-md-7 col-sm-12 col-12 my-auto no-padding">
        <!-- Begin Form -->
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <div class="card">
          <div class="card-header">{{ __('Reset Password') }}</div>

          <div class="card-body">
            <form method="POST" action="{{ route('admin.password.update') }}">
              @csrf

              <input type="hidden" name="token" value="{{ $token }}">

              <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                <div class="col-md-6">
                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                  @error('email')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                <div class="col-md-6">
                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                  @error('password')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                <div class="col-md-6">
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                </div>
              </div>

              <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                  <button type="submit" class="btn btn-primary">
                    {{ __('Reset Password') }}
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- End Right Content -->
    </div>
    <!-- End Row -->
  </div>
  <!-- End Container -->    
  <!-- Begin Vendor Js -->
  <script src="{{url('public/js/jquery.min.js')}}"></script>
  <script src="{{url('public/js/core.min.js')}}"></script>
  <!-- End Vendor Js -->
  <!-- Begin Page Vendor Js -->
  <script src="{{url('public/js/app.js')}}"></script>
  <!-- End Page Vendor Js -->
  <!-- Begin Page Snippets -->
  <script src="{{url('assets/js/animated-tabs.min.js')}}"></script>
  <!-- End Page Snippets -->
</body>
</html>


