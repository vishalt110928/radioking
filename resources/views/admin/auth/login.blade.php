<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Google Fonts -->
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
  <script>
    WebFont.load({
      google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
      active: function() {
        sessionStorage.fonts = true;
      }
    });
  </script>

  <link rel="apple-touch-icon" sizes="180x180" href="{{url('public/img/apple-touch-icon.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{url('public/img/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{url('public/img/favicon-16x16.png')}}">
  <!-- Stylesheet -->
  <link rel="stylesheet" href="{{url('public/assets/vendors/css/base/bootstrap.min.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{url('public/assets/vendors/css/base/elisyam-1.5.min.css')}}">
  <link rel="stylesheet" href="{{url('public/css/style.css')}}">
</head>
<body class="bg-white">
  <!-- Begin Preloader -->
  <div id="preloader">
    <div class="canvas">
      <img src="{{url('public/img/front_logo.png')}}" alt="logo" class="loader-logo">
      <div class="spinner"></div>   
    </div>
  </div>
  <!-- End Preloader -->
  <!-- Begin Container -->
  <div class="container-fluid no-padding h-100">
    <div class="row flex-row h-100 bg-white">
      <!-- Begin Left Content -->
      <div class="col-xl-3 col-lg-5 col-md-5 col-sm-12 col-12 no-padding">
        <div class="elisyam-bg background-03">
          <div class="elisyam-overlay overlay-08"></div>
          <div class="authentication-col-content-2 mx-auto text-center">
            <div class="logo-centered">
              <a href="db-default.html">
                <img src="{{url('public/img/front_logo.png')}}" alt="logo">
              </a>
            </div>
          </div>
        </div>
      </div>
      <!-- End Left Content -->
      <!-- Begin Right Content -->
      <div class="col-xl-9 col-lg-7 col-md-7 col-sm-12 col-12 my-auto no-padding">
        <!-- Begin Form -->
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <div class="authentication-form-2 mx-auto">
          <h3>Sign In</h3>
          <form method="POST" action="{{ url('admin/login') }}">
            @csrf
            <div class="group material-input">
             <input type="text" name="email" class="@error('email') is-invalid @enderror" value="{{ old('email') }}">
             <span class="highlight"></span>
             <span class="bar"></span>
             <label>Email</label>
             @error('email')
             <span class="invalid-feedback" role="alert">
               <strong>{{ $message }}</strong>
             </span>
             @enderror
           </div>
           <div class="group material-input">
             <input type="password" name="password" class="@error('password') is-invalid @enderror" value="">
             <span class="highlight"></span>
             <span class="bar"></span>
             <label>Password</label>
             @error('password')
             <span class="invalid-feedback" role="alert">
               <strong>{{ $message }}</strong>
             </span>
             @enderror
           </div>
           <div class="row">
            <div class="col text-left">
              <div class="styled-checkbox">
                <input type="checkbox" id="remeber" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label for="remeber">Remember me</label>
              </div>
            </div>
            <div class="col text-right">
              <a href="{{url('admin/password/reset')}}">Forgot Password ?</a>
            </div>
          </div>
          <div class="sign-btn text-center">
            <input type="submit" value = "Sign In" class="btn btn-lg btn-gradient-01">
          </div>
        </form>
        <!-- End Sign Up -->
      </div>
    </div>
    <!-- End Form -->                        
  </div>
  <!-- End Right Content -->
</div>
<!-- End Row -->
</div>
<!-- End Container -->    
<!-- Begin Vendor Js -->
<script src="{{url('public/js/jquery.min.js')}}"></script>
<script src="{{url('public/js/core.min.js')}}"></script>
<!-- End Vendor Js -->
<!-- Begin Page Vendor Js -->
<script src="{{url('public/js/app.js')}}"></script>
<!-- End Page Vendor Js -->
<!-- Begin Page Snippets -->
<script src="{{url('assets/js/animated-tabs.min.js')}}"></script>
<!-- End Page Snippets -->
</body>
</html>