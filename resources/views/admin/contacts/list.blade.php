@extends('layouts.admin')
@section('content')
@if(session()->has('success'))
<div class="alert alert-success  alert-dismissible">
  <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> {{session('success')}}
</div>
@elseif(session()->has('error'))
<div class="alert alert-danger  alert-dismissible">
  <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Danger!</strong> {{session('error')}}
</div>
@endif
<div class="row">
  <div class="col-md-12">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Contacts</li>
      </ol>
    </nav>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="widget has-shadow">
      <div class="widget-header bordered no-actions d-flex align-items-center">
       <div class="col-md-8"><h4>ContactsModel</h4></div>
       <div class="col-md-4">
       </div>
     </div>
     <div class="m-2 float-right">
      <a href="#" onclick="contact_delete()" class="btn btn-info">Delete</a>
    </div>
    <div class="well m-5">
      {{Form::open(['url' => 'admin/contacts', 'method' => 'get' , 'class' =>'card' , 'id'=>'create_form'])}}
      <div class="row padd_filter">
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="filter_contacts_name">Name </label>
            <input type="text" name="filter_contacts_name" value="{{$filter_contacts_name}}" placeholder="Enter Name" id="filter_contacts_name" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
          </div>
          <div class="form-group">
            <label class="control-label" for="filter_contacts_email">Email</label>
            <input type="text" name="filter_contacts_email" value="{{$filter_contacts_email}}" placeholder="Enter Email" id="filter_contacts_email" class="form-control">
          </div>
        </div>
        <div class="col-sm-4">
           <label class="control-label" for="filter_contacts_phone">Phone No </label>
          <div class="form-group">
            <input type="text" name="filter_contacts_phone" value="{{$filter_contacts_phone}}" placeholder="Enter Phone No" id="filter_contacts_phone" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="filter_contacts_created">Created At</label>
            <input type="text" name="filter_contacts_created" value="{{$filter_contacts_created}}" placeholder="Enter Created At" id="filter_contacts_created" class="form-control">
          </div>
          {{Form::button('<i class="fa fa-filter"></i> Filter' ,['class'=>'btn btn-primary ml-auto','id'=>"button-filter",'type'=>'submit'])}}

          {{Form::button('<i class="fa fa-reset"></i> Reset' ,['class'=>'btn btn-primary ml-auto reset_filter','id'=>"button-filter"])}}

          {{Form::close()}}
        </div>
      </div>
    </div>
    <div class="widget-body">
      <div class="table-responsive">
        <form class="contacts_delete" method="post" action = "{{url('admin/contacts_delete/')}}">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
          <table class="table table-bordered mb-0">
            <thead>
              <tr>
                <th>
                  <input type="checkbox" name="groupCheck" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" >
                </th>
                <th>
                  @if($orderBy == 'name' && $sort == "desc")
                  <a href ="{{url('admin/contacts/?orderBy=name&sort=asc&'.$query)}}">Name<i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                  @elseif($orderBy == 'name' && $sort == "asc")
                  <a href ="{{url('admin/contacts/?orderBy=name&sort=desc&'.$query)}}">Name <i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                  @else
                  <a href ="{{url('admin/contacts/?orderBy=name&sort=desc&'.$query)}}">Name <i class="fa fa-sort" aria-hidden="true"></i></a>
                  @endif
                </th>
                <th>
                  @if($orderBy == 'email' && $sort == "desc")
                  <a href ="{{url('admin/contacts/?orderBy=email&sort=asc&'.$query)}}">Email <i class="fa fa-sort-desc" aria-hidden="true"></a>
                    @elseif($orderBy == 'email' && $sort == "asc")
                    <a href ="{{url('admin/contacts/?orderBy=email&sort=desc&'.$query)}}">Email <i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                    @else
                    <a href ="{{url('admin/contacts/?orderBy=email&sort=desc&'.$query)}}">Email <i class="fa fa-sort" aria-hidden="true"></i></a>
                    @endif
                  </th>
                  <th>
                    @if($orderBy == 'phone_no' && $sort == "desc")
                    <a href ="{{url('admin/contacts/?orderBy=phone_no&sort=asc&'.$query)}}">Phone No <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                    @elseif($orderBy == 'phone_no' && $sort == "asc") 
                    <a href ="{{url('admin/contacts/?orderBy=phone_no&sort=desc&'.$query)}}">Phone No <i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                    @else
                    <a href ="{{url('admin/contacts/?orderBy=phone_no&sort=desc&'.$query)}}">Phone No <i class="fa fa-sort" aria-hidden="true"></i></a>
                    @endif
                  </th>
                  <th>
                    <a href ="#">Description</a>
                  </th>
                  <th>
                    @if($orderBy == 'created_at' && $sort == "desc")
                    <a href ="{{url('admin/contacts/?orderBy=created_at&sort=asc&'.$query)}}">Created At<i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                    @elseif($orderBy == 'created_at' && $sort == "asc") 
                    <a href ="{{url('admin/contacts/?orderBy=created_at&sort=desc&'.$query)}}">Created At<i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                    @else
                    <a href ="{{url('admin/contacts/?orderBy=created_at&sort=desc&'.$query)}}">Created At <i class="fa fa-sort" aria-hidden="true"></i></a>
                    @endif
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach($contacts as  $contact)
                <tr>
                  <td><input type="checkbox" name="selected[]" value="{{$contact->id}}}" ></td>
                  <td>{{$contact->name}}</td>
                  <td>{{$contact->email}}</td>
                  <td>
                    {{$contact->phone_no}}
                  </td>
                  <td>
                   {{ str_limit($contact->message, $limit = 20, $end = '...')}}
                 </td>
                 <td>
                  @if($contact->created_at)
                  {{ \Carbon\Carbon::parse($contact->created_at)->isoFormat('MMMM Do YYYY, h:mm:ss a')}}
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </form>
      </div>
    </div>
    <div class="pagination_div text-center">
      {{$contacts->appends($appends)->links()}}
    </div>
  </div>
</div>
</div>
@endsection
@section('script')
<script src="{{url('public/js/bootbox.min.js')}}"></script>
<script type="text/javascript">
  function contact_delete(){
    var $this = $(this);
    console.log(this)
    bootbox.confirm("Are you sure?", function(result){
      if(result){
        $('form.contacts_delete').submit();
      }
      else{
      }
    });
  }
  $(document).ready(function(){
    $(document).on('click','.reset_filter',function() {
     $('#create_form').find('input,select').val('');
     $('#create_form').submit();
   })
    var dateRangePicker = ['input[name= "filter_contacts_created"]'];
    dateRangePicker.forEach(PickerFunction);
  })
  function PickerFunction(picker) {
    $(picker).daterangepicker({
      timePicker: true,
      singleDatePicker: true,
      autoUpdateInput: false,
      locale: {
        format: 'DD-MM-YYYY H:mm:ss'
      }
    }, function(chosen_date) {
      $(picker).val(chosen_date.format('DD-MM-YYYY H:mm:ss'));
    });
  }
</script>
@endsection



