@extends('layouts.admin')
@section('title','Create Block')
@section('content')	
<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Create Block</h4>
		</div>
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/blocks', 'method' => 'post' , 'class' =>'card' , 'files' => true , 'id'=>'create_form'])}}
				@csrf
				<div class="card-header">
					<h3 class="card-title"></h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label class="form-label">Enter Title</label>
								{{Form::text('title','',['placeholder'=>'Enter Title' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Publish</label>
								{{Form::checkbox('publish', '1',false,['class' => 'form-control'])}}
							</div>
							<a href="#" class="btn btn-primary ml-auto block_btn" data-block = 0>
								<i class="fa fa-plus" aria-hidden="true"></i>
							</a>
						</div>
						<div class="block_sec">
							
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">       
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="hidden select_div" style="display:none;">
	<div class="form-group">
		<label class="form-label">Positions</label>
		{{Form::number('sort_order','0',['placeholder'=>'Views' ,'class'=>'form-control'])}}
	</div>
	<div class="form-group">
		<label class="form-label">Pages</label>
		{{Form::select('pages',$pages,null, ['placeholder' => 'Select Pages','class'=>'form-control'])}}
	</div>
</div>
<div class="hidden after_select_div" style="display:none;">
	<div class="form-group">
		<label class="form-label">Positions</label>
		{{Form::number('sort_order','0',['placeholder'=>'Views' ,'class'=>'form-control'])}}
	</div>
	<div class="form-group">
		<label class="form-label">Pages</label>
		{{Form::select('pages',$pages,null, ['placeholder' => 'Select Pages','class'=>'form-control'])}}
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
	$(document).ready(function() {
		$(document).on('click','.block_btn',function(){
			var block = $(this).data('block')+1;

			$('.select_div').find('input[name = "sort_order"]').attr('name',"blocks['"+block+"'][sort_order]");
			$('.select_div').find('select[name = "pages"]').attr('name',"blocks['"+block+"'][page]");
		 	var htm = $('.select_div').html();
			$('.block_sec').append(htm);
			 var after_select_div_html =  $('.after_select_div').html();
			$('.select_div').html(after_select_div_html);
			$(this).data('block',block)
		});
	});
</script>
@endsection
