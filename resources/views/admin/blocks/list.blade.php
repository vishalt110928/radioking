@extends('layouts.admin')
@section('content')
@if(session()->has('success'))
<div class="alert alert-success  alert-dismissible">
  <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> {{session('success')}}
</div>
@elseif(session()->has('error'))
<div class="alert alert-danger  alert-dismissible">
  <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Danger!</strong> {{session('error')}}
</div>
@endif
<div class="row">
  <div class="col-md-12">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Blocks</li>
      </ol>
    </nav>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="float-right">
      <a href="{{url('admin/blocks/create')}}" class="btn btn-info">Add</a>
      <a href="#"  class="btn btn-info delete_blocks">Delete</a>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="widget-body">
      <div class="table-responsive">
        <form class="blocks_delete" method="post" action = "{{url('admin/blocks_delete/')}}">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
          <table class="table table-bordered mb-0">
            <thead>
              <tr>
                <th>
                  <input type="checkbox" name="groupCheck" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" >
                </th>
                <th>
                  @if($orderBy == 'title' && $sort == "desc")
                  <a href ="{{url('admin/blocks/?orderBy=title&sort=asc')}}">Title <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                  @elseif($orderBy == 'title' && $sort == "asc")
                  <a href ="{{url('admin/blocks/?orderBy=title&sort=desc')}}">Title <i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                  @else
                  <a href ="{{url('admin/blocks/?orderBy=title&sort=desc')}}">Title <i class="fa fa-sort" aria-hidden="true"></i></a>
                  @endif
                </th>
                <th>
                  <a href ="">Page</a>
                </th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              @foreach($blocks as  $block)
              <tr>
                <td><input type="checkbox" name="selected[]" value="{{$block->id}}}" ></td>
                <td>
                  {{$block->title}}
                </td>
                <td>
                  @foreach($block->pages as $page)
                  {{$page->title}}
                  @endforeach
                </td>
                <td>
                  @if($block->publish)
                  <span style="width:100px;"><span class="badge-text badge-text-small info">Publish</span></span>
                  @else
                  <span style="width:100px;"><span class="badge-text badge-text-small danger">Not Publish</span></span>
                  @endif
                </td>
                <td class="td-actions">
                  <a href="{{url('admin/blocks/'.$block->id.'/edit/')}}"><i class="la la-edit edit"></i></a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </form>
      </div>
      <div class="pagination_div text-center">
        {{$blocks->links()}}
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="{{url('public/js/bootbox.min.js')}}"></script>
<script type="text/javascript">
  function delete_blocks(){
    $('form.blocks_delete').submit();
  }
  $(document).ready(function(){
    $(document).on('click','.delete_blocks',function(){
      var $this = $(this);
      bootbox.confirm("Are you sure?", function(result){
        if(result){
          delete_blocks();
        }
        else{
        }
      });
    });

    $(document).on('click','.reset_filter',function() {
      $('#create_form').find('input,select').val('');
      $('#create_form').submit();
    })
  })
</script>
@endsection



