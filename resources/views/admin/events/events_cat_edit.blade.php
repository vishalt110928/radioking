@extends('layouts.admin')
@section('title','Edit News')
@section('content')	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Edit Events Category</h4>
		</div>

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/events_cat/update/'.$events_cat->id, 'method' => 'put' , 'class' =>'card' , 'files' => true , 'id'=>'edit_form'])}}
				@csrf

				<div class="card-header">
					<h3 class="card-title">Form elements</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label class="form-label">Enter Name</label>
								{{Form::text('name',$events_cat->name,['placeholder'=>'Name' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Publish</label>
								@if(!$events_cat->publish)
								{{Form::checkbox('publish', '1',false,['class' => 'form-control'])}}
								@else 
								{{Form::checkbox('publish', '1',true,['class' => 'form-control'])}}
								@endif
							</div>
							<div class="form-group">
								<label class="form-label">Parent Category</label>
								{{Form::select('parent_cat', array(),$events_cat->parent_cat)}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Positions</label>
								{{Form::number('sort_order',$events_cat->sort_order,['placeholder'=>'Enter Positions' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Description</label>
								{{ Form::textarea('des',$events_cat->des, ['class' => 'form-control', 'required' => '', 'size' =>'50x3']) }}
							</div>
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
	$(function(){
		$("#edit_form").validate({
			rules: {
				name: {
					required : true,
					minlength: 2,
				}
			},
			messages: {
				name: {
					required : "Please specify your name.",
					minlength : "Minimum length should be 2 character.",
				}
			}
		});

	});
</script>
@endsection
