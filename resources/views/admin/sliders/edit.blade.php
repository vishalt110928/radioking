@extends('layouts.admin')
@section('title','Edit News')
@section('content')	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Edit Slider</h4>
		</div>

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/sliders/'.$id, 'method' => 'put' , 'class' =>'card' , 'files' => true , 'id'=>'edit_form'])}}
				@csrf
				<div class="card-header">
					<h3 class="card-title">Form elements</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label class="form-label">Enter Title</label>
								{{Form::text('title',$slider->title,['placeholder'=>'Enter Title' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Publish</label>
								@if($slider->publish)
								{{Form::checkbox('publish', '1','true',['class' => 'form-control'])}}
								@else 
								{{Form::checkbox('publish', '1','false',['class' => 'form-control'])}}
								@endif
							</div>
							<div class="form-group">
								<label class="form-label">Enter Slide Url</label>
								{{Form::text('slide_url',$slider->slide_url,['placeholder'=>'Enter Slide Url' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Publish Time</label>
								{{Form::text('publish_time',$slider->publish_time,['placeholder'=>'Enter Publish Time' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Start Date</label>
								{{Form::text('start_date',$slider->start_date,['placeholder'=>'Enter End Time' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter End Date</label>
								{{Form::text('end_date',$slider->end_date,['placeholder'=>'Enter End Time' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Upload Image </label>
								{{Form::file('image',['class'=>'upload','id'=>'upload'])}}
							</div>
							<img id="img" src="#" alt="your image" style="display: none" />
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
	$(function(){
		$('#upload').change(function(){
			$('#img').css('display','block');
			var input = this;
			var url = $(this).val();
			console.log(url);
			var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
			if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
			{
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#img').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
			else
			{
				$('#img').attr('src', '/assets/no_preview.png');
			}
		});
		$("#edit_form").validate({
			rules: {
				name: {
					required : true,
					minlength: 2,
					maxlength : 30
				},
				
				image : {
					accept : "image/*"
				},
			},
			messages: {
				name: {
					required : "Please specify your name.",
					minlength : "Minimum length should be 2 character.",
					maxlength : "Maximum length should be 30 character.",
				},

				image: {
					accept: "Only image file allowed"
				},
			}
		});
		var dateRangePicker = ['input[name= "publish_time"]','input[name= "start_date"]','input[name= "end_date"]'];
		dateRangePicker.forEach(PickerFunction);
	})
	function PickerFunction(picker) {
		$(picker).daterangepicker({
			timePicker: true,
			singleDatePicker: true,
			autoUpdateInput: false,
			locale: {
				format: 'DD-MM-YYYY H:mm:ss'
			}
		}, function(chosen_date) {
			$(picker).val(chosen_date.format('DD-MM-YYYY H:mm:ss'));
		});
	}
</script>
@endsection
