@extends('layouts.admin')
@section('content')
@if(session()->has('success'))
<div class="alert alert-success  alert-dismissible">
  <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> {{session('success')}}
</div>
@elseif(session()->has('error'))
<div class="alert alert-danger  alert-dismissible">
  <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Danger!</strong> {{session('error')}}
</div>
@endif
<div class="row">
  <div class="col-md-12">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Sliders</li>
      </ol>
    </nav>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="widget has-shadow">
      <div class="widget-header bordered no-actions d-flex align-items-center">
       <div class="col-md-8"><h4>SlidersModel</h4></div>
       <div class="col-md-4">
         {{Form::open(['url' => 'admin/sliders', 'method' => 'get' , 'class' =>'filter_form form-inline', 'id'=>'filter_form'])}}
         <input class="form-control" type="text" name ="filter_sliders" placeholder="Search" aria-label="Search" value="{{$filter_sliders}}">
         {{Form::button('<i class="fa fa-filter"></i>' ,['class'=>'btn btn-primary ml-auto ','id'=>"button-filter",'type'=>'submit'])}}
         {{Form::close()}}
       </div>
     </div>
     <div class="m-2 float-right">
      <a href="{{url('admin/sliders/create')}}" class="btn btn-info">Add</a>
      <a href="#" onclick="delete_sliders()" class="btn btn-info">Delete</a>
    </div>
    <div class="well m-5">
      {{Form::open(['url' => 'admin/sliders', 'method' => 'get' , 'class' =>'card' , 'id'=>'create_form'])}}
      <div class="row padd_filter">
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="input_title">News Title </label>
            <input type="text" name="filter_title" value="{{$filter_title}}" placeholder="Product Name" id="input_title" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
          </div>
          <div class="form-group">
            <label class="control-label" for="input_end_time">End Date</label>
            <input type="text" name="filter_end_date" value="{{$filter_end_date}}" placeholder="Enter end time" id="input_end_time" class="form-control">
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="input_cat">Start Date</label>
            <input type="text" name="filter_start_date" value="{{$filter_start_date}}" placeholder="Enter start date" id="input_start_date" class="form-control">
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="input_publish_time">Publish Time</label>
            <input type="text" name="filter_publish_time" value="{{$filter_publish_time}}" placeholder="Enter Publis time" id="input_publish_time" class="form-control">
          </div>
          {{Form::button('<i class="fa fa-filter"></i> Filter' ,['class'=>'btn btn-primary ml-auto','id'=>"button-filter",'type'=>'submit'])}}
          {{Form::button('<i class="fa fa-reset"></i> Reset' ,['class'=>'btn btn-primary ml-auto reset_filter','id'=>"button-filter"])}}
          {{Form::close()}}
        </div>
      </div>
    </div>
    <div class="widget-body">
      <div class="table-responsive">
        <form class="sliders_delete" method="post" action = "{{url('admin/sliders_delete/')}}">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
          <table class="table table-bordered mb-0">
            <thead>
              <tr>
                <th>
                  <input type="checkbox" name="groupCheck" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" ></th>
                  <th>Image</th>
                  <th>
                    @if($orderBy == 'title' && $sort == "desc")
                    <a href ="{{url('admin/sliders/?orderBy=title&sort=asc&'.$query)}}">Title <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                    @elseif($orderBy == 'title' && $sort == "asc")
                    <a href ="{{url('admin/sliders/?orderBy=title&sort=desc&'.$query)}}">Title <i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                    @else
                    <a href ="{{url('admin/sliders/?orderBy=title&sort=desc&'.$query)}}">Title <i class="fa fa-sort" aria-hidden="true"></i></a>
                    @endif
                  </th>
                  <th>
                    @if($orderBy == 'publish_time' && $sort == "desc")
                    <a href ="{{url('admin/sliders/?orderBy=publish_time&sort=asc&'.$query)}}">Publication Date <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                    @elseif($orderBy == 'publish_time' && $sort == "asc") 
                    <a href ="{{url('admin/sliders/?orderBy=publish_time&sort=desc&'.$query)}}">Publication Date <i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                    @else
                    <a href ="{{url('admin/sliders/?orderBy=publish_time&sort=desc&'.$query)}}">Publication Date <i class="fa fa-sort" aria-hidden="true"></i></a>
                    @endif
                  </th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($sliders as  $slider)
                <tr>
                  <td><input type="checkbox" name="selected[]" value="{{$slider->id}}}" ></td>
                  <td><img src="{{url('public/uploads/'.$slider->image)}}" height="100" width="100"></td>
                  <td>{{$slider->title}}</td>
                  <td>@if($slider->publish_time)
                    {{ \Carbon\Carbon::parse($slider->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')}}
                    @endif
                  </td>
                  <td>
                    @if($slider->publish)
                    <span style="width:100px;"><span class="badge-text badge-text-small info">Publish</span></span>
                    @else
                    <span style="width:100px;"><span class="badge-text badge-text-small danger">Not Publish</span></span>
                    @endif
                  </td>
                  <td class="td-actions">
                    <a href="{{url('admin/sliders/'.$slider->id.'/edit/')}}"><i class="la la-edit edit"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="{{url('public/js/bootbox.min.js')}}"></script>
<script type="text/javascript">
  function delete_sliders(){
    $('form.sliders_delete').submit();
  }
  $(document).ready(function(){
    $(document).on('click','.delete_sliders',function(){
      var $this = $(this);
      console.log(this)
      bootbox.confirm("Are you sure?", function(result){
        if(result){
          $this.parent().find('form').submit();
        }
        else{
        }
      });
    });
    var dateRangePicker = ['input[name= "filter_publish_time"]','input[name= "filter_start_date"]','input[name= "filter_end_date"]'];
    dateRangePicker.forEach(PickerFunction);
  })
  function PickerFunction(picker) {
    $(picker).daterangepicker({
      timePicker: true,
      singleDatePicker: true,
      autoUpdateInput: false,
      locale: {
        format: 'DD-MM-YYYY H:mm:ss'
      }
    }, function(chosen_date) {
      $(picker).val(chosen_date.format('DD-MM-YYYY H:mm:ss'));
    });
  }
</script>
@endsection



