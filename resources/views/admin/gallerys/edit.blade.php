@extends('layouts.admin')
@section('title','Edit Gallerys')
@section('content')	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Edit Gallerys</h4>
		</div>

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/gallerys/'.$id, 'method' => 'put' , 'class' =>'card' , 'files' => true , 'id'=>'edit_form'])}}
				@csrf
				<div class="card-header">
					<h3 class="card-title">Form elements</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label class="form-label">Enter Title</label>
								{{Form::text('title',$gallerys->title,['placeholder'=>'Enter Title' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Publish</label>
								{{Form::checkbox('publish', '1',$gallerys->publish,['class' => 'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Category</label>
								{{Form::select('gallery_cat_id',$gallerys_cats ,$gallerys->gallerys_cat_id, ['placeholder' => 'Select Category','class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Publish Time</label>
								{{Form::text('publish_time',$gallerys->publish_time,['placeholder'=>'Enter Publish Time' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter End Time</label>
								{{Form::text('end_time',$gallerys->end_time,['placeholder'=>'Enter End Time' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Description</label>
								{{ Form::textarea('description',$gallerys->des, ['class' => 'form-control','size' =>'50x3']) }}
							</div>
						</div>
						<div class="col-md-6 col-lg-6">
							<div class="row">
								@foreach($gallerys->photos as $photo)
								<div class="col-md-4">
									<div class="thumbnail">
										<a href="{{url('public/uploads/'.$photo->path)}}" target="_blank">
											<img src="{{url('public/uploads/'.$photo->path)}}" alt="Lights" class="img-thumbnail">
											<div class="caption">
												<a href="{{url('admin/photo_delete/'.$gallerys->id.'/'.$photo->id)}}" class="photo_delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
											</div>
										</a>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
	$(function(){
		$(document).on('click','.photo_delete',function(e){
			e.preventDefault();
			let url = $(this).attr('href');
			var request = $.ajax({
				url: url,
				type: "DELETE",
				dataType: "json",
				beforeSend : function (){

				},
				success:function(response) {
					alert(response.message);
					window.location.reload();
				},
				error : function(response){
					console.log(response)
				},
				complete: function() {

				}
			});

			request.fail(function(jqXHR, textStatus) {
				alert( "Request failed: " + textStatus );
			});
		})
		$('#upload').change(function(){
			$('#img').css('display','block');
			var input = this;
			var url = $(this).val();
			console.log(url);
			var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
			if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
			{
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#img').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
			else
			{
				$('#img').attr('src', '/assets/no_preview.png');
			}
		});
		$("#edit_form").validate({
			rules: {
				name: {
					required : true,
					minlength: 2,
					maxlength : 30
				},
				
				image : {
					accept : "image/*"
				},
			},
			messages: {
				name: {
					required : "Please specify your name.",
					minlength : "Minimum length should be 2 character.",
					maxlength : "Maximum length should be 30 character.",
				},

				image: {
					accept: "Only image file allowed"
				},
			}
		});
		var dateRangePicker = ['input[name= "filter_publish_time"]','input[name= "filter_end_time"]','input[name="publish_time"]','input[name = "end_time" ]'];
		dateRangePicker.forEach(PickerFunction);
	});
	function PickerFunction(picker) {
		$(picker).daterangepicker({
			timePicker: true,
			singleDatePicker: true,
			autoUpdateInput: false,
			locale: {
				format: 'DD-MM-YYYY H:mm:ss'
			}
		}, function(chosen_date) {
			$(picker).val(chosen_date.format('DD-MM-YYYY H:mm:ss'));
		});
	}
</script>
@endsection
