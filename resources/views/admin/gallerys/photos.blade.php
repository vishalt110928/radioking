@extends('layouts.admin')
@section('title','Create Photos Gallerys')
@section('content')
<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Create Photos Gallery</h4>
		</div>
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/gallerys/photos_store', 'method' => 'post' , 'class' =>'card' , 'files' => true , 'id'=>'create_form'])}}
				@csrf
				<div class="card-header">
					<h3 class="card-title"></h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label class="form-label">Category</label>
								{{Form::select('gallery_id',$gallerys , null, ['placeholder' => 'Select Category ','class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Choose Multiple File</label>
								{{Form::file('photos[]',['placeholder'=>'Choose photos Gallery', 'class'=>'form-control photos','multiple'=>'multiple'])}}
							</div>
							<div class="gallery"></div>
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
	$(function() {
		// Multiple images preview in browser
		var imagesPreview = function(input, placeToInsertImagePreview) {
			if (input.files) {
				var filesAmount = input.files.length;
				for (i = 0; i < filesAmount; i++) {
					var reader = new FileReader();
					reader.onload = function(event) {
						$($.parseHTML('<img height="100" width = "100">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
					}
					reader.readAsDataURL(input.files[i]);
				}
			}
		};
		$('.photos').on('change', function() {
			imagesPreview(this, 'div.gallery');
		});
		$("#create_form").validate({
			rules: {
				gallery_id: {
					required: true,
				},
				"photos[]": {
					required: true,
					accept: "image/*"
				},
			},
			messages: {}
		});
	});
</script>
@endsection