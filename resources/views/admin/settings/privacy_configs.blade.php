@extends('layouts.admin')
@section('title','Privacy Configrutataion')
@section('content')	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Privacy Configrutataion</h4>
		</div>

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/privacy_configs_update', 'method' => 'post' , 'class' =>'card' , 'files' => true , 'id'=>'create_edit_form'])}}
				@csrf
				{{Form::hidden('id',isset($privacy_config->id)?$privacy_config->id:'')}}
				<div class="card-header">
					<h3 class="card-title">Privacy Configrutataion</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Privacy Policy</label>
									</div>
									<div class="col-lg-9">
										{{Form::textarea('privacy_policy',isset($privacy_config->privacy_policy)?$privacy_config->privacy_policy:'',['placeholder'=>'Enter Privacy Policy','class'=>'form-control','size'=>'50x3'])}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Policy Link</label>
									</div>
									<div class="col-lg-9">
										{{Form::checkbox('policy_link','1',isset($privacy_config->legal_notice_link)?$privacy_config->legal_notice_link: false)}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Legal Notice</label>
									</div>
									<div class="col-lg-9">
										{{Form::textarea('legal_notice',isset($privacy_config->legal_notice)?$privacy_config->legal_notice:'',['placeholder'=>'Enter Legal Notice','class'=>'form-control','size'=>'50x3'])}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Legal Notice Link</label>
									</div>
									<div class="col-lg-9">
										{{Form::checkbox('legal_notice_link','1',isset($privacy_config->legal_notice_link)? $privacy_config->legal_notice_link: false)}}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
</script>
@endsection
