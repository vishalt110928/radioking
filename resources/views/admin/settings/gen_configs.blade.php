@extends('layouts.admin')
@section('title','General Configuration')
@section('content')	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">General Configuration</h4>
		</div>

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/gen_configs_update', 'method' => 'post' , 'class' =>'card' , 'files' => true , 'id'=>'create_edit_form'])}}
				@csrf
			{{Form::hidden('id',isset($gen_config->id)?$gen_config->id:'')}}
				<div class="card-header">
					<h3 class="card-title">General Configuration</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label class="form-label">Enter Site Title</label>
								{{Form::text('site_title',isset($gen_config->site_title)?$gen_config->site_title:'',['placeholder'=>'Enter Site Title' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Page Title</label>
								{{Form::text('page_title',isset($gen_config->page_title)?$gen_config->page_title:'',['placeholder'=>'Enter Page Title' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Keywords</label>
								{{Form::text('keywords',isset($gen_config->keywords)?$gen_config->keywords:'',['placeholder'=>'Enter Keywords' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Meta Description </label>
								{{Form::textarea('meta_dis',isset($gen_config->meta_dis)?$gen_config->meta_dis:'',['placeholder'=>'Enter Meta Description' , 'class'=>'form-control','size'=> '50x3'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Javascript Code</label>
								{{Form::textarea('js_script',isset($gen_config->js_script)?$gen_config->js_script:'',['placeholder'=>'Enter Javascript Code' , 'class'=>'form-control','size'=> '50x3'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Header Code/Tag</label>
								{{Form::text('head_code',isset($gen_config->head_code)?$gen_config->head_code:'',['placeholder'=>'Enter Header Code/Tag' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Languagae</label>
								{{Form::text('lang',isset($gen_config->lang)?$gen_config->lang:'',['placeholder'=>'Enter Languagae' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter TimeZone</label>
								{{Form::text('time_zone',isset($gen_config->time_zone)?$gen_config->time_zone:'',['placeholder'=>'Enter TimeZone' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Enter Site Maintainance</label>
								{{Form::checkbox('site_maintainance','1',isset($gen_config->site_maintainance)?$gen_config->site_maintainance:false)}}
							</div>
							<div class="site_maintainance"  style= " @if($gen_config->site_maintainance) display:block; @else display: none; @endif">
							<div class="form-group">
								<label class="form-label">Show the player bar during maintainance</label>
								{{Form::checkbox('player_maintainance','1',isset($gen_config->player_maintainance)?$gen_config->player_maintainance: false)}}
							</div>

							<div class="form-group">
								<label class="form-label">Enter Text Maintainance</label>
								{{Form::textarea('text_maintainance',isset($gen_config->text_maintainance)?$gen_config->text_maintainance:'',['placeholder'=>'Enter Site Maintainance' , 'class'=>'form-control','size' =>'50x3'])}}
							</div>
						</div>


						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('input[type= "checkbox"][name="site_maintainance"]').change(function(){
			if($(this).is(":checked")) {
				$('div.site_maintainance').css('display','block');
			}
			else {
				$('div.site_maintainance').css('display','none');
			}
		})
	})
</script>
@endsection
