@extends('layouts.admin')
@section('title','External Apis')
@section('content')	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">External Apis</h4>
		</div>

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/ext_apis_update', 'method' => 'post' , 'class' =>'card' , 'files' => true , 'id'=>'create_edit_form'])}}
				@csrf
				{{Form::hidden('id',isset($ext_api->id)?$ext_api->id:'')}}
				<div class="card-header">
					<h3 class="card-title">External Apis</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<h2>Enter Musical Api</h2>
							<div class="form-group">
								<label class="form-label">Enter Last Fm Key</label>
								{{Form::text('last_fm_key',isset($ext_api->last_fm_key)?$ext_api->last_fm_key:'',['placeholder'=>'Enter Last Fm Key' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Itune Affiliate Code</label>
								{{Form::textarea('itune_affiliate_code',isset($ext_api->itune_affiliate_code)?$ext_api->itune_affiliate_code:'',['placeholder'=>'Itune Affiliate Code' , 'class'=>'form-control','size'=> '50x3'])}}
							</div>
							<h2>Statistics</h2>
							<div class="form-group">
								<label class="form-label">Google Analytics Embed Code</label>
								{{Form::textarea('ga_code',isset($ext_api->ga_code)?$ext_api->ga_code:'',['placeholder'=>'Google Analytics Embed Code' , 'class'=>'form-control','size'=> '50x3'])}}
							</div>
							<h2>Google Recaptcha</h2>
							<div class="form-group">
								<label class="form-label">Google Api Site Key</label>
								{{Form::text('google_api',isset($ext_api->google_api)?$ext_api->google_api:'',['placeholder'=>'Google Api Site Key' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Google Api Secret Key</label>
								{{Form::text('google_api_secret',isset($ext_api->google_api_secret)?$ext_api->google_api_secret:'',['placeholder'=>'Google Api Secret Key' , 'class'=>'form-control'])}}
							</div>
							<h2>Mailchimp</h2>
							<div class="form-group">
								<label class="form-label">Mailchimp Api Key</label>
								{{Form::text('mailchimp_api',isset($ext_api->mailchimp_api)?$ext_api->mailchimp_api:'',['placeholder'=>'Google Api Secret Key' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Unique Last Id</label>
								{{Form::text('unique_last_id',isset($ext_api->unique_last_id)?$ext_api->unique_last_id:'',['placeholder'=>'Unique Last Id' , 'class'=>'form-control'])}}
							</div>
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
</script>
@endsection
