@extends('layouts.admin')
@section('title','Email Configuration')
@section('content')	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Email Configuration</h4>
		</div>

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/email_configs_update', 'method' => 'post' , 'class' =>'card' , 'files' => true , 'id'=>'create_edit_form'])}}
				@csrf
				{{Form::hidden('id',isset($email_config->id)?$email_config->id:'')}}
				<div class="card-header">
					<h3 class="card-title">Email Configuration</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Sender E Mail</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('sender',isset($email_config->sender)?$email_config->sender:'',['placeholder'=>'Enter Sender Email' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Host</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('host',isset($email_config->host)?$email_config->host:'',['placeholder'=>'Enter Host' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Port</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('port',isset($email_config->port)?$email_config->port:'',['placeholder'=>'Enter Port' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">User</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('user',isset($email_config->user)?$email_config->user:'',['placeholder'=>'Enter User' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Password</label>
									</div>
									<div class="col-lg-9">
										{{Form::password('password',['placeholder'=>'Enter password', 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Connection Security</label>
									</div>
									<div class="col-lg-9">
										{{Form::select('con_security',array('1'=>'TLS','2'=>'SSL'),isset($email_config->con_security)?$email_config->con_security:'',['placeholder'=>'Connection Security' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
</script>
@endsection
