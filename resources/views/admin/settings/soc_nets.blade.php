@extends('layouts.admin')
@section('title','Social Networks')
@section('content')	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Social Networks</h4>
		</div>

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/soc_nets_update', 'method' => 'post' , 'class' =>'card' , 'files' => true , 'id'=>'create_edit_form'])}}
				@csrf
				{{Form::hidden('id',isset($soc_net->id)?$soc_net->id:'')}}
				<div class="card-header">
					<h3 class="card-title">Social Networks</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<h2>Link</h2>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Facebook Page Url</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('facebook_url',isset($soc_net->facebook_url)?$soc_net->facebook_url:'',['placeholder'=>'Enter Facebook Url' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Twitter Url</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('twitter_url',isset($soc_net->twitter_url)?$soc_net->twitter_url:'',['placeholder'=>'Enter Twitter Url' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Youtube Url</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('youtube_url',isset($soc_net->youtube_url)?$soc_net->youtube_url:'',['placeholder'=>'Enter Youtube Url' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Instagram Url</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('instagram_url',isset($soc_net->instagram_url)?$soc_net->instagram_url:'',['placeholder'=>'Enter Instagram Url' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Snapchat Url</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('snapchat_url',isset($soc_net->snapchat_url)?$soc_net->snapchat_url:'',['placeholder'=>'Enter Snapchat Url' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Dailymotion Url</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('dailymotion_url',isset($soc_net->dailymotion_url)?$soc_net->dailymotion_url:'',['placeholder'=>'Enter Dailymotion Url' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
								<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Iphone App Url</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('iphone_app_url',isset($soc_net->iphone_app_url)?$soc_net->iphone_app_url:'',['placeholder'=>'Enter Iphone App Url' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Android App Url</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('android_app_url',isset($soc_net->android_app_url)?$soc_net->android_app_url:'',['placeholder'=>'Enter Android App Url' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<h2>Integration</h2>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Facebook Id</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('facebook_id',isset($soc_net->facebook_id)?$soc_net->facebook_id:'',['placeholder'=>'Enter Facebook Id' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Facebook App Id</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('facebook_app_id',isset($soc_net->facebook_app_id)?$soc_net->facebook_app_id:'',['placeholder'=>'Enter Facebook App Id' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Twitter Widget Code</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('twitter_widget_code',isset($soc_net->twitter_widget_code)?$soc_net->twitter_widget_code:'',['placeholder'=>'Enter Twitter Widget Code' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<h2>Twitter Timeline</h2>
								<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Consumer Key</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('twitter_consumer_key',isset($soc_net->twitter_consumer_key)?$soc_net->twitter_consumer_key:'',['placeholder'=>'Enter Consumer Key' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>	<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Consumer Secret</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('twitter_consumer_secret',isset($soc_net->twitter_consumer_secret)?$soc_net->twitter_consumer_secret:'',['placeholder'=>'Enter Consumer Secret' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>	<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Access Token</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('twitter_access_token',isset($soc_net->twitter_access_token)?$soc_net->twitter_access_token:'',['placeholder'=>'Enter Access Token' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>	<div class="form-group">
								<div class="row">
									<div class="col-md-3 text-center">
										<label class="form-label">Access Token Secret</label>
									</div>
									<div class="col-lg-9">
										{{Form::text('twitter_access_token_secret',isset($soc_net->twitter_access_token_secret)?$soc_net->twitter_access_token_secret:'',['placeholder'=>'Enter Access Token Secret' , 'class'=>'form-control'])}}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
</script>
@endsection
