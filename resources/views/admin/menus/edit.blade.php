@extends('layouts.admin')
@section('title','Edit News')
@section('content')	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Edit News</h4>
		</div>

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif	
		@if(session()->has('success'))
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{session('success')}}
		</div>
		@elseif(session()->has('error'))
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> {{session('error')}}
		</div>
		@endif
		<div class="row">
			<div class="col-lg-12">
				{{Form::open(['url' => 'admin/menus/'.$id, 'method' => 'put' , 'class' =>'card' , 'files' => true , 'id'=>'edit_form'])}}
				@csrf
				<div class="card-header">
					<h3 class="card-title">Form elements</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label class="form-label">Enter Title</label>
								{{Form::text('title',$menu->title,['placeholder'=>'Enter Title' , 'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Publish</label>
								{{Form::checkbox('publish', '1',$menu->publish,['class' => 'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">New Tab</label>
								{{Form::checkbox('new_tab', '1',$menu->new_tab,['class' => 'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Position</label>
								{{Form::number('sort_order',$menu->sort_order,['placeholder'=>'Position' ,'class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Pages</label>
								{{Form::select('page_id',$pages,$menu->page_id, ['placeholder' => 'Select pages ','class'=>'form-control'])}}
							</div>
							<div class="form-group">
								<label class="form-label">Parent Menu</label>
								{{Form::select('parent_id',$menus,$menu->parent_id, ['placeholder' => 'Select Parent Menu','class'=>'form-control'])}}
							</div>
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						{{Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
<script>
</script>
@endsection
