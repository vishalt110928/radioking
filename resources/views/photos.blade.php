@extends('layouts.web')
@section('content')
<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="{{url('/')}}" class="toptip detail" title="Home"><i class="fa fa-home"></i></a></li>
			<li class="active">Photos</li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						Photos <a class="rss-link" href="/rss-feed-8" target="_blank"><img class="rss-logo" style="width: 13px; margin-top: 1px;" src="{{url('public/img/rss.png')}}" /><span class="m-l-0 m-r-0 rss-text">RSS</span></a> </h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>
	<div class="row">
		<div class="col-xs-12  col-md-8  list_display">
			<!-- Affichage contenu -->
			<div class="m-t-30">
				<div class="article row bloc-par3">
					@foreach($photos as $photo)
					<div class="layout-bloc col-sm-4 col-xs-6 m-b-40 m-b-10-xs p-l-10-xs p-r-10-xs ">
						<div>
							<div class="col-xs-12 p-l-0 p-r-0">
								<a class="whole-div-link" href="#" class="">
									<div class="cover-preview list-img-zone">
										<img class="list-img-thumb square_230" style='width: 100%;' src="{{url('public/uploads/'.$photo->path)}}">
									</div>
								</a>
							</div>
							<div class="col-xs-12 m-l-0 m-r-0 p-l-0 p-r-0">
								<span class="trans m-t-5">
									</i>
								</span>
							</div>
							<div class="col-xs-12 p-l-0 p-r-0">
								<a class="whole-div-link" href="#" class="list-action-zone">
									<h2 class="ellipsis ellipsis-2l m-t-5">
									</h2>
								</a>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="noclear-xs"></div>
					@endforeach
				</div>
			</div>
			<div class="pagination">
				{{$photos->links()}}
			</div>
		</div>

		<div class="hidden-xs hidden-sm col-md-4" id="sidebar">
			@foreach($sidebars as $sidebar)
			@include('aside.'.$sidebar)
			@endforeach
		</div>
	</div>
</div>
</div>
@endsection