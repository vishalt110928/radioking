@if(! isset($notpushstate) || ($notpushstate != '1'))
@extends('layouts.web')
@section('slider')
@endif
<div class="container m-b-15 centered">
</div>
<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="{{url('/home')}}" class="toptip detail" title="Home"><i class="fa fa-home"></i></a></li>
			<li class="active">Video</li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						Video
						<a class="rss-link" href="#" target="_blank">
							<img class="rss-logo" style="width: 13px; margin-top: 1px;" src="{{url('public/img/rss.png')}}" />
							<span class="m-l-0 m-r-0 rss-text">RSS</span>
						</a>
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>
@if(! isset($notpushstate) || ($notpushstate != '1'))
@endsection
@section('content')
@endif
<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>
	<div class="row">
		<div class="col-xs-12  col-md-8  list_display">
			<!-- Affichage contenu -->
			<div class="m-t-30">
				@foreach($pages['videos'] as $video)
				<div class="row other_elements list_element light_shadow boxed m-l-0 m-r-0 m-b-20 discounted-item lundi mardi mercredi jeudi vendredi ">
					<div class="col-xs-4 p-l-0 p-r-0">
						<a class="whole-div-link img-link detail" href="{{url('video_detail/'.$video->slug)}}" class="list-action-zone">
							<div class="cover-preview list-img-zone">
								<img class="list-img-thumb news_sidebar_other" src="{{url('public/uploads/'.$video->image)}}">
							</div>
						</a>
					</div>
					<div class="col-xs-8 p-r-10-xs">

						<a class="whole-div-link title-link detail" href="{{url('video_detail/'.$video->slug)}}" class="list-action-zone">
							<h4 class="ellipsis ellipsis-2l m-t-10 m-b-10 m-t-5-xs m-b-0-xs">
								{{$video->title}} </h4>
						</a>
						<div class="m-t-0 m-b-15 m-b-0-xs video-date">
							@if($video->publish_time)
							{{ \Carbon\Carbon::parse($video->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')}}
							@endif </div>
						<span class="ellipsis ellipsis-2l hidden-xs list_text">
							{{$video->description}}
						</span>
					</div>
				</div>
				@endforeach
			</div>
				<div class="pagination">
					{{$pages['videos']->links()}}
				</div>
		</div>

		<div class="hidden-xs hidden-sm col-md-4" id="sidebar">
		@foreach($sidebars as $sidebar)
		@include('aside.'.$sidebar)
		@endforeach
		</div>
	</div>
</div>
</div>
@if(! isset($notpushstate) || ($notpushstate != '1'))
@endsection
@endif