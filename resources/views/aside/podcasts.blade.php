<div class="bloc-content bc-one-column">
	<div class="bloc-content-title">
		<div class="bloc-title-readmore">
			<a href="{{url('podcasts')}}" class="read-more-title"><i class="fa fa-arrow-circle-o-right"></i> More</a>
		</div>
		<h2 class="ellipsis ellipsis-1l">Latest podcasts</h2>
	</div>

	<div class="wrap listed">
		@foreach($blocks['podcasts'] as $podcast)
		<div class="row">
			<a href="{{url('podcast_detail/'.$podcast->id)}}" class="detail">
				<div class="col-md-4">
					<div class="cover-preview">
						<img src="{{url('public/uploads/'.$podcast->image)}}" class="cover-img rect_100_67" />
					</div>
				</div>
			</a>
			<div class="col-xs-12 col-md-8">
				<div class="box-play like-inline"  onclick='playFile("@if($podcast->audio_file){{url('public/uploads/'.$podcast->audio_file)}} @else {{url($podcast->podcast_url)}} @endif ","{{$podcast->title}}", "{{url('public/uploads/'.$podcast->image)}}")'>
					<a href="javascript:;">
						<i class="fa fa-fw fa-play-circle"></i>
					</a>
				</div>
				<a href="{{url('podcast_detail/'.$podcast->id)}}" class="detail">
					<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3 m-t-5">
						{{$podcast->title}}
					</h3>
				</a>
			</div>
		</div>
		<div class="row border"></div>
		@endforeach

	</div>
</div>