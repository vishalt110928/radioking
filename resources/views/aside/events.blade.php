<div class="bloc-content bc-one-column">
  <div class="bloc-content-title">
    <div class="bloc-title-readmore">
      <a href="{{url('events')}}" class="read-more-title detail">
        <i class="fa fa-arrow-circle-o-right"></i> More
      </a>
    </div>
    <h2 class="ellipsis ellipsis-1l">Upcoming events</h2>
  </div>

  <div class="wrap listed">
    @foreach($blocks['events'] as $event)
    <div class="row">
      <a href="{{url('event_detail/'.$event->id)}}" class="detail">
        <div class="col-md-4">
          <div class="cover-preview">
            <img src="{{url('public/uploads/'.$event->image)}}" class="cover-img rect_100_67" />
          </div>
        </div>
        <div class="col-xs-12 col-md-8                        ">
          <h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3 m-t-5">
            {{$event->title}} 
          </h3>
          <span class="trans date">
            <i class="fa fa-calendar"></i>
            @if($event->publish_time)
            {{ \Carbon\Carbon::parse($event->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')}}
          @endif </span>
        </div>
      </a>
    </div>
    <div class="row border"></div>
    @endforeach
  </div>
</div>