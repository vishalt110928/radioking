<div class="bloc-content bc-one-column">
  <div class="bloc-content-title">
    <div class="bloc-title-readmore">
      <a href="{{url('/contests')}}" class="read-more-title detail">
        <i class="fa fa-arrow-circle-o-right"></i> More
      </a>
    </div>
    <h2 class="ellipsis ellipsis-1l">Participate</h2>
  </div>

  <div class="wrap listed">
    @foreach($blocks['contests'] as $contest)
    <div class="row">
      <a href="{{url('contest_detail/'.$contest->id)}}" class="detail">
        <div class="col-md-4">
          <div class="cover-preview">
            <img src="{{url('public/uploads/'.$contest->image)}}" class="cover-img rect_100_67" />
          </div>
        </div>
        <div class="col-xs-12 col-md-8                        ">
          <h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3 m-t-5">
            {{$contest->title}}
          </h3>
        </div>
      </a>
    </div>
    <div class="row border"></div>
    @endforeach
  </div>
</div>