<div id="bloc-slide" class="bloc-content carousel carousel-bloc slide slide-13">
	<div class="wrap">
		<div class="row">
			<div class="col-md-12">
				<div class="bloc-content-title slider-bloc-content-title">
					<h2 class="h2-slider ellipsis ellipsis-1l">Programs</h2>
					<div class="carousel-controls carousel-5d397f4f74749-controls"></div>
				</div>
			</div>
		</div>

		<div class="owl-carousel carousel-5d397f4f74749 row " role="listbox">
			@foreach($blocks['programs'] as $program)
			<!-- Si nb_items n'est pas à 0, c'est que la div n'est pas refermée -->
			<div class="item">
				<div>
					<div class="col-md-6">
						@isset($program[0])
						<a href="{{url('program_detail/'.$program[0]['id'])}}" class="detail">
							<div class="cover-preview ">
								<img src="{{url('public/uploads/'.$program[0]['image'])}}" class="cover-img rect_165_110" />
							</div>

							<div class="height-42">
								<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3">
									{{$program[0]['name']}} </h3>
								</div>
							</a>
							@endisset
						</div>
						<div class="col-md-6">
							@isset($program[1])
							<a href="{{url('program_detail/'.$program[1]['id'])}}" class="detail">
								<div class="cover-preview ">
									<img src="{{url('public/uploads/'.$program[1]['image'])}}" class="cover-img rect_165_110" />
								</div>

								<div class="height-42">
									<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3">
										{{$program[1]['name']}} </h3>
									</div>
								</a>
								@endisset
							</div>
						</div>
						<div>
							<div class="col-md-6">
								@isset($program[2])
								<a href="{{url('program_detail/'.$program[2]['id'])}}" class="detail">
									<div class="cover-preview ">
										<img src="{{url('public/uploads/'.$program[2]['image'])}}" class="cover-img rect_165_110" />
									</div>

									<div class="height-42">
										<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3">
											{{$program[2]['name']}} </h3>
										</div>
									</a>
									@endisset
								</div>
								<div class="col-md-6">
									@isset($program[3])
									<a href="{{url('program_detail/'.$program[3]['id'])}}" class="detail">
										<div class="cover-preview ">
											<img src="{{url('public/uploads/'.$program[3]['image'])}}" class="cover-img rect_165_110" />
										</div>

										<div class="height-42">
											<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3">
												{{$program[3]['name']}} </h3>
											</div>
										</a>
										@endisset
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>

				<script type="text/javascript">
					var color = 'white';
					$('.carousel-5d397f4f74749').owlCarousel({
						loop: false,
						margin: 30,
						nav: true,
						navText: ['<span class="carousel-control left" data-slide="prev" style="margin-left= 1px;color: ' + color + ';"><span class="fa fa-chevron-left"></span></span>', '<span class="carousel-control right" data-slide="next" style="color: ' + color + ';"><span class="fa  fa-chevron-right"></span></span>'],
						navContainer: ".carousel-5d397f4f74749-controls",
						autoplay: true,
						rewind: true,
						autoplayTimeout: 6000,
						responsive: {
							0: {
								items: 1,
								margin: 20
							},
							600: {
								items: 1
							},
						}
					});

					$('.carousel-5d397f4f74749-controls').children('.carousel-control').on('click', function() {
						$('.carousel-5d397f4f74749').owlCarousel($(this).attr("data-slide"));
					});
				</script>