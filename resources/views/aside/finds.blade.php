	<div class="bloc-content bc-one-column">
		<div class="bloc-content-title">
			<h2 class="ellipsis ellipsis-1l">Find us on</h2>
		</div>

		<div class="link-socials">
			<div class="link-socials-list">

				<div class="link-socials-item facebook">
					<a class="link-socials-item-link" target="_blank" href="https://www.facebook.com/RadioKingUSA/">
						<span class="link-socials-item-link-hover"></span>
					</a>
					<div class="link-socials-item-popup">
						<span>Facebook</span>
					</div>
				</div>

				<div class="link-socials-item twitter">
					<a class="link-socials-item-link" target="_blank" href="https://twitter.com/radioking">
						<span class="link-socials-item-link-hover"></span>
					</a>
					<div class="link-socials-item-popup">
						<span>Twitter</span>
					</div>
				</div>

				<div class="link-socials-item youtube">
					<a class="link-socials-item-link" target="_blank" href="https://www.youtube.com/channel/UCuA6ECflXH9hpx5iDBa79uw">
						<span class="link-socials-item-link-hover"></span>
					</a>
					<div class="link-socials-item-popup">
						<span>YouTube</span>
					</div>
				</div>

				<div class="link-socials-item ios">
					<a class="link-socials-item-link" target="_blank" href="https://itunes.apple.com/us/app/radio-king/id598626511">
						<span class="link-socials-item-link-hover"></span>
					</a>
					<div class="link-socials-item-popup">
						<span>iPhone</span>
					</div>
				</div>

				<div class="link-socials-item android">
					<a class="link-socials-item-link" target="_blank" href="https://play.google.com/store/apps/details?id=com.icreo.radioking">
						<span class="link-socials-item-link-hover"></span>
					</a>
					<div class="link-socials-item-popup">
						<span>Android</span>
					</div>
				</div>

			</div>
		</div>
	</div>