@if(! isset($notpushstate) || ($notpushstate != '1'))
@extends('layouts.web')
@section('content')
@endif
<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="{{url('/home')}}" class="toptip detail" title="Home"><i class="fa fa-home"></i></a></li>
			<li><a href="{{url('contests')}}">Contest</a></li>
			<li class="active">{{$contest->title}}</li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						{{$contest->title}}
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>
	<div class="row">
		<div class="col-xs-12  col-md-8  ">
			<div class="m-t-30">
				<div class="article afficher-content">
					<div class="wrap">
						<img src="{{url('public/uploads/'.$contest->image)}}" style="width: 100%;" class="img-responsive m-b-5 m-t-20 rect_750_450" />
						<span class="infos-article">
							<i class="fa fa-clock"></i>{{ \Carbon\Carbon::parse($contest->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')}}</span>
							<div class="m-t-15 m-b-15"></div>
							<p>{{$contest->des}}</p>
							<div class="contest-success well well-large alert alert-success" style="display:none;text-align:center;">
								You have been entered into this contest.
							</div>
							<input class="contest_part btn btn-lg btn-block btn-primary" type="button" value="Take part in this contest" data-contest_id= "{{$contest->id}}">
						</div>
					</div>
					<div id="comments" class="row comment-row m-t-20">
						<div class="col-xs-12">
							<div class="form-group">
								<span class="comment-img avatar-default"><i class="fa fa-user"></i></span>
								<div class="fb-comments" data-href="{{request()->fullUrl()}}" data-width="" data-numposts="5"></div>
							</div>

							<div class="clear"></div>
							<div class="form-group" style="float:right;">
								<div class="col-xs-3">
								</div>
							</div>
							<div class="clear"></div>
							<div id="list-comments">
							</div>
							<div class="row">
								<div class="col-xs-12">
								</div>
							</div>

							<!-- Fin commentaires -->
						</div>
					</div>
					<div id="fb-root"></div>
					<script crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
				</div>
			</div>
		</div>
	</div>
	@if(! isset($notpushstate) || ($notpushstate != '1'))
	@endsection
	@endif
	@section('script')
	<script type="text/javascript">
		$(document).ready(function(){
			$(document).on('click', '.contest_part', function(e) {
				e.preventDefault();
				var auth = "{{ auth::check()}}";
				if (auth) {
					elm = $(this);
					var contest_id = elm.data('contest_id');
					$.ajax({
						type: "GET",
						url:"{{url('contest_part/')}}",
						data: {"contest_id":contest_id },
						beforeSend: function(){

						},
						success: function(response) {
							$('div.contest-success').css('display','block');
						},
						error:function(response) {
							alert(response.message);
							console.log(response.message);
						},
						complete:function() {

						}
					});
				}
				else{
					alert("Please login first to take participate.")
				}
			});
		});
	</script>

	@endsection