@if(! isset($notpushstate) || ($notpushstate != '1'))
@extends('layouts.web')
@section('content')
@endif
<script type="text/javascript"></script>

<div class="container m-b-15 centered">
</div>

<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="{{url('/home')}}" class="toptip detail" title="Home"><i class="fa fa-home"></i></a></li>
			<li><a href="{{url('videos')}}">Videos</a></li>
			<li class="active">{{$video->title}}</li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						{{$video->title}}
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>
	<div class="row">
		<div class="col-xs-12  col-md-8  ">
			<!-- Affichage contenu -->
			<div class="m-t-30">
				<div class="article afficher-content">
					<div class="wrap">
						<div class="embed-responsive embed-responsive-16by9 m-t-20 m-b-5">
							{!! $video->video_url !!}
						</div>
						<span class="infos-article">
							<i class="fa fa-clock"></i>{{ \Carbon\Carbon::parse($video->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')}} - <i class="fa fa-eye"></i> {{$video->views}} views</span>


						<p>{{$video->description}}</p>
					</div>
				</div>
			</div>
			<div id="comments" class="row comment-row m-t-20">
					<div class="col-xs-12">
					<div class="form-group">
							<span class="comment-img avatar-default"><i class="fa fa-user"></i></span>
							<div class="fb-comments" data-href="{{request()->fullUrl()}}" data-width="" data-numposts="5"></div>
						</div>

							<div class="clear"></div>
							<div class="form-group" style="float:right;">
								<div class="col-xs-3">
									<a href="javascript:void(0)" id="sendComment" class="btn btn-primary">Comment</a>
								</div>
							</div>
						<div class="clear"></div>
						<div id="list-comments">
						</div>
						<div class="row">
							<div class="col-xs-12">
							</div>
						</div>

						<!-- Fin commentaires -->
					</div>
				</div>
				<div id="fb-root"></div>
				<script crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
		</div>
	</div>
	
</div>
@if(! isset($notpushstate) || ($notpushstate != '1'))
@endsection
@endif