@if(! isset($notpushstate) || ($notpushstate != '1'))
@extends('layouts.web')
@section('content')
@endif
<script type="text/javascript"></script>

<div class="container m-b-15 centered">
</div>

<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="{{url('home')}}" class="toptip detail" title="Home"><i class="fa fa-home"></i></a></li>
			<li><a href="{{url('podcasts')}}" class="detail">Podcasts</a></li>
			<li class="active">{{$podcast->title}}</li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						{{$podcast->title}}
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>


	<div class="row">

		<div class="col-xs-12  col-md-8  ">
			<!-- Affichage contenu -->
			<div class="m-t-30">
				<div class="article afficher-content">
					<div class="wrap">
						<img src="{{url('public/uploads/'.$podcast->image)}}" style="width: 100%;" class="img-responsive m-b-5 m-t-20 rect_750_450" />
						<span class="infos-article">
							<i class="fa fa-clock"></i>{{ \Carbon\Carbon::parse($podcast->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')}}</span>
							<div class="button-zone">
								<button class="btn btn-primary playPodcast player-play" data-title = "{{$podcast->audio_file}}"  data-image = "{{url('public/uploads/'.$podcast->image)}}" data-id = "{{$podcast->id}}" data-play = "	@if(isset($podcast->audio_file) &&  $podcast->audio_file) {{url('public/uploads/'.$podcast->audio_file)}} @else {{$podcast->podcast_url}} @endif" data-download = "@if(isset($podcast->audio_file) &&  $podcast->audio_file) 1 @else 0 @endif" data-type = "podcast" data-autoplay = "1">
									<i class="fa fa-volume-up"></i> Listen podcast </button>
									@if(isset($podcast->audio_file) &&  $podcast->audio_file)
									<a href="{{url('public/uploads/'.$podcast->audio_file)}}" class="btn btn-primary no-ajaxy" style="float: right;" download><i class="fa fa-cloud-download"></i> Download podcast </a>
									@endif
								</div>
								<div class="m-t-15 m-b-15"></div>
								<p>{{$podcast->des}}</p>
								<div class="live_poll">
									<div class="contest-vote well well-large alert alert-success" style="display:none;text-align:center;">
										You have voted successfully.
									</div>
									<form class="poll_ana_form" method="POST" action="{{url('poll_ana')}}">
										@csrf
										<input type="hidden" name="contest_id" value="{{$podcast->id}}">
										<input type="hidden" name="table" value="{{'podcast'}}">
										<div class="form-group">
											<h4 class="des">Are You happy with this podcast?</h4>
											<label><input type="radio" name="pos_neg" value="1" checked>Yes</label>
											<label><input type="radio" name="pos_neg" value="0">No</label>
											<button type="submit" class="btn btn-success poll_ana">Vote</button>
										</div>
									</form>
								</div>
								<canvas id="progressChart" width="400" height="400"></canvas>
							</div>
						</div>

						<div id="comments" class="row comment-row m-t-20">
							<div class="col-xs-12">
								<div class="form-group">
									<span class="comment-img avatar-default"><i class="fa fa-user"></i></span>
									<div class="fb-comments" data-href="{{request()->fullUrl()}}" data-width="" data-numposts="5"></div>
								</div>

								<div class="clear"></div>
								<div class="form-group" style="float:right;">
									<div class="col-xs-3">
										<a href="javascript:void(0)" id="sendComment" class="btn btn-primary">Comment</a>
									</div>
								</div>
								<div class="clear"></div>
								<div id="list-comments">
								</div>
								<div class="row">
									<div class="col-xs-12">
									</div>
								</div>

								<!-- Fin commentaires -->
							</div>
						</div>
						<div id="fb-root"></div>
						<script crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
					</div>
				</div>
			</div>
		</div>
		@if(! isset($notpushstate) || ($notpushstate != '1'))
		@endsection
		@endif
		@section('script')
		<script type="text/javascript" src="{{url('public/js/Chart.js') }}"></script>
		<script>
			$(document).ready(function(){
				$(document).on('click','.playPodcast',function(){
					var title = $(this).data('title');
					var cover = $(this).data('image');
					var url = $(this).data('play');
					var id = $(this).data('id');
					var type = $(this).data('type');
					var download = $(this).data('download');
					var autoplay = $(this).data('autoplay');
					playFile(url, title, cover, id ,type, downlaod,autoplay);
				});

				$(document).on('submit','.poll_ana_form',function(e){
					e.preventDefault();
					var auth = "{{ auth()->check() }}";
					var url = $(this).attr('action');
					var form =$(this).serialize();
					if (auth) {
						$.ajax({
							type: "POST",
							url:url,
							data: form,
							beforeSend: function(){
							},
							success: function(response) {
								$('div.contest-vote').css('display','block');
								$('div.contest-vote').html(response.message);
								chart(response)
							},
							error:function(response) {
								alert(response.message);
								console.log(response.message);
							},
							complete:function() {
							}
						});
					}
					else{
						alert('Please Login First To Vote ')
					}
				});
				$('.progressbar').progressbar({
					value:38
				});
			});
		</script>
		<script type="text/javascript">
			function chart(response) {
				var ctx = document.getElementById('progressChart').getContext('2d');
				var myChart = new Chart(ctx, {
					type: 'bar',
					data: {
						labels: ['Positive', 'Negative'],
						datasets: [{
							label: '# of Votes',
							data: [response.pos_count,response.neg_count],
							backgroundColor: [
							'rgba(75, 192, 192, 0.2)',
							'rgba(255, 99, 132, 0.2)'
							],
							borderColor: [
							'rgba(75, 192, 192, 1)','rgba(255, 99, 132, 1)'],
							borderWidth: 2,
							barPercentage:3
						}]
					},
					options: {
						scales: {
							yAxes: [{
								ticks: {
									max:500,

									beginAtZero: true
								}
							}],
							xAxes: [{
								barPercentage: 0.2
							}]

						}
					}
				});
			}
			$(document).ready(function() {
				var url = "{{url('poll_ana')}}";
				$.ajax({
					type: "POST",
					url:url,
					beforeSend: function(){
					},
					success: function(response) {
						$('div.contest-vote').html(response.message);
						chart(response)
					},
					error:function(response) {
						alert(response.message);
						console.log(response.message);
					},
					complete:function() {
					}
				});
			});
		</script>
		@endsection