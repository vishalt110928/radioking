
<div class="default-sidebar">
  <!-- Begin Side Navbar -->
  <nav class="side-navbar box-scroll sidebar-scroll">
    <!-- Begin Main Navigation -->
    <ul class="list-unstyled">
      <li><a href="#" ><i class="la la-columns"></i><span>Dashboard</span></a>
      </li>
      <li>
        <a href="{{url('admin/contacts')}}" class="@if(isset($active) && $active == 'contacts')active @endif">
          <i class="fa fa-phone" aria-hidden="true"></i><span>Contact Us</span>
        </a>
      </li>
    </ul>
    <span class="heading">Components</span>
    <ul class="list-unstyled">
      <li><a href="#dropdown-forms" aria-expanded="<?php if(isset($active) && $active=='news') echo 'true'; else echo 'false';  ?>" data-toggle="collapse" class=""><i class="la la-list-alt"></i><span>News</span></a>
        <ul id="dropdown-forms" class="list-unstyled pt-0 collapse @if(isset($active) && $active == 'news')show @endif" style="">
          <li><a href="{{url('admin/news/create')}}" class="@if(isset($li_active) && $li_active == 'add_news')active @endif">Add News</a></li>
          <li><a href="{{url('admin/news_cat')}}" class="@if(isset($li_active) && $li_active == 'news_cat')active @endif">Category Management</a></li>
          <li><a href="{{url('admin/news')}}" class="@if(isset($li_active) && $li_active == 'news_manage')active @endif">News Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-app" aria-expanded="<?php if(isset($active) && $active=='events') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
          <i class="fa fa-key" aria-hidden="true"></i><span>Events</span>
        </a>
        <ul id="dropdown-app" class="collapse list-unstyled pt-0  @if(isset($active) && $active == 'events')show @endif">
          <li><a href="{{url('admin/events/create')}}" class="@if(isset($li_active) && $li_active == 'add_event')active @endif">Add Event</a></li>
          <li><a href="{{url('admin/events_cat')}}" class="@if(isset($li_active) && $li_active == 'events_cat')active @endif">Category Management</a></li>
          <li><a href="{{url('admin/events')}}" class="@if(isset($li_active) && $li_active == 'events_manage')active @endif">Events Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-dedi" aria-expanded="<?php if(isset($active) && $active=='dedications') echo 'true'; else echo 'false' ; ?>" data-toggle="collapse">
          <i class="fa fa-dashcube" aria-hidden="true"></i><span>Dedication</span>
        </a>
        <ul id="dropdown-dedi" class="collapse list-unstyled pt-0 @if(isset($active) && $active == 'dedications')show @endif">
          <li><a href="{{url('admin/dedications/create')}}" class="@if(isset($li_active) && $li_active == 'add_dedications')active @endif">Add Dedication</a></li>
          <li><a href="{{url('admin/dedications')}}" class="@if(isset($li_active) && $li_active == 'dedications_manage')active @endif">Dedication Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-cont" aria-expanded="<?php if(isset($active) && $active=='contests') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
          <i class="fa fa-plus-circle" aria-hidden="true"></i><span>Contests</span>
        </a>
        <ul id="dropdown-cont" class="collapse list-unstyled pt-0 @if(isset($active) && $active == 'contests')show @endif">
          <li><a href="{{url('admin/contests/create')}}" class="@if(isset($li_active) && $li_active == 'add_contest')active @endif">Add Contests</a></li>
          <li><a href="{{url('admin/contests_cat')}}" class="@if(isset($li_active) && $li_active == 'contests_cat')active @endif">Category Management</a></li>
          <li><a href="{{url('admin/contests')}}" class="@if(isset($li_active) && $li_active == 'contests_manage')active @endif">Contest Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-slider" aria-expanded="<?php if(isset($active) && $active=='sliders') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
          <i class="fa fa-sliders" aria-hidden="true"></i><span>Sliders</span>
        </a>
        <ul id="dropdown-slider" class="collapse list-unstyled pt-0 @if(isset($active) && $active == 'sliders')show @endif">
          <li><a href="{{url('admin/sliders/create')}}" class="@if(isset($li_active) && $li_active == 'add_slider')active @endif">Add Slider</a></li>
          <li><a href="{{url('admin/sliders')}}" class="@if(isset($li_active) && $li_active == 'sliders_manage')active @endif">Slider Management</a></li>
        </ul>
      </li>
      <li><a href="#dropdown-pod" aria-expanded="<?php if(isset($active) && $active=='podcasts') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-arrows-alt" aria-hidden="true"></i><span>Podcasts</span></a>
        <ul id="dropdown-pod" class="collapse list-unstyled pt-0 @if(isset($active) && $active == 'podcasts')show @endif">
          <li><a href="{{url('admin/podcasts/create')}}" class="@if(isset($li_active) && $li_active == 'add_podcasts')active @endif">Add Podcasts</a></li>
          <li><a href="{{url('admin/podcasts_cat')}}" class="@if(isset($li_active) && $li_active == 'podcasts_cat')active @endif">Category Management</a></li>
          <li><a href="{{url('admin/podcasts')}}" class="@if(isset($li_active) && $li_active == 'podcasts_manage')active @endif">Podcasts Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-play" aria-expanded="<?php if(isset($active) && $active=='playlists') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
          <i class="fa fa-arrow-circle-right" aria-hidden="true"></i><span>Playlists</span>
        </a>
        <ul id="dropdown-play" class="collapse list-unstyled pt-0 @if(isset($active) && $active == 'playlists')show @endif">
          <li><a href="{{url('admin/playlists/create')}}" class="@if(isset($li_active) && $li_active == 'add_playlist')active @endif">Add Playlists</a></li>
          <li><a href="{{url('admin/playlists_cat')}}" class="@if(isset($li_active) && $li_active == 'playlists_cat')active @endif">Category Management</a></li>
          <li><a href="{{url('admin/playlists')}}" class="@if(isset($li_active) && $li_active == 'playlists_manage')active @endif">Playlists Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-art" aria-expanded="<?php if(isset($active) && $active=='artists') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
          <i class="fa fa-address-card" aria-hidden="true"></i><span>Artists</span>
        </a>
        <ul id="dropdown-art" class="collapse list-unstyled pt-0 @if(isset($active) && $active == 'artists')show @endif">
          <li><a href="{{url('admin/artists/create')}}" class="@if(isset($li_active) && $li_active == 'add_artists')active @endif">Add Artists</a></li>
          <li><a href="{{url('admin/artists_cat')}}" class="@if(isset($li_active) && $li_active == 'artists_cat')active @endif">Category Management</a></li>
          <li><a href="{{url('admin/artists')}}" class="@if(isset($li_active) && $li_active == 'artists_manage')active @endif">Artists Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-feed" aria-expanded="<?php if(isset($active) && $active=='livefeeds') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
          <i class="fa fa-rss" aria-hidden="true"></i><span>Livefeeds</span>
        </a>
        <ul id="dropdown-feed" class="collapse list-unstyled pt-0 @if(isset($active) && $active == 'livefeeds')show @endif">
          <li><a href="{{url('admin/livefeeds/create')}}" class="@if(isset($li_active) && $li_active == 'add_livefeed')active @endif">Add Livefeeds</a></li>
          <li><a href="{{url('admin/livefeeds')}}" class="@if(isset($li_active) && $li_active == 'livefeeds_manage')active @endif" >Livefeeds Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-page" aria-expanded="<?php if(isset($active) && $active=='pages') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
          <i class="fa fa-file-code-o" aria-hidden="true"></i><span>Pages</span>
        </a>
        <ul id="dropdown-page" class="collapse list-unstyled pt-0 @if(isset($active) && $active == 'pages')show @endif">
          <li><a href="{{url('admin/pages/create')}}" class="@if(isset($li_active) && $li_active == 'add_page')active @endif">Add Pages</a></li>
          <li><a href="{{url('admin/pages_cat')}}" class="@if(isset($li_active) && $li_active == 'pages_cat')active @endif">Category Management</a></li>
          <li><a href="{{url('admin/pages')}}" class="@if(isset($li_active) && $li_active == 'pages_manage')active @endif">Pages Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-teams" aria-expanded="<?php if(isset($active) && $active=='teams') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
         <i class="fa fa-users" aria-hidden="true"></i><span>Teams</span>
       </a>
       <ul id="dropdown-teams" class="collapse list-unstyled pt-0 @if(isset($active) && $active == 'teams')show @endif">
        <li><a href="{{url('admin/teams/create')}}" class="@if(isset($li_active) && $li_active == 'add_teams')active @endif">Add Member</a></li>
        <li><a href="{{url('admin/teams_cat')}}" class="@if(isset($li_active) && $li_active == 'teams_cat')active @endif">Category Management</a></li>
        <li><a href="{{url('admin/teams')}}" class="@if(isset($li_active) && $li_active == 'teams_manage')active @endif">Teams Management</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-prog" aria-expanded="<?php if(isset($active) && $active=='programs') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-product-hunt" aria-hidden="true"></i><span>Programs</span>
      </a>
      <ul id="dropdown-prog" class="collapse list-unstyled pt-0 @if(isset($active) && $active == 'programs')show @endif">
        <li><a href="{{url('admin/programs/create')}}" class="@if(isset($li_active) && $li_active == 'add_program')active @endif">Add A Program</a></li>
        <li><a href="{{url('admin/programs_cat')}}" class="@if(isset($li_active) && $li_active == 'programs_cat')active @endif" >Category Management</a></li>
        <li><a href="{{url('admin/programs')}}" class="@if(isset($li_active) && $li_active == 'programs_manage')active @endif" >Programs Management</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-track" aria-expanded="<?php if(isset($active) && $active=='tracks') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-music" aria-hidden="true"></i><span>Music Base</span>
      </a>
      <ul id="dropdown-track" class="collapse list-unstyled pt-0 @if(isset($active) && $active == 'tracks')show @endif">
        <li><a href="{{url('admin/tracks')}}" class="@if(isset($li_active) && $li_active == 'tracks_manage')active @endif">Track Management</a></li>
        <li><a href="{{url('admin/tracks/create')}}" class="@if(isset($li_active) && $li_active == 'add_tracks')active @endif">Add A Track</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-gal" aria-expanded="<?php if(isset($active) && $active=='gallerys') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-film" aria-hidden="true"></i><span>Gallery</span>
      </a>
      <ul id="dropdown-gal" class="collapse list-unstyled pt-0 @if(isset($active) && $active == 'gallerys')show @endif">
        <li><a href="{{url('admin/photos')}}" class="@if(isset($li_active) && $li_active == 'gallerys_photos') active @endif">Add Photos</a></li>
        <li><a href="{{url('admin/gallerys/create')}}" class="@if(isset($li_active) && $li_active == 'add_gallerys') active @endif">Add A Gallery</a></li>
        <li><a href="{{url('admin/gallerys_cat')}}" class="@if(isset($li_active) && $li_active == 'gallerys_cat') active @endif">Category Management</a></li>
        <li><a href="{{url('admin/gallerys')}}" class="@if(isset($li_active) && $li_active == 'gallerys_manage') active @endif">Gallery Management</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-vid" aria-expanded="<?php if(isset($active) && $active=='videos') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-youtube-play" aria-hidden="true"></i><span>Videos</span>
      </a>
      <ul id="dropdown-vid" class="collapse list-unstyled pt-0 @if(isset($active) && $active == 'videos')show @endif">
        <li><a href="{{url('admin/videos/create')}}" class="@if(isset($li_active) && $li_active == 'add_video')active @endif">Add A Video</a></li>
        <li><a href="{{url('admin/videos_cat')}}" class="@if(isset($li_active) && $li_active == 'videos_cat')active @endif">Category Management</a></li>
        <li><a href="{{url('admin/videos')}}" class="@if(isset($li_active) && $li_active == 'videos_manage')active @endif">Video Management</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-ads" aria-expanded="<?php if(isset($active) && $active=='ads') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-buysellads" aria-hidden="true"></i><span>Ads</span>
      </a>
      <ul id="dropdown-ads" class="collapse list-unstyled pt-0 @if(isset($active) && $active == 'ads')show @endif">
        <li><a href="{{url('admin/ads/create')}}" class="@if(isset($li_active) && $li_active == 'add_ads')active @endif">Add A Ads</a></li>
        <li><a href="{{url('admin/ads')}}" class="@if(isset($li_active) && $li_active == 'ads_manage')active @endif">Ads Management</a></li>
      </ul>
    </li>
    <li><a href="#dropdown-rss" aria-expanded="<?php if(isset($active) && $active=='rssfeeds') echo 'true'; else echo 'false';  ?>" data-toggle="collapse"><i class="la la-puzzle-piece"></i><span>Rss Feed</span></a>
      <ul id="dropdown-rss" class="collapse list-unstyled pt-0  @if(isset($active) && $active == 'rssfeeds')show @endif">
        <li><a href="{{url('admin/rssfeeds/create')}}" class="@if(isset($li_active) && $li_active == 'add_rssfeed')active @endif">Add A Rss Feed</a></li>
        <li><a href="{{url('admin/rssfeeds_cat')}}" class="@if(isset($li_active) && $li_active == 'rssfeeds_cat')active @endif">Category Management</a></li>
        <li><a href="{{url('admin/rssfeeds')}}" class="@if(isset($li_active) && $li_active == 'rssfeeds_manage')active @endif">Feed Management</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-player" aria-expanded="<?php if(isset($active) && $active=='players') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-stop-circle" aria-hidden="true"></i><span>Players</span>
      </a>
      <ul id="dropdown-player" class="collapse list-unstyled pt-0  @if(isset($active) && $active == 'players')show @endif">
        <li><a href="{{url('admin/players/create')}}" class="@if(isset($li_active) && $li_active == 'add_player')active @endif">Add Players</a></li>
        <li><a href="{{url('admin/players')}}" class="@if(isset($li_active) && $li_active == 'players_manage')active @endif">Players Management</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-user" aria-expanded="<?php if(isset($active) && $active=='users') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-user-o" aria-hidden="true"></i><span>Users</span>
      </a>
      <ul id="dropdown-user" class="collapse list-unstyled pt-0  @if(isset($active) && $active == 'users')show @endif">
        <li><a href="{{url('admin/users/create')}}" class="@if(isset($li_active) && $li_active == 'add_user')active @endif">Add A Rss Feed</a></li>
        <li><a href="{{url('admin/users_role')}}" class="@if(isset($li_active) && $li_active == 'users_role')active @endif">Role Management</a></li>
        <li><a href="{{url('admin/users')}}" class="@if(isset($li_active) && $li_active == 'users_manage')active @endif">User  Management</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-config" aria-expanded="<?php if(isset($active) && $active=='configs') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-cog" aria-hidden="true"></i><span>Configuration</span>
      </a>
      <ul id="dropdown-config" class="collapse list-unstyled pt-0  @if(isset($active) && $active == 'configs')show @endif">
        <li><a href="{{url('admin/gen_configs')}}" class="@if(isset($li_active) && $li_active == 'gen_configs')active @endif">General Settings</a></li>
        <li><a href="{{url('admin/ext_apis')}}" class="@if(isset($li_active) && $li_active == 'ext_apis')active @endif">External Api</a></li>
        <li><a href="{{url('admin/soc_nets')}}" class="@if(isset($li_active) && $li_active == 'soc_nets')active @endif">Social Networks</a></li>
        <li><a href="{{url('admin/chats')}}" class="@if(isset($li_active) && $li_active == 'chats')active @endif">Chat</a></li>
        <li><a href="{{url('admin/email_configs')}}" class="@if(isset($li_active) && $li_active == 'email_configs')active @endif">Email</a></li>
        <li><a href="{{url('admin/privacy_configs')}}" class="@if(isset($li_active) && $li_active == 'privacy_configs')active @endif">Privacy</a></li>
        <li><a href="{{url('admin/menus')}}" class="@if(isset($li_active) && $li_active == 'menus')active @endif">Menus</a></li>
        <li><a href="{{url('admin/blocks')}}" class="@if(isset($li_active) && $li_active == 'blocks')active @endif">Blocks</a></li>
      </ul>
    </li>
  </ul>
</nav>
<!-- End Side Navbar -->
</div>