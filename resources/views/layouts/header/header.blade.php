  <div id="fb-root"></div>
  <nav id="navbarscroll" class="navbar navbar-default navbar-fixed-top container counter-container">
    <!-- header-fixed-top pour fixer le header en haut + enlever le JS -->
    <div class="nav-global">
      <div id="toplink" class="scrolltoplink">
        <div class="container toplink-container">
          <div class="link-contact">
            <div class="dropdown">
              <button class="btn btn-toplink dropdown-toggle" type="button" id="dropdownMenu12" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <i class="fa fa-lock"></i> Member area <span class="caret"></span>
              </button>
              @if(auth()->check())
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu12">
                @if(! auth()->user()->users_role_id)
                <li class="navbarItem-user ">
                  <a href="{{url('user_profile/'.auth()->user()->id)}}"><i class="fa fa-user"></i> My account</a>
                </li>
                @endif
                @if(auth()->user()->users_role_id)
                <li class="navbarItem-user">
                  <a href="{{url('admin/dashboard')}}" target="_blank">
                    <i class="fa fa-unlock"></i> Administration </a>
                  </li>
                  @endif
                  <li role="separator" class="divider navbarItem-user "></li>
                  <li class="navbarItem-user ">
                    <a class="logout" href="javascript:void(0)" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"></i> Log out </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                    </form>
                  </li>
                </ul>
                @else
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu12">
                  <li class="navbarItem-guest">
                    <a href="{{url('register')}}"><i class="fa fa-user"></i>Register</a>
                  </li>
                  <li class="navbarItem-guest">
                    <a href="{{url('login')}}"><i class="fa fa-user"></i> Log in</a>
                  </li>
                  <li class="navbarItem-guest">
                    <a href="{{url('password/resest')}}" role="button" data-toggle="modal"><i class="fa fa-question-circle"></i> Forgot your password ? </a>
                  </li>
                </ul>
                @endif
              </div>
            </div>
            <div class="lang">
              <a id="lang_sel" href="#"  data-toggle="modal" data-target="#lang_modal"> Language
                <img src="{{url('public/img/lang.svg')}}">
              </a>
            </div>
            <div class="link-socials">
              <div class="link-socials-list">
                <div class="link-socials-item facebook">
                  <a class="link-socials-item-link" target="_blank" href="{{$soc_nets->facebook_url}}">
                    <span class="link-socials-item-link-hover">

                    </span>
                  </a>
                  <div class="link-socials-item-popup">
                    <span>Facebook</span>
                  </div>
                </div>
                <div class="link-socials-item twitter">
                  <a class="link-socials-item-link" target="_blank" href="{{$soc_nets->twitter_url}}">
                    <span class="link-socials-item-link-hover"></span>
                  </a>
                  <div class="link-socials-item-popup">
                    <span>Twitter</span>
                  </div>
                </div>
                <div class="link-socials-item youtube">
                  <a class="link-socials-item-link" target="_blank" href="{{$soc_nets->youtube_url}}">
                    <span class="link-socials-item-link-hover"></span>
                  </a>
                  <div class="link-socials-item-popup">
                    <span>YouTube</span>
                  </div>
                </div>
                <div class="link-socials-item ios">
                  <a class="link-socials-item-link" target="_blank" href="{{$soc_nets->iphone_app_url}}">
                    <span class="link-socials-item-link-hover"></span>
                  </a>
                  <div class="link-socials-item-popup">
                    <span>iPhone</span>
                  </div>
                </div>
                <div class="link-socials-item android">
                  <a class="link-socials-item-link" target="_blank" href="{{$soc_nets->android_app_url}}">
                    <span class="link-socials-item-link-hover"></span>
                  </a>
                  <div class="link-socials-item-popup">
                    <span>Android</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="navbarheaderzone">
          <div class="container navheaderbg">
            <span class="openmenuButton hidden-md hidden-lg">
              <i class="fa fa-bars"></i>
            </span>
            <div class="navbar-header">
              <a class="navbar-brand" href="/">
                <img src="{{url('public/img/front_logo.png')}}" alt="logo" class="logo" />
              </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="navbar navbar-nav sf-menu hidden-sm" id="navbar-sfmenu">
            <!--   @foreach($menus as $menu)
              <li>
                <a href="{{url($menu->page->title)}}">
                  {{$menu->title}}
                </a>
                @if($menu->menu_count)
                <ul style="list-style-type : none;">
                  @foreach($menu->menu as $sub_menu)
                  <li>
                    <a href="{{url($sub_menu->page->title)}}">
                      {{$sub_menu->title}}
                    </a>
                    <span class="spacer"></span>
                  </li>
                  @endforeach
                </ul>
                @endif
              </li>
              @endforeach -->
              <li>
                <a href="{{url('home')}}" class="menu">
                  Home
                </a>
              </li>
              <li>

                <a href="{{url('news')}}" class="menu">
                  Radio <span class="sub">
                  </span></a>
                  <ul style="list-style-type : none;">
                    <li>
                      <a href="{{url('news')}}" class="menu">
                        News
                      </a>
                      <span class="spacer"></span>
                    </li>
                    <li>
                      <a href="{{url('programs')}}" class="menu">
                        Shows
                      </a>
                      <span class="spacer"></span>
                    </li>
                    <li>
                      <a href="{{url('teams')}}" class="menu">
                        Team
                      </a>
                      <span class="spacer"></span>
                    </li>
                    <li>
                      <a href="{{url('events')}}" class="menu">
                        Events
                      </a>
                      <span class="spacer"></span>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="{{url('top_10')}}" class="menu">
                    Music <span class="sub"></span></a>
                    <ul style="list-style-type : none;">
                      <li>
                        <a href="{{url('top_10')}}" class="menu">
                          Top 10
                        </a>
                        <span class="spacer"></span>
                      </li>
                      <li>
                        <a href="{{url('artists')}}" class="menu">
                          Artists
                        </a>
                        <span class="spacer"></span>
                      </li>
                      <li>
                        <a href="{{url('playlists')}}" class="menu">
                          Playlist
                        </a>
                        <span class="spacer"></span>
                      </li>
                      <li>
                        <a href="{{url('auth_played_tracks')}}" class="auth_played_tracks">
                          Played tracks
                        </a>
                        <span class="spacer"></span>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="{{url('photos')}}" class="menu">Medias
                      <span class="sub"></span>
                    </a>
                    <ul style="list-style-type : none;">
                      <li>
                        <a href="{{url('photos')}}" class="menu">
                          Photos
                        </a>
                        <span class="spacer"></span>
                      </li>
                      <li>
                        <a href="{{url('podcasts')}}" class="menu">
                          Podcasts
                        </a>
                        <span class="spacer"></span>
                      </li>
                      <li>
                        <a href="{{url('videos')}}" class="menu">
                          Videos
                        </a>
                        <span class="spacer"></span>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="{{url('dedications')}}">Participate <span class="sub"></span></a>
                    <ul style="list-style-type : none;">
                      <li>
                        <a href="{{url('dedications')}}" class="menu">
                          Dedications
                        </a>
                        <span class="spacer"></span>
                      </li>
                      <li>
                        <a href="{{url('contests')}}" class="menu">
                          Contests
                        </a>
                        <span class="spacer"></span>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="{{url('contact_us')}}" class="menu">
                      Contact
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div id="player-header" class="player-radio-bar ">
          <div id="jquery_jplayer_1" class="jp-jplayer"></div>
          <div class="container">
            <div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">
              <div class="jp-type-single">
                <div class="jp-gui jp-interface">
                  <a class="control-play ">
                    <i class="fa fa-play play-button jp-play"></i>
                    <i class="fa fa-pause pause-button jp-stop" style="display: none;"></i>
                    <i class="fa fa-spinner fa-spin player-loader" style="display: none;"></i>
                  </a>
                  <div class="control-infos" id="titrage">
                    <span class="control-cover cover-titrage">
                      <img class="media-cover" src="">
                    </span>
                    <span class="control-track ">
                      <span class="title ellipsis ellipsis-1l podcastPlay"></span>
                      <span class="half-track "> - </span>
                      <span class="artist ellipsis ellipsis-1l hidden"></span>
                      <div class="jp-controls-holder hidden">
                        <div class="jp-progress">
                          <div class="jp-seek-bar">
                            <div class="jp-play-bar"><div class="bullet">
                            </div></div>
                          </div>
                        </div>
                      </div>
                    </span>
                    <span class="control-actions">
                      <a id="player-vote" data-id= "" data-type = "" href="{{url('track_playlist_like')}}"><i class="fa fa-heart-o fa-lg fa-fw"></i></a>
                      <div id="partage">
                        <a class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-share-alt fa-lg fa-fw"></i></a>
                        <ul class="dropdown-menu">
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)">
                            <a href="http://www.facebook.com/sharer.php?u={{request()->fullUrl()}}" class="btn btn-icon btn-social white btn-lg share-facebook" title="Facebook" target="_blank"><i class="fa fa-facebook"></i>
                            </a>
                          </a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)">
                            <a href="http://twitter.com/share?text=[{{request()->uri}}]&url={{request()->fullUrl()}}" class="btn btn-icon btn-social white btn-lg share-twitter" title="Twitter" target="_blank"><i class="fa fa-twitter"></i>
                            </a>

                          </a></li>
                          <li role="presentation">
                            <a role="menuitem" tabindex="-1" href="javascript:void(0)">
                              <a href="#/" class="btn btn-icon btn-social white btn-lg share-linkdin" title="LinkedIn" target="_blank"><i class="fa fa-linkedin"></i>
                              </a>
                            </a>
                          </li>
                          <li role="presentation">
                            <a role="menuitem" tabindex="-1" href="javascript:void(0)">
                              <a href="https://plus.google.com/share?url={{request()->fullUrl()}}" class="btn btn-icon btn-social white btn-lg google-share" title="Google+" target="_blank">
                                <i class="fa fa-google-plus"></i>
                              </a>
                            </a>
                          </li>
                        </ul>
                      </div>

                      <a id="player-download" href="#" target="_blank" class="disabled">
                        <i class="fa fa-cloud-download fa-lg fa-fw"></i>
                      </a>

                    </span>
                    <div class="clear"></div>
                  </div>
                  <span class="container-control-stream">
                    <span class="control-stream">
                      @foreach($players as $player)
                      @if($loop->first)
                      <div class="current_radio">
                        <div id="current-radio" data-type="player" data-url="{{$player->url}}" data-cover="{{url('public/uploads/'.$player->image)}}" data-title="{{$player->name}}" data-autoplay="{{$player->auto_play}}" data-id="{{$player->id}}" data-download="0" onclick='playFile("{{$player->url}}","{{$player->name}}","{{url('public/uploads/'.$player->image)}}","{{$player->id}}","player","0","1")'>
                          <img src="{{url('public/uploads/'.$player->image)}}" class="logo-stream" />
                          <span class="stream-name hidden-xs">{{$player->name}}</span>
                        </div>
                      </div>
                      @else
                      <div id="other-radios">
                        <div class="other_radios">
                          <div class="radio-choice" data-type="player" data-url="{{$player->url}}" data-cover="{{url('public/uploads/'.$player->image)}}" data-title="{{$player->name}}" data-id="{{$player->id}}" data-download="0" onclick='playFile("{{$player->url}}","{{$player->name}}", "{{url('public/uploads/'.$player->image)}}","{{$player->id}}","player","0","1")'>
                            <img src="{{url('public/uploads/'.$player->image)}}" class="logo-stream-others" />
                            <span class="stream-name-others hidden-xs">{{$player->name}}</span>
                          </div>
                        </div>
                      </div>
                      @endif
                      @endforeach
                    </span>
                  </span>
                  <div class="control-actions-right hidden-sm hidden-xs">
                    <div class="last-track-zone">
                      <div class="last-track-button">
                        <a class="control-last-track">
                          <i class="fa fa-music fa-fw fa-lg"></i>
                        </a>
                      </div>
                      <span class="container-last-track-list">
                        <div id="last-track-list" class="last-track-list">
                          <div class="last-tracks">
                            @if(auth()->check())
                            @foreach($last_user_tracks as $track)
                            <div class="row m-l-0 m-r-0" style="margin-top: 0;">
                              <div class="col-sm-2 track-hour">14:06</div>
                              <div class="col-sm-2 track-cover"><img src="{{url('public/uploads/'.$track->image)}}" /></div>
                              <div class="col-sm-6 track-titration ellipsis ellipsis-1l">
                                <div class="track-title ellipsis ellipsis-1l">{{$track->title}}</div>
                                <div class="track-artist ellipsis ellipsis-1l">{{$track->artist}}</div>
                              </div>
                              <div class="col-sm-2 track-download">
                                <a id="player-download" href="{{url('public/uploads/'.$track->audio_sample)}}" download="download"><i class="fa fa-cloud-download fa-lg fa-fw"></i>
                                </a>
                              </div>
                            </div>
                            @endforeach
                            @endif

                          </div>
                        </div>
                      </span>
                    </div>
                    <div class="control-volume">
                      <a id="mute-volume" class="toggle-volume jp-mute">
                        <i class="fa fa-fw fa-volume-off fa-lg"></i>
                      </a>
                      <div class="control-slider-volume">
                        <div id="player-slider-volume" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                          <div class="container-sliderVolume">
                            <div class="bgSliderVolume"></div>
                          </div>
                          <div class="nui-slider-range ui-widget-header ui-corner-all ui-slider-range-mi"></div><span class=""></span>
                        </div>
                      </div>
                      <a id="unmute-volume" class="toggle-volume jp-volume-max" style="float:right;padding-left:5px;">
                        <i class="fa fa-fw fa-volume-up fa-lg"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</nav>
<div id="menu-responsive-overlay"></div>

<script>
  if ($('.logo').width() > 215) {
    $('.logo').addClass('big');
  }
</script>
<nav id="menu-responsive">
  <div class="entete">
    <div class="navbar-header">
      <img src="/upload/design/58e75360ecb707.32746423.png" alt="">
      <span id="closemenuButton"></span>
    </div>
  </div>

  <ul id="content-menuresponsive">
      <!-- @foreach($menus as $menu)
              <li>
                <a href="{{url($menu->page->title)}}">
                  {{$menu->title}}
                </a>
                @if($menu->menu_count)
                <ul style="list-style-type : none;">
                  @foreach($menu->menu as $sub_menu)
                  <li>
                    <a href="{{url($sub_menu->page->title)}}">
                      {{$sub_menu->title}}
                    </a>
                    <span class="spacer"></span>
                  </li>
                  @endforeach
                </ul>
                @endif
              </li>
              @endforeach -->
              <li class="list-group panel">
                <a style="padding-left:0px" href="/">
                  <span class="rLink">Home</span>
                </a>
                <a class="list-group-item collapsed"></a>
                <div class="spacer"></div>
              </li>
              <li class="list-group panel">
                <a data-parent="#content-menuresponsive" data-toggle="collapse" class="list-group-item collapsed" href="#menu_radio">
                  <span class="rLink">Radio</span>
                  <i class="fa fa-caret-down"></i>
                </a>
                <ul id="menu_radio" class="collapse">
                  <li class="list-group panel sub-menu">
                    <a href="/news-1">
                    News </a>
                  </li>
                  <li class="list-group panel sub-menu">
                    <a href="/programs">
                    Shows </a>
                  </li>
                  <li class="list-group panel sub-menu">
                    <a href="/team-1">
                    Team </a>
                  </li>
                  <li class="list-group panel sub-menu">
                    <a href="/events-1">
                    Events </a>
                  </li>
                </ul>
                <div class="spacer"></div>
              </li>
              <li class="list-group panel">
                <a data-parent="#content-menuresponsive" data-toggle="collapse" class="list-group-item collapsed" href="#menu_music">
                  <span class="rLink">Music</span>
                  <i class="fa fa-caret-down"></i>
                </a>
                <ul id="menu_music" class="collapse">
                  <li class="list-group panel sub-menu">
                    <a href="/top10">
                    Top 10 </a>
                  </li>
                  <li class="list-group panel sub-menu">
                    <a href="/artists-all-1">
                    Artists </a>
                  </li>
                  <li class="list-group panel sub-menu">
                    <a href="/musics-1">
                    Playlist </a>
                  </li>
                  <li class="list-group panel sub-menu">
                    <a href="/played-tracks">
                    Played tracks </a>
                  </li>
                </ul>
                <div class="spacer"></div>
              </li>
              <li class="list-group panel">
                <a data-parent="#content-menuresponsive" data-toggle="collapse" class="list-group-item collapsed" href="#menu_medias">
                  <span class="rLink">Medias</span>
                  <i class="fa fa-caret-down"></i>
                </a>
                <ul id="menu_medias" class="collapse">
                  <li class="list-group panel sub-menu">
                    <a href="/photos-1">
                    Photos </a>
                  </li>
                  <li class="list-group panel sub-menu">
                    <a href="/podcasts-1">
                    Podcasts </a>
                  </li>
                  <li class="list-group panel sub-menu">
                    <a href="/videos-1">
                    Videos </a>
                  </li>
                </ul>
                <div class="spacer"></div>
              </li>
              <li class="list-group panel">
                <a data-parent="#content-menuresponsive" data-toggle="collapse" class="list-group-item collapsed" href="#menu_participate">
                  <span class="rLink">Participate</span>
                  <i class="fa fa-caret-down"></i>
                </a>
                <ul id="menu_participate" class="collapse">
                  <li class="list-group panel sub-menu">
                    <a href="/dedicaces-1">
                    Dedications </a>
                  </li>
                  <li class="list-group panel sub-menu">
                    <a href="/contests-1">
                    Contests </a>
                  </li>
                </ul>
                <div class="spacer"></div>
              </li>
              <li class="list-group panel">
                <a style="padding-left:0px" href="{{url('contact_us')}}">
                  <span class="rLink">Contact</span>
                </a>
                <a class="list-group-item collapsed"></a>
                <div class="spacer"></div>
              </li>
            </ul>
          </nav>