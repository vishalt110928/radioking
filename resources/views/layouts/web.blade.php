<!DOCTYPE html>

<head>
  <meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=0" />

  <link href="/upload/51bf0534b8eaa1.96289316.ico" rel="icon" type="image/x-icon" />
  <title>Home - Radio King</title>
  <meta property="og:url" content="{{request()->fullUrl()}}" />
  <meta property="og:type" content="article" />
  <meta property="og:title" content="RadioKing" />
  <meta property="og:description" content="RadioKing | Free Internet Radio | NFL, Sports, Podcasts, Music &amp; News" />
  <meta property="og:image" content="{{url('/public/uploads/logo_image/open_logo.png')}}" />
  <meta name="twitter:card" content="RadioKing" />
  <meta name="twitter:site" content="RadioKing" />
  <meta name="twitter:creator" content="RadioKing" />
  <meta name="description" content="Radio King Site - Description" />
  <meta name="keywords" content="radio" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="en" />
  <meta name="csrf_token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{url('public/css/theme_script.css')}}" media="screen" />

  <link rel="stylesheet" href="{{url('public/css/theme3/mystyle.css')}}">
  <link rel="stylesheet" href="{{url('public/css/slick.css')}}">
  <link rel="stylesheet" href="{{url('public/css/slick-theme.css')}}">
  <link rel="stylesheet" href="{{url('public/css/owl.carousel.min.css')}}">
  <link rel="stylesheet" href="{{url('public/css/owl.theme.default.css')}}">
  <link rel="stylesheet" href="{{url('public/css/jquery-ui.min.css')}}" media="screen" />
  <link rel="stylesheet" href="{{url('public/css/jquery-ui.theme.min.css')}}" media="screen" />
  <!-- <link type="text/css" href="{{url('/public/jplayer/css/jplayer.pink.flag.min.css')}}" rel="stylesheet" /> -->
  <link id="mystylecss" rel="stylesheet" href="{{url('public/css/style.css')}}">
  <meta name="apple-itunes-app" content="app-id=">
  @yield('css')
  <script src="{{url('public/js/js/jquery-3.2.1.min.js')}}"></script>
  <script type="text/javascript" src="{{url('public/js/slick.min.js')}}"></script>
  <script type="text/javascript" src="{{url('public/js/owl.carousel.min.js')}}"></script>
  <script type="text/javascript" src="{{url('public/js/js/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{url('public/js/jquery.validate.min.js')}}"></script>
  <script type="text/javascript" src="{{url('public/js/additional-methods.min.js')}}"></script>
  <script type="text/javascript" src="{{url('public/js/js/jquery-ui.min.js')}}"></script>
  <script type="text/javascript" src="{{url('public/js/js/jplayer.playlist.min.js')}}"></script>
  <script type="text/javascript" src="{{url('public/js/js/jquery.jplayer.min.js')}}"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>
  @include('layouts.header.header')
  <div id="content-to-refresh">
    <div id="container-overlay" class="container p-l-0 p-r-0">
      <div id="overlay" style="display: none;">
        <div id="img-overlay" style="display: none;">
          <i class="fa fa-fw fa-spin fa-spinner" style="font-size: 4em;"></i>
        </div>
        <div id="text-overlay" style="display: none;">
          Loading...
        </div>
      </div>
    </div>
    <div class="content_body">
      @yield('slider')
      @yield('content')
    </div>
  </div>
  @include('layouts.footer.footer')
  @include('modal')

  <script>
    function playFile(url, title, cover, id = null, type = null,download = null,autoplay = null) {
      $("#jquery_jplayer_1").jPlayer("pause");
      $("#jquery_jplayer_1").jPlayer("destroy");
      $('.control-actions #player-vote').attr('data-type', type);
      $('.control-actions #player-vote').attr('data-id', id);

      $('#titrage.control-infos').find('img').attr('src', cover);
      $('#titrage.control-infos').find('.title.ellipsis.ellipsis-1l.podcastPlay').html(title);

      $("#jquery_jplayer_1").jPlayer({
        errorAlerts: true,
        ready: function() {
         if(autoplay == "1"){
          $('a.control-play .jp-play').css('display', 'none');
          $('a.control-play .pause-button').css('display', 'block');
          $(this).jPlayer("setMedia", {
            title: title,
            mp3: url,
            m4a: url,
              cover: cover // Defines the mp3 url
            }).jPlayer("play");

        }else{
          $(this).jPlayer("setMedia", {
            title: title,
            mp3: url,
            m4a: url,
            cover: cover // Defines the mp3 url
          });
          $('a.control-play .pause-button').css('display', 'none');
          $('a.control-play .jp-play').css('display', 'block');
        }
      },
      error: function(event) {
        alert(JSON.stringify(event.jPlayer.error.message));
        console.log(event.jPlayer.error);
        $('a.control-play .jp-play').css('display', 'block');
        $('a.control-play .pause-button').css('display', 'none');
      },
        ended: function() { // The $.jPlayer.event.ended event
          // $(this).jPlayer("play"); // Repeat the media
        },
        swfPath: "{{url('public/js/jplayer.swf')}}",
        cssSelector: {
          volume_bar: ".jp_volume_bar"
        },
      });
      var auth = "{{auth()->check()}}";
      if(type != "player" && type != null) {
        $('.jp-controls-holder').removeClass('hidden');
      }
      else{
        $('.jp-controls-holder').addClass('hidden');
      }
      if (auth && type == "track") {
        if (id) {
          $.ajax({
            method: "POST",
            url: "{{url('played_tracks')}}",
            dataType: "json",
            data: {
              "id": id
            },
            success: function(response) {
              console.log(response);
            },
            error: function(response) {
              console.log(response);
            }
          });
        }
      }
      if(download == "1") {
        $('a#player-download').attr({"download":true,"href":url});
        $('a#player-download').removeClass('disabled').addClass('enabled');
      }
      else{
        $('a#player-download').removeClass('enabled').addClass('disabled');
      }
    }

    function slider() {
      $('#banner-slick').slick({
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        ]
      });
      $('.artist_slick').slick({
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 1,
        responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 6,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }
        ]
      });
    }

    function push_url(route, elm = null) {
      $.ajax({
        method: "GET",
        url: route,
        data: {
          "notpushstate": 1
        },
        beforeSend: function() {},
        success: function(response) {
          $('.content_body').html('').html(response);
          if (elm) {
            slider();
          }
        },
        error: function(response) {},
        complete: function() {
          $('body').addClass('loaded');
          $.ajax({
            method: "GET",
            url: route,
            success: function(response) {
              history.pushState({
                url: route
              }, null, route);
            },
            error: function(response) {},
            complete: function() {}
          });
        }
      });
    }

    function volume(volume) {
      var volume = volume / 100;
      $("#jquery_jplayer_1").jPlayer("unmute");
      $("#jquery_jplayer_1").jPlayer("volume", volume);
    }
    function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }
    function checkCookie(name) {
      var cookie = getCookie(name);
      if (cookie != "") {
        $('div#cookie-bar').css('display','none');
      } else {
       $('div#cookie-bar').css('display','block')
     }
   }

   $(window).on('popstate', function() {
    location.assign(window.location.href)
  });

   $(document).ready(function() {

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
      }
    });
    var url = $('#current-radio').data('url');
    var title = $('#current-radio').data('title');
    var cover = $('#current-radio').data('cover');
    var type = $('#current-radio').data('type');
    var id = $('#current-radio').data('id');
    var download = $('#current-radio').data('download');
    var autoPlay = $('#current-radio').data('autoplay');
    playFile(url, title, cover, id,type,download,autoPlay);
      // if(autoPlay){
      //   setTimeout(function(){
      //     $('a.control-play .jp-play').css('display', 'none');
      //     $('a.control-play .pause-button').css('display', 'block');
      //     $('.jp-stop').trigger('click');
      //     $('.jp-play').trigger('click');
      //   },1000);
      // }
      $(".bgSliderVolume").slider();
      $(".bgSliderVolume").slider('option', 'value', '50');
      volume(50);
      $(".bgSliderVolume").slider({
        change: function(event, ui) {
          volume(ui.value)
        }
      });
      checkCookie("cb-enable");
      $(document).on('click','.cb-enable',function(){
        var exdays = 3;
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = "cb-enable =1;" + expires + ";path=/";
        $('div#cookie-bar').css('display','none');
      });

      $(document).on('click','.cb-policy',function(){
        $('#cb_policy_modal').modal('show'); 
      });

      $(document).on('click','.cb-policy',function(){
        $('#cb_policy_modal').modal('show'); 
      });
      $('input[name = check]').click(function(){
            $('input[name="check"]').not(this).prop('checked', false);
          });
      $('.apply_lang').click(function(){
        $('#lang_form').submit();
      });
      $('#lang_form').validate({
        submitHandler: function (form) {
          return true;
        }
      });


      $(document).on('click', '#player-vote,.player-vote', function(e) {
        e.preventDefault();
        var auth = "{{ auth::check()}}";
        if (auth) {
          elm = $(this);
          var route = elm.attr('href');
          type = elm.data('type');
          id = elm.data('id');
          $.ajax({
            type: "GET",
            url:route,
            data: {"type":type,"id": id },
            beforeSend: function(){

            },
            success: function(response) {
             alert(response.message)
           },
           error:function(response) {
            console.log(response.message);
          },
          complete:function() {

          }
        });

          $(window).scrollTop(0);
        } else {
          alert("Please login first.")
        }
      })
      $('a#unmute-volume').click(function() {
        $(".bgSliderVolume").slider('option', 'value', '100');
      });
      $('a#mute-volume').click(function() {
        $(".bgSliderVolume").slider('option', 'value', '0');
      });
      $('.jp-play').click(function() {
        $('a.control-play .jp-play').css('display', 'none');
        $('a.control-play .player-loader').css('display', 'none');
        $('a.control-play .pause-button').css('display', 'block');
      });
      $(document).on('click', '.jp-stop', function() {
        $('a.control-play .pause-button').css('display', 'none');
        $('a.control-play .jp-play').css('display', 'block');
        $("#jquery_jplayer_1").jPlayer("stop");
      });
      // $(document).on('click','.player-play',function(){
      //   setTimeout(function(){
      //     $('.jp-stop').trigger('click');
      //     $('.jp-play').trigger('click');
      //   }, 1000);

      // });
      $(document).on('click', '.menu,.detail', function(e) {
        e.preventDefault();
        elm = $(this);
        var route = elm.attr('href');
        push_url(route, elm);
        $(window).scrollTop(0);
      });
      $(document).on('click', '.auth_played_tracks', function(e) {
        e.preventDefault();
        var auth = "{{ auth::check()}}";
        if (auth) {
          elm = $(this);
          var route = elm.attr('href');
          push_url(route, elm);
          $(window).scrollTop(0);
        } else {
          alert("Please login first.")
        }
      });
    });
  </script>
  <script>
    $(document).ready(function() {
     slider();
     $(document).on('click', '.other_radios', function() {
      var other_players = $(this).html();
      var current_player = $('.current_radio').html();
      $('.current_radio').html(other_players);
      $(this).html(current_player);
    });

     $("#contact_form").validate({
      rules: {
        name: {
          required: true
        },
        email: {
          required: true,
          email: true
        },
        phone_no: {
          digits: true
        },
        subject: {
          required: true,
          maxlength: 100
        },
        message: {
          required: true,
        }
      },
      messages: {},
      submitHandler: function(form) {
        var formData = $(form).serialize();
        $.ajax({
          method: "POST",
          url: "{{url('/contact_us_store')}}",
          dataType: "json",
          data: formData,
          success: function(response) {
            alert(response.message);
            $('#contact_form')[0].reset();
          },
          error: function(response) {
            console.log(response.status)
            if (response.status == '419') {
              window.location.reload();
            }
            if (response.status == "422") {
              var errors = response.responseJSON.errors;
              for (key in errors) {
                $('#contact_form').find('.contact_' + key).html('').html(errors[key]);
              }
            } else {
              alert('something have gone wrong');
            }
          }
        });
        return false;
      }
    });
   });
 </script>
 @yield('script')
</body>
</html>