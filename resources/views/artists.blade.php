@if(! isset($notpushstate) || ($notpushstate != '1'))
@extends('layouts.web')
@section('slider')
@endif
<div class="container m-b-15 centered">
</div>
<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="{{url('/home')}}" class="toptip detail" title="Home"><i class="fa fa-home"></i></a></li>
			<li class="active">Artists</li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						Artists
						<a class="rss-link" href="#" target="_blank">
							<img class="rss-logo" style="width: 13px; margin-top: 1px;" src="{{url('public/img/rss.png')}}" />
							<span class="m-l-0 m-r-0 rss-text">RSS</span>
						</a>
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>
@if(! isset($notpushstate) || ($notpushstate != '1'))
@endsection
@section('content')
@endif
<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>
	<div class="row">
		<div class="col-xs-12  col-md-8  list_display">
			<!-- Affichage contenu -->
			<div class="m-t-30">
				<div class="centered">
					<ul class="pagination pagination-custom pagination-xs pagination-artiste">
						<li>
							<a href="{{url('artists?name=all')}}"><span class="@if(isset($pages['name']) && $pages['name'] == 'all' ) deactive @endif">All</span></a>
						</li>
						@for ($c = 65; $c <= 90; ++$c) 
						<li>
							<a href="{{url('artists?name='.chr($c))}}"><span class="@if(isset($pages['name']) && $pages['name'] == chr($c) ) deactive @endif">{{chr($c)}}</span></a>
						</li> 
						@endfor
					</ul>
				</div>
				<div class="article">
					@foreach($pages['artists'] as $artist)
					<div class="col-xs-3 col-md-3 m-b-30">
						<div class="col-xs-12 p-l-0 p-r-0">
							<a class="whole-div-link detail" href="{{url('artist_detail/'.$artist->slug)}}" class="list-action-zone">
								<div class="cover-preview list-img-zone">
									<img class="list-img-thumb  square_158"  src="{{url('public/uploads/'.$artist->image)}}">
								</div>
							</a>
						</div>
					</div>
					@endforeach
				</div>
			</div>
			<div class="pagination">
				{{$pages['artists']->links()}}
			</div>
		</div>
		<div class="hidden-xs hidden-sm col-md-4" id="sidebar">
			@foreach($sidebars as $sidebar)
			@include('aside.'.$sidebar)
			@endforeach
		</div>
	</div>
</div>
</div>
@if(! isset($notpushstate) || ($notpushstate != '1'))
@endsection
@endif