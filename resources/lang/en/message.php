<?php

return 
[
	
	'language' =>   'Language',
	
	'language_selection'=>'language selection',
	'language_description'=>'Please select the language(s) of the music you listen to.',
	'english'=>'English',
	'hindi'=>'Hindi',
	'punjabi'=>'punjabi',
	'french'=>'French',
	'german'=>'German',
	'spanish'=>'Spanish',
	'chinese'=>'Chinese',
	'japanese'=>'Japanese',
	'italian'=>'Italian',
	'arabic'=>'Arabic',
	'apply'=>'apply',
];
