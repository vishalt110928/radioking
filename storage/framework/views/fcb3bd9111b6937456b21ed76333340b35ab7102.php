<?php if(! isset($notpushstate) || ($notpushstate != '1')): ?>

<?php $__env->startSection('slider'); ?>
<?php endif; ?>
<div class="container m-b-15 centered">
</div>
<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="index.html" class="toptip" title="Home"><i class="fa fa-home"></i></a></li>
			<li class="active"><?php if(isset($page->title)): ?><?php echo e($page->title); ?> <?php endif; ?></li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						<?php if(isset($page->title)): ?><?php echo e($page->title); ?> <?php endif; ?>
						<a class="rss-link" href="#" target="_blank">
							<img class="rss-logo" style="width: 13px; margin-top: 1px;" src="<?php echo e(url('public/img/rss.png')); ?>" />
							<span class="m-l-0 m-r-0 rss-text">RSS</span>
						</a>
					</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if(! isset($notpushstate) || ($notpushstate != '1')): ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php endif; ?>
<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>
	<div class="row">
		<div class="col-xs-12  col-md-8  list_display">
			<!-- Affichage contenu -->
			<div class="m-t-30">
				<?php if(isset($page->content)): ?>
				<?php echo e($page->content); ?>

				<?php elseif(isset($page['content'])): ?>
				<?php echo e($page['content']); ?>

				<?php endif; ?>
			</div>
		</div>

		<div class="hidden-xs hidden-sm col-md-4" id="sidebar">
		<?php $__currentLoopData = $sidebars; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sidebar): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php echo $__env->make('aside.'.$sidebar, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>
	</div>
</div>
</div>
<?php if(! isset($notpushstate) || ($notpushstate != '1')): ?>
<?php $__env->stopSection(); ?>
<?php endif; ?>
<?php echo $__env->make('layouts.web', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/radioKing/resources/views/pages.blade.php ENDPATH**/ ?>