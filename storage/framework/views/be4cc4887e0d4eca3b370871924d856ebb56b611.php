
<div class="default-sidebar">
  <!-- Begin Side Navbar -->
  <nav class="side-navbar box-scroll sidebar-scroll">
    <!-- Begin Main Navigation -->
    <ul class="list-unstyled">
      <li><a href="#" ><i class="la la-columns"></i><span>Dashboard</span></a>
      </li>
      <li>
        <a href="<?php echo e(url('admin/contacts')); ?>" class="<?php if(isset($active) && $active == 'contacts'): ?>active <?php endif; ?>">
          <i class="fa fa-phone" aria-hidden="true"></i><span>Contact Us</span>
        </a>
      </li>
    </ul>
    <span class="heading">Components</span>
    <ul class="list-unstyled">
      <li><a href="#dropdown-forms" aria-expanded="<?php if(isset($active) && $active=='news') echo 'true'; else echo 'false';  ?>" data-toggle="collapse" class=""><i class="la la-list-alt"></i><span>News</span></a>
        <ul id="dropdown-forms" class="list-unstyled pt-0 collapse <?php if(isset($active) && $active == 'news'): ?>show <?php endif; ?>" style="">
          <li><a href="<?php echo e(url('admin/news/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_news'): ?>active <?php endif; ?>">Add News</a></li>
          <li><a href="<?php echo e(url('admin/news_cat')); ?>" class="<?php if(isset($li_active) && $li_active == 'news_cat'): ?>active <?php endif; ?>">Category Management</a></li>
          <li><a href="<?php echo e(url('admin/news')); ?>" class="<?php if(isset($li_active) && $li_active == 'news_manage'): ?>active <?php endif; ?>">News Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-app" aria-expanded="<?php if(isset($active) && $active=='events') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
          <i class="fa fa-key" aria-hidden="true"></i><span>Events</span>
        </a>
        <ul id="dropdown-app" class="collapse list-unstyled pt-0  <?php if(isset($active) && $active == 'events'): ?>show <?php endif; ?>">
          <li><a href="<?php echo e(url('admin/events/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_event'): ?>active <?php endif; ?>">Add Event</a></li>
          <li><a href="<?php echo e(url('admin/events_cat')); ?>" class="<?php if(isset($li_active) && $li_active == 'events_cat'): ?>active <?php endif; ?>">Category Management</a></li>
          <li><a href="<?php echo e(url('admin/events')); ?>" class="<?php if(isset($li_active) && $li_active == 'events_manage'): ?>active <?php endif; ?>">Events Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-dedi" aria-expanded="<?php if(isset($active) && $active=='dedications') echo 'true'; else echo 'false' ; ?>" data-toggle="collapse">
          <i class="fa fa-dashcube" aria-hidden="true"></i><span>Dedication</span>
        </a>
        <ul id="dropdown-dedi" class="collapse list-unstyled pt-0 <?php if(isset($active) && $active == 'dedications'): ?>show <?php endif; ?>">
          <li><a href="<?php echo e(url('admin/dedications/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_dedications'): ?>active <?php endif; ?>">Add Dedication</a></li>
          <li><a href="<?php echo e(url('admin/dedications')); ?>" class="<?php if(isset($li_active) && $li_active == 'dedications_manage'): ?>active <?php endif; ?>">Dedication Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-cont" aria-expanded="<?php if(isset($active) && $active=='contests') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
          <i class="fa fa-plus-circle" aria-hidden="true"></i><span>Contests</span>
        </a>
        <ul id="dropdown-cont" class="collapse list-unstyled pt-0 <?php if(isset($active) && $active == 'contests'): ?>show <?php endif; ?>">
          <li><a href="<?php echo e(url('admin/contests/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_contest'): ?>active <?php endif; ?>">Add Contests</a></li>
          <li><a href="<?php echo e(url('admin/contests_cat')); ?>" class="<?php if(isset($li_active) && $li_active == 'contests_cat'): ?>active <?php endif; ?>">Category Management</a></li>
          <li><a href="<?php echo e(url('admin/contests')); ?>" class="<?php if(isset($li_active) && $li_active == 'contests_manage'): ?>active <?php endif; ?>">Contest Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-slider" aria-expanded="<?php if(isset($active) && $active=='sliders') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
          <i class="fa fa-sliders" aria-hidden="true"></i><span>Sliders</span>
        </a>
        <ul id="dropdown-slider" class="collapse list-unstyled pt-0 <?php if(isset($active) && $active == 'sliders'): ?>show <?php endif; ?>">
          <li><a href="<?php echo e(url('admin/sliders/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_slider'): ?>active <?php endif; ?>">Add Slider</a></li>
          <li><a href="<?php echo e(url('admin/sliders')); ?>" class="<?php if(isset($li_active) && $li_active == 'sliders_manage'): ?>active <?php endif; ?>">Slider Management</a></li>
        </ul>
      </li>
      <li><a href="#dropdown-pod" aria-expanded="<?php if(isset($active) && $active=='podcasts') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-arrows-alt" aria-hidden="true"></i><span>Podcasts</span></a>
        <ul id="dropdown-pod" class="collapse list-unstyled pt-0 <?php if(isset($active) && $active == 'podcasts'): ?>show <?php endif; ?>">
          <li><a href="<?php echo e(url('admin/podcasts/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_podcasts'): ?>active <?php endif; ?>">Add Podcasts</a></li>
          <li><a href="<?php echo e(url('admin/podcasts_cat')); ?>" class="<?php if(isset($li_active) && $li_active == 'podcasts_cat'): ?>active <?php endif; ?>">Category Management</a></li>
          <li><a href="<?php echo e(url('admin/podcasts')); ?>" class="<?php if(isset($li_active) && $li_active == 'podcasts_manage'): ?>active <?php endif; ?>">Podcasts Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-play" aria-expanded="<?php if(isset($active) && $active=='playlists') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
          <i class="fa fa-arrow-circle-right" aria-hidden="true"></i><span>Playlists</span>
        </a>
        <ul id="dropdown-play" class="collapse list-unstyled pt-0 <?php if(isset($active) && $active == 'playlists'): ?>show <?php endif; ?>">
          <li><a href="<?php echo e(url('admin/playlists/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_playlist'): ?>active <?php endif; ?>">Add Playlists</a></li>
          <li><a href="<?php echo e(url('admin/playlists_cat')); ?>" class="<?php if(isset($li_active) && $li_active == 'playlists_cat'): ?>active <?php endif; ?>">Category Management</a></li>
          <li><a href="<?php echo e(url('admin/playlists')); ?>" class="<?php if(isset($li_active) && $li_active == 'playlists_manage'): ?>active <?php endif; ?>">Playlists Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-art" aria-expanded="<?php if(isset($active) && $active=='artists') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
          <i class="fa fa-address-card" aria-hidden="true"></i><span>Artists</span>
        </a>
        <ul id="dropdown-art" class="collapse list-unstyled pt-0 <?php if(isset($active) && $active == 'artists'): ?>show <?php endif; ?>">
          <li><a href="<?php echo e(url('admin/artists/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_artists'): ?>active <?php endif; ?>">Add Artists</a></li>
          <li><a href="<?php echo e(url('admin/artists_cat')); ?>" class="<?php if(isset($li_active) && $li_active == 'artists_cat'): ?>active <?php endif; ?>">Category Management</a></li>
          <li><a href="<?php echo e(url('admin/artists')); ?>" class="<?php if(isset($li_active) && $li_active == 'artists_manage'): ?>active <?php endif; ?>">Artists Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-feed" aria-expanded="<?php if(isset($active) && $active=='livefeeds') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
          <i class="fa fa-rss" aria-hidden="true"></i><span>Livefeeds</span>
        </a>
        <ul id="dropdown-feed" class="collapse list-unstyled pt-0 <?php if(isset($active) && $active == 'livefeeds'): ?>show <?php endif; ?>">
          <li><a href="<?php echo e(url('admin/livefeeds/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_livefeed'): ?>active <?php endif; ?>">Add Livefeeds</a></li>
          <li><a href="<?php echo e(url('admin/livefeeds')); ?>" class="<?php if(isset($li_active) && $li_active == 'livefeeds_manage'): ?>active <?php endif; ?>" >Livefeeds Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-page" aria-expanded="<?php if(isset($active) && $active=='pages') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
          <i class="fa fa-file-code-o" aria-hidden="true"></i><span>Pages</span>
        </a>
        <ul id="dropdown-page" class="collapse list-unstyled pt-0 <?php if(isset($active) && $active == 'pages'): ?>show <?php endif; ?>">
          <li><a href="<?php echo e(url('admin/pages/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_page'): ?>active <?php endif; ?>">Add Pages</a></li>
          <li><a href="<?php echo e(url('admin/pages_cat')); ?>" class="<?php if(isset($li_active) && $li_active == 'pages_cat'): ?>active <?php endif; ?>">Category Management</a></li>
          <li><a href="<?php echo e(url('admin/pages')); ?>" class="<?php if(isset($li_active) && $li_active == 'pages_manage'): ?>active <?php endif; ?>">Pages Management</a></li>
        </ul>
      </li>
      <li>
        <a href="#dropdown-teams" aria-expanded="<?php if(isset($active) && $active=='teams') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
         <i class="fa fa-users" aria-hidden="true"></i><span>Teams</span>
       </a>
       <ul id="dropdown-teams" class="collapse list-unstyled pt-0 <?php if(isset($active) && $active == 'teams'): ?>show <?php endif; ?>">
        <li><a href="<?php echo e(url('admin/teams/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_teams'): ?>active <?php endif; ?>">Add Member</a></li>
        <li><a href="<?php echo e(url('admin/teams_cat')); ?>" class="<?php if(isset($li_active) && $li_active == 'teams_cat'): ?>active <?php endif; ?>">Category Management</a></li>
        <li><a href="<?php echo e(url('admin/teams')); ?>" class="<?php if(isset($li_active) && $li_active == 'teams_manage'): ?>active <?php endif; ?>">Teams Management</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-prog" aria-expanded="<?php if(isset($active) && $active=='programs') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-product-hunt" aria-hidden="true"></i><span>Programs</span>
      </a>
      <ul id="dropdown-prog" class="collapse list-unstyled pt-0 <?php if(isset($active) && $active == 'programs'): ?>show <?php endif; ?>">
        <li><a href="<?php echo e(url('admin/programs/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_program'): ?>active <?php endif; ?>">Add A Program</a></li>
        <li><a href="<?php echo e(url('admin/programs_cat')); ?>" class="<?php if(isset($li_active) && $li_active == 'programs_cat'): ?>active <?php endif; ?>" >Category Management</a></li>
        <li><a href="<?php echo e(url('admin/programs')); ?>" class="<?php if(isset($li_active) && $li_active == 'programs_manage'): ?>active <?php endif; ?>" >Programs Management</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-track" aria-expanded="<?php if(isset($active) && $active=='tracks') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-music" aria-hidden="true"></i><span>Music Base</span>
      </a>
      <ul id="dropdown-track" class="collapse list-unstyled pt-0 <?php if(isset($active) && $active == 'tracks'): ?>show <?php endif; ?>">
        <li><a href="<?php echo e(url('admin/tracks')); ?>" class="<?php if(isset($li_active) && $li_active == 'tracks_manage'): ?>active <?php endif; ?>">Track Management</a></li>
        <li><a href="<?php echo e(url('admin/tracks/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_tracks'): ?>active <?php endif; ?>">Add A Track</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-gal" aria-expanded="<?php if(isset($active) && $active=='gallerys') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-film" aria-hidden="true"></i><span>Gallery</span>
      </a>
      <ul id="dropdown-gal" class="collapse list-unstyled pt-0 <?php if(isset($active) && $active == 'gallerys'): ?>show <?php endif; ?>">
        <li><a href="<?php echo e(url('admin/photos')); ?>" class="<?php if(isset($li_active) && $li_active == 'gallerys_photos'): ?> active <?php endif; ?>">Add Photos</a></li>
        <li><a href="<?php echo e(url('admin/gallerys/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_gallerys'): ?> active <?php endif; ?>">Add A Gallery</a></li>
        <li><a href="<?php echo e(url('admin/gallerys_cat')); ?>" class="<?php if(isset($li_active) && $li_active == 'gallerys_cat'): ?> active <?php endif; ?>">Category Management</a></li>
        <li><a href="<?php echo e(url('admin/gallerys')); ?>" class="<?php if(isset($li_active) && $li_active == 'gallerys_manage'): ?> active <?php endif; ?>">Gallery Management</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-vid" aria-expanded="<?php if(isset($active) && $active=='videos') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-youtube-play" aria-hidden="true"></i><span>Videos</span>
      </a>
      <ul id="dropdown-vid" class="collapse list-unstyled pt-0 <?php if(isset($active) && $active == 'videos'): ?>show <?php endif; ?>">
        <li><a href="<?php echo e(url('admin/videos/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_video'): ?>active <?php endif; ?>">Add A Video</a></li>
        <li><a href="<?php echo e(url('admin/videos_cat')); ?>" class="<?php if(isset($li_active) && $li_active == 'videos_cat'): ?>active <?php endif; ?>">Category Management</a></li>
        <li><a href="<?php echo e(url('admin/videos')); ?>" class="<?php if(isset($li_active) && $li_active == 'videos_manage'): ?>active <?php endif; ?>">Video Management</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-ads" aria-expanded="<?php if(isset($active) && $active=='ads') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-buysellads" aria-hidden="true"></i><span>Ads</span>
      </a>
      <ul id="dropdown-ads" class="collapse list-unstyled pt-0 <?php if(isset($active) && $active == 'ads'): ?>show <?php endif; ?>">
        <li><a href="<?php echo e(url('admin/ads/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_ads'): ?>active <?php endif; ?>">Add A Ads</a></li>
        <li><a href="<?php echo e(url('admin/ads')); ?>" class="<?php if(isset($li_active) && $li_active == 'ads_manage'): ?>active <?php endif; ?>">Ads Management</a></li>
      </ul>
    </li>
    <li><a href="#dropdown-rss" aria-expanded="<?php if(isset($active) && $active=='rssfeeds') echo 'true'; else echo 'false';  ?>" data-toggle="collapse"><i class="la la-puzzle-piece"></i><span>Rss Feed</span></a>
      <ul id="dropdown-rss" class="collapse list-unstyled pt-0  <?php if(isset($active) && $active == 'rssfeeds'): ?>show <?php endif; ?>">
        <li><a href="<?php echo e(url('admin/rssfeeds/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_rssfeed'): ?>active <?php endif; ?>">Add A Rss Feed</a></li>
        <li><a href="<?php echo e(url('admin/rssfeeds_cat')); ?>" class="<?php if(isset($li_active) && $li_active == 'rssfeeds_cat'): ?>active <?php endif; ?>">Category Management</a></li>
        <li><a href="<?php echo e(url('admin/rssfeeds')); ?>" class="<?php if(isset($li_active) && $li_active == 'rssfeeds_manage'): ?>active <?php endif; ?>">Feed Management</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-player" aria-expanded="<?php if(isset($active) && $active=='players') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-stop-circle" aria-hidden="true"></i><span>Players</span>
      </a>
      <ul id="dropdown-player" class="collapse list-unstyled pt-0  <?php if(isset($active) && $active == 'players'): ?>show <?php endif; ?>">
        <li><a href="<?php echo e(url('admin/players/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_player'): ?>active <?php endif; ?>">Add Players</a></li>
        <li><a href="<?php echo e(url('admin/players')); ?>" class="<?php if(isset($li_active) && $li_active == 'players_manage'): ?>active <?php endif; ?>">Players Management</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-user" aria-expanded="<?php if(isset($active) && $active=='users') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-user-o" aria-hidden="true"></i><span>Users</span>
      </a>
      <ul id="dropdown-user" class="collapse list-unstyled pt-0  <?php if(isset($active) && $active == 'users'): ?>show <?php endif; ?>">
        <li><a href="<?php echo e(url('admin/users/create')); ?>" class="<?php if(isset($li_active) && $li_active == 'add_user'): ?>active <?php endif; ?>">Add A Rss Feed</a></li>
        <li><a href="<?php echo e(url('admin/users_role')); ?>" class="<?php if(isset($li_active) && $li_active == 'users_role'): ?>active <?php endif; ?>">Role Management</a></li>
        <li><a href="<?php echo e(url('admin/users')); ?>" class="<?php if(isset($li_active) && $li_active == 'users_manage'): ?>active <?php endif; ?>">User  Management</a></li>
      </ul>
    </li>
    <li>
      <a href="#dropdown-config" aria-expanded="<?php if(isset($active) && $active=='configs') echo 'true'; else echo 'false';  ?>" data-toggle="collapse">
        <i class="fa fa-cog" aria-hidden="true"></i><span>Configuration</span>
      </a>
      <ul id="dropdown-config" class="collapse list-unstyled pt-0  <?php if(isset($active) && $active == 'configs'): ?>show <?php endif; ?>">
        <li><a href="<?php echo e(url('admin/gen_configs')); ?>" class="<?php if(isset($li_active) && $li_active == 'gen_configs'): ?>active <?php endif; ?>">General Settings</a></li>
        <li><a href="<?php echo e(url('admin/ext_apis')); ?>" class="<?php if(isset($li_active) && $li_active == 'ext_apis'): ?>active <?php endif; ?>">External Api</a></li>
        <li><a href="<?php echo e(url('admin/soc_nets')); ?>" class="<?php if(isset($li_active) && $li_active == 'soc_nets'): ?>active <?php endif; ?>">Social Networks</a></li>
        <li><a href="<?php echo e(url('admin/chats')); ?>" class="<?php if(isset($li_active) && $li_active == 'chats'): ?>active <?php endif; ?>">Chat</a></li>
        <li><a href="<?php echo e(url('admin/email_configs')); ?>" class="<?php if(isset($li_active) && $li_active == 'email_configs'): ?>active <?php endif; ?>">Email</a></li>
        <li><a href="<?php echo e(url('admin/privacy_configs')); ?>" class="<?php if(isset($li_active) && $li_active == 'privacy_configs'): ?>active <?php endif; ?>">Privacy</a></li>
        <li><a href="<?php echo e(url('admin/menus')); ?>" class="<?php if(isset($li_active) && $li_active == 'menus'): ?>active <?php endif; ?>">Menus</a></li>
        <li><a href="<?php echo e(url('admin/blocks')); ?>" class="<?php if(isset($li_active) && $li_active == 'blocks'): ?>active <?php endif; ?>">Blocks</a></li>
      </ul>
    </li>
  </ul>
</nav>
<!-- End Side Navbar -->
</div><?php /**PATH /var/www/html/radioKing/resources/views/layouts/sidebar/admin_sidebar.blade.php ENDPATH**/ ?>