<?php if(! $gen_configs->site_maintainance): ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Service Unavailable</title>

	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

	<!-- Styles -->
	<style>
		html, body {
			background-color: #fff;
			color: #636b6f;
			font-family: 'Nunito', sans-serif;
			font-weight: 100;
			height: 100vh;
			margin: 0;
		}

		.full-height {
			height: 100vh;
		}

		.flex-center {
			align-items: center;
			display: flex;
			justify-content: center;
		}

		.position-ref {
			position: relative;
		}

		.code {
			border-right: 2px solid;
			font-size: 26px;
			padding: 0 15px 0 15px;
			text-align: center;
		}

		.message {
			font-size: 18px;
			text-align: center;
		}
	</style>
</head>
<body>
	<div class="flex-center position-ref full-height">
		<div class="code">
			503
		</div>

		<div class="message" style="padding: 10px;">
			Service Unavailable          
		</div>
	</div>
</body>
</html>
<?php else: ?>
<!DOCTYPE html>

<head>
	<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=0" />

	<link href="/upload/51bf0534b8eaa1.96289316.ico" rel="icon" type="image/x-icon" />
	<title>Home - Radio King</title>
	<meta property="og:url" content="<?php echo e(request()->fullUrl()); ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="RadioKing" />
	<meta property="og:description" content="RadioKing | Free Internet Radio | NFL, Sports, Podcasts, Music &amp; News" />
	<meta property="og:image" content="<?php echo e(url('/public/uploads/logo_image/open_logo.png')); ?>" />
	<meta name="twitter:card" content="RadioKing" />
	<meta name="twitter:site" content="RadioKing" />
	<meta name="twitter:creator" content="RadioKing" />
	<meta name="description" content="Radio King Site - Description" />
	<meta name="keywords" content="radio" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="en" />
	<meta name="csrf_token" content="<?php echo e(csrf_token()); ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo e(url('public/css/theme_script.css')); ?>" media="screen" />

	<link rel="stylesheet" href="<?php echo e(url('public/css/theme3/mystyle.css')); ?>">
	<link id="mystylecss" rel="stylesheet" href="<?php echo e(url('public/css/style.css')); ?>">
	<meta name="apple-itunes-app" content="app-id=">
	<?php echo $__env->yieldContent('css'); ?>
	<script src="<?php echo e(url('public/js/js/jquery-3.2.1.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(url('public/js/js/jplayer.playlist.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(url('public/js/js/jquery.jplayer.min.js')); ?>"></script>
</head>

<body>
	<?php if($gen_configs->player_maintainance): ?>
	<div id="player-header" class="player-radio-bar ">
		<div id="jquery_jplayer_1" class="jp-jplayer"></div>
		<div class="container">
			<div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">
				<div class="jp-type-single">
					<div class="jp-gui jp-interface">
						<a class="control-play ">
							<i class="fa fa-play play-button jp-play"></i>
							<i class="fa fa-pause pause-button jp-stop" style="display: none;"></i>
							<i class="fa fa-spinner fa-spin player-loader" style="display: none;"></i>
						</a>
						<div class="control-infos" id="titrage">
							<span class="control-cover cover-titrage">
								<img class="media-cover" src="">
							</span>
							<span class="control-track ">
								<span class="title ellipsis ellipsis-1l podcastPlay"></span>
								<span class="half-track "> - </span>
								<span class="artist ellipsis ellipsis-1l hidden"></span>
								<div class="jp-controls-holder hidden">
									<div class="jp-progress">
										<div class="jp-seek-bar">
											<div class="jp-play-bar"><div class="bullet">
											</div></div>
										</div>
									</div>
								</div>
							</span>
							<span class="control-actions">
								<a id="player-vote" data-id= "" data-type = "" href="<?php echo e(url('track_playlist_like')); ?>"><i class="fa fa-heart-o fa-lg fa-fw"></i></a>
								<div id="partage">
									<a class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-share-alt fa-lg fa-fw"></i></a>
									<ul class="dropdown-menu">
										<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)">
											<a href="http://www.facebook.com/sharer.php?u=<?php echo e(request()->fullUrl()); ?>" class="btn btn-icon btn-social white btn-lg share-facebook" title="Facebook" target="_blank"><i class="fa fa-facebook"></i>
											</a>
										</a></li>
										<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)">
											<a href="http://twitter.com/share?text=[<?php echo e(request()->uri); ?>]&url=<?php echo e(request()->fullUrl()); ?>" class="btn btn-icon btn-social white btn-lg share-twitter" title="Twitter" target="_blank"><i class="fa fa-twitter"></i>
											</a>

										</a></li>
										<li role="presentation">
											<a role="menuitem" tabindex="-1" href="javascript:void(0)">
												<a href="#/" class="btn btn-icon btn-social white btn-lg share-linkdin" title="LinkedIn" target="_blank"><i class="fa fa-linkedin"></i>
												</a>
											</a>
										</li>
										<li role="presentation">
											<a role="menuitem" tabindex="-1" href="javascript:void(0)">
												<a href="https://plus.google.com/share?url=<?php echo e(request()->fullUrl()); ?>" class="btn btn-icon btn-social white btn-lg google-share" title="Google+" target="_blank">
													<i class="fa fa-google-plus"></i>
												</a>
											</a>
										</li>
									</ul>
								</div>

								<a id="player-download" href="#" target="_blank" class="disabled">
									<i class="fa fa-cloud-download fa-lg fa-fw"></i>
								</a>
							</span>
							<div class="clear"></div>
						</div>
						<span class="container-control-stream">
							<span class="control-stream">
								<?php $__currentLoopData = $players; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $player): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php if($loop->first): ?>
								<div class="current_radio">
									<div id="current-radio" data-type="player" data-url="<?php echo e($player->url); ?>" data-cover="<?php echo e(url('public/uploads/'.$player->image)); ?>" data-title="<?php echo e($player->name); ?>" data-autoplay="<?php echo e($player->auto_play); ?>" data-id="<?php echo e($player->id); ?>" data-download="0" onclick='playFile("<?php echo e($player->url); ?>","<?php echo e($player->name); ?>","<?php echo e(url('public/uploads/'.$player->image)); ?>","<?php echo e($player->id); ?>","player","0","1")'>
										<img src="<?php echo e(url('public/uploads/'.$player->image)); ?>" class="logo-stream" />
										<span class="stream-name hidden-xs"><?php echo e($player->name); ?></span>
									</div>
								</div>
								<?php else: ?>
								<div id="other-radios">
									<div class="other_radios">
										<div class="radio-choice" data-type="player" data-url="<?php echo e($player->url); ?>" data-cover="<?php echo e(url('public/uploads/'.$player->image)); ?>" data-title="<?php echo e($player->name); ?>" data-id="<?php echo e($player->id); ?>" data-download="0" onclick='playFile("<?php echo e($player->url); ?>","<?php echo e($player->name); ?>", "<?php echo e(url('public/uploads/'.$player->image)); ?>","<?php echo e($player->id); ?>","player","0","1")'>
											<img src="<?php echo e(url('public/uploads/'.$player->image)); ?>" class="logo-stream-others" />
											<span class="stream-name-others hidden-xs"><?php echo e($player->name); ?></span>
										</div>
									</div>
								</div>
								<?php endif; ?>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</span>
						</span>
						<div class="control-actions-right hidden-sm hidden-xs">
							<div class="last-track-zone">
								<div class="last-track-button">
									<a class="control-last-track">
										<i class="fa fa-music fa-fw fa-lg"></i>
									</a>
								</div>
								<span class="container-last-track-list">
									<div id="last-track-list" class="last-track-list">
										<div class="last-tracks">
											<?php if(auth()->check()): ?>
											<?php $__currentLoopData = $last_user_tracks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $track): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<div class="row m-l-0 m-r-0" style="margin-top: 0;">
												<div class="col-sm-2 track-hour">14:06</div>
												<div class="col-sm-2 track-cover"><img src="<?php echo e(url('public/uploads/'.$track->image)); ?>" /></div>
												<div class="col-sm-6 track-titration ellipsis ellipsis-1l">
													<div class="track-title ellipsis ellipsis-1l"><?php echo e($track->title); ?></div>
													<div class="track-artist ellipsis ellipsis-1l"><?php echo e($track->artist); ?></div>
												</div>
												<div class="col-sm-2 track-download">
													<a id="player-download" href="<?php echo e(url('public/uploads/'.$track->audio_sample)); ?>" download="download"><i class="fa fa-cloud-download fa-lg fa-fw"></i>
													</a>
												</div>
											</div>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											<?php endif; ?>

										</div>
									</div>
								</span>
							</div>
							<div class="control-volume">
								<a id="mute-volume" class="toggle-volume jp-mute">
									<i class="fa fa-fw fa-volume-off fa-lg"></i>
								</a>
								<div class="control-slider-volume">
									<div id="player-slider-volume" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
										<div class="container-sliderVolume">
											<div class="bgSliderVolume"></div>
										</div>
										<div class="nui-slider-range ui-widget-header ui-corner-all ui-slider-range-mi"></div><span class=""></span>
									</div>
								</div>
								<a id="unmute-volume" class="toggle-volume jp-volume-max" style="float:right;padding-left:5px;">
									<i class="fa fa-fw fa-volume-up fa-lg"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<div id="content-to-refresh">
		<div id="container-overlay" class="container p-l-0 p-r-0">
			<div id="overlay" style="display: none;">
				<div id="img-overlay" style="display: none;">
					<i class="fa fa-fw fa-spin fa-spinner" style="font-size: 4em;"></i>
				</div>
				<div id="text-overlay" style="display: none;">
					Loading...
				</div>
			</div>
		</div>
		<div class="content_body" align="center">
			<h1>
				<?php echo e($gen_configs->text_maintainance); ?>

			</h1>
		</div>
	</div>
	<?php if($gen_configs->player_maintainance): ?>
	<script>
		function playFile(url, title, cover, id = null, type = null,download = null,autoplay = null) {
			$("#jquery_jplayer_1").jPlayer("pause");
			$("#jquery_jplayer_1").jPlayer("destroy");
			$('.control-actions #player-vote').attr('data-type', type);
			$('.control-actions #player-vote').attr('data-id', id);

			$('#titrage.control-infos').find('img').attr('src', cover);
			$('#titrage.control-infos').find('.title.ellipsis.ellipsis-1l.podcastPlay').html(title);

			$("#jquery_jplayer_1").jPlayer({
				errorAlerts: true,
				ready: function() {
					if(autoplay == "1"){
						$('a.control-play .jp-play').css('display', 'none');
						$('a.control-play .pause-button').css('display', 'block');
						$(this).jPlayer("setMedia", {
							title: title,
							mp3: url,
							m4a: url,
              cover: cover // Defines the mp3 url
            }).jPlayer("play");

					}else{
						$(this).jPlayer("setMedia", {
							title: title,
							mp3: url,
							m4a: url,
            cover: cover // Defines the mp3 url
          });
						$('a.control-play .pause-button').css('display', 'none');
						$('a.control-play .jp-play').css('display', 'block');
					}
				},
				error: function(event) {
					alert(JSON.stringify(event.jPlayer.error.message));
					console.log(event.jPlayer.error);
					$('a.control-play .jp-play').css('display', 'block');
					$('a.control-play .pause-button').css('display', 'none');
				},
        ended: function() { // The $.jPlayer.event.ended event
          // $(this).jPlayer("play"); // Repeat the media
        },
        swfPath: "<?php echo e(url('public/js/jplayer.swf')); ?>",
        cssSelector: {
        	volume_bar: ".jp_volume_bar"
        },
      });

		}

		$(document).ready(function() {
			var url = $('#current-radio').data('url');
			var title = $('#current-radio').data('title');
			var cover = $('#current-radio').data('cover');
			var type = $('#current-radio').data('type');
			var id = $('#current-radio').data('id');
			var download = $('#current-radio').data('download');
			var autoPlay = $('#current-radio').data('autoplay');
			playFile(url, title, cover, id,type,download,autoPlay);



			$('.jp-play').click(function() {
				$('a.control-play .jp-play').css('display', 'none');
				$('a.control-play .player-loader').css('display', 'none');
				$('a.control-play .pause-button').css('display', 'block');
			});
			$(document).on('click', '.jp-stop', function() {
				$('a.control-play .pause-button').css('display', 'none');
				$('a.control-play .jp-play').css('display', 'block');
				$("#jquery_jplayer_1").jPlayer("stop");
			});
		});
	</script>
	<?php endif; ?>
</body>
</html>
<?php endif; ?>
<?php /**PATH /var/www/html/radioKing/resources/views/errors/503.blade.php ENDPATH**/ ?>