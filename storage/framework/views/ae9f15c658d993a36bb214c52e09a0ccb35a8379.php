<div class="footer container counter-container hidden-sm hidden-xs">
  <div class="container m-b-30">
    <div class="row">
      <!-- A changer en fonction du type de bloc -->
      <div class="col-xs-12 col-md-4">
        <div class="row">
          <div class="col-xs-12 ">
            <div id="bloc-slide-photo" class="bloc-content carousel carousel-bloc slide">
              <div class="wrap">
                <div class="row">
                  <div class="col-md-12">
                    <div class="bloc-content-title">
                      <div class="bloc-title-readmore">
                        <a href="<?php echo e(url('news')); ?>" class="read-more-title detail">
                          <i class="fa fa-arrow-circle-o-right"></i> More
                        </a>
                      </div>
                      <h3 class="ellipsis ellipsis-1l">Latest news</h3>
                    </div>
                  </div>
                </div>
                <?php if(count($latest_news)): ?> <?php $__currentLoopData = $latest_news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $latest_new): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="row m-b-10">
                  <a href="<?php echo e(url('news_detail/'.$latest_new->id)); ?>" class="detail">
                    <div class="col-md-4">
                      <div class="cover-preview">
                        <img src="<?php echo e(url('public/uploads/'.$latest_new->image)); ?>" class="cover-img rect_100_67" />
                      </div>
                    </div>
                    <div class="col-xs-12 col-md-8 p-l-0">
                      <h4 class="ellipsis ellipsis-2l bloc-ellipsis">
                        <?php echo e($latest_new->title); ?>                            
                      </h4>
                    </div>
                  </a>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
        <!-- end foreach bloc -->
      </div>
      <!-- A changer en fonction du type de bloc -->
      <div class="col-xs-12 col-md-4">
        <div class="row">
          <div class="col-xs-12 ">
            <div id="bloc-slide-photo" class="bloc-content carousel carousel-bloc slide">
              <div class="wrap">
                <div class="row">
                  <div class="col-md-12">
                    <div class="bloc-content-title slider-bloc-content-title">
                      <h3 class="h2-slider ellipsis ellipsis-1l">Photos</h3>
                    </div>
                  </div>
                </div>
                <div id="lightgallery_5d0782e173034">
                  <div class="row" role="listbox">
                    <?php $__currentLoopData = $photos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $photo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item col-md-3">
                      <a class="" href="#">
                        <div class="cover-preview">
                          <img src="<?php echo e(url('public/uploads/'.$photo->path)); ?>" class="cover-img square_83" />
                        </div>
                      </a>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end foreach bloc -->
      </div>
      <!-- A changer en fonction du type de bloc -->
      <div class="col-xs-12 col-md-4">
        <div class="row">
          <div class="col-xs-12 contact-row">
            <div class="row">
              <div class="col-md-12">
                <div class="bloc-content-title">
                  <h3 class="ellipsis ellipsis-1l">Contact us</h3>
                </div>
              </div>
            </div>
            <?php echo e(Form::open(['url' => 'contct_us_store', 'method' => 'post' , 'class' =>'card' , 'id'=>'contact_form'])); ?>

            <?php echo csrf_field(); ?>
            <div class="form-group">
              <?php echo e(Form::text('name','',['placeholder'=>'Name' , 'class'=>'form-control'])); ?>

              <div class="contact_name"></div>
            </div>
            <div class="form-group">
              <?php echo e(Form::text('email','',['placeholder'=>'Email' , 'class'=>'form-control'])); ?>

              <div class="contact_email"></div>
            </div>
            <div class="form-group">
              <?php echo e(Form::text('phone_no','',['placeholder'=>'Phone No' , 'class'=>'form-control'])); ?>

              <div class="contact_phone_no"></div>
            </div>
            <div class="form-group">
              <?php echo e(Form::text('subject','',['placeholder'=>'Subject' , 'class'=>'form-control'])); ?>

              <div class="contact_subject"></div>
            </div>
            <div class="form-group">
              <?php echo e(Form::textarea('message','', ['placeholder'=>'Message','class' => 'form-control','size' =>'50x3'])); ?>

              <div class="contact_message"></div>
            </div>
             <div class="g-recaptcha " data-sitekey="<?php echo e($ext_apis->google_api); ?>"></div>
             <div class="contact_g-recaptcha-response"></div>
             <div class="clear" style="margin-top: 9px"></div> 
            <?php echo e(Form::submit('Submit' ,['class'=>'btn btn-lg btn-block btn-success'])); ?>

            <?php echo e(Form::close()); ?>

          </div>
        </div>
      </div>
      <!-- end foreach colonne -->
    </div>
    <div class="cl"></div>
  </div>
  <div class="mentions-radioking">
    <div class="container">
      <div class="row">
        <div class="col-md-12 copyright text-center">RadioKing</div>
      </div>
    </div>
  </div>
  <div id="cookie-bar" class="fixed bottom" style="z-index:2000;"><p>By using this website you consent to the use of cookies.<a href="#" class="cb-enable">I agree</a><a href="#" class="cb-policy">Learn more</a></p></div>
</div><?php /**PATH /var/www/html/radioKing/resources/views/layouts/footer/footer.blade.php ENDPATH**/ ?>