<div id="dedication_modal" class="modal" role="dialog" data-backdrop="static">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i>
				</button>
				<h4>Send a dedication</h4>
			</div>
			<?php echo e(Form::open(['url' => 'send_dedication', 'method' => 'post' , 'class' =>'card' , 'id'=>'dedication_form'])); ?>

			<?php echo csrf_field(); ?>
			<div class="modal-body">
				<div class="form-group">
					<?php echo e(Form::text('username','',['placeholder'=>'Name' , 'class'=>'form-control'])); ?>

					<div class="dedication_username"></div>
				</div>
				<div class="form-group">
					<?php echo e(Form::textarea('dedication_message','', ['placeholder'=>'Message','class' => 'form-control','size' =>'50x3'])); ?>

					<div class="dedication_dedication_message"></div>
				</div>
			</div>
			<div class="modal-footer">
				<?php echo e(Form::submit('Submit' ,['class'=>'btn btn-lg btn-block btn-success'])); ?>

			</div>
			<?php echo e(Form::close()); ?>

		</div>
	</div>
</div>


<div id="modal_mdp_oublie" class="modal fade" role="dialog" data-backdrop="static">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i>
				</button>
				<h4>Forgot your password? No problem.</h4>
			</div>
			<div class="modal-body">
				<div id="mdp_oublie_errors" style="display : none;"></div>
				<form method="post" id="mdpoublieformnouveau" action="/utilisateurs/index/oublie">
					<div class="form-group">
						<label for="mail_client" class="headline headlinemodal">Type your e-mail address here :</label>
						<input type="text" class="form-control" id="mail_client" name="mail_client" placeholder="Mail" required />
						<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button class="mdpoublie_client modal-mdp-oublie btn btn-success">Send</button>
			</div>
		</div>
	</div>
</div>

<div id="cb_policy_modal" class="modal" role="dialog" data-backdrop="static">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i>
				</button>
				<h4>Privacy & Terms</h4>
			</div>
			<div class="modal-body">
				<?php echo e($privacy_configs->privacy_policy); ?>

			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<div class="ms_lang_popup">
	<div id="lang_modal" class="modal" role="dialog" data-backdrop="static">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i>
					</button>
					<h4 align="center" style="color: white">Language Selection</h4>
				</div>
				<div class="modal-body">
					<p><?php echo e(__('message.language_description')); ?></p>
					<form name="lang_form" id = "lang_form" class="lang_form" action="<?php echo e(url('/lang')); ?>" method="POST">

						<?php echo csrf_field(); ?>
						<div class="error check"></div>
						<ul class="lang_list">
							<li>
								<label class="lang_check_label">
									<?php echo e(__('message.english')); ?>

									<input type="checkbox" name="check" value="en"> 
									<span class="label-text"></span>
								</label>
							</li>
							<li>
								<label class="lang_check_label">
									<?php echo e(__('message.hindi')); ?>

									<input type="checkbox" name="check"  value="hi"> 
									<span class="label-text"></span>
								</label>
							</li>
							<li>
								<label class="lang_check_label">
									<?php echo e(__('message.punjabi')); ?>

									<input type="checkbox" name="check"  value="pa"> 
									<span class="label-text"></span>
								</label>
							</li>
							<li>
								<label class="lang_check_label">
									<?php echo e(__('message.french')); ?>

									<input type="checkbox" name="check"  value="fr"> 
									<span class="label-text"></span>
								</label>
							</li>
							<li>
								<label class="lang_check_label">
									<?php echo e(__('message.german')); ?> 
									<input type="checkbox" name="check" value="de"> 
									<span class="label-text"></span>
								</label>
							</li>
							<li>
								<label class="lang_check_label">
									<?php echo e(__('message.spanish')); ?>

									<input type="checkbox" name="check"  value="es"> 
									<span class="label-text"></span>
								</label>
							</li>
							<li>
								<label class="lang_check_label">
									<?php echo e(__('message.chinese')); ?>

									<input type="checkbox" name="check" value="zh"> 
									<span class="label-text"></span>
								</label>
							</li>
							<li>
								<label class="lang_check_label">
									<?php echo e(__('message.japanese')); ?>

									<input type="checkbox" name="check"  value="ja"> 
									<span class="label-text"></span>
								</label>
							</li>
							<li>
								<label class="lang_check_label">
									<?php echo e(__('message.arabic')); ?>

									<input type="checkbox" name="check"  value="ar"> 
									<span class="label-text"></span>
								</label>
							</li>
							<li>
								<label class="lang_check_label">
									<?php echo e(__('message.italian')); ?>

									<input type="checkbox" name="check"  value="it"> 
									<span class="label-text"></span>
								</label>
							</li>
						</ul>
						<div align="center" class="ms_btn">
							<a href="javascript:void(0)" class="btn btn-success apply_lang" style="margin-top: 17px"> <?php echo e(__('message.apply')); ?></a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php /**PATH /var/www/html/radioKing/resources/views/modal.blade.php ENDPATH**/ ?>