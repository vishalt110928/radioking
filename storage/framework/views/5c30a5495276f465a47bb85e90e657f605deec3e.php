<?php $__env->startSection('content'); ?>
<?php if(session()->has('success')): ?>
<div class="alert alert-success  alert-dismissible">
  <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> <?php echo e(session('success')); ?>

</div>
<?php elseif(session()->has('error')): ?>
<div class="alert alert-danger  alert-dismissible">
  <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Danger!</strong> <?php echo e(session('error')); ?>

</div>
<?php endif; ?>
<div class="row">
  <div class="col-md-12">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Rssfeeds</li>
      </ol>
    </nav>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="widget has-shadow">
      <div class="widget-header bordered no-actions d-flex align-items-center">
       <div class="col-md-8"><h4>RssfeedsModel</h4></div>
       <div class="col-md-4">
        <?php echo e(Form::open(['url' => 'admin/rssfeeds', 'method' => 'get' , 'class' =>'filter_form form-inline', 'id'=>'filter_form'])); ?>

        <input class="form-control" type="text" name ="filter_feeds" placeholder="Search" aria-label="Search" value="<?php echo e($filter_feeds); ?>">
        <?php echo e(Form::button('<i class="fa fa-filter"></i>' ,['class'=>'btn btn-primary ml-auto ','id'=>"button-filter",'type'=>'submit'])); ?>

        <?php echo e(Form::close()); ?>

      </div>
    </div>
    <div class="m-2 float-right">
      <a href="<?php echo e(url('admin/rssfeeds/create')); ?>" class="btn btn-info">Add</a>
      <a href="#" onclick="delete_rssfeeds()" class="btn btn-info">Delete</a>
    </div>
    <div class="well m-5">
      <?php echo e(Form::open(['url' => 'admin/rssfeeds', 'method' => 'get' , 'class' =>'card' , 'id'=>'create_form'])); ?>

      <div class="row padd_filter">
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="input_title">Rssfeeds Title </label>
            <input type="text" name="filter_title" value="<?php echo e($filter_title); ?>" placeholder="Product Name" id="input_title" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
          </div>
          <div class="form-group">
            <label class="control-label" for="input_end_time">End Time</label>
            <input type="text" name="filter_end_time" value="<?php echo e($filter_end_time); ?>" placeholder="Enter end time" id="input_end_time" class="form-control">
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="input_cat">Category</label>
            <?php echo e(Form::select('filter_cat',$rssfeeds_cats ,$filter_cat, ['placeholder' => 'Select Category ','class'=>'form-control'])); ?>

          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="input_publish_time">Publish Time</label>
            <input type="text" name="filter_publish_time" value="<?php echo e($filter_publish_time); ?>" placeholder="Enter Publis time" id="input_publish_time" class="form-control">
          </div>
          <?php echo e(Form::button('<i class="fa fa-filter"></i> Filter' ,['class'=>'btn btn-primary ml-auto','id'=>"button-filter",'type'=>'submit'])); ?>

          <?php echo e(Form::button('<i class="fa fa-reset"></i> Reset' ,['class'=>'btn btn-primary ml-auto reset_filter','id'=>"button-filter"])); ?>

          <?php echo e(Form::close()); ?>

        </div>
      </div>
    </div>
    <div class="widget-body">
      <div class="table-responsive">
        <form class="rssfeeds_delete" method="post" action = "<?php echo e(url('admin/rssfeeds_delete/')); ?>">
          <?php echo e(csrf_field()); ?>

          <?php echo e(method_field('DELETE')); ?>

          <table class="table table-bordered mb-0">
            <thead>
              <tr>
                <th>
                  <input type="checkbox" name="groupCheck" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" >
                </th>
                <th><a href ="#" class="sort_order">Photo</a></th>
                <th>
                  <?php if($orderBy == 'title' && $sort == "desc"): ?>
                  <a href ="<?php echo e(url('admin/rssfeeds/?orderBy=title&sort=asc&'.$query)); ?>">Title <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                  <?php elseif($orderBy == 'title' && $sort == "asc"): ?>
                  <a href ="<?php echo e(url('admin/rssfeeds/?orderBy=title&sort=desc&'.$query)); ?>">Title <i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                  <?php else: ?>
                  <a href ="<?php echo e(url('admin/rssfeeds/?orderBy=title&sort=desc&'.$query)); ?>">Title <i class="fa fa-sort" aria-hidden="true"></i></a>
                  <?php endif; ?>
                </th>
                <th>
                  <?php if($orderBy == 'category' && $sort == "desc"): ?>
                  <a href ="<?php echo e(url('admin/rssfeeds/?orderBy=category&sort=asc&'.$query)); ?>">Category <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                  <?php elseif($orderBy == 'category' && $sort == "asc"): ?> 
                  <a href ="<?php echo e(url('admin/rssfeeds/?orderBy=category&sort=desc&'.$query)); ?>">Category <i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                  <?php else: ?>
                  <a href ="<?php echo e(url('admin/rssfeeds/?orderBy=category&sort=desc&'.$query)); ?>">Category <i class="fa fa-sort" aria-hidden="true"></i></a>
                  <?php endif; ?>
                </th>
                <th>
                  <?php if($orderBy == 'publish_time' && $sort == "desc"): ?>
                  <a href ="<?php echo e(url('admin/rssfeeds/?orderBy=publish_time&sort=asc&'.$query)); ?>">Publication Date <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                  <?php elseif($orderBy == 'publish_time' && $sort == "asc"): ?> 
                  <a href ="<?php echo e(url('admin/rssfeeds/?orderBy=publish_time&sort=desc&'.$query)); ?>">Publication Date <i class="fa fa-sort-asc" aria-hidden="true"></i></a>
                  <?php else: ?>
                  <a href ="<?php echo e(url('admin/rssfeeds/?orderBy=publish_time&sort=desc&'.$query)); ?>">Publication Date <i class="fa fa-sort" aria-hidden="true"></i></a>
                  <?php endif; ?>
                </th>
                <th>Publish</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php $__currentLoopData = $rssfeeds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rssfeed): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <tr>
                <td><input type="checkbox" name="selected[]" value="<?php echo e($rssfeed->id); ?>}" ></td>
                <td><img width="80" class="img-polaroid" style="width : 80px;" src="<?php echo e(url('public/uploads/'.$rssfeed->image)); ?>"></td>
                <td><?php echo e($rssfeed->title); ?></td>
                <td> <?php if(isset($rssfeed->cat->name)): ?>              
                  <?php echo e($rssfeed->cat->name); ?>

                  <?php endif; ?>
                </td>
                <td>
                  <?php if($rssfeed->publish_time): ?>
                  <?php echo e(\Carbon\Carbon::parse($rssfeed->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')); ?>

                  <?php endif; ?>
                </td>
                <td>
                  <?php if($rssfeed->publish): ?>
                  <span style="width:100px;"><span class="badge-text badge-text-small info">Publish</span></span>
                  <?php else: ?>
                  <span style="width:100px;"><span class="badge-text badge-text-small danger">Not Publish</span></span>
                  <?php endif; ?>
                </td>
                <td class="td-actions">
                  <a href="<?php echo e(url('admin/rssfeeds/'.$rssfeed->id.'/edit/')); ?>"><i class="la la-edit edit"></i></a>
                </td>
              </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
          </table>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script src="<?php echo e(url('public/js/bootbox.min.js')); ?>"></script>
<script type="text/javascript">
  function delete_rssfeeds(){
    $('form.rssfeeds_delete').submit();
  }
  $(document).ready(function(){
    $(document).on('click','.delete_rssfeeds',function(){
      var $this = $(this);
      console.log(this)
      bootbox.confirm("Are you sure?", function(result){
        if(result){
          $this.parent().find('form').submit();
        }
        else{
        }
      });
    });

    $(document).on('click','.reset_filter',function() {
     $('#create_form').find('input,select').val('');
     $('#create_form').submit();
   })
    var dateRangePicker = ['input[name= "filter_publish_time"]','input[name= "filter_end_time"]'];
    dateRangePicker.forEach(PickerFunction);
  })
  function PickerFunction(picker) {
    $(picker).daterangepicker({
      timePicker: true,
      singleDatePicker: true,
      autoUpdateInput: false,
      locale: {
        format: 'DD-MM-YYYY H:mm:ss'
      }
    }, function(chosen_date) {
      $(picker).val(chosen_date.format('DD-MM-YYYY H:mm:ss'));
    });
  }
</script>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/radioKing/resources/views/admin/rssfeeds/list.blade.php ENDPATH**/ ?>