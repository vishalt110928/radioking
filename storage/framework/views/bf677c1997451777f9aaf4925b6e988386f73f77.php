<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $__env->yieldContent('title'); ?></title>
  <meta name="description" content="Elisyam is a Web App and Admin Dashboard Template built with Bootstrap 4">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Google Fonts -->
   <meta name="csrf_token" content="<?php echo e(csrf_token()); ?>">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
  <script>
    WebFont.load({
      google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
      active: function() {
        sessionStorage.fonts = true;
      }
    });
  </script>
  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo e(url('public/img/apple-touch-icon.png')); ?>">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(url('public/img/favicon-32x32.png')); ?>">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(url('public/img/favicon-16x16.png')); ?>">
  <!-- Stylesheet -->
  <link rel="stylesheet" href="<?php echo e(url('public/assets/vendors/css/base/bootstrap.min.css')); ?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo e(url('public/assets/vendors/css/base/elisyam-1.5.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(url('public/css/style.css')); ?>">
  <?php echo $__env->yieldContent('css'); ?>
</head>
<body id="page-top">
  <!-- Begin Preloader -->
  <div id="preloader">
    <div class="canvas">
      <img src="<?php echo e(url('public/img/logo.png')); ?>" alt="logo" class="loader-logo">
      <div class="spinner"></div>   
    </div>
  </div>
  <!-- End Preloader -->
  <div class="page">
    <!-- Begin Header -->
   <?php echo $__env->make('layouts.header.admin_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- End Header -->
    <!-- Begin Page Content -->
    <div class="page-content d-flex align-items-stretch">
      <?php echo $__env->make('layouts.sidebar.admin_sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      <!-- End Left Sidebar -->
      <div class="content-inner">
        <div class="container-fluid">
         <?php echo $__env->yieldContent('content'); ?>
       </div>
       <!-- End Container -->
       <!-- Begin Page Footer-->
       <?php echo $__env->make('layouts.footer.admin_footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      
      <!-- End Page Content -->
    </div>
    <!-- Begin Vendor Js -->
    <script src="<?php echo e(url('public/js/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(url('public/js/core.min.js')); ?>"></script>
    <!-- End Vendor Js -->
    <!-- Begin Page Vendor Js -->
    <script src="<?php echo e(url('public/js/nicescroll.min.js')); ?>"></script>
    <script src="<?php echo e(url('public/js/moment.min.js')); ?>"></script>
    <script src="<?php echo e(url('public/js/daterangepicker.js')); ?>"></script>
    <script src="<?php echo e(url('public/js/app.js')); ?>"></script>
    <!-- End Page Vendor Js -->
    <!-- Begin Page Snippets -->
    <script src="<?php echo e(url('public/js/datepicker.js')); ?>"></script>
    <script type="text/javascript">
       $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
        }
      });
    </script>
    <!-- End Page Snippets -->
    <?php echo $__env->yieldContent('script'); ?>
  </body>
  </html><?php /**PATH /var/www/html/radioKing/resources/views/layouts/admin.blade.php ENDPATH**/ ?>