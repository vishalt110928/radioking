<?php $__env->startSection('title','Create Rssfeeds'); ?>
<?php $__env->startSection('content'); ?>	

<div class="my-3 my-md-5 app-content">
	<div class="side-app">
		<div class="page-header">
			<h4 class="page-title">Create Rssfeeds</h4>
		</div>

		<?php if($errors->any()): ?>
		<div class="alert alert-danger">
			<ul>
				<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<li><?php echo e($error); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</ul>
		</div>
		<?php endif; ?>	
		<?php if(session()->has('success')): ?>
		<div class="alert alert-success  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> <?php echo e(session('success')); ?>

		</div>
		<?php elseif(session()->has('error')): ?>
		<div class="alert alert-danger  alert-dismissible">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Danger!</strong> <?php echo e(session('error')); ?>

		</div>
		<?php endif; ?>
		<div class="row">
			<div class="col-lg-12">
				<?php echo e(Form::open(['url' => 'admin/rssfeeds', 'method' => 'post' , 'class' =>'card' , 'files' => true , 'id'=>'create_form'])); ?>

				<?php echo csrf_field(); ?>
				<div class="card-header">
					<h3 class="card-title"></h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label class="form-label">Enter Title</label>
								<?php echo e(Form::text('title','',['placeholder'=>'Enter Title' , 'class'=>'form-control'])); ?>

							</div>
							<div class="form-group">
								<label class="form-label">Publish</label>
								<?php echo e(Form::checkbox('publish', '1',false,['class' => 'form-control'])); ?>

							</div>
							<div class="form-group">
								<label class="form-label">Add Url</label>
								<?php echo e(Form::text('rss_url','',['placeholder'=>'Add Url' ,'class'=>'form-control'])); ?>

							</div>
							<div class="form-group">
								<label class="form-label">Category</label>
								<?php echo e(Form::select('rssfeeds_cat_id',$rssfeeds_cats , null, ['placeholder' => 'Select Category ','class'=>'form-control'])); ?>

							</div>
							<div class="form-group">
								<label class="form-label">Choose An Rss Feed </label>
								<?php echo e(Form::select('rssfeeds_select',array(),"", ['placeholder' => 'Choose an rss feed.','class'=>'form-control'])); ?>

							</div>
							<div class="form-group">
								<label class="form-label">Enter Publish Time</label>
								<?php echo e(Form::text('publish_time','',['placeholder'=>'Enter Publish Time' , 'class'=>'form-control'])); ?>

							</div>
							<div class="form-group">
								<label class="form-label">Enter End Time</label>
								<?php echo e(Form::text('end_time','',['placeholder'=>'Enter End Time' , 'class'=>'form-control'])); ?>

							</div>
							<div class="form-group">
								<label class="form-label">Description</label>
								<?php echo e(Form::textarea('des','', ['class' => 'form-control', 'size' =>'50x3'])); ?>

							</div>
							<div class="form-group">
								<label class="form-label">Upload Image </label>
								<?php echo e(Form::file('image',['class'=>'upload','id'=>'upload'])); ?>

							</div>
							<img id="img" src="#" alt="your image" style="display: none" />
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<div class="d-flex">
						<a href="javascript:void(0)" class="btn btn-link">Cancel</a>
						<?php echo e(Form::submit('Submit' ,['class'=>'btn btn-primary ml-auto'])); ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript" src="<?php echo e(url('public/js/jquery.validate.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('public/js/additional-methods.min.js')); ?>"></script>
<script>
	$(function(){
		$('#upload').change(function(){
			$('#img').css('display','block');
			var input = this;
			var url = $(this).val();
			console.log(url);
			var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
			if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
			{
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#img').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
			else
			{
				$('#img').attr('src', '/assets/no_preview.png');
			}
		});
		$("#create_form").validate({
			rules: {
				name: {
					required : true,
					minlength: 2,
					maxlength : 30
				},
				
				image : {
					required: true,
					accept : "image/*"
				},
			},
			messages: {
				name: {
					required : "Please specify your name.",
					minlength : "Minimum length should be 2 character.",
					maxlength : "Maximum length should be 30 character.",
				},

				image: {
					required: "Image field is required",
					accept: "Only image file allowed"
				},
			}
		});
	  var dateRangePicker = ['input[name= "publish_time"]','input[name= "end_time"]'];
	  dateRangePicker.forEach(PickerFunction);
	})
	function PickerFunction(picker) {
	  $(picker).daterangepicker({
	    timePicker: true,
	    singleDatePicker: true,
	    autoUpdateInput: false,
	    locale: {
	      format: 'DD-MM-YYYY H:mm:ss'
	    }
	  }, function(chosen_date) {
	    $(picker).val(chosen_date.format('DD-MM-YYYY H:mm:ss'));
	  });
	}
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/radioKing/resources/views/admin/rssfeeds/create.blade.php ENDPATH**/ ?>