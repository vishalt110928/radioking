<?php if(! isset($notpushstate) || ($notpushstate != '1')): ?>

<?php $__env->startSection('slider'); ?>
<?php endif; ?>
<div class="container">
	<div id="banner-slick">
		<?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<div>
			<a href="<?php echo e($slider->slide_url); ?>" target="_blank">
				<img src="<?php echo e(url('public/uploads/'.$slider->image)); ?>" alt="" class="slider_banner" />
			</a>
		</div>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</div>
</div>
<?php if(! isset($notpushstate) || ($notpushstate != '1')): ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php endif; ?>
<div class="container body_wrap boxed m-t-15">
	<div class="row">
		<div class="col-md-12">
			<?php if($ads->display_top): ?>
			<?php if($ads->image): ?>
			<img src="<?php echo e(url('public/uploads/'.$ads->image)); ?>">
			<?php else: ?>
			<?php echo $ads->js_code; ?>

			<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
	<!-- A n'utiliser qu'une fois sinon conflit avec les autres sliders -->
	<div class="row">
		<div class="col-xs-12 col-md-8">
			<div class="bloc-content">
				<div class="wrap">
					<div class="row">
						<div class="col-md-12">
							<div class="bloc-content-title">
								<div class="bloc-title-readmore">
									<a href="<?php echo e(url('news')); ?>" class="read-more-title detail">
										<i class="fa fa-arrow-circle-o-right"></i> See more
									</a>
								</div>
								<h2 class="ellipsis ellipsis-1l">News</h2>
							</div>
						</div>
					</div>
					<?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $new): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php if($loop->first): ?>
					<div class="row one_feature m-b-25 itemBloc">
						<div class="col-md-6">
							<div class="cover-preview">
								<a href="<?php echo e(url('news_detail/'.$new->slug)); ?>" class="detail">
									<img src="<?php echo e(url('public/uploads/'.$new->image)); ?>" class="cover-img news_sidebar_first" />
								</a>
							</div>
						</div>
						<div class="col-md-6">
							<a href="<?php echo e(url('news_detail/'.$new->slug)); ?>" class="detail">
								<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-1">
									<?php echo e($new->title); ?>

								</h3>
							</a>
							<span class="trans date">
								<i class="fa fa-clock-o"></i>
								<?php if($new->publish_time): ?>
								<?php echo e(\Carbon\Carbon::parse($new->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')); ?>

								<?php endif; ?>
							</span>
							<p class="ellipsis ellipsis-4l m-t-15">
								<?php echo e($new->description); ?>

							</p>
						</div>
					</div>
					<?php else: ?>
					<div class="col-xs-6 col-sm-4 itemBloc p-l-10-xs p-r-10-xs m-b-10-xs">
						<a href="<?php echo e(url('news_detail/'.$new->slug)); ?>" class="detail">
							<div class="cover-preview ">
								<img src="<?php echo e(url('public/uploads/'.$new->image)); ?>" class="cover-img news_sidebar_other" />
							</div>
							<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-2 ">
								<?php echo e($new->title); ?>

							</h3>
						</a>
					</div>
					<div class="noclear-xs"></div>
					<?php endif; ?>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-4">
			<div class="bloc-content bc-one-column">
				<div class="bloc-content-title">
					<div class="bloc-title-readmore">
						<a href="<?php echo e(url('top_10')); ?>" class="read-more-title detail"><i class="fa fa-arrow-circle-o-right"></i> More</a>
					</div>
					<h2 class="ellipsis ellipsis-1l">Top Music</h2>
				</div>
				<div class="wrap listed">
					<?php $__currentLoopData = $tracks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $track): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="row track-row">
						<div class="col-xs-3 col-md-4">
							<a>
								<div class="cover-preview">
									<img src="<?php echo e(url('public/uploads/'.$track->image)); ?>" class="cover-img square_img_100" />
									<span class="cover-infos">
										<strong><span class="top-position"><?php echo e(++$loop->index); ?></span></strong>
									</span>
								</div>
							</a>
						</div>
						<div class="col-xs-9 col-md-8">
							<div class="box-play player-play like-inline" onclick='playFile("<?php echo e(url('public/uploads/'.$track->audio_sample)); ?>","<?php echo e($track->title); ?>", "<?php echo e(url('public/uploads/'.$track->image)); ?>","<?php echo e($track->id); ?>","track" ,"1","1")'>
								<a href="javascript:;">
									<i class="fa fa-fw fa-play-circle"></i>
								</a>
							</div>
							<h4 class="bloc-track-title ellipsis ellipsis-1l">
								<?php echo e($track->title); ?>

							</h4>
							<span class="bloc-track-artist ellipsis ellipsis-1l"><?php echo e($track->artist); ?></span>
							<a target="_blank" class="btn btn-default-bloc" href="<?php echo e($track->buy_link); ?>">
								<i class="fa fa-shopping-cart"></i> Buy this track </a>
							</div>
						</div>
						<div class="row border"></div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
			</div>
		</div>
		<!-- Podcaste section -->
		<div class="cl"></div>
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="bloc-content bc-one-column">
					<div class="bloc-content-title">
						<div class="bloc-title-readmore">
							<a href="<?php echo e(url('podcasts')); ?>" class="read-more-title detail">
								<i class="fa fa-arrow-circle-o-right"></i> More
							</a>
						</div>
						<h2 class="ellipsis ellipsis-1l">Podcasts</h2>
					</div>
					<div class="wrap listed">
						<?php $__currentLoopData = $podcasts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $podcast): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="row">
							<a href="<?php echo e(url('podcast_detail/'.$podcast->slug)); ?>" class="detail">
								<div class="col-md-4">
									<div class="cover-preview">
										<img src="<?php echo e(url('public/uploads/'.$podcast->image)); ?>" class="cover-img square_img_100" />
									</div>
								</div>
							</a>
							<div class="col-xs-12 col-md-8">
								<div class="box-play player-play like-inline" onclick='playFile("<?php if($podcast->audio_file): ?><?php echo e(url('public/uploads/'.$podcast->audio_file)); ?> <?php else: ?> <?php echo e(url($podcast->podcast_url)); ?> <?php endif; ?> ","<?php echo e($podcast->title); ?>", "<?php echo e(url('public/uploads/'.$podcast->image)); ?>","<?php echo e($podcast->id); ?>","podcast" ,"0","1")'>
									<a href="javascript:;">
										<i class="fa fa-fw fa-play-circle"></i>
									</a>
								</div>
								<a href="<?php echo e(url('podcast_detail/'.$podcast->slug)); ?>" class="detail">
									<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3 m-t-5">
										<?php echo e($podcast->title); ?>

									</h3>
								</a>
							</div>
						</div>
						<div class="row border"></div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-4">
				<div class="bloc-content bc-one-column">
					<div class="bloc-content-title">
						<div class="bloc-title-readmore">
							<a href="<?php echo e(url('playlists')); ?>" class="read-more-title detail"><i class="fa fa-arrow-circle-o-right"></i> More
							</a>
						</div>
						<h2 class="ellipsis ellipsis-1l">Playlist</h2>
					</div>
					<div class="wrap listed">
						<?php $__currentLoopData = $playlists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $playlist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="row track-row">
							<div class="col-xs-3 col-md-4">
								<a>
									<div class="cover-preview">
										<img src="<?php echo e(url('public/uploads/'.$playlist->image)); ?>" class="cover-img square_img_100" />
										<span class="">
											<strong></strong>
										</span>
										<div class="cover-play player-play" onclick='playFile("<?php echo e(url('public/uploads/'.$playlist->audio_sample)); ?>","<?php echo e($playlist->title); ?>", "<?php echo e(url('public/uploads/'.$playlist->image)); ?>","<?php echo e($playlist->id); ?>","<?php echo e('playlist'); ?>","1","1")'>
											<i class="fa fa-fw fa-play-circle"></i>
										</div>
									</div>
								</a>
							</div>
							<div class="col-xs-9 col-md-8">
								<div class="box-play like-music like-inline">
									<span class="hidden datastring"></span>
									<a class ="player-vote" data-type ="playlist", data-id = "<?php echo e($playlist->id); ?>"  href="<?php echo e(url('track_playlist_like')); ?>">
										<i class="fa fa-fw fa-heart-o"></i>
									</a>
								</div>
								<h4 class="bloc-track-title ellipsis ellipsis-1l">
									<?php echo e($playlist->title); ?>

								</h4>
								<span class="bloc-track-artist ellipsis ellipsis-1l"><?php if(isset($playlist->cat->name)): ?> <?php echo e($playlist->cat->name); ?> <?php endif; ?></span>
								<a target="itunes_store" class="btn btn-default-bloc" href="<?php echo e($playlist->buy_link); ?>">
									<i class="fa fa-shopping-cart"></i> Buy this track
								</a>
							</div>
						</div>
						<div class="row border"></div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-4">
				<div class="bloc-content bc-one-column">
					<div class="bloc-content-title">
						<div class="bloc-title-readmore">
							<a href="<?php echo e(url('programs')); ?>" class="read-more-title detail"><i class="fa fa-arrow-circle-o-right"></i> More
							</a>
						</div>
						<h2 class="ellipsis ellipsis-1l">Shows</h2>
					</div>
					<div class="wrap ">
						<div class="row">
							<?php $__currentLoopData = $programs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $program): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<a href="<?php echo e(url('program_detail/'.$program->slug)); ?>" class="detail">
								<div class="col-xs-6">
									<div class="cover-preview">
										<img src="<?php echo e(url('public/uploads/'.$program->image)); ?>" class="cover-img rect_165_110" />
									</div>
									<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3 m-t-5">
										<?php echo e($program->name); ?>

									</h3>
								</div>
							</a>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Podcast section -->
		<!--  Video section -->
		<div class="cl"></div>
		<div class='row'>
			<div class="col-xs-12 col-md-12">
				<div class="bloc-content">
					<div class="wrap">
						<div class="row">
							<div class="col-md-12">
								<div class="bloc-content-title">
									<div class="bloc-title-readmore">
										<a href="<?php echo e(url('videos')); ?>" class="read-more-title detail"><i class="fa fa-arrow-circle-o-right"></i> See more
										</a>
									</div>
									<h2 class="ellipsis ellipsis-1l">Latest videos</h2>
								</div>
							</div>
						</div>
						<div class="row row-m-10-xs">
							<?php $__currentLoopData = $videos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($loop->index < 3): ?>
							<div class="col-md-4 col-xs-6 m-b-20 p-r-10-xs p-l-10-xs">
								<a href="<?php echo e(url('video_detail/'.$video->slug)); ?>" class="detail">
									<div class="cover-preview">
										<img src="<?php echo e(url('public/uploads/'.$video->image)); ?>" class="cover-img rect_360_203" />
										<div class="hover_type ">
											<span class="hovervideo"></span>
										</div>
									</div>
									<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-2">
										<?php echo e($video->title); ?>

									</h3>
								</a>
							</div>
							<?php else: ?>
							<div class="col-md-2 col-xs-6 m-b-20 p-r-10-xs p-l-10-xs">
								<a href="<?php echo e(url('video_detail/'.$video->slug)); ?>" class="detail">
									<div class="cover-preview">
										<img src="<?php echo e(url('public/uploads/'.$video->image)); ?>" class="rect_165_110">
										<span class="hovervideo"></span>
									</div>
									<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3">
										<?php echo e($video->title); ?>

									</h3>
								</a>
							</div>
							<?php if($loop->index % 2): ?>
							<div class=" noclear-sm"></div>
							<?php elseif($loop->index == 3): ?>
							<div class="clear noclear-sm"></div>
							<?php else: ?>
							<div class=" clear-sm"></div>
							<?php endif; ?>
							<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Video Section -->
		<!--Contest Section  -->
		<div class="cl">
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-8">
				<div class="bloc-content">
					<div class="wrap">
						<div class="row">
							<div class="col-md-12">
								<div class="bloc-content-title">
									<div class="bloc-title-readmore">
										<a href="<?php echo e(url('contests')); ?>" class="read-more-title detail"><i class="fa fa-arrow-circle-o-right"></i> See more
										</a>
									</div>
									<h2 class="ellipsis ellipsis-1l">Enter the contest</h2>
								</div>
							</div>
						</div>
						<?php $__currentLoopData = $contests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $contest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php if($loop->index == 0): ?>
						<div class="row one_feature m-b-25 itemBloc">
							<a href="<?php echo e(url('contest_detail/'.$contest->slug)); ?>" class="detail">
								<div class="col-md-6">
									<div class="cover-preview">
										<img src="<?php echo e(url('public/uploads/'.$contest->image)); ?>" class="cover-img  news_sidebar_first" />
									</div>
								</div>
								<div class="col-md-6">
									<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-1">
										<?php echo e($contest->title); ?>

									</h3>
									<span class="trans date">
										<i class="fa fa-clock-o"></i>
										<?php if($contest->publish_time): ?>
										<?php echo e(\Carbon\Carbon::parse($contest->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')); ?>

									<?php endif; ?> </span>
									<p class="ellipsis ellipsis-4l m-t-15">
										<?php echo e($contest->des); ?>

									</p>
								</div>
							</a>
						</div>
						<?php else: ?>
						<div class="col-xs-6 col-sm-4 itemBloc p-l-10-xs p-r-10-xs m-b-10-xs">
							<a href="<?php echo e(url('contest_detail/'.$contest->slug)); ?>" class="detail">
								<div class="cover-preview ">
									<img src="<?php echo e(url('public/uploads/'.$contest->image)); ?>" class="cover-img news_sidebar_other" />
								</div>
								<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-2 ">
									<?php echo e($contest->title); ?>

								</h3>
							</a>
						</div>
						<div class="noclear-xs"></div>
						<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-4">
				<div class="bloc-content bc-one-column">
					<div class="bloc-content-title">
						<div class="bloc-title-readmore">
							<a href="<?php echo e(url('events')); ?>" class="read-more-title"><i class="fa fa-arrow-circle-o-right"></i> More
							</a>
						</div>
						<h2 class="ellipsis ellipsis-1l">Events</h2>
					</div>
					<div class="wrap listed">
						<?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="row">
							<a href="<?php echo e(url('event_detail/'.$event->id)); ?>" class="detail">
								<div class="col-md-4">
									<div class="cover-preview">
										<img src="<?php echo e(url('public/uploads/'.$event->image)); ?>" class="cover-img rect_100_67" />
									</div>
								</div>
								<div class="col-xs-12 col-md-8                        ">
									<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3 m-t-5">
										<?php echo e($event->title); ?>

									</h3>
									<span class="trans date">
										<i class="fa fa-calendar"></i>
										<?php if($event->publish_time): ?>
										<?php echo e(\Carbon\Carbon::parse($event->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')); ?>

									<?php endif; ?> </span>
								</div>
							</a>
						</div>

						<div class="row border"></div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>
			</div>
		</div>
		<!-- End Contest Section -->
		<!-- Artist Section -->
		<div class="cl"></div>
		<div class='row'>
			<div class="col-xs-12 col-md-12">
				<div id="bloc-slide" class="bloc-content carousel carousel-bloc slide slide-full">
					<div class="wrap">
						<div class="row">
							<div class="col-md-12">
								<div class="bloc-content-title slider-bloc-content-title">
									<h2 class="h2-slider ellipsis ellipsis-1l">Artists</h2>
								</div>
							</div>
						</div>
						<div class="row artist_slick" role="listbox">
							<?php $__currentLoopData = $artists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $artist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div>
								<a href="<?php echo e(url('artist_detail/'.$artist->slug)); ?>" class="detail">
									<div class="cover-preview ">
										<img src="<?php echo e(url('public/uploads/'.$artist->image)); ?>" class="cover-img rect_165_193" />
									</div>
								</a>
								<a href="<?php echo e(url('artist_detail/'.$artist->slug)); ?>" class="detail">
									<h3 class="ellipsis ellipsis-2l bloc-ellipsis h3-3">
										<?php echo e($artist->name); ?>

									</h3>
								</a>
							</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--End Artist Section -->
		<!-- Modal Section -->
		<div class="cl">
		</div>
	</div>
	<!-- End Modal Section -->
	<?php if(! isset($notpushstate) || ($notpushstate != '1')): ?>
	<?php $__env->stopSection(); ?>
	<?php endif; ?>
<?php echo $__env->make('layouts.web', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/radioKing/resources/views/home.blade.php ENDPATH**/ ?>