<header class="header">
  <nav class="navbar fixed-top">
    <!-- Begin Search Box-->
    <div class="search-box">
      <button class="dismiss"><i class="ion-close-round"></i></button>
      <form id="searchForm" action="#" role="search">
        <input type="search" placeholder="Search something ..." class="form-control">
      </form>
    </div>
    <!-- End Search Box-->
    <!-- Begin Topbar -->
    <div class="navbar-holder d-flex align-items-center align-middle justify-content-between">
      <!-- Begin Logo -->
      <div class="navbar-header">
        <a href="db-default.html" class="navbar-brand">
          <div class="brand-image brand-big">
            <img src="<?php echo e(url('public/img/front_logo.png')); ?>" alt="logo" class="logo-big" style = "height:90px;width:100px">
          </div>
          <div class="brand-image brand-small">
            <img src="<?php echo e(url('public/img/header_logo.png')); ?>" alt="logo" class="logo-small">
          </div>
        </a>
        <!-- Toggle Button -->
        <a id="toggle-btn" href="#" class="menu-btn active">
          <span></span>
          <span></span>
          <span></span>
        </a>
        <!-- End Toggle -->
      </div>
      <!-- End Logo -->
      <!-- Begin Navbar Menu -->
      <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center pull-right">
        <li class="nav-item dropdown">
          <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
            <i class="la la-bell animated infinite swing"></i>
            <?php if(!$viewed): ?> 
            <span class="badge-pulse"></span>
            <?php endif; ?>
          </a>
          <ul aria-labelledby="notifications" class="dropdown-menu notification" style="display: none;">
            <li>
              <div class="notifications-header">
                <div class="title">Notifications (<?php echo e($not_viewed); ?>)</div>
                <div class="notifications-overlay"></div>
                <img src="<?php echo e(url('public/img/header_logo.png')); ?>" alt="..." class="img-fluid" style="width: 100px;height: 100px;">
              </div>
            </li>
            <?php $__currentLoopData = $notifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notification): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li>
              <a href="<?php echo e(url($notification->href)); ?>">
                <div class="message-body">
                  <div class="message-body-heading">
                    <?php echo e($notification->title); ?>

                  </div>
                  <span class="date">
                    <?php if(isset($notification->ago )): ?>
                    <?php echo e($notification->ago); ?> hours ago
                    <?php endif; ?>
                  </span>
                </div>
              </a>
            </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <li>
              <a rel="nofollow" href="<?php echo e(url('admin/viewed_all')); ?>" class="dropdown-item all-notifications text-center">View All Notifications</a>
            </li>
          </ul>
        </li>
        <?php if(auth()->check()): ?>
        <!-- User -->
        <li class="nav-item dropdown">
          <a id="user" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
            <?php if(auth()->user()->image): ?>
            <img src="<?php echo e(url('public/uploads/'.auth()->user()->image)); ?>" alt="..." class="avatar rounded-circle">
            <?php else: ?>
            <i class="fa fa-user" aria-hidden="true"></i>
            <?php endif; ?>
          </a>
          <ul aria-labelledby="user" class="user-size dropdown-menu">
            <li>
              <form class="logout_form" method="post" action="<?php echo e(url('admin/logout')); ?>">
                <?php echo csrf_field(); ?>
              </form>
              <a href="#" onclick="$(this).parent().find('form.logout_form').submit()" class="dropdown-item text-center">
                <?php echo e('Logout'); ?>

              </a>
            </li>
          </ul>
        </li>
        <?php endif; ?>
        <!-- End User -->
      </ul>
      <!-- End Navbar Menu -->
    </div>
    <!-- End Topbar -->
  </nav>
</header><?php /**PATH /var/www/html/radioKing/resources/views/layouts/header/admin_header.blade.php ENDPATH**/ ?>