<?php if(! isset($notpushstate) || ($notpushstate != '1')): ?>

<?php $__env->startSection('content'); ?>
<?php endif; ?>
<div class="container breadcrumb-container">
	<div class="container p-l-0 p-r-0">
		<ol class="breadcrumb ellipsis ellipsis-1l">
			<li><a href="<?php echo e(url('/home')); ?>" class="toptip detail" title="Home"><i class="fa fa-home"></i></a></li>
			<li><a href="<?php echo e(url('contests')); ?>">Contest</a></li>
			<li class="active"><?php echo e($contest->title); ?></li>
		</ol>
	</div>
</div>
<div class="title-super-container container">
	<div class="container title-container">
		<div class="row">
			<div class="col-xs-12 layout-title p-t-20 p-b-30">
				<div>
					<h1 class="main_title">
						<?php echo e($contest->title); ?>

					</h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container body_wrap boxed">
	<div class="layout-headtitle-border"></div>
	<div class="row">
		<div class="col-xs-12  col-md-8  ">
			<div class="m-t-30">
				<div class="article afficher-content">
					<div class="wrap">
						<img src="<?php echo e(url('public/uploads/'.$contest->image)); ?>" style="width: 100%;" class="img-responsive m-b-5 m-t-20 rect_750_450" />
						<span class="infos-article">
							<i class="fa fa-clock"></i><?php echo e(\Carbon\Carbon::parse($contest->publish_time)->isoFormat('MMMM Do YYYY, h:mm:ss a')); ?></span>
							<div class="m-t-15 m-b-15"></div>
							<p><?php echo e($contest->des); ?></p>
							<div class="contest-success well well-large alert alert-success" style="display:none;text-align:center;">
								You have been entered into this contest.
							</div>
							<input class="contest_part btn btn-lg btn-block btn-primary" type="button" value="Take part in this contest" data-contest_id= "<?php echo e($contest->id); ?>">
							<div class="live_poll">
								<div class="contest-vote well well-large alert alert-success" style="display:none;text-align:center;">
									You have voted successfully.
								</div>
								<form class="poll_ana_form" method="POST" action="<?php echo e(url('poll_ana')); ?>">
									<?php echo csrf_field(); ?>
									<input type="hidden" name="contest_id" value="<?php echo e($contest->id); ?>">
									<div class="form-group">
										<h4 class="des">Are You happy with this podcast?</h4>
										<label><input type="radio" name="pos_neg" value="1" checked>Yes</label>
										<label><input type="radio" name="pos_neg" value="0">No</label>
										<button type="submit" class="btn btn-success poll_ana">Vote</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div id="comments" class="row comment-row m-t-20">
						<div class="col-xs-12">
							<div class="form-group">
								<span class="comment-img avatar-default"><i class="fa fa-user"></i></span>
								<div class="fb-comments" data-href="<?php echo e(request()->fullUrl()); ?>" data-width="" data-numposts="5"></div>
							</div>

							<div class="clear"></div>
							<div class="form-group" style="float:right;">
								<div class="col-xs-3">
								</div>
							</div>
							<div class="clear"></div>
							<div id="list-comments">
							</div>
							<div class="row">
								<div class="col-xs-12">
								</div>
							</div>

							<!-- Fin commentaires -->
						</div>
					</div>
					<div id="fb-root"></div>
					<script crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
				</div>
			</div>
		</div>
	</div>
	<?php if(! isset($notpushstate) || ($notpushstate != '1')): ?>
	<?php $__env->stopSection(); ?>
	<?php endif; ?>
	<?php $__env->startSection('script'); ?>
	<script type="text/javascript">
		$(document).ready(function(){
			$(document).on('click', '.contest_part', function(e) {
				e.preventDefault();
				var auth = "<?php echo e(auth::check()); ?>";
				if (auth) {
					elm = $(this);
					var contest_id = elm.data('contest_id');
					$.ajax({
						type: "GET",
						url:"<?php echo e(url('contest_part/')); ?>",
						data: {"contest_id":contest_id },
						beforeSend: function(){

						},
						success: function(response) {
							$('div.contest-success').css('display','block');
						},
						error:function(response) {
							alert(response.message);
							console.log(response.message);
						},
						complete:function() {

						}
					});
				}
				else{
					alert("Please login first to take participate.")
				}
			});
			$(document).on('submit','.poll_ana_form',function(e){
				e.preventDefault();
				var url = $(this).attr('action');
				var form =$(this).serialize();
				$.ajax({
					type: "POST",
					url:url,
					data: form,
					beforeSend: function(){

					},
					success: function(response) {
						$('div.contest-vote').css('display','block');
					},
					error:function(response) {
						alert(response.message);
						console.log(response.message);
					},
					complete:function() {

					}
				});
			});
		});
	</script>

	<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.web', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/radioKing/resources/views/contest_detail.blade.php ENDPATH**/ ?>