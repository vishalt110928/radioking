<?php

use App\Page;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('route', function () {
	$getRouteList = array();
	$routes = Route::getRoutes();
	foreach ($routes as $route) {
		if ($route->methods()[0] == "GET" && (strpos($route->uri, 'admin') === false)) {

			$getRouteList[]['title'] = explode('/', $route->uri)[0];
		}
		/** @var \Illuminate\Routing\Route $route  */
		/* echo $route->uri. PHP_EOL;
      echo "<br>";*/
	}
	Page::insert($getRouteList);
});
Auth::routes();
Route::prefix('admin')->group(function () {
	Route::get('login', 'Auth\LoginController@showLoginForm');
	Route::get('sign_up', 'Auth\RegisterController@showRegistrationForm');
	Route::post('login', 'Auth\LoginController@login');
	Route::post('sign_up', 'Auth\RegisterController@register');
	Route::post('logout', 'Auth\LoginController@logout');
	Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
	Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
	Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');
	Route::get('password/update', 'Auth\ResetPasswordController@reset')->name('admin.password.update');
});


Route::prefix('admin')->middleware(['auth','checkadmin'])->group(function () {
	Route::get('dashboard', 'Admin\DashboardController@index')->name('dashboard');
	Route::resource('news', 'Admin\NewsController');
	Route::get('news_cat', 'Admin\NewsController@news_cat_index');
	Route::get('news_cat/create', 'Admin\NewsController@news_cat_create');
	Route::post('news_cat/store', 'Admin\NewsController@news_cat_store');
	Route::get('news_cat/edit/{id}', 'Admin\NewsController@news_cat_edit');
	Route::put('news_cat/update/{id}', 'Admin\NewsController@news_cat_update');
	Route::delete('news_cat_destroy/{id}', 'Admin\NewsController@news_cat_destroy');
	Route::delete('news_cat_delete', 'Admin\NewsController@news_cat_delete');
	Route::delete('news_delete', 'Admin\NewsController@news_delete');

	Route::resource('events', 'Admin\EventController');
	Route::get('events_cat', 'Admin\EventController@events_cat_index');
	Route::get('events_cat/create', 'Admin\EventController@events_cat_create');
	Route::post('events_cat/store', 'Admin\EventController@events_cat_store');
	Route::get('events_cat/edit/{id}', 'Admin\EventController@events_cat_edit');
	Route::put('events_cat/update/{id}', 'Admin\EventController@events_cat_update');
	Route::delete('events_cat_destroy/{id}', 'Admin\EventController@events_cat_destroy');
	Route::delete('events_cat_delete', 'Admin\EventController@events_cat_delete');
	Route::delete('events_delete', 'Admin\EventController@events_delete');

	Route::resource('dedications', 'Admin\DedicationController');
	Route::delete('dedications_delete', 'Admin\DedicationController@dedication_delete');

	Route::resource('contests', 'Admin\ContestController');
	Route::get('contests_cat', 'Admin\ContestController@contests_cat_index');
	Route::get('contests_cat/create', 'Admin\ContestController@contests_cat_create');
	Route::post('contests_cat/store', 'Admin\ContestController@contests_cat_store');
	Route::get('contests_cat/edit/{id}', 'Admin\ContestController@contests_cat_edit');
	Route::put('contests_cat/update/{id}', 'Admin\ContestController@contests_cat_update');
	Route::delete('contests_cat_destroy/{id}', 'Admin\ContestController@contests_cat_destroy');
	Route::delete('contests_cat_delete', 'Admin\ContestController@contests_cat_delete');
	Route::delete('contests_delete', 'Admin\ContestController@contests_delete');

	Route::resource('sliders', 'Admin\SliderController');
	Route::delete('sliders_delete', 'Admin\SliderController@slider_delete');

	Route::resource('podcasts', 'Admin\PodcastsContorller');
	Route::get('podcasts_cat', 'Admin\PodcastsContorller@podcasts_cat_index');
	Route::get('podcasts_cat/create', 'Admin\PodcastsContorller@podcasts_cat_create');
	Route::post('podcasts_cat/store', 'Admin\PodcastsContorller@podcasts_cat_store');
	Route::get('podcasts_cat/edit/{id}', 'Admin\PodcastsContorller@podcasts_cat_edit');
	Route::put('podcasts_cat/update/{id}', 'Admin\PodcastsContorller@podcasts_cat_update');
	Route::delete('podcasts_cat_destroy/{id}', 'Admin\PodcastsContorller@podcasts_cat_destroy');
	Route::delete('podcasts_cat_delete', 'Admin\PodcastsContorller@podcasts_cat_delete');
	Route::delete('podcasts_delete', 'Admin\PodcastsContorller@podcasts_delete');

	Route::resource('playlists', 'Admin\PlaylistController');
	Route::get('playlists_cat', 'Admin\PlaylistController@playlists_cat_index');
	Route::get('playlists_cat/create', 'Admin\PlaylistController@playlists_cat_create');
	Route::post('playlists_cat/store', 'Admin\PlaylistController@playlists_cat_store');
	Route::get('playlists_cat/edit/{id}', 'Admin\PlaylistController@playlists_cat_edit');
	Route::put('playlists_cat/update/{id}', 'Admin\PlaylistController@playlists_cat_update');
	Route::delete('playlists_cat_destroy/{id}', 'Admin\PlaylistController@playlists_cat_destroy');
	Route::delete('playlists_cat_delete', 'Admin\PlaylistController@playlists_cat_delete');
	Route::delete('playlists_delete', 'Admin\PlaylistController@playlists_delete');

	Route::resource('artists', 'Admin\ArtistController');
	Route::get('artists_cat', 'Admin\ArtistController@artists_cat_index');
	Route::get('artists_cat/create', 'Admin\ArtistController@artists_cat_create');
	Route::post('artists_cat/store', 'Admin\ArtistController@artists_cat_store');
	Route::get('artists_cat/edit/{id}', 'Admin\ArtistController@artists_cat_edit');
	Route::put('artists_cat/update/{id}', 'Admin\ArtistController@artists_cat_update');
	Route::delete('artists_cat_destroy/{id}', 'Admin\ArtistController@artists_cat_destroy');
	Route::delete('artists_cat_delete', 'Admin\ArtistController@artists_cat_delete');
	Route::delete('artists_delete', 'Admin\ArtistController@artists_delete');

	Route::resource('livefeeds', 'Admin\LivefeedController');
	Route::delete('livefeeds_delete', 'Admin\LivefeedController@livefeeds_delete');


	Route::resource('pages', 'Admin\PageController');
	Route::get('pages_cat', 'Admin\PageController@pages_cat_index');
	Route::get('pages_cat/create', 'Admin\PageController@pages_cat_create');
	Route::post('pages_cat/store', 'Admin\PageController@pages_cat_store');
	Route::get('pages_cat/edit/{id}', 'Admin\PageController@pages_cat_edit');
	Route::put('pages_cat/update/{id}', 'Admin\PageController@pages_cat_update');
	Route::delete('pages_cat_destroy/{id}', 'Admin\PageController@pages_cat_destroy');
	Route::delete('pages_cat_delete', 'Admin\PageController@pages_cat_delete');
	Route::delete('pages_delete', 'Admin\PageController@pages_delete');

	Route::resource('teams', 'Admin\TeamController');
	Route::get('teams_cat', 'Admin\TeamController@teams_cat_index');
	Route::get('teams_cat/create', 'Admin\TeamController@teams_cat_create');
	Route::post('teams_cat/store', 'Admin\TeamController@teams_cat_store');
	Route::get('teams_cat/edit/{id}', 'Admin\TeamController@teams_cat_edit');
	Route::put('teams_cat/update/{id}', 'Admin\TeamController@teams_cat_update');
	Route::delete('teams_cat_destroy/{id}', 'Admin\TeamController@teams_cat_destroy');
	Route::delete('teams_cat_delete', 'Admin\TeamController@teams_cat_delete');
	Route::delete('teams_delete', 'Admin\TeamController@teams_delete');

	Route::resource('programs', 'Admin\ProgramController');
	Route::get('programs_cat', 'Admin\ProgramController@programs_cat_index');
	Route::get('programs_cat/create', 'Admin\ProgramController@programs_cat_create');
	Route::post('programs_cat/store', 'Admin\ProgramController@programs_cat_store');
	Route::get('programs_cat/edit/{id}', 'Admin\ProgramController@programs_cat_edit');
	Route::put('programs_cat/update/{id}', 'Admin\ProgramController@programs_cat_update');
	Route::delete('programs_cat_destroy/{id}', 'Admin\ProgramController@programs_cat_destroy');
	Route::delete('programs_cat_delete', 'Admin\ProgramController@programs_cat_delete');
	Route::delete('programs_delete', 'Admin\ProgramController@programs_delete');

	Route::resource('tracks', 'Admin\TrackController');
	Route::delete('tracks_delete', 'Admin\TrackController@tracks_delete');

	Route::resource('gallerys', 'Admin\GalleryController');
	Route::get('gallerys_cat', 'Admin\GalleryController@gallerys_cat_index');
	Route::get('gallerys_cat/create', 'Admin\GalleryController@gallerys_cat_create');
	Route::post('gallerys_cat/store', 'Admin\GalleryController@gallerys_cat_store');
	Route::get('gallerys_cat/edit/{id}', 'Admin\GalleryController@gallerys_cat_edit');
	Route::put('gallerys_cat/update/{id}', 'Admin\GalleryController@gallerys_cat_update');
	Route::delete('gallerys_cat_destroy/{id}', 'Admin\GalleryController@gallerys_cat_destroy');
	Route::delete('gallerys_cat_delete', 'Admin\GalleryController@gallerys_cat_delete');
	Route::delete('gallerys_delete', 'Admin\GalleryController@gallerys_delete');
	Route::get('photos', 'Admin\GalleryController@gallerys_photos');
	Route::post('gallerys/photos_store', 'Admin\GalleryController@gallerys_photos_store');
	Route::delete('photo_delete/{gallery_id}/{photo_id}', 'Admin\GalleryController@photo_delete');

	Route::resource('videos', 'Admin\VideoController');
	Route::get('videos_cat', 'Admin\VideoController@videos_cat_index');
	Route::get('videos_cat/create', 'Admin\VideoController@videos_cat_create');
	Route::post('videos_cat/store', 'Admin\VideoController@videos_cat_store');
	Route::get('videos_cat/edit/{id}', 'Admin\VideoController@videos_cat_edit');
	Route::put('videos_cat/update/{id}', 'Admin\VideoController@videos_cat_update');
	Route::delete('videos_cat_destroy/{id}', 'Admin\VideoController@videos_cat_destroy');
	Route::delete('videos_cat_delete', 'Admin\VideoController@videos_cat_delete');
	Route::delete('videos_delete', 'Admin\VideoController@videos_delete');

	Route::resource('ads', 'Admin\AdsController');
	Route::delete('ads_delete', 'Admin\AdsController@ads_delete');

	Route::resource('rssfeeds', 'Admin\RssfeedController');
	Route::get('rssfeeds_cat', 'Admin\RssfeedController@rssfeeds_cat_index');
	Route::get('rssfeeds_cat/create', 'Admin\RssfeedController@rssfeeds_cat_create');
	Route::post('rssfeeds_cat/store', 'Admin\RssfeedController@rssfeeds_cat_store');
	Route::get('rssfeeds_cat/edit/{id}', 'Admin\RssfeedController@rssfeeds_cat_edit');
	Route::put('rssfeeds_cat/update/{id}', 'Admin\RssfeedController@rssfeeds_cat_update');
	Route::delete('rssfeeds_cat_destroy/{id}', 'Admin\RssfeedController@rssfeeds_cat_destroy');
	Route::delete('rssfeeds_cat_delete', 'Admin\RssfeedController@rssfeeds_cat_delete');
	Route::delete('rssfeed_delete', 'Admin\RssfeedController@rss_delete');

	Route::resource('players', 'Admin\PlayerController');
	Route::delete('players_delete', 'Admin\PlayerController@players_delete');

	Route::resource('users', 'Admin\UserController');
	Route::get('users_role', 'Admin\UserController@users_role_index');
	Route::get('users_role/create', 'Admin\UserController@users_role_create');
	Route::post('users_role/store', 'Admin\UserController@users_role_store');
	Route::get('users_role/edit/{id}', 'Admin\UserController@users_role_edit');
	Route::put('users_role/update/{id}', 'Admin\UserController@users_role_update');
	Route::delete('users_role_destroy/{id}', 'Admin\UserController@users_role_destroy');
	Route::delete('users_role_delete', 'Admin\UserController@users_role_delete');
	Route::delete('users_delete', 'Admin\UserController@users_delete');

	Route::get('gen_configs', 'Admin\SettingsController@gen_configs');
	Route::post('gen_configs_update', 'Admin\SettingsController@gen_configs_update');

	Route::get('ext_apis', 'Admin\SettingsController@ext_apis');
	Route::post('ext_apis_update', 'Admin\SettingsController@ext_apis_update');

	Route::get('soc_nets', 'Admin\SettingsController@soc_nets');
	Route::post('soc_nets_update', 'Admin\SettingsController@soc_nets_update');

	Route::get('chats', 'Admin\SettingsController@chats');
	Route::post('chats_update', 'Admin\SettingsController@chats_update');

	Route::get('email_configs', 'Admin\SettingsController@email_configs');
	Route::post('email_configs_update', 'Admin\SettingsController@email_configs_update');

	Route::get('privacy_configs', 'Admin\SettingsController@privacy_configs');
	Route::post('privacy_configs_update', 'Admin\SettingsController@privacy_configs_update');

	Route::resource('menus', 'Admin\MenuController');
	Route::delete('menus_delete', 'Admin\MenuController@menus_delete');

	Route::resource('blocks', 'Admin\BlockController');
	Route::delete('blocks_delete', 'Admin\BlockController@blocks_delete');

	Route::get('contacts','Admin\ContactsController@index')->name('contacts');
	Route::delete('contacts_delete', 'Admin\ContactsController@contacts_delete');

	Route::get('viewed_all','Admin\DashboardController@viewed_all')->name('viewed_all');

});

Route::get('/', 'RadioKingController@index')->name('home');
Route::get('/home', 'RadioKingController@index')->name('home');

//   Route::get('news', 'RadioKingController@news')->name('news');
//   Route::get('programs', 'RadioKingController@programs')->name('programs');
//   Route::get('teams', 'RadioKingController@teams')->name('teams');
//   Route::get('events', 'RadioKingController@events')->name('events');
//   Route::get('top_10', 'RadioKingController@music')->name('music');
//   Route::get('artists', 'RadioKingController@artists')->name('artists');
//   Route::get('playlists', 'RadioKingController@playlists')->name('playlists');
//   Route::get('podcasts', 'RadioKingController@podcasts')->name('podcasts');
//   Route::get('videos', 'RadioKingController@videos')->name('videos');
//   Route::get('dedications', 'RadioKingController@dedications')->name('dedications');
//   Route::get('contests', 'RadioKingController@contests')->name('contests');
Route::get('contact_us', 'RadioKingController@contact_us')->name('contact_us');
Route::post('contact_us_store', 'RadioKingController@contact_us_store')->name('contact_us_store');
Route::get('news_detail/{slug}', 'RadioKingController@news_detail');
Route::get('team_detail/{slug}', 'RadioKingController@team_detail');
Route::get('program_detail/{slug}', 'RadioKingController@program_detail');
Route::get('podcast_detail/{slug}', 'RadioKingController@podcast_detail');
Route::get('video_detail/{slug}', 'RadioKingController@video_detail');
Route::get('contest_detail/{slug}', 'RadioKingController@contest_detail');
Route::get('event_detail/{slug}', 'RadioKingController@event_detail');
Route::get('artist_detail/{slug}', 'RadioKingController@artist_detail');
Route::get('user_profile/{id}','RadioKingController@user_profile')->name('user_profile');
Route::put('user_profile_store/{id}','RadioKingController@user_profile_store')->name('user_profile_store');
Route::post('played_tracks','RadioKingController@played_tracks')->name('played_tracks');
Route::get('auth_played_tracks','RadioKingController@auth_played_tracks')->name('auth_played_tracks');
Route::get('photos','RadioKingController@photos');
Route::post('send_dedication', 'RadioKingController@send_dedication')->name('page');
Route::get('track_playlist_like','RadioKingController@track_playlist_like')->middleware('web')->name('track-playlist_like');
Route::post('contest_part','RadioKingController@contest_part')->name('contest_part');
Route::post('/lang', 'LanguageController@select_lang')->name('lang');
Route::feeds();
Route::get('genrate_feed','RadioKingController@feed')->name('feed');
Route::post('poll_ana','RadioKingController@poll_ana')->name('poll_ana');
Route::get('{slug}', 'RadioKingController@page')->name('page');
